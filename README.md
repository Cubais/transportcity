# Readme
This Readme was created due to commit **[0d34333]** in the **origin/master** branch, created **8.9.2020 18:59**.
This project is using **Unity 2020.1.2f1**

## Unity Editor
When Unity Editor is open, the wrong scene might be loaded.
To load the right scene, click on the **Scene** folder and double-click on the **GameScene**.
## Camera controls
The following table shows how to control camera movement, to be able to move around the city.

|Key|Effect|
|:---:|---|
|W|Moves camera forward|
|A|Moves camera to the left|
|S|Moves camera to the right|
|D|Moves camera backward|
|Q|Moves camera down (closer to the ground)|
|E|Moves camera up|
|Hold **RMB** + moving mouse| Rotating camera|
|Holding **SHIFT**| Speeds up camera movement|

## Game UI
This section describes what you can see on the screen, when you start the game.

|Number|Description|
|:---:|---|
|1|You can see numbers 0-3. Each of them indicates speed of the simulation e.g. **0 = pause, 1 = normal** etc.|
|2|Here is the information about the time and date. YYYY-MM-DD HH:MM|
|3|In the lower panel you have couple choices what to do. (For now just **Buses** tab works)|
<br>
![Transport City UI](https://github.com/Cubais/TransporCityHelp/blob/master/TransportCityUI.png?raw=true)

## Building and managing bus line
In this section you will learn how to build a bus line and assign buses to them. Game allows to create circular bus lines only.
#### Building bus stop

Follow the following steps to build a stop:
1. Click on the **Buses** tab in the lower panel
2. Click on the **Bus Lines Manager** tab (new UI window shows up)
3. In the upper left corner of the new UI window, click on the "**+**" button  (this will create a new bus line)
4. Next to the newly created bus line click on the **Edit line path button**
5. Move the mouse cursor over the road, where you want to build a bus stop (it's shown in the following picture (1))
![Bus Lines UI](https://github.com/Cubais/TransporCityHelp/blob/master/BusLines1.png?raw=true)
6. Press **LMB** to build a bus stop
7. Create couple more bus stops (route between them is highlited)
8. To finish creating the bus line, you need to click on the first stop
9. Click on the **End editing** button, next to lower panel

#### Buying buses and assigninig them to the lines
1. Click on the **Buses** tab in the lower panel
2. Click on the **Bus Manager** tab (new UI window shows up)
3. In the upper left corner of the new UI window, click on the "**+**" button  (this will create a new bus)
4. Next to the newly created bus, click on the **Assign to line** button
5. Choose line, where you want to assign the bus
6. Bus is now assigned to the bus line!

> **Buses has an ability to break down after they traveled some distance** (repairing is not implemented yet)

## People
People simulation is in progress and could be seen in the following commits in a game mode.

#### Creating people in Editor mode
You can create people in Editor mode though.
Follow the following steps in order to create some people:
> Important buttons are highlited in the picture below
1. In the **Hierarchy panel** find the dropdown button next to the **DontDestroyOnLoad** text and click on it
2. Click on the **PeopleManager** game object
3. On the right side in the **Inspector** tab, you can see **People Manager (Script)**
4. To create people insert an integer in the field **People Count**
5. After inserting the number, click on the empty checkbox next to the **Create People** text

![Creatung People Guide](https://github.com/Cubais/TransporCityHelp/blob/master/PeopleCreatingGuide.png?raw=true)

It will create inserted number of people. Persons are colored by different color, based on theirs category.

|Color|Meaning|
|:---:|---|
|Green|Represents children|
|Red|Represents adults|
|Blue|Represents pensioners|


## Cars
Car's simulation is already implemented and nearly finished, althought you cannot see them in this commit in the Game Mode.

#### Creating cars in Editor mode
You can create cars in Editor mode though.
Follow the following steps in order to create some cars:
> Important buttons are highlited in the picture below
1. In the **Hierarchy panel** find the **CarCreator** game object and click on it
2. On the right side in the **Inspector** tab, you can see **Car Creator (Script)**
3. To create cars insert an integer in the field **Car Count**
4. After inserting the number, click on the empty checkbox next to the **Activator** text

![Creatung People Guide](https://github.com/Cubais/TransporCityHelp/blob/master/CreatingCarsGuide.png?raw=true)