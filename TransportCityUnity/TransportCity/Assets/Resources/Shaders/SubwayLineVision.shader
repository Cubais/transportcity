﻿Shader "Custom/SubwayLineVision"
{
	Properties
	{
		_Color1("Color1", Color) = (1,1,1,1)
	}

	SubShader
	{
		Tags { "Queue" = "Overlay" }
			   
		Pass
		{
			ZTest Always
			Color[_Color1]
		}
	}
}
