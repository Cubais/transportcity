﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[InitializeOnLoad()]
public class WaypointEditor
{
    // Draw Gizmos only if road segment is selected
    [DrawGizmo(GizmoType.Pickable | GizmoType.InSelectionHierarchy)]
    public static void OnDrawSceneGizmos(Waypoint waypoint, GizmoType gizmoType)
    {
        if ((gizmoType & GizmoType.Selected) != 0)
        {
            Gizmos.color = Color.red;
        }
        else
        {
            Gizmos.color = Color.red * 0.5f;
        }
                
        Gizmos.DrawSphere(waypoint.transform.position, .2f);

        waypoint.DrawNextWaypointGizmo();
    }
}
