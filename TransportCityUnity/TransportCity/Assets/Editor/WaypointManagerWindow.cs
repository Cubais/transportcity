﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class WaypointManagerWindow : EditorWindow
{
    public Transform waypointRoot;
    
    [MenuItem("Tools/Waypoint Editor")]
    public static void Open()
    {
        GetWindow<WaypointManagerWindow>();
    }

    public void OnGUI()
    {
        SerializedObject obj = new SerializedObject(this);

        EditorGUILayout.PropertyField(obj.FindProperty("waypointRoot"));
        
        if (waypointRoot == null)
        {
            EditorGUILayout.HelpBox("Plase assign a root transform", MessageType.Warning);
        }
        else
        {
            GUILayout.BeginVertical("Buttons");
            DrawButtons();
            GUILayout.EndVertical();
        }

        obj.ApplyModifiedProperties();
    }

    private void DrawButtons()
    {
        if (GUILayout.Button("Create car StraightWaypoint"))
        {
            CreateCarWaypoint<StraightWaypoint>();
        }
        
        if (GUILayout.Button("Create car IntersectionWaypoint"))
        {
            CreateCarWaypoint<IntersectionWaypoint>();
        }
    }

    /// <summary>
    /// Creates and link waypoint
    /// </summary>
    /// <typeparam name="WaypointType">Desired type of waypoint</typeparam>
    private void CreateCarWaypoint<WaypointType>()
    {
        var newWaypoint = CreateWaypoint<WaypointType>();

        var selectedWaypoint = Selection.activeGameObject?.GetComponent<Waypoint>();

        LinkWaypoints(selectedWaypoint, newWaypoint as Waypoint);
    }

    /// <summary>
    /// Creates waypoint of given type
    /// </summary>
    /// <typeparam name="WaypointType">Type of waypoint</typeparam>
    /// <returns></returns>
    private WaypointType CreateWaypoint<WaypointType>()
    {
        GameObject waypointObject = new GameObject(typeof(WaypointType) + "-" + waypointRoot.childCount, typeof(WaypointType));

        var collider = waypointObject.AddComponent<SphereCollider>();
        collider.radius = 0.1f;

        waypointObject.transform.SetParent(waypointRoot, false);
        waypointObject.tag = "CarWaypoint";

        return waypointObject.GetComponent<WaypointType>();
    }

    /// <summary>
    /// Link two waypoints
    /// </summary>
    /// <param name="selectedWaypoint">Waypoint to be linked to</param>
    /// <param name="newWaypoint">Waypoint linked to selectedWaypoint</param>
    private void LinkWaypoints(Waypoint selectedWaypoint, Waypoint newWaypoint)
    {
        if (newWaypoint == null)
            return;

        Selection.activeObject = newWaypoint;

        if (selectedWaypoint == null)
            return;

        selectedWaypoint.LinkWithWaypoint(newWaypoint);

        newWaypoint.transform.position = selectedWaypoint.transform.position;
        newWaypoint.transform.forward = selectedWaypoint.transform.forward;
    }
}
