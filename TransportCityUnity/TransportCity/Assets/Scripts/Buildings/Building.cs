﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

using static Constants;

public class Building : MonoBehaviour
{
    /// <summary>
    /// Position in BuildingManager
    /// </summary>
    public Vector2Int MapPosition { get; internal set; }

    /// <summary>
    /// Lowest local position the building is on
    /// </summary>
    public Vector2Int FirstMapPosition { get; internal set; }
    
    /// <summary>
    /// Highest local position the building is on
    /// </summary>
    public Vector2Int LastMapPosition { get; internal set; }

    /// <summary>
    /// Size of the building
    /// </summary>
    [SerializeField]
    internal int size;

    /// <summary>
    /// Capacity of building (how many residents can live there)
    /// </summary>    
    public int capacity = 50;

    /// <summary>
    /// How many people is currently moved in
    /// </summary>
    public int ResidentsNumber => residents.Count;

    /// <summary>
    /// Is there at least one space available?
    /// </summary>
    public bool IsSpaceLeft => ((capacity - ResidentsNumber) > 0);

    /// <summary>
    /// Purpose of the building (living, working, shopping)
    /// </summary>
    public ZoneType Type { get; internal set; }

    /// <summary>
    /// Closest road waypoint for this building
    /// </summary>
    public GameObject ClosestWaypoint { get; internal set; }

    /// <summary>
    /// In which cardinal direction is the door oriented (how is the building rotated)
    /// </summary>
    public CardinalDirection DoorDirection { get; internal set; }

    /// <summary>
    /// Road this building is facing (buildings on the corner are near two roads, but they are facing one of them)
    /// </summary>
    public GameObject FacingRoad { get; internal set; }

    /// <summary>
    /// Place in front of the door of this building
    /// </summary>
    public GameObject inFrontOfDoor;

    /// <summary>
    /// Idicates whether this building is school
    /// </summary>
    public bool isSchool = false;

    internal readonly List<Person> residents = new List<Person>();

    // A list of people that are currently inside the building.
    internal List<Person> peopleInBuilding = new List<Person>();

    

    private void Start() 
    {        
        if (capacity == 0)
            Debug.LogError("Capacity not set for building");    
    }

    /// <summary>
    /// Initializes the most basic things for this building
    /// </summary>
    public void Init(Vector2Int mapPosition, CardinalDirection direction, GameObject facingRoad, ZoneType zoneType)    
    {
        MapPosition = mapPosition;
        DoorDirection = direction;
        FacingRoad = facingRoad;

        Type = zoneType;

        ClosestWaypoint = SetClosestWaypoint();
    }

    /// <summary>
    /// Calculates first and last local positions of this building, so we know where exactly this building is in building manager.
    /// </summary>
    /// <param name="downLeftCorner">downLeftCorner from <see cref="RoadGraphGeneration"/> script</param>
    public void CalculateWholePosition(Vector2Int downLeftCorner)
    {
        Vector2 worldPosition = new Vector2(transform.position.x, transform.position.z);
        
        // Shift "road" downLeftCorner to our downLeftCorner
        Vector2 shift = new Vector2(-(roadPrefabSize / 2) - zoneWidth, -(roadPrefabSize / 2) - zoneWidth);
        
        var center = worldPosition - downLeftCorner - shift;

        FirstMapPosition = new Vector2Int((int)(center.x - size / 2f), (int)(center.y - size / 2f));
        LastMapPosition = new Vector2Int((int)(center.x + size / 2f) - 1, (int)(center.y + size / 2f) - 1);
    }

    /// <summary>
    /// Moves resident into this building and notifiy BuildingManager about it
    /// </summary>    
    /// <param name="newResident">Person to move in</param>
    public void ResidentMovedIn(Person newResident)
    {        
        // If there is enough space inside
        if (residents.Count != capacity)
        {
            residents.Add(newResident);
            BuildingManager.instance.PersonMovedIn(this);
        }
    }

	/// <summary>
	/// Updates map position (its position in building manager), if it changes (the array gets bigger for example)
    /// </summary>
	/// <param name="mapPosition"></param>
    public void UpdateMapPosition(Vector2Int mapPosition)
	{
		MapPosition = mapPosition;
	}

    /// <summary>
    /// Moves out the given person of this building
    /// </summary>
    /// <param name="currentResident">Resident to move out</param>
    public void ResidentMovedOut(Person currentResident)
    {
        if (residents.Contains(currentResident))
        {
            residents.Remove(currentResident);
            BuildingManager.instance.PersonMovedOut(this);
        }
    }

    /// <summary>
    /// Returns list of all residents living in this building.
    /// </summary>
    /// <returns></returns>
    public Person[] GetResidents()
    {
        return residents.ToArray();
    }
    public int Size()
    {
        return size;
    }

    /// <summary>
    /// Sets closest road waypoint to this building
    /// </summary>
    private GameObject SetClosestWaypoint()
    {
        var carWaypoint = FacingRoad.GetComponent<RoadSegment>().GetClosestWaypointToPosition(inFrontOfDoor.transform.position);
        return carWaypoint.gameObject;
    }

    private void OnDestroy()
    {
        if (!BuildingManager.areOnDestroyMethodsEnabled)
            return;

        // Notify people to move out
        var residentsTemp = residents.ToArray();
        foreach (var resident in residentsTemp)
        {
            resident?.OnMoveOut(this, false);
        }
    }
}

