﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering;
using static Constants;

public class BuildingManager : Builder
{
    public static BuildingManager instance;

    public static bool areOnDestroyMethodsEnabled = true;

    private const int maxRandomTries = 1000;

    /// <summary>
    /// Returns true if manager was already initialized
    /// </summary>
    public bool IsInitialized { get; internal set; }

    public int totalLivingCapacity;

    public List<Building> schools = new List<Building>();
        
    internal List<HashSet<Building>> buildingsOfZones;

    // Indicates what is the average percentual occupancy (residents / capacity) for whole zone, indexed by ZoneTypes
    public float[] buildingZonesAverageOccupancyRate;

    // Indicates total number of people using buildings of given ZoneType, indexed by ZoneTypes
    internal int[] buildingZonesTotalOccupancy;

    internal bool[,] treePlantation;

    private void Awake() 
    {
        if (BuildingManager.instance == null)
        {
            instance = this;
        }

        buildingsOfZones = new List<HashSet<Building>>();
        buildingZonesAverageOccupancyRate = new float[3];
        buildingZonesTotalOccupancy = new int[3];

        for (int i = 0; i < 3; i++)
        {
            buildingsOfZones.Add(new HashSet<Building>());
        }

        generator = GetComponent<RoadGraphGeneration>();
        zoneController = GetComponent<ZoneController>();        
    }

    /// <summary>
    /// Initializing manager with all the necessary data
    /// </summary>
    public void Init(BlockData[,] blockMap, Building[,] buildingMap, List<Building> buildings)
    {
        this.blockMap = blockMap;
        this.buildingMap = buildingMap;
        this.buildings = buildings;
        treePlantation = new bool[blockMap.GetLength(0), blockMap.GetLength(1)];

        foreach (var building in buildings)
        {
            buildingsOfZones[(int) building.Type].Add(building);

            if (building.isSchool && !schools.Contains(building))
            {
                schools.Add(building);
            }
        }
        // Recalculate total living capacity (some buildings may have been destroyed)
        totalLivingCapacity = buildingsOfZones[(int)ZoneType.Residential].Sum(x => x.capacity);
        Debug.Log("Total Living Capacity: " + totalLivingCapacity);

        PeopleManager.instance.GeneratePeopleOnStart();

        IsInitialized = true;

        downLeftCorner = generator.southWestCorner;

        if (schools.Count < 1)
        {
            AddMandatorySchools();
            Debug.Log($"Schools added manually.");
        }

        PlantTrees();
    }

    /// <summary>
    /// Updates manager when the road map changes.
    /// </summary>
    public void UpdateManager(BlockData[,] blockMap, Building[,] buildingMap)
    {
        this.blockMap = blockMap;
        this.buildingMap = buildingMap;

        downLeftCorner = generator.southWestCorner;

        if (treePlantation.GetLength(0) != blockMap.GetLength(0) || treePlantation.GetLength(1) != blockMap.GetLength(1))
        {
            var newTrees = new bool[blockMap.GetLength(0), blockMap.GetLength(1)];
            var shift = new Vector2Int((blockMap.GetLength(0) - treePlantation.GetLength(0)) / 2, (blockMap.GetLength(1) - treePlantation.GetLength(1)) / 2);

            for(int i = 0; i < treePlantation.GetLength(0); i++)
            {
                for (int j = 0; j < treePlantation.GetLength(1); j++)
                {
                    newTrees[shift.x + i, shift.y + j] = treePlantation[i, j];
                }
            }

            treePlantation = newTrees;
        }
    }

    /// <summary>
    /// Updates Manager when load is performed.
    /// </summary>
    public void LoadSyncManager(BlockData[,] blockMap, Building[,] buildingMap, List<Building> buildings)
    {
        this.buildings = buildings;
        UpdateManager(blockMap, buildingMap);
    }

    /// <summary>
    /// Builds buildings in the new district and updates other variables
    /// </summary>
    public void BuildNewBuildings()
    {
        PlantTrees();
        BuildBuildings(this);

        foreach (var b in buildings)
        {            
            if (!buildingsOfZones[(int)b.Type].Contains(b))
            {
                buildingsOfZones[(int)b.Type].Add(b);
                if (b.Type == ZoneType.Residential)
                {
                    totalLivingCapacity += b.capacity;
                }
            }

            if (b.isSchool && !schools.Contains(b))
            {
                schools.Add(b);
            }
        }

        AdjustOccupancyRatio(ZoneType.Residential);
        AdjustOccupancyRatio(ZoneType.Commercial);
        AdjustOccupancyRatio(ZoneType.Work);

        // Recalculate total living capacity (some buildings may have been destroyed)
        totalLivingCapacity = buildingsOfZones[(int)ZoneType.Residential].Sum(x => x.capacity);
    }

    /// <summary>
    /// Checks if manager is initialized, if it's not, throws an Exception
    /// </summary>
    private void CheckInitialization()
    {
        if (!IsInitialized)
        {
            throw new System.Exception($"Building Manager is not initialized yet. Try to initialize him before continuing.");
        }
    }

    /// <summary>
    /// Adds building on the coordinates (warning: coordinates (x,y) have to be near road, otherwise it will fail)
    /// </summary>
    public void AddBuilding(int x, int y)
    {
        CheckInitialization();

        Build(x, y, this);
    }

    /// <summary>
    /// Adds a specific building on the coordinates (warning: building must have at least one block near a road, otherwise it will fail)
    /// </summary>
    /// <param name="x">Numerically last position of the building on the x axis</param>
    /// <param name="y">Numerically first position of the building on the y axis</param>
    public void AddBuilding(int x, int y, string buildingName)
    {
        CheckInitialization();

        Build(x, y, buildingName, this);
    }

    /// <summary>
    /// Demolishes referenced building
    /// </summary>
    /// <param name="building"></param>
    public void DemolishBuilding(GameObject building)
    {
        CheckInitialization();

        Building buildingScript = building.GetComponent<Building>();

        building.SetActive(false);

        if (buildingScript.isSchool && schools.Contains(buildingScript))
        {
            schools.Remove(buildingScript);
        }

        // Cleanup - delete it from BuildingMap and BlockMap...
        buildings.Remove(buildingScript);
        RemoveBuilding(buildingScript);

        // Remove from BuildingMap and BlockMap: BuildingMap has same building on all its blocks
        for (int x = buildingScript.FirstMapPosition.x; x <= buildingScript.LastMapPosition.x; x++)
        {
            for (int y = buildingScript.FirstMapPosition.y; y <= buildingScript.LastMapPosition.y; y++)
            {
                if (AreIndexesInBounds(x,y) && buildingMap[x, y] == buildingScript) 
                {
                    blockMap[x, y].ChangeBlockState(BlockState.Free);
                    buildingMap[x, y] = null;
                }
                else
                {
                    throw new Exception($"This building has incorrect data and is not on searched blocks, " +
                        $"blocks searched: {buildingScript.FirstMapPosition} - {buildingScript.LastMapPosition}");
                }
            }
        }

        // We need to reset neighbouring roads data 
        // (we set them once and then just never build there if it isn't near a road, but when we delete a building somewhere, there may become a road there)
        for (int x = buildingScript.FirstMapPosition.x - maxBuildingSize; x <= buildingScript.LastMapPosition.x + maxBuildingSize; x++)
        {
            for (int y = buildingScript.FirstMapPosition.y - maxBuildingSize; y <= buildingScript.LastMapPosition.y + maxBuildingSize; y++)
            {
                if (AreIndexesInBounds(x, y) && blockMap[x, y] != null) 
                { 
                    blockMap[x, y].ResettingNeigboringRoads(); 
                }
            }
        }

        Destroy(building);
        //Debug.Log($"Building destroyed!");
    }


    /// <summary>
    /// Returs random building of set type
    /// </summary>
    public Building GetRandomBuilding(ZoneType zoneType)
    {
        // The idea here is that it could take couple of tries to finally randomly generate fitting building
        // So we randomly choose up to maxRandomTries times 
        // and if that won't work we're going to choose randomly, but with O(n) time complexity with guaranteed result
        // (if there is any)

        CheckInitialization();

        System.Random rnd = new System.Random();
        
        for (int i = 0; i < maxRandomTries; i++)
        {            
            int x = rnd.Next(0, buildingMap.GetLength(0) - 1);
            int y = rnd.Next(0, buildingMap.GetLength(1) - 1);

            if (buildingMap[x,y] != null && buildingMap[x,y].Type == zoneType && buildingMap[x, y].IsSpaceLeft)
            {
                return buildingMap[x, y];
            }
        }

        // If complete randomness wasn't good enough, we'll generate correct random result, a lot slowlier, but guaranteed
        Building building = buildingMap.Cast<Building>().                                                                       // flatten buildingMap
            Where(b => b.Type == zoneType && b.IsSpaceLeft).                                                                    // filter correct buildings (correct zone & have space)
            Select(b => new { r = rnd.NextDouble(), id = b.MapPosition.x * blockMap.GetLength(0) + b.MapPosition.y, b }).       // little bit of randomness - create anonymous class {random, unique, building}
            Min().                                                                                                              // select minimum such class (compares through random first, then position)
            b;                                                                                                                  // select building
        
        return building;
    }

    /// <summary>
    /// Get building of given zone type, where is still place to live
    /// </summary>
    /// <param name="zoneType">ZoneType of building</param>
    /// <returns>Building in the given ZoneType with free capacity</returns>
    public Building GetBuildingOfZone(ZoneType zoneType)
    {
        var searchedBuildings = buildingsOfZones[(int)zoneType];
        var occupancyRate = buildingZonesAverageOccupancyRate[(int)zoneType];
                
        foreach (var building in searchedBuildings)
        {
            var buildingOccupancy = building.ResidentsNumber / (float)building.capacity;
            if (building.IsSpaceLeft && buildingOccupancy <= occupancyRate)
            {
                return building;
            }            
        }

        // Should never reach this line
        return null;
    }

    /// <summary>
    /// Get random school
    /// </summary>
    /// <returns>Random school</returns>
    public Building GetSchool()
    {
        var schoolCount = schools.Count;
        var schoolIndex = UnityEngine.Random.Range(0, schoolCount);

        if (schoolCount == 0)
        {            
            return GetRandomBuilding(ZoneType.Work);
        }

        return schools[schoolIndex];
    }

    /// <summary>
    /// Returns building that's locally saved at this location
    /// </summary>
    public Building GetBuilding(int x, int y) => buildingMap[x, y];

    /// <summary>
    /// Returns building with given world position
    /// </summary>
    /// <param name="worldPosition">Correct world position</param>
    /// <returns>Building with given world position</returns>
    public Building GetBuildingAtWorldPosition(Vector3 worldPosition)
    {
        int offsetX = downLeftCorner.x - (zoneWidth + roadPrefabSize / 2);
        int offsetZ = downLeftCorner.y - (zoneWidth + roadPrefabSize / 2);

        int xPosition = (int)worldPosition.x - offsetX;
        int yPosition = (int)worldPosition.z - offsetZ;

        if (AreIndexesInBounds(xPosition, yPosition))
        {
            var building = GetBuilding(xPosition, yPosition);

            if (building.gameObject.transform.position.y == worldPosition.y)
            {
                return building;
            }
            else
            {
                return null;
            }
        }
        else
        {
            return null;
        }
    }
    
    /// <summary>
    /// Returns building with given world position
    /// </summary>
    /// <param name="worldPosition">X and Z coordinates of a world position (exact height is hard to calcuate right)</param>
    /// <returns>Building with given world position</returns>
    public Building GetBuildingWorldPosition(Vector2 worldPosition)
    {
        int offsetX = downLeftCorner.x - (zoneWidth + roadPrefabSize / 2);
        int offsetY = downLeftCorner.y - (zoneWidth + roadPrefabSize / 2);

        int xPosition = (int)worldPosition.x - offsetX;
        int yPosition = (int)worldPosition.y - offsetY;

        if (AreIndexesInBounds(xPosition, yPosition))
        {
            var building = GetBuilding(xPosition, yPosition);

            return building;
        }
        else
        {
            return null;
        }
	}

    public void PersonMovedIn(Building building)
    {
        buildingZonesTotalOccupancy[(int)building.Type]++;
        AdjustOccupancyRatio(building.Type);
    }

    public void PersonMovedOut(Building building)
    {
        buildingZonesTotalOccupancy[(int)building.Type]--;
        AdjustOccupancyRatio(building.Type);
    }

    /// <summary>
    /// When building without using the building manager to do so, manager needs to know, where will this building be.
    /// </summary>
    public void ExternalBuildObject(Vector2 from, Vector2 to, bool areLocalCoords)    
    {
        Vector2Int start = new Vector2Int((int)from.x,(int)from.y);
        Vector2Int end = new Vector2Int((int)to.x, (int)to.y);

        if (!areLocalCoords)
        {
            // If we've got world position, transform it to local position (array position)
            int offsetX = downLeftCorner.x - (zoneWidth + roadPrefabSize / 2);
            int offsetY = downLeftCorner.y - (zoneWidth + roadPrefabSize / 2);

            start = new Vector2Int((int)(from.x - offsetX), (int)(from.y - offsetY));
            end = new Vector2Int((int)(to.x - offsetX), (int)(to.y - offsetY));
        }

        for(int x = start.x; x <= end.x; x++)
        {
            for (int y = start.y; y <= end.y; y++)
            {
                if (blockMap[x,y] != null && blockMap[x,y].State == BlockState.Free)
                {
                    blockMap[x, y].ChangeBlockState(BlockState.Taken);
                }
                else
                {
                    throw new ArgumentException($"Object is built somewhere, where block state is {blockMap[x, y].State} instead of {BlockState.Free}");
                }
            }
        }

    }

    private void RemoveBuilding(Building building)
    {
        buildingsOfZones[(int)building.Type].Remove(building);
    }

    private bool AreIndexesInBounds(int x, int y)
    {
        return (x >= 0 && x < blockMap.GetLength(0) && y >= 0 && y < blockMap.GetLength(1));
    }
    
    /// <summary>
    /// Adjusts occupancy ratio (indicating how many people live per building) of given zone type
    /// </summary>
    /// <param name="zoneType">Zone type to adjust occupancy ratio</param>
    public void AdjustOccupancyRatio(ZoneType zoneType)
    {
        float totalCapacity = buildingsOfZones[(int) zoneType].Sum(x => x.capacity);
        var peopleUsingBuidings = buildingZonesTotalOccupancy[(int)zoneType];
        var occupancyRatio = peopleUsingBuidings / totalCapacity;

        buildingZonesAverageOccupancyRate[(int)zoneType] = occupancyRatio;

        if (occupancyRatio == 1) 
        { 
            Debug.Log($"Buildings of {zoneType} are full! Total capacity: {totalCapacity} / People living here: {peopleUsingBuidings}.");
            
        }
    }

    /// <summary>
    /// If there aren't enough randomly generated school, this will generate another 2 destroying buildings in the process.
    /// </summary>
    private void AddMandatorySchools()
    {
        var firstSchool = GetRandomBuilding(ZoneType.Work);
        var secondSchool = GetRandomBuilding(ZoneType.Work);
        while(secondSchool == firstSchool)
        {
            secondSchool = GetRandomBuilding(ZoneType.Work);
        }

        var firstsMapPosition = new Vector2Int(firstSchool.LastMapPosition.x,firstSchool.FirstMapPosition.y);
        var secondsMapPosition = new Vector2Int(secondSchool.LastMapPosition.x, secondSchool.FirstMapPosition.y);

        // Buildings in place of a first and second school should get destroyed in the process
        AddBuilding(firstsMapPosition.x, firstsMapPosition.y, "School");
        AddBuilding(secondsMapPosition.x, secondsMapPosition.y, "School");

        schools.Add(buildingMap[firstsMapPosition.x, firstsMapPosition.y]);
        schools.Add(buildingMap[secondsMapPosition.x, secondsMapPosition.y]);

        Debug.Log($"{string.Join(", ",schools)} are the schools here.");
    }

    private void PlantTrees()
    {       
        // Preparing bool array which contains false for every block we cannot plant tree on and true for every block where, theoretically, there can be a tree planted
        bool[,] canBePlantedHere = new bool[blockMap.GetLength(0), blockMap.GetLength(1)];

        // The idea here is that we find one block and get all blocks where we can plant trees in a row, then fill them with true, so we don't look at them again
        // It saves us time, because we don't have to look if each block it in closed space, but we can only do it in one dimension and not in both simultaneously
        for (int i = 0; i < canBePlantedHere.GetLength(0); i++)
        {
            for (int j = 0; j < canBePlantedHere.GetLength(1); j++)
            {
                // treePlantation is an array which contains all possible places for trees from the last call of this function 
                // (used when new districts are added so that we don't generate trees in other parts of the city as well)
                if (!treePlantation[i,j] && !canBePlantedHere[i, j] && (blockMap[i, j] == null || blockMap[i, j].State == BlockState.None))
                {
                    var lastcorner = FindLastPlantingBlock(i, j, canBePlantedHere);
                    if (lastcorner == 0) continue;

                    // Now fill all the tiles between (x,y) and lastcorner with true - there can be trees planted and we don't need to look at them again
                    for (int x = i; x <= lastcorner; x++)
                    {
                        canBePlantedHere[x, j] = true;
                    }
                }
            }
        }

        // Copying all possible building places into treePlantation (needed to help with districts)
        treePlantation = PerformOrOnArray(treePlantation, canBePlantedHere);

        // Now we go through this array and randomly generate trees
        List<Vector2Int> tobePlanted = new List<Vector2Int>();
        System.Random rnd = new System.Random();
        for (int i = 0; i < canBePlantedHere.GetLength(0); i++)
        {
            for (int j = 0; j < canBePlantedHere.GetLength(1); j++)
            { 
                if (rnd.NextDouble() < treePlantingProbability && CanBePlantedHere(i, j, canBePlantedHere))
                {
                    var position = new Vector2Int(i, j);
                    tobePlanted.Add(position);

                    // Now we cannot plant here, plus new blockMap needs to know that trees are there.
                    for (int x = i; x < i + treeSize; x++)
                    {
                        for (int y = j; y < j + treeSize; y++)
                        {
                            canBePlantedHere[x, y] = false;

                            Vector3 worldPosition = generator.ConvertTo3DAtCityHeight(downLeftCorner + position - new Vector2Int(zoneWidth + roadPrefabSize / 2 - 1, zoneWidth + roadPrefabSize / 2 - 1));
                            BlockData block = new BlockData();
                            block.Initialize(position, worldPosition, ZoneType.None, BlockState.Tree);
                            blockMap[x,y] = block;
                        }
                    }
                }
            }
        }

        // At last, instantiate them
        foreach(var treePosition in tobePlanted)
        {
            var treePrefab = Resources.Load<GameObject>($"{folderPathBuildingPrefabs}/Tree");
            Instantiate(treePrefab, blockMap[treePosition.x, treePosition.y].WorldPosition, Quaternion.identity, buildingParentObject.transform);
        }
    }

    /// <summary>
    /// Finds last block we can plant tree on to the right of (x,y) coordinates.
    /// </summary>
    /// <returns>x coordinate of the last valid block (on which trees can be planted)</returns>
    private int FindLastPlantingBlock(int x, int y, bool[,] canBePlantedHere)
    {
        // First, check that (x,y) is the first corner => they are surrounded by buildings or building places (BlockState.Free/Taken) from at least one side
        if (x == 0 || y == 0 || !IsClosedInThreeDirections(x, y)) return 0;
        if ((blockMap[x, y - 1] == null || (blockMap[x, y - 1].State != BlockState.Free && blockMap[x, y - 1].State != BlockState.Taken)) &&
            (blockMap[x - 1, y] == null || (blockMap[x - 1, y].State != BlockState.Free && blockMap[x - 1, y].State != BlockState.Taken)))
        { 
            return 0; 
        }

        // Then go in the direction of x and look if it is closed and where
        int highestx = x;
        while (highestx < canBePlantedHere.GetLength(0) && !canBePlantedHere[highestx, y] && (blockMap[highestx, y] == null || blockMap[highestx, y].State == BlockState.None)) 
        {
            highestx++;
        }
        if (highestx == canBePlantedHere.GetLength(0)) return 0;
        highestx --;

        
        return highestx;
    }

    /// <summary>
    /// Looks in three directions in coordinates (i,j) of blockMap array if the area around (i,j) is closed.
    /// (Closed means that there is a road in this direction somewhere)
    /// </summary>
    /// <returns>True, if there is a road to the up, down and left somewhere in blockMap array from (i,j), false otherwise.</returns>
    private bool IsClosedInThreeDirections(int i, int j)
    {
        int x = i;
        int y = j;

        while (x >= 0 && (blockMap[x, j] == null || blockMap[x, j].State == BlockState.None)) x--;
        bool isClosedx = x >= 0;

        while (y >= 0 && (blockMap[i, y] == null || blockMap[i, y].State == BlockState.None)) y--;
        bool isClosedy = y >= 0;

        y = j;
        while (y < blockMap.GetLength(1) && (blockMap[i, y] == null || blockMap[i, y].State == BlockState.None)) y++;
        isClosedy &= y < blockMap.GetLength(1);

        return isClosedx & isClosedy;
    }

    private bool CanBePlantedHere(int i, int j, bool[,] canBePlantedHere)
    {
        bool isSpace = true;
        for (int x = i; x < i + treeSize; x++)
        {
            for (int y = j; y < j + treeSize; y++)
            {
                if (x >= canBePlantedHere.GetLength(0) || y >= canBePlantedHere.GetLength(1) || !canBePlantedHere[x, y])
                {
                    isSpace = false;
                }
            }
        }

        return isSpace;
    }

    /// <summary>
    /// Performs OR operation on array and returns resulting array. Both input array have to have the same lengths of dimensions.
    /// </summary>
    private bool[,] PerformOrOnArray(bool[,] first, bool[,] second)
    {
        if (first.GetLength(0) != second.GetLength(0) || first.GetLength(1) != second.GetLength(1))
        {
            throw new ArgumentException($"Arrays on input do not have same lengths of both dimensions.");
        }

        bool[,] result = new bool[first.GetLength(0), first.GetLength(1)];

        for (int i = 0; i < result.GetLength(0); i++)
        {
            for (int j = 0; j < result.GetLength(0); j++)
            {
                result[i, j] = first[i, j] || second[i, j];
            }
        }

        return result;
    }
}
