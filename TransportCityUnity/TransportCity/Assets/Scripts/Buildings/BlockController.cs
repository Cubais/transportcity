﻿using UnityEngine;

using static Constants;

public class BlockController : MonoBehaviour
{
    // Terminology: 
    // block:   cube, 1 square of zone
    // zone:    outside of road, empty game object, contains children - blocks

    [SerializeField]
    private bool useToUpdateQuads;

    void Awake()
    {
        if (IsColliding())
        {
            // Destroy is not called immediately, but we need to destroy it anyways, 
            // so we just set it as unactive and plan the destruction

            gameObject.SetActive(false);
            Destroy(gameObject);
        }
    }

    /// <summary>
    /// Checks if this blocks is in the same place as another block.
    /// </summary>
    /// <returns>True if this block is in the same place as another one, false otherwise.</returns>
    private bool IsColliding()
    {
        var colliders = Physics.OverlapSphere(gameObject.transform.position, buildingBlockColliderRadius);

        foreach (var collider in colliders)
        {
            if (IsSupposedToBeDestroyed(collider))
            {
                //Debug.Log($"Just casually colliding with {collider.name}");
                return true;
            }            
        }
        return false;
    }

    /// <summary>
    /// Checks <c>gameObject</c>'s placement is the same as another GameObject's Collider and they cannot be on the same place together.
    /// </summary>
    /// <param name="collider">Collider of gameObject that's colliding with this gameObject.</param>
    /// <returns><c>True</c> if gameObject is interfering with another gameObject's collider, <c>false</c> otherwise.</returns>
    private bool IsSupposedToBeDestroyed(Collider collider)
    {
        return collider.gameObject != gameObject &&
            (collider.CompareTag(blockTag) || collider.CompareTag(roadTag));
    }

    /// <summary>
    /// If this block is to be used to update the quads, it notifies the RoadZone script of road to which
    /// the block is attached.
    /// </summary>
    private void OnDestroy()
    {
        if (useToUpdateQuads)
        {
            transform.parent.parent.GetComponent<RoadZone>().UpdateZoneMarkers(gameObject);
        }
    }
}
