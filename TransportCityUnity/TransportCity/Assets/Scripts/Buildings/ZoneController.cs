﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

using static Constants;

// Script execution order is higher, the script has to go after RoadGraphGeneration.cs
public class ZoneController : MonoBehaviour
{
    // Terminology: 
    // block:   cube, 1 square of zone
    // zone:    outside of road, empty game object, contains children - blocks

    /// <summary>
    /// Key: road, Value: zones for that road; 
    /// <c>zoneMap[x,y]</c> has info if building zone has free space on coordinates <c>[x,y]</c>.
    /// Coordinates <c>[0,0]</c> are for the block furthest to the road on the left.
    /// <c>1</c> if the space is free, <c>0</c> if it's taken by another road.
    /// </summary>
    public Dictionary<GameObject, int[,]> zoneMap;

    /// <summary>
    /// Key: world position, Value: road;
    /// Good for getting neighbour roads for some road.
    /// </summary>
    public Dictionary<Vector3, GameObject> roadPositions;

    /// <summary>
    /// Key: road, Value: type of zones his zones are (residential, work, commercial)
    /// Each zone has its own type/purpose, type determines which buildings will be built in the zone.
    /// </summary>
    public Dictionary<GameObject, ZoneType> zoneTypes;

    /// <summary>
    /// An array containing information about type of zone each road for every city part has.
    /// </summary>
    internal ZoneType[] cityPartTypes;

    /// <summary>
    /// Parent of all roads
    /// </summary>
    [SerializeField]
    private GameObject roadParent;
    
    // The colors associated with different zones.
    internal Dictionary<ZoneType, Color> zoneColors;

    // Determines whether zones are currently being shown.
    private bool showingZones = false;

    private void Awake()
    {
        zoneMap = new Dictionary<GameObject, int[,]>();
        roadPositions = new Dictionary<Vector3, GameObject>();
        zoneTypes = new Dictionary<GameObject, ZoneType>();
        
        zoneColors = new Dictionary<ZoneType, Color>();
        zoneColors.Add(ZoneType.None, new Color(1f, 1f, 1f, 0.5f));
        zoneColors.Add(ZoneType.Commercial, new Color(0f, 1f, 1f, 0.5f));
        zoneColors.Add(ZoneType.Residential, new Color(1f, 0f, 1f, 0.5f));
        zoneColors.Add(ZoneType.Work, new Color(1f, 1f, 0f, 0.5f));
    }

    private void Start()
    {
        Debug.Log("Zone Maps");

        var roadGraphGeneration = GetComponent<RoadGraphGeneration>();

        FillZoneMap();
        ChooseTypeForZones(roadGraphGeneration);

        UI_API.GetInstance().RegisterForStatisticsButtons(ToggleZones);
    }

    #region Creating zones

    /// <summary>
    /// Fills zoneMap dictionary properly with gameObject and its zones.
    /// </summary>
    public void FillZoneMap()
    {
        for (int i = 0; i < roadParent.transform.childCount; i++)
        {
            var road = roadParent.transform.GetChild(i).gameObject;

            if (road.CompareTag(roadTag))
            {
                var zones = GetEmptyBlocksInZones(road);
                zoneMap.Add(road, zones);
                roadPositions.Add(road.transform.position, road);
            }
        }
    }

    /// <summary>
    /// Updates zoneMap and other important dictionaries with new districts, so there can be buildings built there
    /// </summary>
    /// <param name="supermodels">New and changed RoadGraphGeneration's array of every road model in the city</param>
    public void UpdateZoneMap(GameObject[,] supermodels)
    {
        System.Random rand = new System.Random();
        ZoneType districtType = (ZoneType)rand.Next(3);

        foreach (var road in supermodels)
        {
            if (road != null && !zoneMap.TryGetValue(road, out _))
            {
                var zones = GetEmptyBlocksInZones(road);

                // if we have this position registered, it means there was already a road on this position
                // we have to keep its zoneType
                if (roadPositions.TryGetValue(road.transform.position, out var oldroad))
                {
                    roadPositions[road.transform.position] = road;
                }

                // This should not happen, road is not in zoneMap, it shouln't be here either
                if (zoneTypes.TryGetValue(road, out _))
                {
                    throw new Exception($"{road} on {road.transform.position} is in zoneTypes, but not in zoneMap in ZoneController!");
                }

                if (oldroad != null && zoneTypes.TryGetValue(oldroad, out var type))
                {
                    zoneTypes.Add(road, type);
                }
                else
                {
                    zoneTypes.Add(road, districtType);
                }

                zoneMap.Add(road, zones);

            }
        }
    }

    /// <summary>
    /// For a given game object calculates which blocks are already taken.
    /// </summary>
    /// <param name="road"></param>
    /// <returns>An array of ints, 1 means the block is free, 0 means the block is taken by another road.</returns>
    private int[,] GetEmptyBlocksInZones(GameObject road)
    {
        // Getting zones
        var zones = GetZoneObjectsNextToRoad(road);

        // For a zone, getting it's "emptiness" array (0 - block not there, 1 - zone is free)
        int zonesCount = zones.Count;
        int[,] zoneBlocks = null;

        for (int i = 0; i < zonesCount; i++)
        {
            zoneBlocks = FillBlocksByZone(zones[i]);
        }

        return zoneBlocks;
    }

    /// <summary>
    /// Returns all zones for a given road. Checks for unique tag of zone and if it's active in game scene.
    /// </summary>
    /// <param name="road"></param>
    /// <returns></returns>
    private List<Transform> GetZoneObjectsNextToRoad(GameObject road)
    {
        List<Transform> zones = new List<Transform>();

        for (int i = 0; i < road.transform.childCount; i++)
        {
            var child = road.transform.GetChild(i);
            if (child.CompareTag(zoneTag) && child.gameObject.activeInHierarchy)
            {
                zones.Add(child);
            }
        }

        return zones;
    }

    /// <summary>
    /// Fills zone array correctly with data about blocks that are still there
    /// <c>(</c>translates names of zone's children (cubes) into positions in the array and fills it accordingly<c>)</c>.
    /// </summary>
    /// <param name="zone">The zone we are checking.</param>
    private int[,] FillBlocksByZone(Transform zone)
    {
        int[,] blocks = new int[
            roadPrefabSize + 2 * zoneWidth,
            roadPrefabSize + 2 * zoneWidth];

        for (int i = 0; i < zone.childCount; i++)
        {
            var block = zone.GetChild(i);
            if (block.CompareTag(blockTag) && block.gameObject.activeInHierarchy)
            {
                var nameSplit = block.name.Split(' ');
                var positionString = nameSplit[nameSplit.Length - 1].Substring(1, nameSplit[nameSplit.Length - 1].Length - 2);

                if (!int.TryParse(positionString, out int position))
                {
                    Debug.Log($"Something wrong with {block.name}");
                    continue;
                }

                int xCoord = position / blocks.GetLength(1);
                int yCoord = position % blocks.GetLength(1);

                blocks[xCoord, yCoord] = 1;
            }
        }

        return blocks;
    }
    
    #endregion

    #region Assigning types

    /// <summary>
    /// Assignes each road of the city its type (residential, work, commercial).
    /// </summary>
    /// <param name="generator"><see cref="RoadGraphGeneration"/> that contains all road models of a city.</param>
    public void ChooseTypeForZones(RoadGraphGeneration generator)
    {
        // The plan is:
        // - we have few constants for how much of a city belongs to work/residential/commercial zones
        // - we divide the city into equal parts (by area)
        // - then we asssign each part of the city its zone
        //   - each road belonging to this part of the city gets zone in zoneTypes

        // Variable initialization
        int width = generator.centerDistrict.models.GetLength(0);
        int height = generator.centerDistrict.models.GetLength(1);
        int blockSizeHorizontal = (int) Math.Ceiling(width / (decimal) cityZonesDivision);
        int blockSizeVertical = (int) Math.Ceiling(height / (decimal) cityZonesDivision);
        System.Random rand = new System.Random();
        cityPartTypes = new ZoneType[cityZonesDivision * cityZonesDivision];

        // [type]Left means how much of city should have [type] and it's unassigned yet 
        // (so, when a part gets assigned [type], zonePart is subtracted from this [type]Left --> [type]Left -= zonePart)
        float workLeft = workZonePart;
        float resLeft = residentialZonePart;
        float comLeft = 1 - workLeft - resLeft;
        float partSize = 1f / (cityZonesDivision * cityZonesDivision);

        // Random permutation
        int[] randomPermutation = GetRandomPermutation(cityZonesDivision * cityZonesDivision, rand);

        // Type calculation
        foreach (int number in randomPermutation)
        {
            // eff[type]Left -- (effective) when [type]Left is less than one part of the city, it is zero 
            // (just to not mess with calculations, when some parts still can still be assigned)

            float effResLeft = (resLeft >= partSize) ? resLeft : 0;
            float effWorkLeft = (workLeft >= partSize) ? workLeft : 0;
            float effComLeft = (comLeft >= partSize) ? comLeft : 0;
            float sum = effResLeft + effWorkLeft + effComLeft;
            ZoneType chosen;

            // sum == 0 means that this part of the city is last (or second to last) to be assigned
            // Here we "roll the dice" probabilistically to choose the type for this part
            if (sum == 0)
            {
                chosen = ChooseType(resLeft, workLeft, comLeft, rand);
            }
            else
            {
                chosen = ChooseType(effResLeft, effWorkLeft, effComLeft, rand);
            }

            switch (chosen)
            {
                case ZoneType.Residential:
                    resLeft = Mathf.Clamp01(resLeft - partSize);
                    break;
                case ZoneType.Work:
                    workLeft = Mathf.Clamp01(workLeft - partSize);
                    break;
                case ZoneType.Commercial: 
                    comLeft = Mathf.Clamp01(comLeft - partSize);
                    break;
                default: throw new ArgumentException("Chosen type of the zone is not recognized");
            }

            cityPartTypes[number] = chosen;
        } 

        // Assigning types - each road gets corresponding type
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                var model = generator.centerDistrict.models[i, j];
                if (model != null)
                {
                    var type = cityPartTypes[(i / blockSizeHorizontal) * cityZonesDivision + (j / blockSizeVertical)];
                    zoneTypes.Add(model, type);

                    // Set the color of the adjacent rectangles based on what zone the road belongs to.
                    RoadZone rz = model.GetComponent<RoadZone>();
                    rz.InitializeZoneMarkerList();
                    rz.SetZoneMarkersColour(zoneColors[type]);
                }
            }
        }

    }
    
    /// <summary>
    /// Randomly chooses type how much of each type we're supposed to still assign.
    /// </summary>
    private ZoneType ChooseType(float resLeft, float workLeft, float comLeft, System.Random rand)
    {
        float sum = resLeft + workLeft + comLeft;
        var r = rand.NextDouble();

        if (r < resLeft / sum)
        {
            return ZoneType.Residential;
        }
        else if (r < (resLeft + workLeft) / sum)
        {
            return ZoneType.Work;
        }
        else
        {
            return ZoneType.Commercial;
        }
    }

    /// <summary>
    /// Returns random permutation of set size.
    /// </summary>
    /// <returns></returns>
    private int[] GetRandomPermutation(int size, System.Random rand)
    {
        int[] numbers = new int[size];

        for (int i = 0; i < size; i++)
        {
            numbers[i] = i;
        }

        return numbers.OrderBy(x => rand.NextDouble()).ToArray();
    }
    
    #endregion

    #region Debugging

    /// <summary>
    /// Just for debugging purposes as we have no idea otherwise if it works.
    /// Flattens the given array, gives delimiter after row and multiplies what's in an array by it's flattened position.
    /// </summary>
    /// <param name="intArray"></param>
    /// <returns></returns>
    private string FlattenWithPosition(int[,] intArray)
    {
        string flattened = "";

        for (int i = 0; i < intArray.GetLength(0); i++)
        {
            for(int j = 0; j < intArray.GetLength(1); j++)
                flattened += ((i * intArray.GetLength(1) + j) * intArray[i, j]) + " ";
            flattened += "|";
        }

        return flattened;
    }

    /// <summary>
    /// Just for debugging purposes. Returns string stating type for each city part linearly (from east to west, from north to south)
    /// </summary>
    /// <returns></returns>
    private string WriteTypesForCityParts()
    {
        string res = "";
        foreach(var type in cityPartTypes)
        {
            res += type.ToString() + ",";
        }

        return res;
    }

    #endregion

    /// <summary>
    /// Displays/disables showing the zones if show button 2 is clicked.
    /// </summary>
    /// <param name="buttonIndex"></param>
    private void ToggleZones(int buttonIndex)
    {
        if (buttonIndex == 2)
        {
            ToggleZones(!showingZones);
        }
        else
        {
            ToggleZones(false);
        }
    }
    /// <summary>
    /// Displays/disables showing the zones
    /// </summary>
    /// <param name="showZones">True <=> show zones</param>
    public void ToggleZones(bool showZones)
    {
        foreach (Transform road in roadParent.transform)
        {
            road.GetComponent<RoadZone>().ToggleZoneMarkers(showZones);
            showingZones = showZones;
        }
    }
}
