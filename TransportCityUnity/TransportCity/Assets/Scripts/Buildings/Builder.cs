﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

using static Constants;

/// <summary>
/// Class holding all the code for proper building all the types of buildings
/// </summary>
public abstract class Builder : MonoBehaviour
{
    // Note: Class is just abstract class, so BuildingCreator and BuildingManager can share the same code.
    // Therefore this class is not supposed to be attached to any script.

    internal RoadGraphGeneration generator;

    internal ZoneController zoneController;

    internal BlockData[,] blockMap;

    internal Building[,] buildingMap;

    internal List<Building> buildings;
    
    [SerializeField]
    internal const float buildProbability = 1f;

    [SerializeField]
    internal GameObject buildingParentObject;

    internal Vector2Int downLeftCorner;

    /// <summary>
    /// Builds buildings into the city, looks at zone type and building map. 
    /// If buildings are already built, checks each block to see if any other building spaces became free and fills them with buildings.
    /// </summary>
    public void BuildBuildings(BuildingManager manager)
    {
        System.Random rnd = new System.Random();

        for (int x = 0; x < blockMap.GetLength(0); x++)
        {
            for (int y = 0; y < blockMap.GetLength(1); y++)
            {
                SetNeighboringRoads(x, y);

                if (rnd.NextDouble() < buildProbability)
                {
                    Build(x, y, manager);
                }
            }
        }
    }

    /// <summary>
    /// Sets all neighboring roads for a building block given by its x and y coordinates.
    /// </summary>
    internal void SetNeighboringRoads(int x, int y)
    {
        if (blockMap[x, y] == null || blockMap[x, y].State == BlockState.None || blockMap[x, y].State == BlockState.Road)
        {
            return;
        }

        var block = blockMap[x, y];
        if (block.AreNeighboringRoadsSet())
        {
            return;
        }

        block.SettingNeighboringRoads();

        if (blockMap.GetLength(0) - 1 != x &&
            blockMap[x + 1, y] != null &&
            blockMap[x + 1, y].State == BlockState.Road)
        {
            block.SetNeighboringRoad(CardinalDirection.East);
            block.AddRoad(blockMap[x + 1, y].Road, CardinalDirection.East);
        }
        if (x != 0 &&
            blockMap[x - 1, y] != null &&
            blockMap[x - 1, y].State == BlockState.Road)
        {
            block.SetNeighboringRoad(CardinalDirection.West);
            block.AddRoad(blockMap[x - 1, y].Road, CardinalDirection.West);
        }
        if (blockMap.GetLength(1) - 1 != y &&
            blockMap[x, y + 1] != null &&
            blockMap[x, y + 1].State == BlockState.Road)
        {
            block.SetNeighboringRoad(CardinalDirection.North);
            block.AddRoad(blockMap[x, y + 1].Road, CardinalDirection.North);
        }
        if (y != 0 &&
            blockMap[x, y - 1] != null &&
            blockMap[x, y - 1].State == BlockState.Road)
        {
            block.SetNeighboringRoad(CardinalDirection.South);
            block.AddRoad(blockMap[x, y - 1].Road, CardinalDirection.South);
        }
    }

    /// <summary>
    /// Builds the largest possible building on the given coordinates.
    /// The coordinates need to point to a block near a road, otherwise nothing will happen.
    /// </summary>
    internal void Build(int x, int y, BuildingManager manager)
    {
        if (blockMap[x, y] != null &&
            blockMap[x, y].IsNearARoad() &&
            blockMap[x, y].State == BlockState.Free)
        {
            // Get size and direction -> we are building from (x,y) in this direction 
            // -> (x,y) will be exactly the opposite corner of the building if you look at it from when building is standing
            (int maxsize, var northsouth, var eastwest) = GetMaxFit(x, y);

            // Calculate center of building
            var toCenter = CalculateCenter(maxsize, northsouth, eastwest);

            // Direction the building will face + nearest road in this direction
            CardinalDirection direction = blockMap[x, y].SelectFacingRoadDirection();
            GameObject nearestRoad = blockMap[x, y].GetNeighboringRoadsFromDirection(direction);

            // Build building
            var buildingObject = BuildOneBuilding(x, y, maxsize, toCenter, direction);
            var building = buildingObject.GetComponent<Building>();

            if (building.isSchool && !BuildingManager.instance.schools.Contains(building))
                BuildingManager.instance.schools.Add(building);

            building.Init(new Vector2Int(x, y), direction, nearestRoad, blockMap[x, y].Type);                        
            building.CalculateWholePosition(downLeftCorner);

            buildings.Add(building);

            int nsDir = GetDirectionMultiplier(northsouth);
            int ewDir = GetDirectionMultiplier(eastwest);
            for (int i = 0; i < maxsize; i++)
            {
                for (int j = 0; j < maxsize; j++)
                {
                    blockMap[x + (i * ewDir), y + (j * nsDir)].ChangeBlockState(BlockState.Taken);
                    buildingMap[x + (i * ewDir), y + (j * nsDir)] = building;
                }
            }
        }
    }

    /// <summary>
    /// Builds a building with the given name given the coordinates of its upper left corner.
    /// </summary>
    internal void Build(int x, int y, string buildingName, BuildingManager manager)
    {
        if (blockMap[x, y] != null)
        {            
            GameObject newBuildingPrefab = Resources.Load<GameObject>($"Prefabs/Buildings/{buildingName}");
            int size = newBuildingPrefab.GetComponent<Building>().Size();

            // Search for the block of this building that is near a road.
            BlockData nearRoadBlock = null;
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    if (i == 0 || i == size - 1 || j == 0 || j == size - 1)
                    {
                        if (x - i >= 0 && y + j < blockMap.GetLength(1))
                        {
                            if (blockMap[x - i, y + j] != null && blockMap[x - i, y + j].IsNearARoad())
                            {
                                nearRoadBlock = blockMap[x - i, y + j];
                            }
                        }
                    }
                }
            }

            // Go through the squares that this building should occupy and destroy any building currently occupying it.
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    if (blockMap[x - i, y + j] != null && blockMap[x - i, y + j].State == BlockState.Taken)
                    {
                        manager.DemolishBuilding(buildingMap[x - i, y + j].gameObject);
                    }
                }
            }

            // Direction the building will face + nearest road in this direction
            CardinalDirection direction = nearRoadBlock.SelectFacingRoadDirection();
            GameObject nearestRoad = nearRoadBlock.GetNeighboringRoadsFromDirection(direction);

            // Build building
            var buildingObject = BuildOneSpecificBuilding(x, y, new Vector2(-size / 2, size / 2), direction, newBuildingPrefab);
            var building = buildingObject.GetComponent<Building>();

            building.Init(new Vector2Int(x, y), direction, nearestRoad, blockMap[x, y].Type);            
            building.CalculateWholePosition(downLeftCorner);

            buildings.Add(building);

            int ewDir = GetDirectionMultiplier(CardinalDirection.West);
            int nsDir = GetDirectionMultiplier(CardinalDirection.North);
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    blockMap[x + (i * ewDir), y + (j * nsDir)].ChangeBlockState(BlockState.Taken);
                    buildingMap[x + (i * ewDir), y + (j * nsDir)] = building;
                }
            }
        }
    }

    /// <summary>
    /// Calculates center of a building in coordinates given one corner of a building, size and direction in which we are building
    /// </summary>
    internal Vector2 CalculateCenter(int size, CardinalDirection northsouth, CardinalDirection eastwest)
    {
        int nsDir = GetDirectionMultiplier(northsouth);
        int ewDir = GetDirectionMultiplier(eastwest);

        return new Vector2((size - 1) * ewDir / 2f, (size - 1) * nsDir / 2f);
    }

    /// <summary>
    /// Looks around this building block and returns maximum size and direction where this (maximum size) building fits.
    /// </summary>
    /// <returns>Maximum size of building starting this building block, cardinal direction in north-south direction and direction in east-west direction.</returns>
    internal (int, CardinalDirection, CardinalDirection) GetMaxFit(int x, int y)
    {
        for (int size = maxBuildingSize; size > 0; size--)
        {
            if (DoesFitSquare(x, y, size, CardinalDirection.South, CardinalDirection.East))
            {
                return (size, CardinalDirection.South, CardinalDirection.East);
            }
            if (DoesFitSquare(x, y, size, CardinalDirection.North, CardinalDirection.East))
            {
                return (size, CardinalDirection.North, CardinalDirection.East);
            }
            if (DoesFitSquare(x, y, size, CardinalDirection.South, CardinalDirection.West))
            {
                return (size, CardinalDirection.South, CardinalDirection.West);
            }
            if (DoesFitSquare(x, y, size, CardinalDirection.North, CardinalDirection.West))
            {
                return (size, CardinalDirection.North, CardinalDirection.West);
            }
        }

        return (0, CardinalDirection.None, CardinalDirection.None);
    }

    /// <summary>
    /// Checks if the building fits in these directions
    /// </summary>
    internal bool DoesFitSquare(int x, int y, int size, CardinalDirection northsouth, CardinalDirection eastwest)
    {
        int nsDir = GetDirectionMultiplier(northsouth);
        int ewDir = GetDirectionMultiplier(eastwest);

        var block = blockMap[x, y];
        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                if (x + (i * ewDir) < 0 || x + (i * ewDir) > blockMap.GetLength(0) ||
                    y + (j * nsDir) < 0 || y + (j * nsDir) > blockMap.GetLength(1) ||
                    blockMap[x + (i * ewDir), y + (j * nsDir)] == null ||
                    blockMap[x + (i * ewDir), y + (j * nsDir)].State != BlockState.Free ||
                    blockMap[x + (i * ewDir), y + (j * nsDir)].Type != block.Type)
                    return false;
            }
        }

        return true;
    }

    /// <summary>
    /// Calculates the correct coordinates and instantiates the given building.
    /// </summary>
    internal GameObject BuildOneSpecificBuilding(int x, int y, Vector2 toCenter, CardinalDirection direction, GameObject toInstantiate)
    {
        var pos = blockMap[x, y].Position;
        var rot = GetRotationFromDirection(direction);
        Vector3 rotation = new Vector3(0, rot, 0);

        // Shift "road" downLeftCorner to our downLeftCorner
        Vector2 shift = new Vector2(-(roadPrefabSize / 2) - zoneWidth + .5f, -(roadPrefabSize / 2) - zoneWidth + .5f);

        // Calculate correct world position
        var realWorldPosition = generator.ConvertTo3DAtCityHeight(downLeftCorner + pos);
        realWorldPosition += new Vector3(shift.x + toCenter.x, /*buildingHeight / 2f*/0, shift.y + toCenter.y);

        // Build (return this gameObject)
        return Instantiate(toInstantiate, realWorldPosition, Quaternion.Euler(rotation), buildingParentObject.transform);
    }

    /// <summary>
    /// Picks the correct building to instantiate and builds it.
    /// </summary>
    internal GameObject BuildOneBuilding(int x, int y, int size, Vector2 toCenter, CardinalDirection direction)
    {
        var type = blockMap[x, y].Type;
        if (type == ZoneType.None)
        {
            Debug.Log("Shiiiit");
        }
        return BuildOneSpecificBuilding(x, y, toCenter, direction, GetBuildingPrefab(type.ToString(), size, size));
    }

    /// <summary>
    /// Gets a specific prefab of a building
    /// </summary>
    internal static GameObject GetBuildingPrefab(string buildingZone, int width, int length)
    {
        int numOfBuilding;
        if (buildingZone == ZoneType.Residential.ToString() && width == 3 && length == 3)
        {
            numOfBuilding = UnityEngine.Random.Range(1,7);
            return Resources.Load<GameObject>($"{folderPathBuildingPrefabs}/Home{numOfBuilding}");
        }   

        if (buildingZone == ZoneType.Commercial.ToString() && width == 3 && length == 3)
        {
            numOfBuilding = UnityEngine.Random.Range(1, 8);
            return Resources.Load<GameObject>($"{folderPathBuildingPrefabs}/Shop{numOfBuilding}");
        }

        if (buildingZone == ZoneType.Work.ToString() && width == 3 && length == 3)
        {
            var random = UnityEngine.Random.Range(0f, 1f);
            GameObject building;
            if (random < buildSchoolProbability)
            {
                building = Resources.Load<GameObject>($"{folderPathBuildingPrefabs}/School");
            }
            else
            {
                numOfBuilding = UnityEngine.Random.Range(1, 6);
                building = Resources.Load<GameObject>($"{folderPathBuildingPrefabs}/Work{numOfBuilding}");
            }

            return building;
        }

        if (width == 2 && length == 2)
        {
            return Resources.Load<GameObject>($"{folderPathBuildingPrefabs}/Chimney");
        }

        // Else 1x1 building
        return Resources.Load<GameObject>($"{folderPathBuildingPrefabs}/Transmitter");
    }

    /// <summary>
    /// Returns multiplier which we can use to go through the array with cycle in both directions
    /// </summary>
    internal static int GetDirectionMultiplier(CardinalDirection direction)
    {
        switch (direction)
        {
            case CardinalDirection.North:
                return 1;
            case CardinalDirection.South:
                return -1;
            case CardinalDirection.East:
                return 1;
            case CardinalDirection.West:
                return -1;
            default: throw new ArgumentException($"{direction} is not recognized as a direction");
        }
    }

    internal static int GetRotationFromDirection(CardinalDirection direction)
    {
        switch (direction)
        {
            case CardinalDirection.North:
                return 0;
            case CardinalDirection.South:
                return 180;
            case CardinalDirection.East:
                return 90;
            case CardinalDirection.West:
                return 270;
            default: throw new ArgumentException($"{direction} is not recognized as a direction");
        }
    }

    /// <summary>
    /// Converts world coordinates to block map coordinates.
    /// </summary>
    public Vector2Int WorldToMapCoordinates(Vector3 worldCoordinates)
    {
        int offsetX = downLeftCorner.x - (zoneWidth + roadPrefabSize / 2);
        int offsetY = downLeftCorner.y - (zoneWidth + roadPrefabSize / 2);

        Vector2 result = new Vector2(worldCoordinates.x, worldCoordinates.z);
        result.x -= offsetX;
        result.y -= offsetY;

        return new Vector2Int((int)Math.Floor(result.x), (int)Math.Floor(result.y));
    }
}
