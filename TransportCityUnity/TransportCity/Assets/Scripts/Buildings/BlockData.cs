﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

using static Constants;

public class BlockData
{
    /// <summary>
    /// Position in buildingMap array
    /// </summary>
    public Vector2Int Position { get; internal set; }

    /// <summary>
    /// Position of this corresponding block in the world
    /// </summary>
    public Vector3 WorldPosition { get; internal set; }

    /// <summary>
    /// Type of the zone the block belongs to.
    /// </summary>
    public ZoneType Type { get; internal set; }

    /// <summary>
    /// State the block is in.
    /// </summary>
    public BlockState State { get; internal set; }

    internal GameObject[] neighboringRoads;

    internal bool areNeighboringRoadsSet = false;

    /// <summary>
    /// Returns true if there is neighboring road from this direction.
    /// </summary>
    // USAGE: IsThereNeighboringRoad((int)Direction.North) returns true if block directly neighbors the road from north
    public bool[] IsThereNeighboringRoad { get; internal set; }

    /// <summary>
    /// If this square is a road, this is the reference to this road.
    /// </summary>
    public GameObject Road { get; internal set; }

    /// <summary>
    /// Initializes block with basic information.
    /// </summary>
    public void Initialize(Vector2Int position, Vector3 worldPosition, ZoneType type, BlockState state)
    {
        Position = position;
        WorldPosition = worldPosition;
        Type = type;
        State = state;

        neighboringRoads = new GameObject[5];
        IsThereNeighboringRoad = new bool[5];
    }

    /// <summary>
    /// When this block is under a road, we'll need the reference to it.
    /// </summary>
    public void SetRoad(GameObject road)
    {
        if (State == BlockState.Road)
        { 
            Road = road; 
        }
        else
        {
            throw new System.Exception("Cannot set a road if there actually isn't a road");
        }
    }

    /// <summary>
    /// Blocks neighboring to this are not set on initialization, therefore this returns false before setting neighboring roads.
    /// </summary>
    public bool AreNeighboringRoadsSet() => areNeighboringRoadsSet;

    /// <summary>
    /// This should be called before setting neighboring roads.
    /// </summary>
    public void SettingNeighboringRoads() => areNeighboringRoadsSet = true;

    /// <summary>
    /// This should be called after adding new district and deleting buildings on these and neighbouring blocks
    /// </summary>
    public void ResettingNeigboringRoads() => areNeighboringRoadsSet = false;

    /// <summary>
    /// Returns true if this blocks neighbors road from some side. Make sure neighboring roads are set before calling this method.
    /// </summary>
    public bool IsNearARoad() => IsThereNeighboringRoad.Any(x => x == true);

    /// <summary>
    /// Adds a road to the list of the roads neighboring the block.
    /// </summary>
    /// <param name="road"></param>
    public void AddRoad(GameObject road, CardinalDirection direction)
    {
        if (road.CompareTag(roadTag))
        {
            neighboringRoads[(int)direction] = road;
        }
        else
        {
            Debug.Log($"{road.name} is not a road, therefore it was not added.");
        }
    }

    /// <summary>
    /// Returns all neighboring roads.
    /// </summary>
    /// <returns></returns>
    public GameObject[] GetRoads()
    {
        return neighboringRoads;
    }

    /// <summary>
    /// Changes <see cref="BlockState"/> of the block to a new one.
    /// </summary>
    /// <param name="newState">New state of the block</param>
    public void ChangeBlockState(BlockState newState)
    {
        State = newState;
    }

    /// <summary>
    /// Tells this building block that it is near a road in direction in the argument.
    /// </summary>
    /// <param name="direction">Road is in this direction from building block. 
    /// If called with Direction.None, it just sets flag that neighboring roads were being set.</param>
    public void SetNeighboringRoad(CardinalDirection direction)
    {
        if (direction != CardinalDirection.None) 
        { 
            IsThereNeighboringRoad[(int)direction] = true; 
        }
    }

    /// <summary>
    /// Selects one direction in which there is a road
    /// </summary>
    public CardinalDirection SelectFacingRoadDirection()
    {
        for (int i = 0; i < neighboringRoads.Length; i++)
        {
            if (IsThereNeighboringRoad[i])
            {
                return (CardinalDirection)i;
            }
        }

        return CardinalDirection.None;
    }

    public GameObject GetNeighboringRoadsFromDirection(CardinalDirection direction)
    {
        GameObject obj;

        if (IsThereNeighboringRoad[(int)direction])
        {
            obj = neighboringRoads[(int)direction];
        }
        else
        {
            obj = null;
            Debug.Log($"There is no road in direction {direction}.");
        }

        return obj;
    }

    /// <summary>
    /// Updates position of this block in blockMap array, when it changes.
    /// </summary>
    /// <param name="position"></param>
    public void UpdatePosition(Vector2Int position)
    {
        Position = position;
    }
}
