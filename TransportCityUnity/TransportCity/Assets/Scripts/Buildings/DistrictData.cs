﻿using UnityEngine;

using static Constants;

public class DistrictData
{
    public readonly Vector2Int blockCount;
    public readonly int blockSize;

    public Vector2Int downLeftCorner;

    public Vector2Int citySize;
    public readonly Vector2 cityCenter;

    public bool[,] roads;
    public bool[,] junctions;
    public bool[,] crosswalks;
    public GameObject[,] models;

    public CardinalDirection directionFromMainDistrict;

    public readonly DistrictState districtState;

    public DistrictData(Vector2Int blockCount, int blockSize, DistrictState districtState, CardinalDirection fromMainDistrict = CardinalDirection.None)
    {
        this.blockCount = blockCount;
        this.blockSize = blockSize;

        citySize = blockCount * blockSize + Vector2Int.one;
        cityCenter = downLeftCorner + (citySize / 2);

        directionFromMainDistrict = fromMainDistrict;

        this.districtState = districtState;
    }
    internal DistrictData(Vector2Int blockCount, int blockSize, Vector2 cityCenter, DistrictState districtState)
    {
        this.blockCount = blockCount;
        this.blockSize = blockSize;
        this.districtState = districtState;
        this.cityCenter = cityCenter;
    }


    public void DistrictArraysInit()
    {
        roads = new bool[citySize.x, citySize.y];
        junctions = new bool[citySize.x, citySize.y];
        crosswalks = new bool[citySize.x, citySize.y];
        models = new GameObject[citySize.x, citySize.y];
    }

    public void SetCorner(Vector2Int downLeftCorner)
    {
        this.downLeftCorner = downLeftCorner;
    }
}
