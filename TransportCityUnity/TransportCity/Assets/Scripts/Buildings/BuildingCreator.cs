﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

using static Constants;

// Script execution order is higher, the script has to go after ZoneController.cs
public class BuildingCreator : Builder
{
    private void Start()
    {
        Debug.Log("Buildings");

        generator = GetComponent<RoadGraphGeneration>();
        zoneController = GetComponent<ZoneController>();
        
        // Prepare data for building
        MakeBuildingMap();

        // Build (from Builder class)
        BuildBuildings(BuildingManager.instance);

        // Initialize manager
        BuildingManager.instance.Init(blockMap, buildingMap, buildings);
    }

    /// <summary>
    /// Fills the map of the city (1m x 1m) with information what tile is building block, where is the road, which tiles are unoccupied and which tiles belong to which zone.
    /// </summary>
    public void MakeBuildingMap()
    {
        var models = generator.supermodels;
        int width = models.GetLength(0);
        int height = models.GetLength(1);
        downLeftCorner = generator.southWestCorner;

        blockMap = new BlockData[
            width * roadPrefabSize + 2 * zoneWidth,
            height * roadPrefabSize + 2 * zoneWidth
            ];

        buildingMap = new Building[
            width * roadPrefabSize + 2 * zoneWidth,
            height * roadPrefabSize + 2 * zoneWidth
            ];

        buildings = new List<Building>();

        // The idea here is to look at all the roads from models array (from RoadGraphGeneration)
        // For each road we get its zone and type
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if (models[i, j] != null)
                {
                    AddRoadZonesToMaps(new Vector2Int(i, j), blockMap);
                }
            }
        }
    }

    /// <summary>
    /// Updates building map (should be called after changing roads of the city, so building are generated only at places where they're supposed to)
    /// </summary>
    public void UpdateBuildingMap()
    {
        var models = generator.supermodels;
        int width = models.GetLength(0);
        int height = models.GetLength(1);

        // If supermodels array in RoadGraphGeneration got bigger (new "wave" of districts started), this gets bigger than (0,0)
        Vector2Int startIndex = downLeftCorner - generator.southWestCorner;

        downLeftCorner = generator.southWestCorner;

        BlockData[,] newBlockMap = new BlockData[
            width * roadPrefabSize + 2 * zoneWidth,
            height * roadPrefabSize + 2 * zoneWidth
            ];

        Building[,] newBuildingMap = new Building[
            width * roadPrefabSize + 2 * zoneWidth,
            height * roadPrefabSize + 2 * zoneWidth
            ];

        bool change = (startIndex != Vector2Int.zero);

        for (int m = 0; m < blockMap.GetLength(0); m++)
        {
            for (int n = 0; n < blockMap.GetLength(1); n++)
            {
                newBlockMap[m + startIndex.x, n + startIndex.y] = blockMap[m, n];
                newBuildingMap[m + startIndex.x, n + startIndex.y] = buildingMap[m, n];

                if (change && blockMap[m, n] != null )
                {
                    newBlockMap[m + startIndex.x, n + startIndex.y].UpdatePosition(new Vector2Int(m + startIndex.x, n + startIndex.y));
                }
                if (change && buildingMap[m, n] != null)
                {
                    newBuildingMap[m + startIndex.x, n + startIndex.y].UpdateMapPosition(new Vector2Int(m + startIndex.x, n + startIndex.y));
                    newBuildingMap[m + startIndex.x, n + startIndex.y].CalculateWholePosition(downLeftCorner);
                }
            }
        }

        // The idea here is to look at all the roads from models array (from RoadGraphGeneration)
        // For each road we get its zone and type
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if (models[i,j] != null &&
                       (newBlockMap[roadPrefabSize * i + zoneWidth, roadPrefabSize * j + zoneWidth] == null ||
                       newBlockMap[roadPrefabSize * i + zoneWidth, roadPrefabSize * j + zoneWidth].Road != models[i, j]))
                {
                    AddRoadZonesToMaps(new Vector2Int(i, j), newBlockMap);
                }
            }
        }

        blockMap = newBlockMap;
        buildingMap = newBuildingMap;

        BuildingManager.instance.UpdateManager(blockMap, buildingMap);
    }

    private int[,] RotateZoneToWorldPosition(float yRotation, int[,] zone)
    {
        int[,] zoneWorld = new int[zone.GetLength(0), zone.GetLength(1)];

        for (int x = 0; x < zone.GetLength(0); x++)
        {
            for (int y = 0; y < zone.GetLength(1); y++)
            {
                var rotatedPosition = GetRotatedValue((int)yRotation, x, y);
                zoneWorld[rotatedPosition.x, rotatedPosition.y] = zone[x, y];
            }
        }
        return zoneWorld;
    }

    private void AddRoadZonesToMaps(Vector2Int coordinates, BlockData[,] blockMap)
    {
        int i = coordinates.x;
        int j = coordinates.y;
        var model = generator.supermodels[i, j];

        if (!zoneController.zoneMap.TryGetValue(model, out var zone))
        {
            throw new Exception("Road is unknown by ZoneController");
        }

        if (!zoneController.zoneTypes.TryGetValue(model, out var type))
        {
            throw new Exception("Unknown road found");
        }

        // ZoneController works with zones relatively to the road, but the road may be rotated,
        // which means that if we want to work with zone relatively to the scene, we need to rotate the zone
        // Zone is 12x12 (3 blocks around road from each side), we need to "rotate it back" - traverse it and mark it in the right place
        Vector3 rotation = model.transform.eulerAngles;
        int[,] zoneWorldPos = RotateZoneToWorldPosition(rotation.y, zone);

        // Add with right rotation by zones into buildingMap
        for (int a = 0; a < zoneWorldPos.GetLength(0); a++)
        {
            for (int b = 0; b < zoneWorldPos.GetLength(1); b++)
            {
                // If there is a free block on this space in the zone, calculate this block's data
                if (zoneWorldPos[a, b] == 1)
                {
                    Vector2Int position = new Vector2Int(roadPrefabSize * i + a, roadPrefabSize * j + b);
                    Vector3 worldPosition = generator.ConvertTo3DAtCityHeight(downLeftCorner + position - new Vector2Int(zoneWidth + roadPrefabSize / 2, zoneWidth + roadPrefabSize / 2));

                    BlockData block = new BlockData();
                    BlockState blockState = (blockMap[roadPrefabSize * i + zoneWidth, roadPrefabSize * j + zoneWidth] != null &&
                            blockMap[roadPrefabSize * i + zoneWidth, roadPrefabSize * j + zoneWidth].State == BlockState.Road) ?
                        blockMap[position.x, position.y].State :
                        BlockState.Free;

                    if (blockState == BlockState.None)
                    {
                        Debug.Log($"looking at {position}, but there's nothing! At ({roadPrefabSize * i + zoneWidth},{roadPrefabSize * j + zoneWidth}) should be road");
                    }

                    block.Initialize(position, worldPosition + new Vector3(0.5f, 0, 0.5f), type, blockState);

                    blockMap[position.x, position.y] = block;
                }

            }
        }

        // Add BlockState.Road where the road is
        for (int a = zoneWidth; a < zoneWorldPos.GetLength(0) - zoneWidth; a++)
        {
            for (int b = zoneWidth; b < zoneWorldPos.GetLength(1) - zoneWidth; b++)
            {
                Vector2Int position = new Vector2Int(roadPrefabSize * i + a, roadPrefabSize * j + b);
                Vector3 worldPosition = generator.ConvertTo3DAtCityHeight(downLeftCorner + position - new Vector2Int(zoneWidth + roadPrefabSize / 2, zoneWidth + roadPrefabSize / 2));

                BlockData block = new BlockData();
                block.Initialize(position, worldPosition, type, BlockState.Road);
                block.SetRoad(model);

                blockMap[position.x, position.y] = block;
            }
        }

        
    }

    private Vector2Int GetRotatedValue(int rotation, int x, int y)
    {
        int i;
        int j;

        rotation /= 90;

        if(rotation % 2 == 0)
        {
            i = x;
            j = y;
        }
        else
        {
            i = y;
            j = x;
        }

        if ((rotation) / 2 == 1) 
        {
            i = roadPrefabSize + 2 * zoneWidth - 1 - i;
        }

        if ((rotation + 1) / 2 == 1) 
        {
            j = roadPrefabSize + 2 * zoneWidth - 1 - j;
        }

        return new Vector2Int(i,j);
    }
}
