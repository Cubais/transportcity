﻿/// <summary>
/// An interface for vehicles used in public transportation. It exist only so that it is possible to store these vehicles
/// in variables without specifying what kind of vehicle it is, but it has no functionality.
/// </summary>
public interface IVehicle
{}
