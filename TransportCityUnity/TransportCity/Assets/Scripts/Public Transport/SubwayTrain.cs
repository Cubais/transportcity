﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using static Constants;

public class SubwayTrain : MonoBehaviour, IVehicle
{
    public SubwayInstanceManager.SubwayTrainInstance instance;

    // When the train accumulates this amount of damage, it breaks down.
    public float damageMax = 100;

    // The initial value of the damage threshold.
    public float initialThreshold = 70;

    // The amount by which the damage threshold is to be increased when it is reached.
    public float thresholdStep = 10;

    private MeshRenderer meshRenderer;

    // The speed at which the train should move under normal circumstances.
    [SerializeField]
    internal float defaultSpeed = 10;

    // The train's current speed.
    internal float speed = 0;

    // The stop to which the train is currently headed.
    internal IStop currentStop;

    internal Vector3 destination;

    // Indicates whether the traun is currently stopping or has already stopped.
    internal bool stopping = false;

    // Indicates whether the train is currently accelerating or is at least not stopping.
    internal bool accelerating = false;

    // Determines whether the train is cycling through previous stops or next stops on its way.
    internal bool goingBack = false;

    // Indicates whether the train's instructions are in the process of being updated.
    internal bool waitingForUpdate = true;

    // A list of stops that the train has already passed through. Used to determine what stop it should go to
    // when its next stop is deleted.
    internal List<IStop> passedThrough = new List<IStop>();

    // The time that the HandlePassengers coroutine should wait between picking up/getting out passengers.
    internal float timeToWait = 0.2f;

    internal List<Person> passengers = new List<Person>();

    // A list of trains that were at a stop when this train arrived.
    internal List<SubwayTrain> preceedingTrains = new List<SubwayTrain>();

    // Indicates whether the train has already broken down or not.
    internal bool broken = false;

    // If the damage gets to this threshold, a notification is thrown.
    internal float threshold;

    // The amount of wear-and-tear that has accumulated on the train.
    internal float damage = 0;

    internal ChangeSubwayTrainSpeedCoroutineContext changeSpeedCC = new ChangeSubwayTrainSpeedCoroutineContext();

    // The position of the train in the previous frame.
    internal Vector3 prevPos;

    internal SubwayTrainHandlePassengerCoroutineContext subwayTrainHandlePassengerCC = new SubwayTrainHandlePassengerCoroutineContext();

    private void Awake()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        prevPos = transform.position;
        threshold = initialThreshold;
    }

    private void Update()
    {
        if (!broken)
        {
            // If the train has a destination, it should be moving.
            if (destination != null && (transform.position - destination).magnitude >= 0.3f)
            {
                // If the train is close to its destination, it should start decelerating.
                if ((transform.position - destination).magnitude <= 3)
                {
                    if (!stopping)
                    {
                        float deceleration = (speed * speed) / (2 * (transform.position - destination).magnitude);
                        Stop(deceleration * 0.05f);
                    }
                }
                else if (speed != defaultSpeed && !accelerating)
                {
                    Go(defaultSpeed);
                }

                transform.rotation = Quaternion.LookRotation(
                    Vector3.RotateTowards(transform.forward, destination - transform.position, speed * Time.deltaTime, 0.0f)
                );
                transform.position = Vector3.MoveTowards(
                    transform.position,
                    destination,
                    speed * Time.deltaTime
                );
            }

            // If the train has arrived at its destination, give it new directions.
            if (speed == 0 && waitingForUpdate && preceedingTrains.Count == 0)
            {
                StartCoroutine(HandlePassengers());
                waitingForUpdate = false;
            }

            // Accumulated damage increases with distance.
            damage += (prevPos - transform.position).magnitude / 200f;
            prevPos = transform.position;
            AssessDamage();
        }

        for (int i = preceedingTrains.Count - 1; i >= 0; i--)
        {
            if (!preceedingTrains[i].stopping || !preceedingTrains[i].gameObject.activeSelf || preceedingTrains[i].instance.line.Id != instance.line.Id)
            {
                preceedingTrains.RemoveAt(i);
            }
        }
    }

    /// <summary>
    /// Adds the train to a line and gives it its initial directions.
    /// </summary>
    /// <param name="startingStop"> The stop at which the train is spawned. </param>
    public void SendOut(IStop startingStop, bool direction)
    {
        instance.line.Vehicles.Add(this);
        goingBack = !direction;

        if (direction)
        {
            currentStop = startingStop.GetNextStop(instance.line.Id);
        }
        else
        {
            currentStop = startingStop.GetPreviousStop(instance.line.Id);
        }

        destination = currentStop.GetPosition();
        passedThrough.Add(startingStop);
    }

    /// <summary>
    /// Stops the train.
    /// </summary>
    /// <param name="rate"> The rate at which the train should decelerate. </param>
    public void Stop(float rate)
    {
        accelerating = false;
        stopping = true;
        StopAllCoroutines();
        StartCoroutine(ChangeSpeed(0, -rate));
    }

    /// <summary>
    /// Starts up the train.
    /// </summary>
    /// <param name="rate"> The rate at which the train should accelerate. </param>
    public void Go(float rate)
    {
        stopping = false;
        accelerating = true;
        StopAllCoroutines();
        StartCoroutine(ChangeSpeed(defaultSpeed, rate));
    }

    /// <summary>
    /// Makes the train visible/invisible.
    /// </summary>
    /// <param name="onOrOff"> true <=> make the train visible </param>
    public void ToggleVisibility(bool onOrOff)
    {
        meshRenderer.enabled = onOrOff;
    }

    /// <summary>
    /// A temporary method until train damage is implemented
    /// Should return the train's current health
    /// </summary>
    /// <returns>The health of the train</returns>
    public float GetHealth()
    {
        return damageMax - damage;
    }

    /// <summary>
    /// Decreases the damage that the train has accumulated so far.
    /// </summary>
    /// <param name="amount"> The amount by which the damage is to be decreased.</param>
    public void Repair(float amount)
    {
        damage -= amount;

        if (broken && damage < damageMax)
        {
            broken = false;

            while (threshold - thresholdStep > damage)
            {
                threshold -= thresholdStep;
            }
        }
    }

    /// <summary>
    /// Repairs the train fully
    /// </summary>
    public void Repair()
    {
        damage = 0;
        broken = false;
        threshold = initialThreshold;
    }

    /// <summary>
    /// Determines whether this train and another are going in the same train.
    /// </summary>
    /// <param name="otherGoingBack"> The value of the "goingBack" variable of the other train. </param>
    public bool SameDirection(bool otherGoingBack)
    {
        return goingBack == otherGoingBack;
    }

    /// <summary>
    /// Returns true if this train is slower than another train whose speed is given through the otherSpeed parameter,
    /// or if this trains speed is 0.
    /// </summary>
    public bool IsSlower(float otherSpeed)
    {
        return speed < otherSpeed || speed == 0;
    }

    /// <summary>
    /// A temporary method until train damage is implemented
    /// Move the train to this location and rotation without breaking
    /// </summary>
    /// <param name="location">Where to move the train</param>
    /// <param name="rotation">New rotation of the train</param>
    public void MoveToLocationWithoutBreaking(Vector3 location, Quaternion rotation)
    {
        transform.position = location;
        prevPos = location;
        transform.rotation = rotation;
        goingBack = false;
        waitingForUpdate = true;
    }

    /// <summary>
    /// Returns true if the id of the line to which this train belongs is the same as the id of another trains line
    /// which is passed through a parameter.
    /// </summary>
    public bool SameLine(int otherId)
    {
        return instance.line.Id == otherId;
    }

    /// <summary>
    /// Makes all the passengers get off the train (they appear at the last stop that the train had visited).
    /// </summary>
    public void KickEveryone()
    {
        SubwayStop exit;

        if (speed == 0)
        {
            exit = (SubwayStop)currentStop;
        }
        else if (!goingBack)
        {
            exit = (SubwayStop)currentStop.GetPreviousStop(instance.line.Id);
        }
        else
        {
            exit = (SubwayStop)currentStop.GetNextStop(instance.line.Id);
        }

        for (int i = passengers.Count - 1; i >= 0; i--)
        {
            passengers[i].ExitVehicle(exit.inFrontOfDoor.transform.position);
            passengers[i].UpdateDestination(passengers[i].GetCurrentBuildingDestination());
            passengers.Remove(passengers[i]);            
        }
    }

    /// <summary>
    /// Makes the train slow down or speed up to the desired speed by gradually changing it every 0.05 seconds.
    /// </summary>
    /// <param name="to">The desired speed.</param>
    /// <param name="amountPerIteration">The amount by which the speed should change every iteration.</param>
    internal IEnumerator ChangeSpeed(float to, float amountPerIteration)
    {
        changeSpeedCC.isRunning = true;
        changeSpeedCC.targetSpeed = to;
        changeSpeedCC.speedChangePerSecond = amountPerIteration;

        while (true)
        {
            if (speed == to || Mathf.Abs(speed - to) < Mathf.Abs(speed + amountPerIteration - to))
            {
                speed = to;
                break;
            }

            speed += amountPerIteration;

            yield return new WaitForSeconds(0.05f);
        }

        changeSpeedCC.isRunning = false;
    }

    // Decides which stop the train should go to next.
    internal IEnumerator HandlePassengers()
    {
        subwayTrainHandlePassengerCC.isRunning = true;

        yield return new WaitForSeconds(1f);

        // Check which passengers need to get off here and kick them out.
        int i = passengers.Count - 1;
        while (i >= 0)
        {
            Person p = passengers[i];

            if (currentStop == p.PathStops.Peek())
            {
                p.PathStops.Pop();
                p.ExitVehicle(((SubwayStop)currentStop).inFrontOfDoor.transform.position);                
                passengers.Remove(p);
                yield return new WaitForSeconds(timeToWait);
            }

            i--;
        }

        // Pick up new passengers.
        List<Person> newPassengers = currentStop.GetPeopleForLine(instance.line);
        while (newPassengers.Count != 0)
        {
            int index = newPassengers.Count - 1;

            if (CheckDirection(newPassengers[index].PathStops))
            {
                if (!newPassengers[index].isInsideVehicle)
                {
                    newPassengers[index].GetIntoVehicle(gameObject, TransportType.PublicTransport);
                    passengers.Add(newPassengers[index]);
                }

            ((SubwayStop)currentStop).RemovePerson(newPassengers[index]);
                newPassengers.RemoveAt(index);
                MoneyManager.instance.GainMoneyFlow(subwayTicketCost);
                yield return new WaitForSeconds(timeToWait);
            }
            else
            {
                newPassengers.RemoveAt(index);
            }
        }

        // Decide where to go next.
        if ((!goingBack && currentStop.GetNextStop(instance.line.Id) != null) ||
            (goingBack && currentStop.GetPreviousStop(instance.line.Id) == null))
        {
            // If the second parameter isn't true, then the stop to which the train was supposed to go next was deleted.
            if (goingBack && currentStop.GetNextStop(instance.line.Id) != null)
            {
                passedThrough.Clear();
            }

            passedThrough.Add(currentStop);

            currentStop = currentStop.GetNextStop(instance.line.Id);
            goingBack = false;
        }
        else
        {
            // If the second parameter isn't true, then the stop to which the train was supposed to go next was deleted.
            if (!goingBack && currentStop.GetPreviousStop(instance.line.Id) != null)
            {
                passedThrough.Clear();
            }

            passedThrough.Add(currentStop);            

            currentStop = currentStop.GetPreviousStop(instance.line.Id);
            goingBack = true;
        }

        // If the destinationStop is null, that means that the stop that the train was supposed to go to next was deleted.
        // In such a case, it should iterate through the path until it finds a stop it hasn't visited yet.
        if (currentStop == null)
        {
            // This parameter was flipped in the previous block of code.
            goingBack = !goingBack;

            List<IStop> path = instance.line.Path;
            
            if (!goingBack)
            {
                i = 0;

                while (i < path.Count && passedThrough.Contains(path[i]))
                {
                    i++;
                }

                if (i == path.Count)
                {
                    currentStop = path[i - 1];
                }
                else
                {
                    currentStop = path[i];
                }
            }
            else
            {
                i = path.Count - 1;

                while (i >= 0 && passedThrough.Contains(path[i]))
                {
                    i--;
                }

                if (i == -1)
                {
                    currentStop = path[i + 1];
                }
                else
                {
                    currentStop = path[i];
                }
            }
        }

        destination = currentStop.GetPosition();
        waitingForUpdate = true;

        subwayTrainHandlePassengerCC.isRunning = false;
    }

    /// <summary>
    /// Checks whether the direction in which the person wants to go is the same as the direction in which the train is going.
    /// </summary>
    /// <param name="stops"> A stack of stops that the person wants to go through. </param>
    private bool CheckDirection(Stack<IStop> stops)
    {
        if (stops == null || stops.Count == 0)
        {
            return false;
        }

        Stack<IStop> copy = new Stack<IStop>(new Stack<IStop>(stops));
        copy.Pop();
        bool direction = instance.line.Path.IndexOf(copy.Peek()) > instance.line.Path.IndexOf(currentStop);

        if (currentStop == instance.line.Path[0] || currentStop == instance.line.Path[instance.line.Path.Count - 1])
        {
            return goingBack == direction;
        }

        return goingBack == !direction;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.isTrigger && other.CompareTag("SubwayTrain"))
        {
            SubwayTrain otherTrain = other.GetComponent<SubwayTrain>();

            if (otherTrain.SameDirection(goingBack) && otherTrain.IsSlower(speed) && otherTrain.SameLine(instance.line.Id))
            {
                preceedingTrains.Add(otherTrain);
            }
        }
    }

    /// <summary>
    /// Assesses whether to throw a notification and whether the train should break down.
    /// </summary>
    private void AssessDamage()
    {
        if (damage >= threshold && damage < damageMax)
        {
            UI_API.GetInstance().DisplayNotification(
                $"Train {instance.name} is in a bad condition.",
                UI_API.NotificationLevel.WARN
                );
            threshold += thresholdStep;
        }
        if (damage >= damageMax)
        {
            BreakDown();
            UI_API.GetInstance().DisplayNotification(
                $"Train {instance.name} broke down.",
                UI_API.NotificationLevel.CRITICAL
                );
        }
    }

    /// <summary>
    /// Stops the train and disables its Update loop.
    /// </summary>
    private void BreakDown()
    {
        broken = true;
        Stop(1f);
    }
}
