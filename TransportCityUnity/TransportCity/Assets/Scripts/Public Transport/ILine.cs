﻿using System.Collections.Generic;
using UnityEngine;

public interface ILine
{
    /// <summary>
    /// The id of the line
    /// </summary>
    int Id { get; set; }

    /// <summary>
    /// The name of the line
    /// </summary>
    string Name { get; set; }

    /// <summary>
    /// The Color associated with the line
    /// </summary>
    Color Color { get; set; }

    /// <summary>
    /// A list of vehicles assigned to the line.
    /// </summary>
    List<IVehicle> Vehicles { get; set; }

    /// <summary>
    /// The path of the line
    /// </summary>
    List<IStop> Path { get; set; }

    /// <summary>
    /// Determines whether the line is closed.
    /// </summary>
    bool Closed { get; set; }
}
