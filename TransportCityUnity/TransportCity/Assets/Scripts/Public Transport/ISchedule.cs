﻿/// <summary>
/// An interface used by public transport lines to compute the recommended number of vehicles for the line
/// based on a time interval given by the player.
/// </summary>
public interface ISchedule
{
    /// <summary>
    /// The average interval at which vehicles should arrive at each stop
    /// </summary>
    int Interval { get; set; }

    /// <summary>
    /// Automatically calculated required number of vehicles in order to fulfill the interval
    /// </summary>
    int RequiredNumberOfVehicles { get; }

    void UpdateRequiredNumberOfVehicles();
}
