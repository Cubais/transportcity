﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

using static Constants;

public class Bus : CarNavigation, IVehicle
{
    // The BusInstance that holds this Bus script.
    public BusInstanceManager.BusInstance instance;

    // When the bus accumulates this amount of damage, it breaks down.
    public float damageMax = 100;

    // The initial value of the threshold
    public float initialThreshold = 70;

    // The amount by which the damage threshold is to be increased when it is reached.
    public float thresholdStep = 10;

    // The stop to which the bus is currently headed or at which it is currently stopped.
    internal IStop currentStop;

    // Indicates whether the bus has just stopped. (If yes, it should be given new directions.)
    internal bool justStopped = true;

    // The time that the HandlePassengers coroutine should wait between picking up/getting out passengers.
    internal float timeToWait = 0.2f;

    // The amount of wear-and-tear that has accumulated on the bus.
    internal float damage = 0;

    // The position of the bus in the previous frame.
    internal Vector3 prevPos;

    // Indicates whether the bus has already broken down or not.
    internal bool broken = false;

    // If the damage gets to this threshold, a notification is thrown.
    internal float threshold;

    internal BusHandlePassengerCoroutineContext busHandlePassengerCC = new BusHandlePassengerCoroutineContext();

    private void Awake()
    {
        var carNav = GetComponent<CarNavigation>();
        carNav.vehicleType = Constants.VehicleType.Bus;
    }

    private List<Person> passengers = new List<Person>();    

    private void Start()
    {
        prevPos = transform.position;
        threshold = initialThreshold;
    }

    private new void Update()
    {
        if (!broken)
        {
            base.Update();

            // If the bus has arrived at its destination, give it new directions.
            if (path.Count == 0 && carAtDestination && justStopped)
            {
                StartCoroutine(HandlePassengers());
                justStopped = false;
            }

            // Accumulated damage increases with distance.
            damage += (prevPos - transform.position).magnitude / 20f;
            prevPos = transform.position;
            AssessDamage();
        }
    }

    /// <summary>
    /// Adds the bus to a line and sends it on its way.
    /// </summary>
    /// <param name="startingStop"> The stop at which the bus is to be spawned. </param>
    public void SendOut(BusStop startingStop)
    {
        instance.line.Vehicles.Add(this);
        path = startingStop.GetNextPath(instance.line.Id);
        currentStop = startingStop.GetNextStop(instance.line.Id);
        destination = currentStop.GetAdjacentWaypoint().transform.position;
    }

    public float GetHealth()
    {
        return damageMax - damage;
    }

    /// <summary>
    /// Decreases the damage that the bus has accumulated so far.
    /// </summary>
    /// <param name="amount"> The amount by which the damage is to be decreased.</param>
    public void Repair(float amount)
    {
        damage -= amount;

        if (broken && damage < damageMax)
        {
            broken = false;

            while (threshold - thresholdStep > damage)
            {
                threshold -= thresholdStep;
            }
        }
    }

    /// <summary>
    /// Repairs the bus fully
    /// </summary>
    public void Repair()
    {
        damage = 0;
        broken = false;
        threshold = initialThreshold;
    }

    /// <summary>
    /// Move the bus to this location and rotation without breaking
    /// </summary>
    /// <param name="location">Where to move the bus</param>
    /// <param name="rotation">New rotation of the bus</param>
    public void MoveToLocationWithoutBreaking(Vector3 location, Quaternion rotation)
    {
        transform.position = location;
        prevPos = location;
        transform.rotation = rotation;
        justStopped = true;
    }

    /// <summary>
    /// Makes all the passengers get off the bus.
    /// </summary>
    public void KickEveryone()
    {
        Vector3 pos;

        if (speed == 0)
        {
            pos = currentStop.GetPosition();
        }
        else
        {
            NavMeshHit nMHit = new NavMeshHit();
            NavMesh.SamplePosition(transform.position, out nMHit, 5, NavMesh.AllAreas);
            pos = nMHit.position;
        }

        for (int i = passengers.Count - 1; i >= 0; i--)
        {
            passengers[i].ExitVehicle(pos);
            passengers[i].UpdateDestination(passengers[i].GetCurrentBuildingDestination());
            passengers.Remove(passengers[i]);
        }
    }

    internal IEnumerator HandlePassengers()
    {
        busHandlePassengerCC.isRunning = true;

        yield return new WaitForSeconds(1f);

        // Check which passengers need to get off here and kick them out.
        int i = passengers.Count - 1;
        while(i >= 0)
        {
            Person p = passengers[i];

            if (currentStop == p.PathStops.Peek())
            {
                p.PathStops.Pop();
                p.ExitVehicle(currentStop.GetPosition());                
                passengers.Remove(p);
                yield return new WaitForSeconds(timeToWait);
            }

            i--;
        }

        // Pick up new passengers.
        List<Person> newPassengers = currentStop.GetPeopleForLine(instance.line);
        while(passengers.Count <= instance.type.capacity && newPassengers.Count != 0)
        {
            newPassengers[newPassengers.Count - 1].GetIntoVehicle(gameObject, TransportType.PublicTransport);
            passengers.Add(newPassengers[newPassengers.Count - 1]);
            ((BusStop)currentStop).RemovePerson(newPassengers[newPassengers.Count - 1]);
            newPassengers.RemoveAt(newPassengers.Count - 1);
            MoneyManager.instance.GainMoneyFlow(busTicketCost);
            yield return new WaitForSeconds(timeToWait);
        }

        // This is a failsafe for the case when the player will start altering the bus line while buses are already present -
        // if the current stop has no next stop, the bus will simply wait.
        while (currentStop.GetNextStop(instance.line.Id) == null)
        {
            yield return new WaitForSeconds(2f);
        }

        path = ((BusStop)currentStop).GetNextPath(instance.line.Id);
        currentStop = currentStop.GetNextStop(instance.line.Id);
        destination = currentStop.GetAdjacentWaypoint().transform.position;
        justStopped = true;

        busHandlePassengerCC.isRunning = false;
    }

    /// <summary>
    /// Stops the bus and disables its Update loop.
    /// </summary>
    private void BreakDown()
    {
        broken = true;
        Stop(1f);
    }

    /// <summary>
    /// Assesses whether to throw a notification and whether the bus should break down.
    /// </summary>
    private void AssessDamage()
    {
        if (damage >= threshold && damage < damageMax)
        {
            UI_API.GetInstance().DisplayNotification(
                $"Bus {instance.name} is in a bad condition.", 
                UI_API.NotificationLevel.WARN
                );
            threshold += thresholdStep;
        }
        if (damage >= damageMax)
        {
            BreakDown();
            UI_API.GetInstance().DisplayNotification(
                $"Bus {instance.name} broke down.",
                UI_API.NotificationLevel.CRITICAL
                );
        }
    }
}
