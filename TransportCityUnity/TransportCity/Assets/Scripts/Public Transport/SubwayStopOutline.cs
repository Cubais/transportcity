﻿using UnityEngine;
using static Constants;

public enum SubwayOutlineState
{
    NotColliding,                       // <=> the outline isn't overlapping with any stops
    CollidingWithOneStop,               // <=> the outline is overlapping with only one stop (and possibly other buildings as well)
    CollidingWithBuildingsAndStops,     // <=> the outline is overlapping with multiple buildings, at least two of which are stops
}

public class SubwayStopOutline : MonoBehaviour
{
    // The blockMap coordinates of the centre of the outline.
    public Vector2Int centre;

    // The three possible colours of this outline. The first signals that the outline isn't colliding with any buildings,
    // the second one that it is colliding with other buildings and that they would be destroyed, should a stop be built there,
    // the third that it is just colliding with one building and it is a stop that isn't yet a part of this line.
    public Color notOverlappingColour, overlappingColour, overAnotherStopColour;

    // The line that is currently being built.
    public SubwayLineManager.SubwayLine currentLine;

    // The Renderer component of this outline.
    private Renderer outlineRenderer;

    // The light at the centre of the outline.
    private Light centreLight;

    // The amount of subway stops that this outline is currently overlapping with.
    private int overlappingStops = 0;

    // The position of the outline in the previous tick.
    private Vector3 previousPos;

    private void Start()
    {
        outlineRenderer = GetComponent<Renderer>();
        centreLight = GetComponentInChildren<Light>();

        centre = new Vector2Int(-1, -1);

        SetColour(notOverlappingColour);
    }

    private void Update()
    {
        // When the ouline's position changes, check what buildings it is overlapping with.
        if (transform.position != previousPos)
        {
            overlappingStops = 0;

            Collider[] overlapping = Physics.OverlapBox(gameObject.transform.position, transform.localScale / 2 * 0.95f, transform.rotation);
            foreach (Collider c in overlapping)
            {
                if (c.CompareTag(buildingTag))
                {
                    if (c.GetComponent<SubwayStop>())
                    {
                        overlappingStops++;
                    }
                    
                    if (overlappingStops == 1 && !currentLine.Path.Contains(c.GetComponent<SubwayStop>()))
                    {
                        SetColour(overAnotherStopColour);
                    }
                    else if (overlappingStops != 0)
                    {
                        SetColour(overlappingColour);
                    }
                }
            }

            if (overlappingStops == 0)
            {
                SetColour(notOverlappingColour);
            }

            previousPos = transform.position;
        }
    }

    public SubwayOutlineState GetState()
    {
        if (outlineRenderer.material.color == notOverlappingColour)
        {
            return SubwayOutlineState.NotColliding;
        }
        else if (outlineRenderer.material.color == overAnotherStopColour)
        {
            return SubwayOutlineState.CollidingWithOneStop;
        }
        else
        {
            return SubwayOutlineState.CollidingWithBuildingsAndStops;
        }
    }

    public void ResetTriggerCount()
    {
        overlappingStops = 0;
    }

    /// <summary>
    /// Changes the colour of the outline material as well as of the light in its centre.
    /// </summary>
    private void SetColour(Color c)
    {
        if (outlineRenderer.material.color != c)
        {
            outlineRenderer.material.color = c;

            Color lightColour = c;
            lightColour.a = 1;

            centreLight.color = lightColour;
        }
    }
}
