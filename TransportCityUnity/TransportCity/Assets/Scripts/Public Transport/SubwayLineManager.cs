﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubwayLineManager : MonoBehaviour, ILineManager
{
    public class SubwayLine : ILine
    {
        /// <summary>
        /// The id of the line
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The name of the line
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The Color associated with the line
        /// </summary>
        public Color Color { get; set; }

        /// <summary>
        /// A list of trains assigned to the line.
        /// </summary>
        public List<IVehicle> Vehicles { get; set; }

        /// <summary>
        /// The path of the line
        /// </summary>
        public List<IStop> Path { get; set; }

        /// <summary>
        /// The schedule of the line
        /// </summary>
        public ISchedule schedule;

        public bool Closed { get; set; }

        // Index of the stop at which the next train is to be spawned.
        internal int nextStopIndex = 0;

        // The direction in which a newly spawned train should move.
        // true <=> direction from Path[0] to Path[Path.Count - 1]
        internal bool direction = true;

        public SubwayLine()
        {
            Path = new List<IStop>();
            Vehicles = new List<IVehicle>();
            Closed = true;
        }

        public void AddStop(SubwayStop stop)
        {
            if (Path.Count > 0)
            {
                Path[Path.Count - 1].AddStop(stop, Id, true);
            }
            Path.Add(stop);
        }

        public void RemoveStop(SubwayStop stop)
        {
            stop.Lines.Remove(Id);

            IStop previous = stop.GetPreviousStop(Id);
            IStop next = stop.GetNextStop(Id);

            if (next != null)
            {
                next.RemoveStop(Id, false);
                stop.RemoveStop(Id, true);
            }

            if (previous != null)
            {
                previous.RemoveStop(Id, true);
                stop.RemoveStop(Id, false);

                // Connect the two stops with the line renderer of the latter one.
                if (next != null)
                {
                    previous.AddStop(next, Id, true);
                    ((SubwayStop)next).lineRenderers[Id].SetPosition(0, previous.GetPosition() + new Vector3(0, 1, 0));
                }
            }

            Path.Remove(stop);
        }

        /// <summary>
        /// Adds a train to the line and spawns it on the first stop
        /// </summary>
        /// <param name="instance">The train instance to spawn the train for</param>
        /// <param name="visibility"> Determines whether the train should be visible when spawned. </param>
        public void AddTrain(SubwayInstanceManager.SubwayTrainInstance instance, bool visibility)
        {
            if (Path.Count <= 1)
            {
                return;
            }

            if (instance.line != null)
            {
                instance.UnassignFromLine();
            }

            if (nextStopIndex >= Path.Count)
            {
                nextStopIndex = 0;
                direction = true;
            }

            if (nextStopIndex == Path.Count - 1)
            {
                direction = false;
            }

            if (nextStopIndex == 0)
            {
                direction = true;
            }

            SubwayTrain train;
            if (instance.train == null)
            {
                train = Instantiate(
                        Resources.Load<GameObject>("Prefabs/Subway/SubwayTrain"),
                        Path[nextStopIndex].GetPosition() + new Vector3(0, -1, 0),
                        Quaternion.identity
                    ).GetComponent<SubwayTrain>();

                instance.train = train;
                train.instance = instance;
            }
            else
            {
                train = instance.train;
                train.gameObject.SetActive(true);
                train.MoveToLocationWithoutBreaking(Path[nextStopIndex].GetPosition(), Quaternion.identity);
            }

            foreach (Material m in train.GetComponent<MeshRenderer>().materials)
            {
                m.SetColor("_Color1", Color);
            }

            instance.line = this;
            train.ToggleVisibility(visibility);
            train.SendOut(Path[nextStopIndex], direction);

            // Compute the index of the next stop where a train should be spawned and the direction of that train.
            if (!direction)
            {
                nextStopIndex = (Path.Count - 1) - nextStopIndex - 1;
                if (nextStopIndex < 0)
                {
                    nextStopIndex = (Path.Count - 1) - 1;
                }

                direction = true;               
            }
            else
            {
                nextStopIndex = (Path.Count - 1) - nextStopIndex;
                if (nextStopIndex == Path.Count)
                {
                    nextStopIndex = 1;
                }

                direction = false;
            }
        }

        /// <summary>
        /// Makes the line and the trains that belong to it visible/invisible.
        /// </summary>
        /// <param name="onOrOff"> true <=> turn visible </param>
        public void ToggleVisibility(bool onOrOff, bool subwayVisionActive)
        {
            Material rendererMat;
            float yOffset;
            if (subwayVisionActive)
            {
                rendererMat = Resources.Load<Material>("Materials/SubwayLineMaterial");
                yOffset = -1;
            }
            else
            {
                rendererMat = new Material(Shader.Find("Legacy Shaders/Particles/Alpha Blended Premultiply"));
                yOffset = 1;
            }

            foreach (SubwayStop s in Path)
            {
                // This check is necessary because the last stop doesn't have any line renderers.
                if (s.lineRenderers.ContainsKey(Id))
                {
                    s.lineRenderers[Id].gameObject.SetActive(onOrOff);

                    if (onOrOff)
                    {
                        s.lineRenderers[Id].material = rendererMat;
                        for (int i = 0; i < s.lineRenderers[Id].positionCount; i++)
                        {
                            Vector3 newPos = s.lineRenderers[Id].GetPosition(i);
                            newPos.y = s.transform.position.y + yOffset;
                            s.lineRenderers[Id].SetPosition(i, newPos);
                        }
                    }
                }

                if (subwayVisionActive)
                {
                    s.sprite.enabled = onOrOff;
                }
            }

            foreach (SubwayTrain t in Vehicles)
            {
                t.ToggleVisibility(onOrOff);
            }
        }
    }

    // The colour gradient that is used when drawing the subway tunnels.
    [SerializeField]
    private Gradient tunnelGradient;

    // Singleton.
    private static SubwayLineManager instance;

    // A list of all existing subway lines.
    public List<ILine> subwayLines = new List<ILine>();

    // Determines whether the subway lines are currently being displayed.
    public bool LinesDisplayed { get; internal set; }

    internal int nextLineIndex = 0;

    public int LinesCount => subwayLines.Count;

    private void Awake()
    {
        instance = this;
        LinesDisplayed = false;
    }

    private void Start()
    {
        UI_API.GetInstance().RegisterForStatisticsButtons(ToggleAllLines);
    }

    /// <summary>
    /// Get singleton instance
    /// </summary>
    /// <returns>Singleton instance of this class</returns>
    public static SubwayLineManager GetInstance()
    {
        return instance;
    }

    /// <summary>
    /// Get all subway lines
    /// </summary>
    /// <returns>A List of all subway lines</returns>
    public List<ILine> GetSubwayLines()
    {
        return subwayLines;
    }

    /// <summary>
    /// Create a new subway line with a random Color and return it
    /// </summary>
    /// <returns>A new subway line with random Color</returns>
    public SubwayLine CreateNewSubwayLine()
    {
        SubwayLine subwayLine = new SubwayLine();
        subwayLine.Id = nextLineIndex++;
        subwayLine.Name = "SubwayLine" + subwayLine.Id;
        subwayLine.Color = new Color(Random.value, Random.value, Random.value);
        subwayLine.Vehicles = new List<IVehicle>();
        subwayLine.Path = new List<IStop>();
        subwayLine.schedule = new SubwaySchedule(subwayLine);

        subwayLines.Add(subwayLine);
        return subwayLine;
    }

    /// <summary>
    /// Spawns a subway train at the first stop of the specified subway line.
    /// </summary>
    /// <param name="trainInstance">The subway train instance to be spawned</param>
    /// <param name="subwayLine">The subway line to spawn the train on</param>
    public void AddTrainInstanceToSubwayLine(SubwayInstanceManager.SubwayTrainInstance trainInstance, SubwayLine subwayLine)
    {
        subwayLine.AddTrain(trainInstance, LinesDisplayed);
    }

    /// <summary>
    /// Makes all the subway lines visible/invisible when a button with a specific index is clicked.
    /// </summary>
    public void ToggleAllLines(int buttonIndex)
    {
        if (buttonIndex == 3)
        {
            ToggleAllLines(!LinesDisplayed);
        }
        else
        {
            ToggleAllLines(false);
        }
    }

    /// <summary>
    /// Makes all the subway lines visible/invisible.
    /// </summary>
    /// <param name="onOrOff"> true <=> turn them visible </param>
    public void ToggleAllLines(bool onOrOff)
    {
        if (onOrOff != LinesDisplayed)
        {
            Camera.main.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = onOrOff;

            foreach (SubwayLine line in subwayLines)
            {
                line.ToggleVisibility(onOrOff, true);
            }

            LinesDisplayed = !LinesDisplayed;
        }
    }

    /// <summary>
    /// Returns a list of all the existing subway lines.
    /// </summary>
    /// <returns></returns>
    public List<ILine> GetLines()
    {
        return subwayLines;
    }
}
