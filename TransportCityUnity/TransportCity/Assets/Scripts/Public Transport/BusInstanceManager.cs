﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BusInstanceManager : MonoBehaviour
{
    public class BusInstance
    {
        /// <summary>
        /// The ID of the vehicle
        /// </summary>
        public int id;

        /// <summary>
        /// The name of the vehicle
        /// </summary>
        public string name;

        /// <summary>
        /// The type of the bus
        /// </summary>
        public BusType type;

        /// <summary>
        /// The script of the bus prefab instance associated with this BusInstance
        /// </summary>
        public Bus bus;

        /// <summary>
        /// The line to which the bus is assigned.
        /// </summary>
        public BusLineManager.BusLine line;
    }

    public class BusType
    {
        /// <summary>
        /// The name of the bus type
        /// </summary>
        public string name;

        /// <summary>
        /// The price of a new bus of this type
        /// </summary>
        public float price;

        /// <summary>
        /// The capacity of the buses of this type as a number of people
        /// </summary>
        public int capacity;

        /// <summary>
        /// The durability of buses of this bus type
        /// </summary>
        public float durability;

        /// <summary>
        /// Path to the prefab
        /// </summary>
        public string path;
    }

    // A List of all bus instances
    internal List<BusInstance> buses;

    // A List of bus types
    internal List<BusType> busTypes;

    internal int nextBusIndex = 0;

    // Singleton
    private static BusInstanceManager instance;

    /// <summary>
    /// Number of bought buses
    /// </summary>
    public int BusCount => buses.Count;

    void Awake()
    {
        buses = new List<BusInstance>();
        busTypes = new List<BusType>();
        FillBusTypes();
        instance = this;
    }

    /// <summary>
    /// Get singleton instance
    /// </summary>
    /// <returns>Singleton instance of this class</returns>
    public static BusInstanceManager GetInstance()
    {
        return instance;
    }

    /// <summary>
    /// Get all buses
    /// </summary>
    /// <returns>A List of all buses</returns>
    public List<BusInstance> GetBusInstances()
    {
        return buses;
    }

    /// <summary>
    /// Get all bus types
    /// </summary>
    /// <returns>A List of all bus types</returns>
    public List<BusType> GetBusTypes()
    {
        return busTypes;
    }

    /// <summary>
    /// Create a new bus and return it
    /// </summary>
    /// <param name="type">The type of the new bus instance</param>
    /// <returns>The newly created BusInstance object</returns>
    public BusInstance CreateNewBusInstance(BusType type)
    {
        BusInstance newBus = new BusInstance();

        newBus.id = nextBusIndex++;
        newBus.name = "Bus" + newBus.id;
        newBus.type = type;
        newBus.bus = null;

        buses.Add(newBus);
        return newBus;
    }

    /// <summary>
    /// Delete a bus instance
    /// </summary>
    /// <param name="instance">The BusInstance to be deleted</param>
    public void DeleteBusInstance(BusInstance instance)
    {
        buses.Remove(instance);

        if (instance.bus != null)
        {
            if (instance.line != null)
            {
                instance.line.Vehicles.Remove(instance.bus);
            }

            Destroy(instance.bus.gameObject);
        }
    }

    // A (temporary) method to generate bus types
    private void FillBusTypes()
    {
        BusType type1 = new BusType();
        BusType type2 = new BusType();

        type1.name = "Karosa CityBus";
        type1.price = 10000;
        type1.capacity = 60;
        type1.durability = 100;
        type1.path = "Prefabs/Buses/Bus1";

        type2.name = "Ikarus 280";
        type2.price = 6000;
        type2.capacity = 40;
        type2.durability = 85;
        type2.path = "Prefabs/Buses/Bus2";

        busTypes.Add(type1);
        busTypes.Add(type2);
    }
}
