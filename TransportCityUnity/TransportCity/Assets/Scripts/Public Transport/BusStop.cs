﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class BusStop : MonoBehaviour, IStop
{
    // The waypoint adjacent to the stop.
    public Waypoint adjacentWaypoint;

    // The waypoint through which a vehicle enters the bus stop (used to block entry when spawning buses).
    public Waypoint entranceWaypoint;

    // The key is a bus line id and the value is the line renderer associated with that bus line.
    // It is necessary to have multiple line renderers in case all the bus lines are being shown.
    public Dictionary<int, LineRenderer> lineRenderers = new Dictionary<int, LineRenderer>();

    // A list of ids of lines that this stop is a part of.
    public List<int> Lines { get; set; }

    // The keys in these dictionaries are ids of bus lines and the values are the next/previous
    // bus stop in that line.
    internal Dictionary<int, BusStop> next = new Dictionary<int, BusStop>();
    internal Dictionary<int, BusStop> previous = new Dictionary<int, BusStop>();

    // The keys are ids of bus lines and the values are stacks of directions to the next stop.
    internal Dictionary<int, Stack<Direction>> nextPath = new Dictionary<int, Stack<Direction>>();

    // Determines whether a bus can be spawned at the station or not.
    // True <=> it can't be spawned.
    [SerializeField]
    internal bool occupied = false;

    // A list of the people currently at the bus stop.
    [SerializeField]
    internal List<Person> peopleAtStop = new List<Person>();

    // A list of BusInstances that are waiting to be spawned.
    [SerializeField]
    internal Queue<BusInstanceManager.BusInstance> instancesToSpawn = new Queue<BusInstanceManager.BusInstance>();

    // When spawning buses, the script waits this amount of time to spawn a bus before it blocks the entrance.
    [SerializeField]
    private float timeBeforeBlocking = 3f;

    // Determines whether the entrance has been blocked.
    [SerializeField]
    internal bool blockedEntrance = false;

    // The last vehicle to enter the stop's trigger collider. Used to determine whether a bus can be spawned.
    [SerializeField]
    internal CarNavigation lastToEnter;

    // A list of vehicles that are currently intersecting the stop's trigger collider.
    [SerializeField]
    internal List<CarNavigation> vehiclesInTrigger = new List<CarNavigation>();

    internal SpawnBusesCoroutineContext spawnBusesCC = new SpawnBusesCoroutineContext();
    private void Awake()
    {
        Lines = new List<int>();
    }

    private void Update()
    {
        for (int i = peopleAtStop.Count - 1; i >= 0; i--)
        {
            if (!peopleAtStop[i].meshRenderer.enabled)
            {
                peopleAtStop.RemoveAt(i);
            }
        }
    }

    /// <summary>
    /// Toggles occupied to true if a vehicle enters the stop's trigger collider.
    /// Also registers people that come to the stop.
    /// </summary>
    private void OnTriggerEnter(Collider other)
    {
        if (!other.isTrigger)
        {
            CarNavigation cn = other.GetComponent<CarNavigation>();
            if (cn)
            {
                if (vehiclesInTrigger.Count == 0)
                {
                    occupied = true;
                }
                vehiclesInTrigger.Add(cn);

                lastToEnter = cn;
            }
            else if (other.tag == "Person")
            {
                peopleAtStop.Add(other.GetComponent<Person>());
            }
        }
    }

    /// <summary>
    /// Toggles occupied to false if the last vehicle leaves the stop's trigger collider.
    /// Also removes people that leave the stop.
    /// </summary>
    private void OnTriggerExit(Collider other)
    {
        if (!other.isTrigger)
        {
            if (other.GetComponent<CarNavigation>())
            {
                vehiclesInTrigger.Remove(other.GetComponent<CarNavigation>());

                if (vehiclesInTrigger.Count == 0)
                {
                    occupied = false;
                    lastToEnter = null;
                }
            }
            else if (other.tag == "Person")
            {
                peopleAtStop.Remove(other.GetComponent<Person>());
            }
        }
    }

    /// <summary>
    /// Adds a stop to 'next' and also a path leading to it to 'nextPath'.
    /// </summary>
    /// <param name="id"> The id of the line to which the stop is to be added. </param>
    /// <param name="stop"> The stop that is to be added. </param>
    public void AddNextStop(int id, BusStop stop)
    {
        if (!Lines.Contains(id))
        {
            Lines.Add(id);
        }

        next.Remove(id);
        next.Add(id, stop);
        if (stop != null)
        {
            nextPath.Remove(id);
            nextPath.Add(id, PathManager.GetInstance().GetPath(adjacentWaypoint, stop.adjacentWaypoint));
        }
    }

    /// <summary>
    /// Adds a stop to 'previous'.
    /// </summary>
    /// <param name="id"> The id of the line to which the stop is to be added. </param>
    /// <param name="stop"> The stop that is to be added. </param>
    public void AddPreviousStop(int id, BusStop stop)
    {
        if (!Lines.Contains(id))
        {
            Lines.Add(id);
        }

        previous.Remove(id);
        previous.Add(id, stop);
    }

    /// <summary>
    /// Adds a stop to a given line. Also adds this stop to the tables of the stop that is being added.
    /// </summary>
    /// <param name="id"> The id to which the line is to be added. </param>
    /// <param name="stop"> The stop that is to be added. </param>
    /// <param name="nextOrPrevious"> A boolean indicating whether the stop is supposed to come before or after this stop. 
    /// (true <=> the stop is to be added to the 'next' dictionary) </param>
    public void AddStop(IStop stop, int id, bool nextOrPrevious)
    {
        if(nextOrPrevious)
        {
            AddNextStop(id, (BusStop)stop);
            if (stop != null)
            {
                ((BusStop)stop).AddPreviousStop(id, this);
            }
        }
        else
        {
            AddPreviousStop(id, (BusStop)stop);
            if (stop != null)
            {
                ((BusStop)stop).AddNextStop(id, this);
            }
        }
    }

    /// <summary>
    /// Removes a stop from the tables.
    /// </summary>
    /// <param name="id"> The id of the line from which the stop is to be discarded. </param>
    /// <param name="nextOrPrevious"> A bolean that indicates whether the stop is to discarded from the 'next' or 'previous' tables. 
    /// (true <=> the stop is to be removed from the 'next' dictionary) </param>
    public void RemoveStop(int id, bool nextOrPrevious)
    {
        if (nextOrPrevious)
        {
            next.Remove(id);
            nextPath.Remove(id);
        }
        else
        {
            previous.Remove(id);
        }
    }

    /// <summary>
    /// Returns the next stop or null if there is no next stop for the given id in the 'next' table.
    /// </summary>
    /// <param name="id"> The id of the line from which the next stop is being requested. </param>
    /// <returns> The requested stop or null. </returns>
    public IStop GetNextStop(int id)
    {
        if (next.ContainsKey(id))
        {
            return next[id];
        }

        return null;
    }

    /// <summary>
    /// Returns the previous stop or null if there is no previous stop for the given id in the 'previous' table.
    /// </summary>
    /// <param name="id"> The id of the line from which the previous stop is being requested. </param>
    /// <returns> The requested stop or null. </returns>
    public IStop GetPreviousStop(int id)
    {
        if (previous.ContainsKey(id))
        {
            return previous[id];
        }

        return null;
    }

    /// <summary>
    /// Returns the path to the next stop or null if there is no path for the given id in the 'nextPath' table.
    /// </summary>
    /// <param name="id"> The id of the line from which the path is being requested. </param>
    /// <returns> The requested path or null. </returns>
    public Stack<Direction> GetNextPath(int id)
    {
        if (nextPath.ContainsKey(id))
        {
            // Creating a Stack from an existing Stack reverses the elements, so a double construction must be used.
            return new Stack<Direction>(new Stack<Direction>(nextPath[id]));
        }

        return null;
    }

    /// <summary>
    /// Returns the path to the previous stop or null if there is no previous stop for the given id in the 'previous' table.
    /// </summary>
    /// <param name="id"> The id of the line from which the path is being requested. </param>
    /// <returns> The requested path or null. </returns>
    public Stack<Direction> GetPreviousPath(int id)
    {
        if (previous.ContainsKey(id))
        {
            // Creating a Stack from a Stack reverses the elements.
            return new Stack<Direction>(previous[id].GetNextPath(id));
        }
        else
        {
            return null;
        }
    }

    // Removes the entries for the given line from all tables and destroys the stop if it is no longer a part of any line.
    public void RemoveFromLine(int id)
    {
        Lines.Remove(id);

        next.Remove(id);
        if(next.Count == 0)
        {
            Destroy(gameObject);
        }

        previous.Remove(id);
        nextPath.Remove(id);
    }

    /// <summary>
    /// Returns true if a bus can't currently be spawned at the station, false otherwise.
    /// </summary>
    public bool IsOccupied()
    {
        return occupied;
    }

    /// <summary>
    /// Returns a list of people that want to board a given line.
    /// </summary>
    public List<Person> GetPeopleForLine(ILine line)
    {
        List<Person> result = new List<Person>();

        foreach (Person p in peopleAtStop)
        {
            if (p.PathLines != null && p.PathLines.Count != 0 && p.PathLines.Peek() == line)
            {
                result.Add(p);
            }
        }

        return result;
    }

    public Waypoint GetAdjacentWaypoint()
    {
        return adjacentWaypoint;
    }

    public Vector3 GetPosition()
    {
        return transform.position;
    }

    public void AddBusInstanceToSpawn(BusInstanceManager.BusInstance instance)
    {        
        instancesToSpawn.Enqueue(instance);
        if (instancesToSpawn.Count == 1)
        {
            StartCoroutine(SpawnBuses());
        }
    }

    internal IEnumerator SpawnBuses()
    {
        spawnBusesCC.isRunning = true;

        // The bus that was waiting to be spawned at the beginning of the previous iteration.
        BusInstanceManager.BusInstance prev = null;

        while (true)
        {
            // Block the entry waypoint in 'timeBeforeBlocking' seconds. The condition ensures that
            // the Invoke only gets called once per bus.
            if (instancesToSpawn.Count != 0 && prev != instancesToSpawn.Peek() && !blockedEntrance)
            {
                Invoke("BlockEntrance", timeBeforeBlocking);
            }

            prev = instancesToSpawn.Peek();

            // If there are no more buses to spawn, finish the coroutine.
            if (instancesToSpawn.Count == 0)
            {
                if (blockedEntrance)
                {
                    entranceWaypoint.ChangeOccupied(false);
                    blockedEntrance = false;
                }
                break;
            }

            // If the stop isn't occupied or the last car to enter its trigger collider is stopped, spawn a bus.
            if (!occupied || (blockedEntrance && vehiclesInTrigger.Count == 1 && lastToEnter.IsStopping()))
            {
                SpawnBus(instancesToSpawn.Dequeue());

                // If the entrance hasn't been blocked yet, cancel the Invoke of BlockEntrance.
                if (!blockedEntrance)
                {
                    CancelInvoke("BlockEntrance");
                }

                // The condition for finishing the coroutine is included here too, because otherwise
                // it could happen that the player would spawn an additional bus during the 0.05s window,
                // which could cause two instances of the coroutine to run simultaneously.
                if (instancesToSpawn.Count == 0)
                {
                    if (blockedEntrance)
                    {
                        entranceWaypoint.ChangeOccupied(false);
                        blockedEntrance = false;
                    }
                    break;
                }
            }

            yield return new WaitForSeconds(0.05f);
        }

        spawnBusesCC.isRunning = false;
    }

    /// <summary>
    /// Spawns a bus at this stop.
    /// </summary>
    /// <param name="instance"> The bus instance that the spawned bus is supposed to represent. </param>
    private void SpawnBus(BusInstanceManager.BusInstance instance)
    {
        Bus bus;

        if (instance.bus == null)
        {
            // If the bus is null, spawn a new one and set it up
            bus = Instantiate(
                Resources.Load<GameObject>(instance.type.path),
                adjacentWaypoint.transform.position,
                Quaternion.Euler(transform.rotation.eulerAngles - new Vector3(0, 180, 0)),
                CarCreator.instance.transform
            ).GetComponent<Bus>();
            instance.bus = bus;
            bus.instance = instance;

            CarNavigation.allCars.Add(bus);
        }
        else
        {
            // If the bus already exists, only activate it and move it to position
            bus = instance.bus;
            bus.gameObject.SetActive(true);
            bus.MoveToLocationWithoutBreaking(
                adjacentWaypoint.transform.position,
                Quaternion.Euler(transform.rotation.eulerAngles - new Vector3(0, 180, 0))
            );
        }

        bus.SetStartingWaypoint(adjacentWaypoint);
        bus.SendOut(this);

        // If the bus window is active, redraw it
        BusInstanceWindowManager busWindow = BusInstanceWindowManager.Instance;
        if (busWindow.gameObject.activeSelf)
        {
            busWindow.RepopulateScrollViewWithBuses();
        } 
    }

    /// <summary>
    /// Blocks the entrance waypoint so that buses can be spawned.
    /// </summary>
    private void BlockEntrance()
    {
        if (instancesToSpawn.Count != 0)
        {
            entranceWaypoint.ChangeOccupied(true);
            blockedEntrance = true;
        }
    }

    public void RemovePerson(Person p)
    {
        peopleAtStop.Remove(p);
    }

    /// <summary>
    /// Returns all of the other stops on the map.
    /// </summary>
    public List<IStop> Neighbours()
    {
        HashSet<IStop> result = new HashSet<IStop>();

        foreach (ILine line in SubwayLineManager.GetInstance().GetLines())
        {
            if (line.Closed)
            {
                result.UnionWith(line.Path);
            }
        }

        foreach (ILine line in BusLineManager.GetInstance().GetLines())
        {
            if (line.Closed)
            {
                result.UnionWith(line.Path);
            }
        }

        result.Remove(this);

        return result.ToList();
    }

    /// <summary>
    /// Returns the position of the point at which the stop can be entered.
    /// </summary>
    public Vector3 EntryPoint()
    {
        return transform.position;
    }

    public bool Exists()
    {
        return this != null;
    }
}
