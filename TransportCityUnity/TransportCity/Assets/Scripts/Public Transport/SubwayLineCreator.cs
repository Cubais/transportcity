﻿using UnityEngine;
using UnityEngine.EventSystems;
using static Constants;

public class SubwayLineCreator : MonoBehaviour
{
    private static SubwayLineCreator instance;

    // A reference to the global EvenSystem object in order to detect when the mouse is over the UI.
    public EventSystem eventSystem;

    public BuildingManager buildingManager;

    public SubwayLineManager lineManager;

    private bool buildingModeActive = false;

    // A prefab of the subway stop outline.
    [SerializeField]
    private GameObject outline;

    // An instance of the subway stop outline.
    private SubwayStopOutline outlineInstance;

    // The building block that the player was previously pointing at. Used to detect when the
    // player starts pointing at a different block.
    private Collider prevBlock;

    // The layermask used when raycasting in the building mode (it ignores buildings and the Ignore Raycast layer).
    private LayerMask lm;

    // The subway line that is currently being built.
    private SubwayLineManager.SubwayLine currentLine;

    // A line renderer used to draw the tunnels.
    private LineRenderer lr;

    // The amount by which the y coordinate of the positions of the line renderer is to be shifted.
    internal Vector3 lineRendererYShift = new Vector3(0, -1, 0);

    // The position at which the outline is placed when it isn't supposed to be seen (destroying or deactivating it would be problematic).
    private Vector3 defaultOutlineInstancePosition = new Vector3(-100, -100, -100);

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        if (outline == null)
        {
            outline = Resources.Load<GameObject>("Prefabs/Buildings/SubwayStopOutline");
        }

        lm = 1 << 8;
        lm += 1 << 2;
        lm = ~lm;

        // Register subway line edit handlers in the UI API
        UI_API uiApi = UI_API.GetInstance();
        uiApi.RegisterForSubwayPathEditButtons(Activate);
        uiApi.RegisterForSubwayPathEditEndButton(Deactivate);
    }

    private void Update()
    {
        // Allows the player to place stops on building blocks by left-clicking them
        // and to destroy stops by right-clicking them.
        if (buildingModeActive)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Input.GetMouseButtonDown(1))
            {
                if (Physics.Raycast(ray, out hit, Mathf.Infinity))
                {
                    SubwayStop clickedStop = hit.collider.GetComponent<SubwayStop>();
                    if (clickedStop && currentLine.Path.Contains(clickedStop) && currentLine.Vehicles.Count == 0)
                    {
                        DiscardStop(clickedStop);
                    }
                }
            }
            
            // Change the collider's position if the player starts pointing at a different building block.
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, lm) && hit.collider.CompareTag(blockTag) && hit.collider != prevBlock)
            {
                Vector3 newPosition = GetCentreCoordinates(buildingManager.WorldToMapCoordinates(hit.collider.transform.position));
                newPosition.y = buildingHeight / 2f + buildingManager.generator.cityBaseHeight;

                outlineInstance.transform.position = newPosition;

                if (lr != null)
                {
                    lr.SetPosition(lr.positionCount - 1, newPosition + lineRendererYShift);
                }

                prevBlock = hit.collider;
            }

            // Tries to create a subway stop if the player left-clicks while the outline instance is placed over some building blocks.
            // Also checks if the mouse is over the UI -> blocks the click.
            if (Input.GetMouseButtonDown(0) && outlineInstance.centre != new Vector2Int(-1, -1) && !eventSystem.IsPointerOverGameObject(-1))
            {
                Vector2Int coordinates = outlineInstance.centre;

                // If the outline is currently colliding with only a single building which is a subway stop,
                // either add it to the current line (if it isn't already present), or do nothing.
                if (outlineInstance.GetState() == SubwayOutlineState.CollidingWithOneStop)
                {
                    SubwayStop stopToAdd = null;
                    for (int i = 1; i >= -1; i--)
                    {
                        for (int j = -1; j <= 1; j++)
                        {
                            if (buildingManager.blockMap[coordinates.x + i, coordinates.y + j].State == BlockState.Taken)
                            {
                                stopToAdd = buildingManager.buildingMap[coordinates.x + i, coordinates.y + j].GetComponent<SubwayStop>();
                            }
                        }
                    }

                    if (!currentLine.Path.Contains(stopToAdd))
                    {
                        currentLine.AddStop(stopToAdd);
                        if (lr != null)
                        {
                            stopToAdd.lineRenderers.Add(currentLine.Id, lr);
                        }
                        InstantiateLineRenderer(stopToAdd.transform.position);
                        lr.positionCount++;
                        lr.SetPosition(lr.positionCount - 1, lr.GetPosition(lr.positionCount - 2));

                        outlineInstance.transform.position = defaultOutlineInstancePosition;
                    }
                }
                // Otherwise, if the outline isn't colliding with any stops, create a stop
                // (which destroys any buildings that it is overlapping with).
                else if (outlineInstance.GetState() != SubwayOutlineState.CollidingWithBuildingsAndStops)
                {                   
                    buildingManager.AddBuilding(coordinates.x + 1, coordinates.y - 1, "SubwayStop");
                    SubwayStop stop = buildingManager.buildingMap[coordinates.x, coordinates.y].GetComponent<SubwayStop>();
                    currentLine.AddStop(stop);
                    if (lr != null)
                    {
                        stop.lineRenderers.Add(currentLine.Id, lr);
                    }
                    InstantiateLineRenderer(stop.transform.position);
                    lr.positionCount++;
                    lr.SetPosition(lr.positionCount - 1, lr.GetPosition(lr.positionCount - 2));

                    outlineInstance.transform.position = defaultOutlineInstancePosition;
                }
            }
        }        
    }

    public static SubwayLineCreator GetInstance() => instance;

    /// <summary>
    /// Starts subway line construction
    /// </summary>
    /// <param name="subwayLine">The subway line being built</param>
    public void Activate(SubwayLineManager.SubwayLine subwayLine)
    {
        currentLine = subwayLine;
        ToggleBuildingMode(true);
    }

    /// <summary>
    /// Ends the subway line building
    /// </summary>
    public void Deactivate()
    {
        ToggleBuildingMode(false);
    }

    /// <summary>
    /// Activates/deactivates building mode, in which a player can create stops by left-clicking.
    /// </summary>
    /// <param name="onOrOff"> true <=> turn on </param>
    private void ToggleBuildingMode(bool onOrOff)
    {
        if (buildingModeActive != onOrOff)
        {
            if (onOrOff)
            {
                outlineInstance = Instantiate(outline, defaultOutlineInstancePosition, Quaternion.identity).GetComponent<SubwayStopOutline>();
                outlineInstance.currentLine = currentLine;

                if (currentLine.Path.Count != 0)
                {
                    InstantiateLineRenderer(currentLine.Path[currentLine.Path.Count - 1].GetPosition());
                    lr.positionCount++;
                    lr.SetPosition(lr.positionCount - 1, currentLine.Path[currentLine.Path.Count - 1].GetPosition());
                }
            }
            else
            {
                if (lr != null)
                {
                    Destroy(lr.gameObject);
                }
                Destroy(outlineInstance.gameObject);

                currentLine.ToggleVisibility(false, false);                
                currentLine = null;
            }
        }

        buildingModeActive = onOrOff;
    }

    /// <summary>
    /// Gets the world coordinates of the centre of a building with dimensions 3x3, given a building block's position.
    /// It does this by searching for blocks that neighbour a road (since every building has to neighbour a road).
    /// </summary>
    private Vector3 GetCentreCoordinates(Vector2Int blockPos)
    {
        BlockData bd = buildingManager.blockMap[blockPos.x, blockPos.y];

        // If the block is already near a road, then the centre is one block away in one or both directions.
        if (bd != null && bd.IsNearARoad())
        {
            int x = blockPos.x;
            int y = blockPos.y;

            if (bd.IsThereNeighboringRoad[(int)CardinalDirection.North])
            {
                y -= 1;
            }
            if (bd.IsThereNeighboringRoad[(int)CardinalDirection.East])
            {
                x -= 1;
            }
            if (bd.IsThereNeighboringRoad[(int)CardinalDirection.South])
            {
                y += 1;
            }
            if (bd.IsThereNeighboringRoad[(int)CardinalDirection.West])
            {
                x += 1;
            }

            outlineInstance.centre = new Vector2Int(x, y);
            return buildingManager.blockMap[x, y].WorldPosition;
        }
        else
        {
            // Look for a bulding block that is next to a road and is one block away from the given block in one or both directions.
            // In such a case, the block that was given is the centre.
            for (int i = -1; i <= 1; i++)
            {
                for (int j = -1; j <= 1; j++)
                {
                    if (blockPos.x + i >= 0 &&
                        blockPos.y + j >= 0 &&
                        blockPos.x + i < buildingManager.blockMap.GetLength(0) &&
                        blockPos.y + j < buildingManager.blockMap.GetLength(1))
                    {
                        BlockData bd2 = buildingManager.blockMap[blockPos.x + i, blockPos.y + j];
                        if (bd2 != null && bd2.IsNearARoad())
                        {
                            outlineInstance.centre = blockPos;
                            return bd.WorldPosition;
                        }
                    }
                }
            }

            // Look for a bulding block that is next to a road and is two blocks away from the given block in one or both directions.
            // In such a case, the centre is one block away from the given block in one or both directions.
            for (int i = -2; i <= 2; i += 2)
            {
                for (int j = -2; j <= 2; j += 2)
                {
                    if (blockPos.x + i >= 0 &&
                        blockPos.y + j >= 0 &&
                        blockPos.x + i < buildingManager.blockMap.GetLength(0)
                        && blockPos.y + j < buildingManager.blockMap.GetLength(1))
                    {
                        BlockData bd2 = buildingManager.blockMap[blockPos.x + i, blockPos.y + j];
                        if (bd2 != null && bd2.IsNearARoad())
                        {
                            outlineInstance.centre = new Vector2Int(blockPos.x + i / 2, blockPos.y + j / 2);
                            return buildingManager.blockMap[blockPos.x + i / 2, blockPos.y + j / 2].WorldPosition;
                        }
                    }
                }
            }
        }

        return outlineInstance.transform.position;
    }

    private void InstantiateLineRenderer(Vector3 firstPosition)
    {
        lr = Instantiate(Resources.Load<LineRenderer>(lineRendererPath), outlineInstance.transform.position, Quaternion.identity);
        lr.positionCount = 1;
        lr.startWidth = 4;
        lr.endWidth = 4;
        lr.SetPosition(lr.positionCount - 1, firstPosition - lineRendererYShift);
    }

    private void DiscardStop(SubwayStop stop)
    {
        if (stop.lineRenderers.ContainsKey(currentLine.Id))
        {
            Destroy(stop.lineRenderers[currentLine.Id].gameObject);
        }

        // Adjust the currently used line renderer if the last stop was deleted.
        if ((IStop)stop == currentLine.Path[currentLine.Path.Count - 1] && currentLine.Path.Count >= 2)
        {
            Destroy(lr.gameObject);
            InstantiateLineRenderer(currentLine.Path[currentLine.Path.Count - 2].GetPosition());

            lr.positionCount++;
            if (outlineInstance.transform.position != defaultOutlineInstancePosition)
            {                
                lr.SetPosition(1, outlineInstance.transform.position + lineRendererYShift);
            }
            else
            {
                lr.SetPosition(lr.positionCount - 1, lr.GetPosition(0) - lineRendererYShift);
            }
        }

        // Delete the second stop's line renderer if the first stop is being deleted.
        if ((IStop)stop == currentLine.Path[0] && currentLine.Path.Count >= 2)
        {
            Destroy(((SubwayStop)currentLine.Path[1]).lineRenderers[currentLine.Id].gameObject);
            ((SubwayStop)currentLine.Path[1]).lineRenderers.Remove(currentLine.Id);
        }

        if (currentLine.Path.Count == 1)
        {
            Destroy(lr.gameObject);
            lr = null;
        }

        currentLine.RemoveStop(stop);

        if (stop.IsDisconnected())
        {
            buildingManager.DemolishBuilding(stop.gameObject);
        }        
    }
}
