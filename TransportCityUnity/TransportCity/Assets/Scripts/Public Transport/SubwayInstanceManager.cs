﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubwayInstanceManager : MonoBehaviour
{
    public class SubwayTrainInstance
    {
        /// <summary>
        /// The ID of the train
        /// </summary>
        public int id;

        /// <summary>
        /// The name of the train
        /// </summary>
        public string name;

        /// <summary>
        /// The type of the train
        /// </summary>
        public object type;

        /// <summary>
        /// The script of the train prefab instance associated with this SubwayTrainInstance
        /// </summary>
        public SubwayTrain train;

        /// <summary>
        /// The line to which the train is assigned.
        /// </summary>
        public SubwayLineManager.SubwayLine line;

        /// <summary>
        /// Unassign this subway train instance from its SubwayLine
        /// </summary>
        public void UnassignFromLine()
        {
            if (line == null || train == null)
            {
                return;
            }

            train.KickEveryone();

            line.Vehicles.Remove(train);
            line = null;

            train.MoveToLocationWithoutBreaking(
                new Vector3(10000, 10000, 10000),
                Quaternion.Euler(new Vector3(0, 0, 0))
            );

            train.gameObject.SetActive(false);
        }
    }

    // A List of all subway train instances
    internal List<SubwayTrainInstance> trains;

    internal int nextTrainIndex = 0;

    // Singleton
    private static SubwayInstanceManager instance;

    /// <summary>
    /// Number of bought trains
    /// </summary>
    public int TrainCount => trains.Count;

    void Awake()
    {
        trains = new List<SubwayTrainInstance>();
        instance = this;
    }

    /// <summary>
    /// Get singleton instance
    /// </summary>
    /// <returns>Singleton instance of this class</returns>
    public static SubwayInstanceManager GetInstance()
    {
        return instance;
    }

    /// <summary>
    /// Get all trains
    /// </summary>
    /// <returns>A List of all trains</returns>
    public List<SubwayTrainInstance> GetSubwayTrainInstances()
    {
        return trains;
    }

    /// <summary>
    /// Create a new train instance and return it
    /// </summary>
    /// <returns>The newly created SubwayTrainInstance object</returns>
    public SubwayTrainInstance CreateNewSubwayTrainInstance()
    {
        SubwayTrainInstance newTrain = new SubwayTrainInstance();

        newTrain.id = nextTrainIndex++;
        newTrain.name = "Train" + newTrain.id;
        newTrain.type = null;
        newTrain.train = null;

        trains.Add(newTrain);
        return newTrain;
    }

    /// <summary>
    /// Delete a train instance
    /// </summary>
    /// <param name="instance">The SubwayTrainInstance to be deleted</param>
    public void DeleteTrainInstance(SubwayTrainInstance instance)
    {
        trains.Remove(instance);

        if (instance.train != null)
        {
            if (instance.line != null)
            {
                instance.line.Vehicles.Remove(instance.train);
            }

            Destroy(instance.train.gameObject);
        }
    }
}
