﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using static Constants;

public class BusLineCreator : MonoBehaviour
{
    // A reference to the global EvenSystem object in order to detect when the mouse is over the UI.
    public EventSystem eventSystem;

    // The alpha value of the colour of the finished part of a line.
    public float finishedGradientAlpha = 1f;

    // The alpha value of the colour of the line from the last stop to the outline.
    public float toOutlineradientAlpha = 0.5f;

    // Stores the prefab of the outline of the bus stop.
    private GameObject outline;

    // Stores the actual instance of the outline of the bus stop.
    private GameObject outlineInstance;

    // Stores the prefab of the bus stop.
    private GameObject stop;

    // Stores the main camera.
    private new Camera camera;

    // While this boolean is true, the player can build bus stops.
    private bool activator;

    // The previous collider that was hit by raycasting.
    // Used to detect when the player moves the mouse to another road segment / another side of a road segment.
    private Collider lastCollider;

    // The line that is currently being edited.
    private BusLineManager.BusLine currentLine;

    // The path to the current position of the outline instance.
    private Stack<Direction> currentPath;

    // The amount by which the drawn bus line should be higher than the ground.
    internal Vector3 drawnLineYOffset = new Vector3(0, 3.2f, 0);

    // The currently used line renderer (the one which belongs to the currently last stop).
    private LineRenderer lr;

    // The colour gradient of the finished part of the bus line.
    private Gradient finishedPathGradient;

    // The colour gradient of the line from the last stop to the outline.
    private Gradient gradientToOutline;

    // The layermask used when placing bus stops (it ignores buildings and the Ignore Raycast layer).
    private LayerMask lm;

    private static BusLineCreator instance;

    private void Awake()
    {
        instance = this;

        lm = 1 << 8;
        lm += 1 << 2;
        lm = ~lm;
    }

    private void Start()
    {
        outline = Resources.Load<GameObject>("Prefabs/Stops/Stop_Outline");
        stop = Resources.Load<GameObject>("Prefabs/Stops/BusStop");
        camera = Camera.main;

        // Registers the appropriate UI API handlers.
        UI_API uiApi = UI_API.GetInstance();
        uiApi.RegisterForBusPathEditButtons(Activate);
        uiApi.RegisterForBusPathEditEndButton(Deactivate);
    }

    private void Update()
    {
        // By pushing the left muse button, the player can build a new stop or connect an existing one to the line.
        // Also checks if the mouse is over the UI, in which case, the code below isn't executed.
        if (Input.GetMouseButtonDown(0) && outlineInstance && !eventSystem.IsPointerOverGameObject(-1))
        {
            BusStopOutline outlineComponent = outlineInstance.GetComponent<BusStopOutline>();

            // If the outline is over a single bus stop.
            if (outlineComponent.triggerEnterCount == 1)
            {
                BusStop stop = outlineComponent.intersectingStopCollider.GetComponent<BusStop>();

                // If the stop isn't contained in the current line, then it is to be added to it.
                if (!currentLine.Path.Contains(stop))
                {
                    if (currentLine.GetLastStop())
                    {
                        stop.AddStop(currentLine.GetLastStop(), currentLine.Id, false);
                    }
                    currentLine.Path.Add(stop);
                    Destroy(outlineInstance);
                    currentPath = null;

                    if (lr != null)
                    {
                        lr.colorGradient = finishedPathGradient;
                    }
                    lr = Instantiate(Resources.Load<LineRenderer>(lineRendererPath), stop.transform);
                    stop.lineRenderers.Add(currentLine.Id, lr);
                    lr.colorGradient = gradientToOutline;
                }
                // Otherwise, if the player is pointing at the first stop in the line, this closes the loop
                // and disables further adding of stops (given that there is more than one stop in the line already).
                // The stops can still be removed though.
                else if((BusStop)currentLine.Path[0] == stop && currentLine.Path.Count != 1)
                {
                    stop.AddStop(currentLine.GetLastStop(), currentLine.Id, false);
                    Destroy(outlineInstance);
                    currentPath = null;
                    currentLine.Closed = true;

                    if (lr != null)
                    {
                        lr.colorGradient = finishedPathGradient;
                    }
                    lr = null;
                }
            }
            // If the outline is over a road, create a new stop on that spot.
            else if (outlineComponent.triggerEnterCount == 0)
            {
                GameObject newStopObj = Instantiate(
                    stop,
                    outlineInstance.transform.position,
                    outlineInstance.transform.rotation
                );
                BusStop newStop = newStopObj.GetComponent<BusStop>();
                newStop.adjacentWaypoint = GetAdjacentWP(lastCollider);
                newStop.entranceWaypoint = GetEntranceWaypoint(lastCollider);
                newStop.AddStop(currentLine.GetLastStop(), currentLine.Id, false);
                currentLine.Path.Add(newStop);
                Destroy(outlineInstance);
                currentPath = null;

                if (lr != null)
                {
                    lr.colorGradient = finishedPathGradient;
                }
                lr = Instantiate(Resources.Load<LineRenderer>(lineRendererPath), newStop.transform);
                newStop.lineRenderers.Add(currentLine.Id, lr);
                lr.colorGradient = gradientToOutline;

                newStop.GetComponentInChildren<SpriteRenderer>().color = currentLine.Color;
                newStop.transform.Find("Sprite").transform.position = newStop.transform.Find("Sprite").transform.position + drawnLineYOffset;
            }
        }

        // If the player clicks the right mouse button, he/she can remove a stop from a bus line.
        // Also checks if the mouse is over the UI, in which case, the code below isn't executed.
        if (Input.GetMouseButtonUp(1) && activator && !eventSystem.IsPointerOverGameObject(-2))
        {
            Ray r = camera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(r, out hit, Mathf.Infinity, lm) && hit.collider.CompareTag(busStopTag))
            {
                BusStop stop = hit.collider.GetComponent<BusStop>();

                // If the stop that the player is pointing at is contained in the current line
                // (and the either isn't closed and has no vehicles or has at least 3 stops), remove it.
                if (currentLine.Path.Contains(stop) && currentLine.Vehicles.Count == 0 && (!currentLine.Closed || currentLine.Path.Count >= 3))
                {
                    DiscardStop(stop);
                }
            }
        }

        // The logic of the bus stop building mode.
        if (activator && !currentLine.Closed)
        {
            // If the mouse is over the UI, don't update the outline.
            if (eventSystem.IsPointerOverGameObject())
            {
                return;
            }

            // Use raycasting to determine what the player is pointing at.
            Ray r = camera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(r, out hit, Mathf.Infinity, lm))
            {
                // If the player is pointing at an IRoad.
                if (hit.collider.CompareTag(roadTag) && hit.transform.GetComponent<RoadSegment>()?.type == SegmentType.IRoad)
                {
                    // If there is no outline instance, create one.
                    if (!outlineInstance)
                    {
                        outlineInstance = Instantiate(outline, hit.transform);
                        outlineInstance.GetComponent<BusStopOutline>().path = currentLine.Path;
                        ChangePosition(hit.transform, hit.transform.GetComponent<RoadSegment>().GetColliderSide(hit.collider));
                        currentPath = GetPath(hit.collider);
                        TogglePathVisibility(true, hit.collider);
                    }
                    // Otherwise check whether the player has started poiting to a different collider and if so,
                    // adjust the position of the outline instance accordingly.
                    else
                    {
                        if (hit.collider != lastCollider)
                        {
                            outlineInstance.SetActive(true);
                            TogglePathVisibility(false, lastCollider);
                            ChangePosition(hit.transform, hit.transform.GetComponent<RoadSegment>().GetColliderSide(hit.collider));
                            currentPath = GetPath(hit.collider);
                            TogglePathVisibility(true, hit.collider);
                        }
                    }

                    lastCollider = hit.collider;
                }
                // If the player is pointing at a bus stop.
                else if (hit.collider.CompareTag(busStopTag))
                {
                    // Fetch the collider of the road segment on which the stop is placed.
                    Collider roadSideCollider;
                    Transform stopWaypoint = hit.collider.GetComponent<BusStop>().adjacentWaypoint.transform;
                    Transform roadSegment = stopWaypoint.parent.parent;
                    if (stopWaypoint == roadSegment.GetChild(0).GetChild(1))
                    {
                        roadSideCollider = roadSegment.GetComponent<RoadSegment>().left;
                    }
                    else
                    {
                        roadSideCollider = roadSegment.GetComponent<RoadSegment>().right;
                    }

                    if (outlineInstance && roadSideCollider != lastCollider)
                    {
                        // If the player is pointing at a stop that isn't a part of the current bus line or that is the first stop
                        // in the current line (and the current line has at least two stops), the outline should be
                        // placed on the same position as that stop (indicating that the stop can be added).
                        if (!currentLine.Path.Contains(hit.collider.GetComponent<BusStop>()) || 
                            (hit.collider.GetComponent<IStop>() == currentLine.Path[0]) && currentLine.Path.Count >= 2)
                        {
                            TogglePathVisibility(false, lastCollider);
                            ChangePosition(
                                roadSideCollider.transform,
                                roadSegment.GetComponent<RoadSegment>().GetColliderSide(roadSideCollider)
                            );
                            currentPath = GetPath(roadSideCollider);
                            TogglePathVisibility(true, roadSideCollider);

                            lastCollider = roadSideCollider;
                        }
                    }
                }
            }
        }
        // When the bus stop building mode is deactivated, get rid of the outline instance.
        else
        {
            if (outlineInstance)
            {
                Destroy(outlineInstance);
            }
        }

        // If the player holds down ctrl and presses z (and is currently editing a closed line), the line will be re-opened.
        if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.Z) && currentLine != null)
        {
            if (currentLine.Closed && currentLine.Vehicles.Count == 0)
            {
                BusStop lastStop = currentLine.GetLastStop();
                lastStop.RemoveStop(currentLine.Id, true);
                currentLine.Closed = false;

                lr = lastStop.lineRenderers[currentLine.Id];
                lr.colorGradient = gradientToOutline;
                lr.positionCount = 0;                
            }
        }
    }

    public static BusLineCreator GetInstance()
    {
        return instance;
    }

    /// <summary>
    /// Changes the position of the outline to be on the appropriate side of a road.
    /// </summary>
    /// <param name="transformToTurnAround">The Transform component of the road.</param>
    /// <param name="side">The side on which the road is supposed to be.</param>
    private void ChangePosition(Transform transformToTurnAround, bool side)
    {
        // Copy both the position and rotation from the prefab and adjust them based on whether
        // the outline is supposed to be on the left or the right side of the road.

        int xPosSign = 1;
        int yAngleAdjustment = 0;
        if (side)
        {
            xPosSign = -1;
            yAngleAdjustment = 180;
        }

        outlineInstance.transform.parent = transformToTurnAround;

        Vector3 pos = outline.transform.position;
        outlineInstance.transform.localPosition = new Vector3(xPosSign * pos.x, pos.y, pos.z);

        Vector3 rot = outline.transform.eulerAngles;
        outlineInstance.transform.localEulerAngles = new Vector3(rot.x, yAngleAdjustment + rot.y, rot.z);

        outlineInstance.transform.parent = null;
    }

    /// <summary>
    /// Activates bus line editing
    /// </summary>
    /// <param name="line"> The bus line that is to be editied. </param>
    public void Activate(BusLineManager.BusLine line)
    {
        if (!activator)
        {
            // If the bus lines are being displayed, disable them.
            BusLineManager.GetInstance().ToggleAllLines(false);

            currentLine = line;

            // Set up the gradients based on the colour of the current line.
            finishedPathGradient = currentLine.gradient;

            GradientAlphaKey[] gak2 = new GradientAlphaKey[1];

            gak2[0].alpha = toOutlineradientAlpha;
            gak2[0].time = 1.0f;

            gradientToOutline = new Gradient();
            gradientToOutline.SetKeys(finishedPathGradient.colorKeys, gak2);
            
            activator = true;
            ToggleWholePathVisibility(true);
        }
    }

    /// <summary>
    /// Deactivates bus line editing.
    /// </summary>
    public void Deactivate()
    {
        if (activator)
        {
            activator = false;
            ToggleWholePathVisibility(false);

            // If there exists a path from the last stop to a bus stop outline, stop showing it.
            if (currentPath != null)
            {
                TogglePathVisibility(false, lastCollider);
            }

            currentPath = null;
            currentLine = null;
        }
    }

    /// <summary>
    /// Gets a path from the last stop of the current line (if there is one) to the position of the bus stop outline.
    /// </summary>
    /// <param name="adjacentCollider"> The road segment collider adjacent to the outline's current position. </param>
    /// <returns> The requested path or null. </returns>
    private Stack<Direction> GetPath(Collider adjacentCollider)
    {
        // If the current line doesn't have a last stop, it has no stops at all, so no path is fetched.
        if(currentLine.GetLastStop() == null)
        {
            return null;
        }
        return PathManager.GetInstance().GetPath(
            currentLine.GetLastStop().adjacentWaypoint,
            GetAdjacentWP(adjacentCollider)
        );
    }

    /// <summary>
    /// Fetches the waypoint adjacent to a bus stop colldier or road segment collider.
    /// </summary>
    /// <param name="adjacentCollider"> The collider next to which the waypoint is supposed to be. </param>
    /// <returns> The requested waypoint. </returns>
    private Waypoint GetAdjacentWP(Collider adjacentCollider)
    {
        // If 'adjacentCollider' is attached to a bus stop, just return its 'adjacentWaypoint' variable.
        if (adjacentCollider.GetComponent<BusStop>() != null)
        {
            return adjacentCollider.GetComponent<BusStop>().adjacentWaypoint;
        }

        // Otherwise it should be atached to a road segment, in which case the script checks whether
        // the collider is the right or left collider and returns a waypoint base on that.
        // (Note that this bit of code depends on the specific structure of the IRoad model!!!)
        if (adjacentCollider == adjacentCollider.GetComponent<RoadSegment>().left)
        {
            return adjacentCollider.transform.GetChild(0).GetChild(1).GetComponent<Waypoint>();
        }
        else
        {
            return adjacentCollider.transform.GetChild(0).GetChild(4).GetComponent<Waypoint>();
        }
    }

    /// <summary>
    /// Gets the entrance waypoint on the given side of a road segment.
    /// (The first waypoint a vehicle encounters when it enters the segment.)
    /// </summary>
    /// <param name="adjacentCollider"> The collider next to which the waypoint is supposed to be. </param>
    private Waypoint GetEntranceWaypoint(Collider adjacentCollider)
    {
        if (adjacentCollider == adjacentCollider.GetComponent<RoadSegment>().left)
        {
            return adjacentCollider.transform.GetChild(0).GetChild(0).GetComponent<Waypoint>();
        }
        else
        {
            return adjacentCollider.transform.GetChild(0).GetChild(3).GetComponent<Waypoint>();
        }
    }

    /// <summary>
    /// Enables/disables the visualization of the path.
    /// </summary>
    /// <param name="drawPath"> Determines whether the path is to be drawn or deleted. </param>
    /// <param name="endCollider"> The collider that is at the end of the path that is to be drawn/deleted. </param>
    private void TogglePathVisibility(bool drawPath, Collider endCollider)
    {
        if (currentLine.GetLastStop() == null)
        {
            return;
        }

        if (drawPath)
        {
            Waypoint start = currentLine.GetLastStop().adjacentWaypoint;
            Waypoint end = GetAdjacentWP(endCollider);
            DrawPath(start, end, PathManager.GetInstance().GetPath(start, end), lr);
        }
        else
        {
            lr.positionCount = 0;
        }
    }

    /// <summary>
    /// Draws/deletes the whole bus line.
    /// </summary>
    /// <param name="drawPath"> Determines whether the line is to be drawn or deleted. </param>
    public void ToggleWholePathVisibility(bool drawPath)
    {
        if (currentLine.Path == null || currentLine.Path.Count == 0)
        {
            return;
        }

        currentLine.ToggleLine(drawPath);
    }

    /// <summary>
    /// Removes the given stop from the current line.
    /// </summary>
    private void DiscardStop(BusStop stop)
    {
        IStop next = stop.GetNextStop(currentLine.Id);
        BusStop previous = (BusStop)stop.GetPreviousStop(currentLine.Id);

        // If the stop has a previous and a next stop, the path between them should be updated.
        if (next != null && previous != null)
        {
            // First discard the previous paths.
            previous.lineRenderers[currentLine.Id].positionCount = 0;
            Destroy(stop.lineRenderers[currentLine.Id]);
            stop.lineRenderers.Remove(currentLine.Id);

            // Replace the update the tables of the next and previous stops.
            previous.AddStop(next, currentLine.Id, true);

            // Draw the new path.
            DrawPath(
                previous.GetAdjacentWaypoint(),
                next.GetAdjacentWaypoint(),
                previous.GetNextPath(currentLine.Id),
                previous.lineRenderers[currentLine.Id]
            );
        }
        // If it only has a previous stop, then the path to it should stop being shown.
        else if (next == null && previous != null)
        {
            // Discard the previous paths.
            previous.lineRenderers[currentLine.Id].positionCount = 0;
            Destroy(stop.lineRenderers[currentLine.Id]);
            stop.lineRenderers.Remove(currentLine.Id);

            // Update the table of the previous stop.
            previous.RemoveStop(currentLine.Id, true);

            // Update the line renderer.
            lr = previous.lineRenderers[currentLine.Id];
            lr.colorGradient = gradientToOutline;
        }
        // If it only has a next stop, then the path to it should stop being shown.
        else if (next != null && previous == null)
        {
            // Discard the previous path.
            Destroy(stop.lineRenderers[currentLine.Id]);
            stop.lineRenderers.Remove(currentLine.Id);

            // Update the table of the next stop.
            next.RemoveStop(currentLine.Id, false);
        }

        stop.RemoveFromLine(currentLine.Id);
        currentLine.Path.Remove(stop);

        // If the stop only had a previous stop and there exists an outline, then the path to it has to be re-drawn.
        if (outlineInstance && next == null && previous != null)
        {
            TogglePathVisibility(true, lastCollider);
        }
    }

    /// <summary>
    /// Draws the given path from start to end using the given line renderer.
    /// </summary>
    public void DrawPath(Waypoint start, Waypoint end, Stack<Direction> path, LineRenderer lineRenderer)
    {
        Waypoint current = start;
        lineRenderer.positionCount = 1;
        lineRenderer.SetPosition(0, current.transform.position + drawnLineYOffset);

        while (current != end)
        {
            if (current is StraightWaypoint)
            {
                current = current.GetNextWaypoint();
            }
            else
            {
                current = current.GetNextWaypoint(path.Pop());
            }

            lineRenderer.positionCount++;
            lineRenderer.SetPosition(lineRenderer.positionCount - 1, current.transform.position + drawnLineYOffset);
        }
    }
}
