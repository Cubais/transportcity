﻿using System.Collections.Generic;
using UnityEngine;

public interface IStop
{
    /// <summary>
    /// A list of ids of lines that this stop is a part of.
    /// </summary>
    List<int> Lines { get; set; }

    /// <summary>
    /// Returns a waypoint closest to the stop.
    /// </summary>
    Waypoint GetAdjacentWaypoint();

    /// <summary>
    /// Returns the stop's position.
    /// </summary>
    Vector3 GetPosition();

    /// <summary>
    /// Adds a stop to the stop's collection of next stops or previous stops.
    /// </summary>
    /// <param name="stop"> The stop to be added. </param>
    /// <param name="id"> The id of the line to which it is to be added. </param>
    /// <param name="nextOrPrevious"> Determines whether the stop is to be treated as a next stop or a previous stop </param>
    void AddStop(IStop stop, int id, bool nextOrPrevious);

    IStop GetNextStop(int id);

    IStop GetPreviousStop(int id);

    /// <summary>
    /// Removes either the next or the previous stop relative to this stop in a given line.
    /// </summary>
    /// <param name="id"> The id of the given line. </param>
    /// <param name="nextOrPrevious"> Determines whether the next or previous stop should be removed. true <=> next </param>
    void RemoveStop(int id, bool nextOrPrevious);

    /// <summary>
    /// Returns a list of people that want to get on the specified line.
    /// </summary>
    List<Person> GetPeopleForLine(ILine line);

    /// <summary>
    /// Returns a list of the stops neighbours.
    /// </summary>
    List<IStop> Neighbours();

    /// <summary>
    /// Returns the position of the point at which the stop can be entered.
    /// </summary>
    Vector3 EntryPoint();

    /// <summary>
    /// Returns true if the stops gameobject hasn't been destroyed.
    /// </summary>
    bool Exists();
}
