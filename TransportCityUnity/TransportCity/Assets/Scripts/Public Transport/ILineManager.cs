﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ILineManager
{
    /// <summary>
    /// Returns a list of all the existing lines of the transport type that the manager is in charge of.
    /// </summary>
    List<ILine> GetLines();
}
