﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class SubwayStop : Building, IStop
{
    // The key is a subway line id and the value is the line renderer associated with that subway line.
    // It is necessary to have multiple line renderers in case all the subway lines are being shown.
    public Dictionary<int, LineRenderer> lineRenderers = new Dictionary<int, LineRenderer>();

    // The 2D sprite that is shown in the subway viewing mode.
    public SpriteRenderer sprite;

    // A list of ids of lines that this stop is a part of.
    public List<int> Lines { get; set; }

    // The keys in these dictionaries are ids of subway lines and the values are the next
    // and previous stops in those lines respectively.
    internal Dictionary<int, SubwayStop> nextStop = new Dictionary<int, SubwayStop>();
    internal Dictionary<int, SubwayStop> previousStop = new Dictionary<int, SubwayStop>();

    private void Awake()
    {
        Lines = new List<int>();
    }

    public IStop GetNextStop(int id)
    {
        if (nextStop.ContainsKey(id))
        {
            return nextStop[id];
        }

        return null;
    }

    public IStop GetPreviousStop(int id)
    {
        if (previousStop.ContainsKey(id))
        {
            return previousStop[id];
        }

        return null;
    }

    /// <summary>
    /// Returns the waypoint closest to this stop.
    /// </summary>
    public Waypoint GetAdjacentWaypoint()
    {
        return ClosestWaypoint.GetComponent<Waypoint>();
    }

    public Vector3 GetPosition()
    {
        return transform.position;
    }

    /// <summary>
    /// Adds a stop to 'next' and also a path leading to it to 'nextPath'.
    /// </summary>
    /// <param name="id"> The id of the line to which the stop is to be added. </param>
    /// <param name="stop"> The stop that is to be added. </param>
    public void AddNextStop(int id, SubwayStop stop)
    {
        if (!Lines.Contains(id))
        {
            Lines.Add(id);
        }

        nextStop.Remove(id);
        nextStop.Add(id, stop);
    }

    /// <summary>
    /// Adds a stop to 'previous'.
    /// </summary>
    /// <param name="id"> The id of the line to which the stop is to be added. </param>
    /// <param name="stop"> The stop that is to be added. </param>
    public void AddPreviousStop(int id, SubwayStop stop)
    {
        if (!Lines.Contains(id))
        {
            Lines.Add(id);
        }

        previousStop.Remove(id);
        previousStop.Add(id, stop);
    }

    /// <summary>
    /// Adds a stop to a given line. Also adds this stop to the tables of the stop that is being added.
    /// </summary>
    /// <param name="id"> The id to which the line is to be added. </param>
    /// <param name="stop"> The stop that is to be added. </param>
    /// <param name="nextOrPrevious"> A boolean indicating whether the stop is supposed to come before or after this stop. 
    /// (true <=> the stop is to be added to the 'next' dictionary) </param>
    public void AddStop(IStop stop, int id, bool nextOrPrevious)
    {
        if (nextOrPrevious)
        {
            AddNextStop(id, (SubwayStop)stop);
            if (stop != null)
            {
                ((SubwayStop)stop).AddPreviousStop(id, this);
            }
        }
        else
        {
            AddPreviousStop(id, (SubwayStop)stop);
            if (stop != null)
            {
                ((SubwayStop)stop).AddNextStop(id, this);
            }
        }
    }

    /// <summary>
    /// Removes a stop from the tables.
    /// </summary>
    /// <param name="id"> The id of the line from which the stop is to be discarded. </param>
    /// <param name="nextOrPrevious"> A bolean that indicates whether the stop is to discarded from the 'next' or 'previous' tables. 
    /// (true <=> the stop is to be removed from the 'next' dictionary) </param>
    public void RemoveStop(int id, bool nextOrPrevious)
    {
        if (nextOrPrevious)
        {
            nextStop.Remove(id);
        }
        else
        {
            previousStop.Remove(id);
        }
    }

    /// <summary>
    /// Return true if the stop has no next or previous stops in its nextStop and previousStop dictionaries.
    /// (Which means that it isn't connected t any line).
    /// </summary>
    public bool IsDisconnected()
    {
        return nextStop.Count == 0 && previousStop.Count == 0;
    }

    /// <summary>
    /// Returns a list of people that want to board a given line.
    /// </summary>
    public List<Person> GetPeopleForLine(ILine line)
    {
        List<Person> result = new List<Person>();

        foreach (Person p in peopleInBuilding)
        {
            if (p.PathLines!= null && p.PathLines.Count != 0 && p.PathLines.Peek() == line)
            {
                result.Add(p);
            }
        }

        return result;
    }

    public void RemovePerson(Person p)
    {
        peopleInBuilding.Remove(p);
    }

    /// <summary>
    /// Returns all the other stops on the map.
    /// </summary>
    public List<IStop> Neighbours()
    {
        HashSet<IStop> result = new HashSet<IStop>();

        foreach (ILine line in SubwayLineManager.GetInstance().GetLines())
        {
            if (line.Closed)
            {
                result.UnionWith(line.Path);
            }
        }

        foreach (ILine line in BusLineManager.GetInstance().GetLines())
        {
            if (line.Closed)
            {
                result.UnionWith(line.Path);
            }
        }

        result.Remove(this);

        return result.ToList();
    }

    /// <summary>
    /// Returns the position of the point at which the stop can be entered.
    /// </summary>
    public Vector3 EntryPoint()
    {
        return inFrontOfDoor.transform.position;
    }

    public bool Exists()
    {
        return this != null;
    }
}
