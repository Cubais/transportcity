﻿using UnityEngine;
using static Constants;

public class BusSchedule : ISchedule
{
    private int interval;
    private BusLineManager.BusLine line;

    public BusSchedule(BusLineManager.BusLine line)
    {
        this.line = line;
        Interval = 30;
    }

    public int Interval 
    {
        get => interval;
        set
        {
            interval = value;
            UpdateRequiredNumberOfVehicles();
        }
    }

    public int RequiredNumberOfVehicles { get; private set; }

    public void UpdateRequiredNumberOfVehicles()
    {
        float dist = Scheduler.GetInstance().GetApproximateLineDistance(line);
        RequiredNumberOfVehicles = (int)(Mathf.Ceil((dist / avgVehicleSpeed) / interval) * 0.75f);
        RequiredNumberOfVehicles = Mathf.Max(RequiredNumberOfVehicles, 1);
    }
}
