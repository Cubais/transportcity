﻿using System.Collections;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor.Build.Content;
#endif

using UnityEngine;

public class BusLineManager : MonoBehaviour, ILineManager
{
    /// <summary>
    /// A data class to hold information about one bus line
    /// </summary>
    public class BusLine : ILine
    {
        /// <summary>
        /// The id of the line
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The name of the line
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The Color associated with the line
        /// </summary>
        public Color Color { get; set; }

        /// <summary>
        /// A list of buses assigned to the line.
        /// </summary>
        public List<IVehicle> Vehicles { get; set; }

        /// <summary>
        /// The path of the line
        /// </summary>
        public List<IStop> Path { get; set; }

        /// <summary>
        /// The schedule of the line
        /// </summary>
        public ISchedule schedule;

        /// <summary>
        /// Indicates whether the line has been connected to its end, forming a cycle and closing the line.
        /// </summary>
        public bool Closed { get; set; }

        // The gradient for this line.
        public Gradient gradient;

        // Index of the stop at which the next bus is to be spawned.
        internal int nextStopIndex = 0;

        public BusLine()
        {
            Path = new List<IStop>();
            Vehicles = new List<IVehicle>();
        }

        /// <summary>
        /// Returns the last stop in the line.
        /// </summary>
        public BusStop GetLastStop()
        {
            if (Path.Count == 0)
            {
                return null;
            }

            return (BusStop)Path[Path.Count - 1];
        }

        /// <summary>
        /// Cycles through stops adding buses that are to be spawned there.
        /// </summary>
        public void AddBus(BusInstanceManager.BusInstance instance)
        {
            if (nextStopIndex >= Path.Count)
            {
                nextStopIndex = 0;
            }

            ((BusStop)Path[nextStopIndex]).AddBusInstanceToSpawn(instance);
            nextStopIndex = nextStopIndex - 1;
            if (nextStopIndex == -1)
            {
                nextStopIndex = Path.Count - 1;
            }
        }

        /// <summary>
        /// Either draws the line or makes it stop being shown.
        /// </summary>
        /// <param name="drawLine"></param>
        public void ToggleLine(bool drawLine)
        {
            if (Path.Count >= 1)
            {
                IStop currentStop = Path[0];
                IStop prev = null;

                // Cycle through all the stops in the line.
                while (currentStop.GetNextStop(Id) != null && prev != (IStop)GetLastStop())
                {
                    ((BusStop)currentStop).lineRenderers[Id].gameObject.SetActive(drawLine);

                    ((BusStop)currentStop).GetComponentInChildren<SpriteRenderer>().enabled = drawLine;

                    prev = currentStop;
                    currentStop = currentStop.GetNextStop(Id);
                }
            }
        }
    }

    // A list of all bus lines
    internal List<ILine> busLines;

    // The next index of a new line
    internal int nextLineIndex = 0;

    // Singleton.
    private static BusLineManager instance;

    // Determines whether the bus lines are currently being displayed.
    internal bool linesDisplayed = false;
    
    public int LinesCount => busLines.Count;

    private void Awake()
    {
        busLines = new List<ILine>();
        instance = this;
    }

    private void Start()
    {
        UI_API.GetInstance().RegisterForStatisticsButtons(ToggleAllLines);
    }

    public static BusLineManager GetInstance()
    {
        return instance;
    }

    /// <summary>
    /// Create a new bus line with a random Color and return it
    /// </summary>
    /// <returns>A new bus line with random Color</returns>
    public BusLine CreateNewBusLine()
    {
        BusLine busLine = new BusLine();
        busLine.Id = nextLineIndex++;
        busLine.Name = "BusLine" + busLine.Id;
        busLine.Color = new Color(Random.value, Random.value, Random.value);
        busLine.Vehicles = new List<IVehicle>();
        busLine.Path = new List<IStop>();
        busLine.Closed = false;
        busLine.schedule = new BusSchedule(busLine);
        busLine.gradient = GetGradientFromColor(busLine.Color);

        busLines.Add(busLine);
        return busLine;
    }

    /// <summary>
    /// Get all bus lines
    /// </summary>
    /// <returns>A List of all bus lines</returns>
    public List<ILine> GetLines()
    {
        return busLines;
    }

    /// <summary>
    /// Spawns a bus at the first stop of the specified bus line.
    /// </summary>
    /// <param name="busInstance">The bus instance to be spawned</param>
    /// <param name="busLine">The bus line to spawn the bus on</param>
    public void AddBusInstanceToBusLine(BusInstanceManager.BusInstance busInstance, BusLineManager.BusLine busLine)
    {
        // The bus line must be closed
        if (!busLine.Closed)
        {
            Debug.Log("The path of the bus line must be closed when you are assigning buses to it!");
            return;
        }

        busInstance.line = busLine;
        busLine.AddBus(busInstance);
    }

    /// <summary>
    /// Unassign a bus instance from its BusLine
    /// </summary>
    /// <param name="busInstance">The bus instance to unassign</param>
    public void UnassignBusInstance(BusInstanceManager.BusInstance busInstance)
    {
        BusLine line = busInstance.line;
        Bus bus = busInstance.bus;

        if (line == null || bus == null)
        {
            return;
        }

        bus.KickEveryone();

        line.Vehicles.Remove(bus);
        busInstance.line = null;

        bus.MoveToLocationWithoutBreaking(
            new Vector3(10000, 10000, 10000),
            Quaternion.Euler(new Vector3(0, 0, 0))
        );

        bus.gameObject.SetActive(false);
    }

    /// <summary>
    /// Displays or disables drawing all bus lines when a button with a specific index is clicked.
    /// </summary>
    public void ToggleAllLines(int buttonIndex)
    {
        if (buttonIndex == 1)
        {
            ToggleAllLines(!linesDisplayed);
        }
        else
        {
            ToggleAllLines(false);
        }
    }

    /// <summary>
    /// Displays or disables drawing all bus lines.
    /// </summary>
    /// <param name="b"> True <=> draw lines. </param>
    public void ToggleAllLines(bool b)
    {
        if (b != linesDisplayed)
        {
            if (busLines.Count == 0)
            {
                linesDisplayed = false;
                return;
            }

            // If a bus line is currently being edited, editing is deactivated.
            BusLineCreator.GetInstance().Deactivate();

            foreach (BusLine line in busLines)
            {
                line.ToggleLine(!linesDisplayed);
            }

            linesDisplayed = !linesDisplayed;
        }
    }

    /// <summary>
    /// Creates a gradient for the given colour.
    /// </summary>
    internal Gradient GetGradientFromColor(Color c)
    {
        GradientColorKey[] gck = new GradientColorKey[1];
        GradientAlphaKey[] gak = new GradientAlphaKey[1];

        gck[0].color = c;
        gck[0].time = 1.0f;

        gak[0].alpha = BusLineCreator.GetInstance().finishedGradientAlpha;
        gak[0].time = 1.0f;

        Gradient result = new Gradient();
        result.SetKeys(gck, gak);

        return result;
    }
}
