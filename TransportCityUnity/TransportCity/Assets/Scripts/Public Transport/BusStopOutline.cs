﻿using System.Collections.Generic;
using UnityEngine;
using static Constants;

public class BusStopOutline : MonoBehaviour
{
    // Keeps track of the number of objects in the outline's trigger collider.
    public int triggerEnterCount = 0;

    // The three possible colours of the outline. finishLineColour signals that the player is pointing at the first stop
    // (so left-clicking woudld finish the line). defaultColour signals that a stop can be added at the given place,
    // so that also applies to when the outline is over a stop from a different line. cantBuildHereColour speaks for itself, I think.
    public Color defaultColour, cantBuildHereColour, finishLineColour;

    // The path of the bus line that is currently being built.
    public List<IStop> path;

    // The stop that is currently intersecting the outline's collider.
    public Collider intersectingStopCollider;

    // The Renderer component of this outline.
    private Renderer outlineRenderer;

    // The light at the centre of the outline.
    private Light centreLight;

    internal Color currentColour;

    // The previous position of the outline.
    internal Vector3 previousPos;

    private void Start()
    {
        outlineRenderer = GetComponent<Renderer>();
        centreLight = GetComponentInChildren<Light>();
        currentColour = defaultColour;

        SetColour(defaultColour);
    }

    private void Update()
    {
        // When the ouline's position changes, check whether it is overlapping with any bus stops.
        if (transform.position != previousPos)
        {
            triggerEnterCount = 0;

            Collider[] overlapping = Physics.OverlapBox(gameObject.transform.position, transform.localScale / 2 * 0.95f, transform.rotation);
            foreach (Collider c in overlapping)
            {
                if (c.CompareTag(busStopTag))
                {
                    triggerEnterCount++;

                    if (triggerEnterCount == 1 && path.Count > 1 && c.GetComponent<IStop>() == path[0])
                    {
                        SetColour(finishLineColour);
                    }
                    else if (triggerEnterCount == 1 && !path.Contains(c.GetComponent<IStop>()))
                    {
                        SetColour(defaultColour);
                    }
                    else
                    {
                        SetColour(cantBuildHereColour);
                    }

                    intersectingStopCollider = c;
                }
            }

            if (triggerEnterCount == 0)
            {
                SetColour(defaultColour);
            }

            previousPos = transform.position;
        }
    }

    /// <summary>
    /// Changes the colour of the outline material as well as of the light in its centre.
    /// </summary>
    private void SetColour(Color c)
    {
        if (currentColour != c) {
            outlineRenderer.material.color = c;

            Color lightColour = c;
            lightColour.a = 1;

            centreLight.color = lightColour;

            currentColour = c;
        }
    }
}
