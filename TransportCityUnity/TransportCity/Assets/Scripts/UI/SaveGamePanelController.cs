﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SaveGamePanelController : MonoBehaviour
{
    // Various UI elements references
    public GameObject loadItemPrefab;
    public GameObject contentPanel;
    public GameObject clickBlockPanel;
    public InputField inputField;
    public GameObject menuPanel;
    public GameObject menuClickBlockPanel;

    public SaveManager saveManager;

    private RectTransform rectTransform;

    void Start()
    {
        rectTransform = GetComponent<RectTransform>();
    }

    void Update()
    {
        // Hide the window if the user left-clicks outside
        if (Input.GetMouseButtonDown(0) &&
            gameObject.activeSelf &&
            !RectTransformUtility.RectangleContainsScreenPoint(
                rectTransform,
                Input.mousePosition,
                null)
            )
        {
            clickBlockPanel.SetActive(false);
            gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Resets the content of the Save Game window
    /// </summary>
    public void ResetContent()
    {
        // Destroy all children
        foreach (Transform child in contentPanel.transform)
        {
            Destroy(child.gameObject);
        }

        // Create UI buttons for each save
        foreach ((string fileName, DateTime lastEdit) in saveManager.EnumerateSaves())
        {
            GameObject go = Instantiate(loadItemPrefab, contentPanel.transform);
            Text fileNameText = go.transform.GetChild(0).GetComponent<Text>();
            Text lastEditText = go.transform.GetChild(1).GetComponent<Text>();
            Button itemButton = go.GetComponent<Button>();

            fileNameText.text = fileName;
            lastEditText.text = lastEdit.ToString();

            string fileNameCopy = fileName; // Copy value for lambda capture
            itemButton.onClick.AddListener(() => {
                inputField.text = fileNameCopy;
            });
        }
    }

    /// <summary>
    /// The handler for the Save Game button
    /// </summary>
    public void SaveGameButton()
    {
        try
        {
            saveManager.SaveGame(inputField.text);
            ResetContent();
            clickBlockPanel.SetActive(false);
            menuPanel.SetActive(false);
            menuClickBlockPanel.SetActive(false);
            gameObject.SetActive(false);
        }
        catch (IOException)
        {
            // Display an error message
            UI_API.GetInstance().DisplayNotification("Save failed due to bad file name", UI_API.NotificationLevel.CRITICAL);
        }
    }
}
