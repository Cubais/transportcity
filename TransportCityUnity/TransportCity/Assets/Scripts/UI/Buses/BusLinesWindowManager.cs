﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using static Constants;

public class BusLinesWindowManager : MonoBehaviour
{
    // Various GameObjects from the UI
    public GameObject contentPanel;
    public GameUIController controller;
    public GameObject window;
    public GameObject endBusLinePathEditButton;

    // The bus line manager
    private BusLineManager lineManager;

    // A prefab of a UI panel for each line
    private GameObject listViewItem;

    // UI API reference
    private UI_API uiApi;

    // Rect transform of the window
    private RectTransform rectTransform;

    private void Awake()
    {
        lineManager = BusLineManager.GetInstance();
    }

    private void Start()
    {
        uiApi = UI_API.GetInstance();
        listViewItem = Resources.Load<GameObject>($"Prefabs/UI/Buses/BusLineItem");
        rectTransform = GetComponent<RectTransform>();
        PopulateScrollView();

        uiApi.LineEditing = false;
    }

    private void Update()
    {
        // If the player presses escape, he stops editing the bus line.
        if (uiApi.LineEditing && Input.GetKeyDown(KeyCode.Escape))
        {
            EndBusLinePathEditButton();
        }

        // Hide the window if the user left-clicks outside
        if (Input.GetMouseButtonDown(0) &&
            !uiApi.MenuUp &&
            gameObject.activeSelf &&
            !RectTransformUtility.RectangleContainsScreenPoint(
                rectTransform,
                Input.mousePosition,
                null)
            )
        {
            gameObject.SetActive(false);
        }
    }

    private void OnEnable()
    {
        RepopulateScrollView();
    }

    /// <summary>
    /// A handler for the "new bus line" button
    /// </summary>
    public void AddLineButton()
    {
        if (busLineCost > MoneyManager.instance.Money)
        {
            MoneyManager.instance.ThrowNotEnoughMoneyPopUp();
            return;
        }
        MoneyManager.instance.SpendMoneyOneTime(busLineCost, $"One bus line bought for ${busLineCost}.");

        lineManager.CreateNewBusLine();
        RepopulateScrollView();
    }

    /// <summary>
    /// A handler for the "close bus lines window" button
    /// </summary>
    public void WindowCloseButton()
    {
        window.SetActive(false);
    }

    /// <summary>
    /// A handler for the "end bus line path edit" button
    /// </summary>
    public void EndBusLinePathEditButton()
    {
        uiApi.LineEditing = false;
        endBusLinePathEditButton.SetActive(false);
        window.SetActive(true);
        uiApi.ServeBusPathEditEndCallback();
    }

    // Delete all scroll view elements and create new ones
    private void RepopulateScrollView()
    {
        foreach (Transform child in contentPanel.transform)
        {
            Destroy(child.gameObject);
        }
        PopulateScrollView();
    }

    // Create UI panels for the scroll view, one panel for each bus line
    private void PopulateScrollView()
    {
        foreach(BusLineManager.BusLine busLine in lineManager.GetLines())
        {
            // Instantiate panel
            GameObject go = Instantiate(listViewItem, contentPanel.transform);

            // Get text and button
            Text nameText = go.transform.GetChild(0).gameObject.GetComponent<Text>();
            Button pathButton = go.transform.GetChild(1).gameObject.GetComponent<Button>();
            BusIntervalPicker intervalPicker = go.transform.GetChild(2).gameObject.GetComponent<BusIntervalPicker>();
            Text numOfBusesText = go.transform.GetChild(3).gameObject.GetComponent<Text>();

            // Set name text
            nameText.text = busLine.Name;

            // Set the BusLine for interval picker to edit
            intervalPicker.Line = busLine;

            // Set number of assigned buses text
            numOfBusesText.text = busLine.Vehicles.Count.ToString();

            // Set button action, copy BusLine reference for lambda capture
            BusLineManager.BusLine busLineCopy = busLine;
            pathButton.onClick.AddListener(() => {
                EditBusLine(busLineCopy);
            });
        }
    }

    // A function to enter editing a bus line, called from the bus lines list
    private void EditBusLine(BusLineManager.BusLine busLine)
    {
        uiApi.LineEditing = true;
        uiApi.ServeBusPathEditCallback(busLine);
        endBusLinePathEditButton.SetActive(true);
        window.SetActive(false);
    }
}
