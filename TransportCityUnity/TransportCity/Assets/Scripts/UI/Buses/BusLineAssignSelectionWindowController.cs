﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class BusLineAssignSelectionWindowController : MonoBehaviour
{
    public BusLineManager busLineManager;
    public NotificationManager notificationManager;

    // The rect transform of the selection window
    private RectTransform rectTransform; 
    
    // A prefab of the list view item (the button)
    private GameObject listViewItem;

    // A cache for spacing of the vertical layout group
    private float verticalLayoutGroupSpacing;

    // UI API instance
    private UI_API uiApi;

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        listViewItem = Resources.Load<GameObject>($"Prefabs/UI/Buses/BusLineAssignSelectionItem");
        verticalLayoutGroupSpacing = GetComponent<VerticalLayoutGroup>().spacing;
        busLineManager = BusLineManager.GetInstance();

        uiApi = UI_API.GetInstance();
    }

    private void Update()
    {
        // Hide the window if the user left-clicks outside
        if (Input.GetMouseButtonDown(0) &&
            !uiApi.MenuUp &&
            gameObject.activeSelf &&
            !RectTransformUtility.RectangleContainsScreenPoint(
                rectTransform,
                Input.mousePosition,
                null)
            )
        {
            gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Moves the BusLine assign selection window to specified location and updates its contents (list of BusLines)
    /// </summary>
    /// <param name="location">The location to move the window to. The upper left corner of the window will be at this location</param>
    /// <param name="busInstance">The BusInstance which is going to be assigned to the specific BusLine when the button is pressed</param>
    public void MoveToLocationAndUpdate(Vector2 location, BusInstanceManager.BusInstance busInstance)
    {
        gameObject.SetActive(true);

        // Destroy all children
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }

        // We need to calculate the total height of the added elements because the height of the UI element
        // is not updated until the next frame.
        float itemHeight = -1f;
        float totalHeight = 0f;

        // If there are no closed bus lines, notify the player
        if (!busLineManager.GetLines().Any(x => x.Closed))
        {
            notificationManager.AddNotification("You must first create a bus line and create a circular route for it", UI_API.NotificationLevel.WARN);
        }

        // Create new children, one child for each closed bus line
        foreach (BusLineManager.BusLine busLine in busLineManager.GetLines())
        {
            // Skip (dont display) unclosed bus lines
            if (!busLine.Closed)
            {
                continue;
            }

            GameObject item = Instantiate(listViewItem, transform);

            if (itemHeight < 0f)
            {
                // Calculate the item height once
                itemHeight = item.GetComponent<LayoutElement>().preferredHeight;
            }
            else
            {
                // Add spacing when we are not in the first iteration
                totalHeight += verticalLayoutGroupSpacing;
            }
            totalHeight += itemHeight;

            // Set the button text to the name of the bus line
            Text itemText = item.transform.GetChild(0).GetComponent<Text>();
            itemText.text = busLine.Name;

            // Set the button action to assign the bus to the line, also copy loop variable for lambda capture
            var busLineCopy = busLine;
            item.GetComponent<Button>().onClick.AddListener(() => {
                busLineManager.UnassignBusInstance(busInstance);
                busLineManager.AddBusInstanceToBusLine(busInstance, busLineCopy);
                gameObject.SetActive(false);
            });
        }
        
        // Shift the UI panel lower by the calculated height
        rectTransform.position = new Vector2(location.x, location.y - totalHeight * transform.lossyScale.y);
    }
}
