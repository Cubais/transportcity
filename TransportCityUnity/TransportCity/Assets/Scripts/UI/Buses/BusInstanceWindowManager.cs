﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

using static Constants;

public class BusInstanceWindowManager : MonoBehaviour
{
    // Various GameObjects from the UI
    public GameObject contentPanel;
    public GameUIController controller;
    public GameObject window;
    public BusLineAssignSelectionWindowController busLineAssignSelectionWindow;
    public GameObject plusButton, backButton;
    public Transform legendPanel;

    public BusInstanceManager instanceManager;
    public BusLineManager lineManager;
    
    /// <summary>
    /// The singleton instance of this class
    /// </summary>
    public static BusInstanceWindowManager Instance { get; private set; }

    // List UI prefabs
    private GameObject busInstanceListViewItem;
    private GameObject busTypeListViewItem;

    // UI API reference
    private UI_API uiApi;

    // Rect transform of the window
    private RectTransform rectTransform;

    private Text[] legendPanelTexts;

    // A cache of references for updating the health texts of buses
    private List<(Bus, Text)> healthTextsCache;

    // A cache of name change controllers
    private List<BusInstanceNameChangeController> nameChangeControllersCache;

    // A currency formatter
    private NumberFormatInfo currencyFormat;

    private void Awake()
    {
        // Singleton
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        // Get references to the Text components of the legend
        legendPanelTexts = new Text[5];
        for (int i = 0; i < 5; i++)
        {
            legendPanelTexts[i] = legendPanel.GetChild(i).GetComponent<Text>();
        }
    }

    private void Start()
    {
        uiApi = UI_API.GetInstance();
        busInstanceListViewItem = Resources.Load<GameObject>($"Prefabs/UI/Buses/BusInstanceItem");
        busTypeListViewItem = Resources.Load<GameObject>($"Prefabs/UI/Buses/BusTypeItem");
        rectTransform = GetComponent<RectTransform>();

        // Set currency format for display when buying buses
        currencyFormat = new NumberFormatInfo
        {
            CurrencySymbol = "$"
        };

        BackButton();
    }

    private void Update()
    {
        // Update all health texts on the screen
        UpdateAllHealthTexts();

        // Hide the window if the user left-clicks outside
        if (Input.GetMouseButtonDown(0) &&
            !uiApi.MenuUp &&
            !uiApi.BusAssignWindowUp &&
            gameObject.activeSelf &&
            !RectTransformUtility.RectangleContainsScreenPoint(
                rectTransform,
                Input.mousePosition,
                null)
            )
        {
            gameObject.SetActive(false);
        }
    }

    private void OnEnable()
    {
        if (uiApi != null)
        {
            BackButton();
        }
    }

    /// <summary>
    /// Delete all scroll view elements and generate a list of bus instances
    /// </summary>
    public void RepopulateScrollViewWithBuses()
    {
        DestroyChildrenInScrollView();
        PopulateScrollViewWithBuses();
    }

    /// <summary>
    /// Delete all scroll view elements and generate a list of bus types for purchase
    /// </summary>
    public void RepopulateScrollViewWithBusTypes()
    {
        DestroyChildrenInScrollView();
        PopulateScrollViewWithBusTypes();
    }

    public void EnableNamesEditing()
    {
        foreach (BusInstanceNameChangeController nameChangeController in nameChangeControllersCache)
        {
            nameChangeController.OtherEditing = false;
        }
    }

    public void DisableNamesEditing()
    {
        foreach (BusInstanceNameChangeController nameChangeController in nameChangeControllersCache)
        {
            nameChangeController.OtherEditing = true;
        }
    }

    /// <summary>
    /// A handler for the + button in the top bar
    /// Switch to a bus buying view
    /// </summary>
    public void PlusButton()
    {
        plusButton.SetActive(false);
        backButton.SetActive(true);
        uiApi.BusInstanceWindowShowingBusTypes = true;

        legendPanelTexts[0].text = "Name";
        legendPanelTexts[1].text = "Capacity";
        legendPanelTexts[2].text = "Durability";
        legendPanelTexts[3].text = "Price";
        legendPanelTexts[4].text = "Number to Buy";

        RepopulateScrollViewWithBusTypes();
    }

    /// <summary>
    /// A handler for the Back button in the top bar
    /// Return back to bus instances view
    /// </summary>
    public void BackButton()
    {
        backButton.SetActive(false);
        plusButton.SetActive(true);
        uiApi.BusInstanceWindowShowingBusTypes = false;

        legendPanelTexts[0].text = "Name";
        legendPanelTexts[1].text = "Type";
        legendPanelTexts[2].text = "Line";
        legendPanelTexts[3].text = "Health";
        legendPanelTexts[4].text = "";

        RepopulateScrollViewWithBuses();
    }

    /// <summary>
    /// A handler for the "close bus vehicles window" button
    /// </summary>
    public void WindowCloseButton()
    {
        window.SetActive(false);
    }

    // Update all health texts in the open window
    private void UpdateAllHealthTexts()
    {
        if (healthTextsCache == null)
        {
            return;
        }

        foreach ((Bus bus, Text text) in healthTextsCache)
        {
            float health = bus.GetHealth();
            // Same color as all the text in the window (= 50/256)
            Color textColor = new Color(0.1953125f, 0.1953125f, 0.1953125f);

            // If the health is below zero or zero, round it to zero and set the text color to red
            if (health <= 0)
            {
                health = 0;
                textColor = Color.red;
            }

            text.text = health.ToString("F2");
            text.color = textColor;
        }
    }

    // Create UI panels for the scroll view, one panel for each bus line
    private void PopulateScrollViewWithBuses()
    {
        foreach (var busInstance in instanceManager.GetBusInstances())
        {
            // Instantiate panel
            GameObject go = Instantiate(busInstanceListViewItem, contentPanel.transform);

            // Get text and button references from the newly instantiated list item
            Text nameText = go.transform.GetChild(0).gameObject.GetComponent<Text>();
            Text typeText = go.transform.GetChild(1).gameObject.GetComponent<Text>();
            Text lineText = go.transform.GetChild(2).gameObject.GetComponent<Text>();
            Text healthText = go.transform.GetChild(3).gameObject.GetComponent<Text>();
            Button repairButton = go.transform.GetChild(4).gameObject.GetComponent<Button>();
            Button sellButton = go.transform.GetChild(5).gameObject.GetComponent<Button>();
            Button unassignButton = go.transform.GetChild(6).gameObject.GetComponent<Button>();
            Button assignButton = go.transform.GetChild(7).gameObject.GetComponent<Button>();

            // Set name text and type text and line text
            nameText.text = busInstance.name;
            typeText.text = busInstance.type.name;
            lineText.text = busInstance.line == null ? "None" : busInstance.line.Name;

            // Add the repair text to the cache to be updated via Update
            if (busInstance.bus != null)
            {
                healthTextsCache.Add((busInstance.bus, healthText));
            }
            else
            {
                healthText.text = "";
            }

            BusInstanceNameChangeController nameChangeController = go.GetComponent<BusInstanceNameChangeController>();
            nameChangeController.BusInstance = busInstance;
            nameChangeControllersCache.Add(nameChangeController);

            // Set button action, copy BusLine reference for lambda capture
            BusInstanceManager.BusInstance busInstanceCopy = busInstance;

            // Set up button on click listneres via lambdas
            repairButton.onClick.AddListener(() => {
                if (busInstanceCopy.bus == null)
                {
                    return;
                }

                int cost = (int)(busInstanceCopy.type.price * repairFromCostMultiplier * (1 - (busInstanceCopy.bus.GetHealth() / 100f)));
                if (cost > MoneyManager.instance.Money)
                {
                    MoneyManager.instance.ThrowNotEnoughMoneyPopUp();
                    return;
                }
                MoneyManager.instance.SpendMoneyFlow(cost);
                busInstanceCopy.bus.Repair();
            });

            sellButton.onClick.AddListener(() => {
                int cost = (int)(busInstanceCopy.type.price * sellFromCostMultiplier);
                if (cost > MoneyManager.instance.Money)
                {
                    MoneyManager.instance.ThrowNotEnoughMoneyPopUp();
                    return;
                }
                MoneyManager.instance.GainMoneyOneTime(cost, $"Bus sold for ${cost}.");
                busInstanceCopy.bus?.Repair();

                instanceManager.DeleteBusInstance(busInstanceCopy);
                RepopulateScrollViewWithBuses();
            });

            unassignButton.onClick.AddListener(() => {
                lineManager.UnassignBusInstance(busInstanceCopy);
            });

            assignButton.onClick.AddListener(() => {
                // Calculate some numbers for the selection window update method call
                RectTransform buttonRectTransform = assignButton.GetComponent<RectTransform>();
                Vector3 buttonPosition = buttonRectTransform.position;
                Rect buttonRect = buttonRectTransform.rect;
                Vector3 worldUiScale = assignButton.transform.lossyScale;

                // Update the selection window
                busLineAssignSelectionWindow.MoveToLocationAndUpdate(
                    new Vector2(
                        buttonPosition.x + (buttonRect.width / 2) * worldUiScale.x,
                        buttonPosition.y + (buttonRect.height / 2) * worldUiScale.y
                    ),
                    busInstanceCopy
                );
            });
        }

        // At the end, update all health texts
        UpdateAllHealthTexts();
    }

    private void PopulateScrollViewWithBusTypes()
    {
        foreach(var type in instanceManager.GetBusTypes())
        {
            // Instantiate panel
            GameObject go = Instantiate(busTypeListViewItem, contentPanel.transform);

            // Get text and button references from the newly instantiated list item
            Text nameText = go.transform.GetChild(0).gameObject.GetComponent<Text>();
            Text capacityText = go.transform.GetChild(1).gameObject.GetComponent<Text>();
            Text durabilityText = go.transform.GetChild(2).gameObject.GetComponent<Text>();
            Text priceText = go.transform.GetChild(3).gameObject.GetComponent<Text>();
            InputField numberInputField = go.transform.GetChild(4).gameObject.GetComponent<InputField>();
            Button purchaseButton = go.transform.GetChild(5).gameObject.GetComponent<Button>();
            Text purchaseButtonText = purchaseButton.transform.GetChild(0).gameObject.GetComponent<Text>();

            // Set texts of the newly created list view items
            nameText.text = type.name;
            capacityText.text = type.capacity.ToString();
            durabilityText.text = type.durability.ToString();
            priceText.text = type.price.ToString("C", currencyFormat);
            numberInputField.text = "1";
            purchaseButtonText.text = $"Purchase ({type.price.ToString("C", currencyFormat)})";

            // Copy iterable value because of lambda capture
            var typeCopy = type;

            // Update purchase button text when the number of buses to buy changes
            numberInputField.onValueChanged.AddListener((string text) => {
                bool validNumber = int.TryParse(text, out int value);

                if (!validNumber || value <= 0)
                {
                    return;
                }

                purchaseButtonText.text = $"Purchase ({(value * typeCopy.price).ToString("C", currencyFormat)})";
            });

            // Buy the buses when the player presses the purchase button
            purchaseButton.onClick.AddListener(() => {
                bool validNumber = int.TryParse(numberInputField.text, out int numberToBuy);

                if (!validNumber || numberToBuy <= 0)
                {
                    return;
                }

                // TODO: Subtract this money in money manager, or return if there is insufficient funds
                float cost = typeCopy.price * numberToBuy;

                if (cost > MoneyManager.instance.Money) 
                {
                    MoneyManager.instance.ThrowNotEnoughMoneyPopUp();
                    return;
                }
                MoneyManager.instance.SpendMoneyOneTime((int)cost, $"{numberToBuy} buses bought for {cost}$.");

                for (int i = 0; i < numberToBuy; i++)
                {
                    instanceManager.CreateNewBusInstance(typeCopy);
                }

                // Return to bus view
                BackButton();
            });
        }
    }

    // Destroy all children in the scroll view and reset caches
    private void DestroyChildrenInScrollView()
    {
        healthTextsCache = new List<(Bus, Text)>();
        nameChangeControllersCache = new List<BusInstanceNameChangeController>();

        foreach (Transform child in contentPanel.transform)
        {
            Destroy(child.gameObject);
        }
    }
}
