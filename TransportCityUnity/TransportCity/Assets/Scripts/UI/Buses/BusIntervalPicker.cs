﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

public class BusIntervalPicker : MonoBehaviour
{
    public InputField inputField;
    public Text requiredVehiclesText;

    public BusLineManager.BusLine Line { get; set; }

    private void Start()
    {
        UpdateInputFieldText();
        Line.schedule.UpdateRequiredNumberOfVehicles();
    }

    private void Update()
    {
        requiredVehiclesText.text = Line.schedule.RequiredNumberOfVehicles.ToString();
    }

    public void InputFieldEditEnd()
    {
        if (inputField.text.Length == 0 || inputField.text.Equals("-"))
        {
            UpdateInputFieldText();
            return;
        }

        // Will work because there can only be valid integers in the text field
        int parsed = int.Parse(inputField.text);

        if (parsed < 1)
        {
            UpdateInputFieldText();
        }
        else
        {
            Line.schedule.Interval = parsed;
        }
    }

    public void ButtonUp()
    {
        if (Line.schedule.Interval >= 999)
        {
            return;
        }

        Line.schedule.Interval++;
        UpdateInputFieldText();
    }

    public void ButtonDown()
    {
        if (Line.schedule.Interval <= 0)
        {
            return;
        }

        Line.schedule.Interval--;
        UpdateInputFieldText();
    }

    private void UpdateInputFieldText()
    {
        inputField.text = Line.schedule.Interval.ToString();
    }
}
