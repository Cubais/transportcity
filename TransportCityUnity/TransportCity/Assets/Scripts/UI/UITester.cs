﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UITester : MonoBehaviour
{
    // A small temporary class for testing the features of the UI

    public TimeManager timeManager;

    private UI_API uiApi;

    private float lastUpdate;

    void Start()
    {
        // Try calling various UI API functions
        uiApi = UI_API.GetInstance();
        uiApi.SetCarUsageSlider(0.5f);
        uiApi.SetHappinessSlider(-0.6f);
        uiApi.SetCurrentMoney(54_000);
        uiApi.SetCurrentMoneyFlow(10_000);
        uiApi.SetTimeText("123.23.23 test:test");
        uiApi.RegisterForStatisticsButtons(StatHandler);
        uiApi.RegisterForTimeButtons(TimeHandler);
        uiApi.RegisterForBulldozer(BulldozerHandler);
        uiApi.RegisterForBusPathEditButtons(PathEditHandler);

        for (int i = 12; i > 0; i--)
        {
            timeManager.RegisterAlarmClock(timeManager.CurrentDateTime.AddHours(i), TimeManagerCallbackTest);
        }

        timeManager.RegisterDayChangeHandler(TimeManagerCallbackTest);
    }

    void Update()
    {
        // Send a random notification every two seconds
        if (Time.unscaledTime - lastUpdate < 2)
        {
            return;
        }

        lastUpdate = Time.unscaledTime;
        switch(UnityEngine.Random.Range(0, 3))
        {
            case 0:
                uiApi.DisplayNotification("test notification", UI_API.NotificationLevel.INFO);
                break;
            case 1:
                uiApi.DisplayNotification("test notification", UI_API.NotificationLevel.WARN);
                break;
            case 2:
                uiApi.DisplayNotification("test notification", UI_API.NotificationLevel.CRITICAL);
                break;
        }
         
    }

    public void StatHandler(int index)
    {
        Debug.Log($"stat clicked, index {index}");
    }

    public void TimeHandler(int index)
    {
        Debug.Log($"time clicked, index {index}");
    }

    public void BulldozerHandler()
    {
        Debug.Log($"bulldozer clicked");
    }

    public void PathEditHandler(BusLineManager.BusLine busLine)
    {
        Debug.Log($"{busLine.Name} path edit button was clicked");
    }

    public void TimeManagerCallbackTest()
    {
        Debug.Log("Time Manager test");
        Debug.Log(timeManager.CurrentDateTime);
    }
}
