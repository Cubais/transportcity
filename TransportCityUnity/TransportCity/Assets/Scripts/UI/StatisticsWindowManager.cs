﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Markup;
using UnityEngine;
using UnityEngine.UI;

public class StatisticsWindowManager : MonoBehaviour
{
    enum TimePeriod
    {
        NONE,
        DAY,
        WEEK,
        MONTH,
        YEAR,
        ALL
    }

    // UI elements references
    public GameObject listItemPrefab;
    public Transform leftContentPanel;
    public Transform graphContainer;
    public Text maxText, avgText, minText;
    public ColorBlock highlightColor;
    public ColorBlock defaultColor;
    public Transform timePeriodButtonsParent;

    private StatisticsDataManager dataManager;
    private UI_API uiApi;
    private RectTransform rectTransform;
    private RectTransform graphContainerRectTransform;
    private List<Button> timePeriodButtons;

    private TimePeriod timePeriod = TimePeriod.DAY;

    private string activeKey;

    private void Awake()
    {
        dataManager = StatisticsDataManager.instance;
        uiApi = UI_API.GetInstance();
        rectTransform = GetComponent<RectTransform>();
        graphContainerRectTransform = graphContainer.GetComponent<RectTransform>();
    }

    private void Start()
    {
        // Create the list of all time period buttons in order to change their colors
        timePeriodButtons = new List<Button>();

        foreach (Transform child in timePeriodButtonsParent)
        {
            timePeriodButtons.Add(child.GetComponent<Button>());
        }

        // Pop the last one since that is the close button
        timePeriodButtons.RemoveAt(timePeriodButtons.Count - 1);
    }

    private void Update()
    {
        // Hide the window if the user left-clicks outside
        if (Input.GetMouseButtonDown(0) &&
            !uiApi.MenuUp &&
            gameObject.activeSelf &&
            !RectTransformUtility.RectangleContainsScreenPoint(
                rectTransform,
                Input.mousePosition,
                null)
            )
        {
            gameObject.SetActive(false);
        }
    }

    private void OnEnable()
    {
        Start();

        timePeriod = TimePeriod.DAY;
        HighlightButton(0);

        // Destroy all children in the left panel
        foreach (Transform child in leftContentPanel)
        {
            Destroy(child.gameObject);
        }

        string firstKey = null;
        
        // Fill the left panel with buttons with names of the series to switch currently viewed series
        // Save the first series in order to display it later.
        foreach (string key in dataManager.NumericSeries.Keys)
        {
            if (firstKey == null)
            {
                firstKey = key;
            }

            // Instantiate the button and get some references
            GameObject listItem = Instantiate(listItemPrefab, leftContentPanel);
            Text listItemText = listItem.GetComponentInChildren<Text>();
            Button listItemButton = listItem.GetComponentInChildren<Button>();

            // Set the button text
            listItemText.text = key;

            // Set the button action
            string keyCopy = key;
            listItemButton.onClick.AddListener(() => {
                activeKey = keyCopy;
                ShowGraph(keyCopy);
            });
        }

        // Display the first shown series
        if (firstKey != null)
        {
            activeKey = firstKey;
            ShowGraph(firstKey);
        }
    }

    /// <summary>
    /// A handler for the X button to close the window
    /// </summary>
    public void WindowCloseButton()
    {
        gameObject.SetActive(false);
    }

    /// <summary>
    /// A handler for the time period buttons in the graph UI
    /// </summary>
    /// <param name="index">Specifies the index of the button pressed</param>
    public void TimePeriodButton(int index)
    {
        switch (index)
        {
            case 0:
                timePeriod = TimePeriod.DAY;
                break;
            case 1:
                timePeriod = TimePeriod.WEEK;
                break;
            case 2:
                timePeriod = TimePeriod.MONTH;
                break;
            case 3:
                timePeriod = TimePeriod.YEAR;
                break;
            case 4:
                timePeriod = TimePeriod.ALL;
                break;
        }

        ShowGraph(activeKey);
        HighlightButton(index);
    }

    // Show a graph in the graph container.
    // The key parameter specifies the name of the graph to be shown.
    private void ShowGraph(string key)
    {
        // Destroy all children
        foreach (Transform line in graphContainer)
        {
            Destroy(line.gameObject);
        }

        // Get the series
        List<(long date, float value)> values = dataManager.NumericSeries[key];

        // Calculate the timestamp of current time minus the specified time period
        long curTime = TimeManager.instance.ConvertDateTimeToMilliseconds(TimeManager.instance.CurrentDateTime);
        long curTimeMinusPeriod;

        switch (timePeriod)
        {
            case TimePeriod.DAY:
                curTimeMinusPeriod = curTime - 86400000;
                break;
            case TimePeriod.WEEK:
                curTimeMinusPeriod = curTime - 604800000;
                break;
            case TimePeriod.MONTH:
                curTimeMinusPeriod = curTime - 2592000000;
                break;
            case TimePeriod.YEAR:
                curTimeMinusPeriod = curTime - 31536000000;
                break;
            default:
                // includes TimePeriod.ALL
                curTimeMinusPeriod = values[0].date;
                break;
        }

        List<(long date, float value)> newValues;

        if (curTimeMinusPeriod < values[0].date)
        {
            // Create a new List with a zero in the specified past and a zero before the game started
            newValues = new List<(long date, float value)>
            {
                (curTimeMinusPeriod, 0),
                (values[0].date - 1, 0)
            };
        }
        else
        {
            newValues = new List<(long date, float value)>();
        }

        // Filter out all values older than the specified period and append them to the list
        newValues.AddRange(values.FindAll(x => x.date >= curTimeMinusPeriod));

        values = newValues;
        
        if (values.Count < 1)
        {
            return;
        }

        // Calculate the min and max values and times in one pass
        long maxDate = long.MinValue;
        long minDate = long.MaxValue;
        float maxValue = float.MinValue;
        float minValue = float.MaxValue;
        foreach ((long l, float f) in values)
        {
            if (maxDate < l)
            {
                maxDate = l;
            }

            if (minDate > l)
            {
                minDate = l;
            }

            if (maxValue < f)
            {
                maxValue = f;
            }

            if (minValue > f)
            {
                minValue = f;
            }
        }

        // If there is only one value, create the range manually in order not to break the graph
        if (minDate == maxDate)
        {
            minDate = values[0].date - 1;
            maxDate = values[0].date + 1;
        }

        if (minValue == maxValue)
        {
            minValue = values[0].value - 1;
            maxValue = values[0].value + 1;
        }

        // Set the max, avg and min texts
        if (minValue < 10 && maxValue < 10)
        {
            maxText.text = maxValue.ToString("F1");
            avgText.text = ((maxValue + minValue) / 2).ToString("F1");
            minText.text = minValue.ToString("F1");
        }
        else
        {
            maxText.text = maxValue.ToString("F0");
            avgText.text = ((maxValue + minValue) / 2).ToString("F0");
            minText.text = minValue.ToString("F0");
        }

        // For each (time, value) pair, create a line and position it correctly
        Vector2? previous = null;
        foreach ((long dt, float f) in values)
        {
            // Recalculate the (time, value) pair into UI coordinates in the graph container
            Vector2 rescaledPosition = new Vector2(
                graphContainerRectTransform.rect.width * ((dt - minDate) / (float)(maxDate - minDate)),
                graphContainerRectTransform.rect.height * ((f - minValue) / (maxValue - minValue))
            );

            // We can only proceed if this is the second iteration, since we need the previous point
            if (previous == null)
            {
                previous = rescaledPosition;
                continue;
            }

            // Extract from nullable
            Vector2 prevDot = (Vector2)previous;

            // Instantiate a line and set parent
            GameObject line = new GameObject("dotConnection", typeof(Image));
            line.transform.SetParent(graphContainer);
            RectTransform lineRectTransform = line.GetComponent<RectTransform>();

            // Calculate direction and distance between the previous and current dot
            Vector2 dir = (rescaledPosition - prevDot).normalized;
            float distance = Vector2.Distance(prevDot, rescaledPosition);

            // Set anchor to lower left
            lineRectTransform.anchorMin = new Vector2(0, 0);
            lineRectTransform.anchorMax = new Vector2(0, 0);

            // Set size
            lineRectTransform.sizeDelta = new Vector2(distance * graphContainer.lossyScale.x, 2);

            // Set position
            lineRectTransform.anchoredPosition = prevDot + dir * distance * 0.5f;

            // Set rotation
            if (dir.y >= 0)
            {
                lineRectTransform.localEulerAngles = new Vector3(0, 0, -Vector2.Angle(dir, Vector2.left));
            }
            else
            {
                lineRectTransform.localEulerAngles = new Vector3(0, 0, -Vector2.Angle(dir, Vector2.right));
            }

            previous = rescaledPosition;
        }
    }

    // Highlight the specified time period button and unhighlight all other buttons
    private void HighlightButton(Button button)
    {
        foreach (Button timePeriodButton in timePeriodButtons)
        {
            timePeriodButton.colors = defaultColor;
        }

        button.colors = highlightColor;
    }

    // Highlight the specified time period button by index and unhighlight all other buttons
    private void HighlightButton(int buttonIndex)
    {
        HighlightButton(timePeriodButtons[buttonIndex]);
    }
}
