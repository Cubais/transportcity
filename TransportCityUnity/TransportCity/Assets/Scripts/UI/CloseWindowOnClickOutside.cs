﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseWindowOnClickOutside : MonoBehaviour
{
    public GameObject[] alsoSetActiveFalse;

    private RectTransform rectTransform;

    private void Start()
    {
        rectTransform = GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) &&
            gameObject.activeSelf &&
            !RectTransformUtility.RectangleContainsScreenPoint(
                rectTransform,
                Input.mousePosition,
                null)
            )
        {
            foreach (var otherGameObject in alsoSetActiveFalse)
            {
                otherGameObject.SetActive(false);
            }

            gameObject.SetActive(false);
        }
    }
}
