﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    // Time manager for unpausing the game
    public TimeManager timeManager;

    // Some public UI elements to be controlled by the controller
    public GameObject menuClickBlockPanel;
    public LoadGamePanelController loadWindow;
    public GameObject loadWindowClickBlockPanel;
    public SaveGamePanelController saveWindow;
    public GameObject saveWindowClickBlockPanel;

    // The RectTransform of the menu window
    private RectTransform rectTransform;

    // UI API instance
    private UI_API uiApi;

    private void Start()
    {
        rectTransform = GetComponent<RectTransform>();
        uiApi = UI_API.GetInstance();
    }

    private void Update()
    {
        // Hide the window if the user left-clicks outside
        if (Input.GetMouseButtonDown(0) &&
            !uiApi.PopUpWindowUp &&
            !loadWindow.gameObject.activeSelf &&
            !saveWindow.gameObject.activeSelf &&
            gameObject.activeSelf &&
            !RectTransformUtility.RectangleContainsScreenPoint(
                rectTransform,
                Input.mousePosition,
                null)
            )
        {
            ReturnToGameButton();
        }
    }

    /// <summary>
    /// A handler for the Return to Game button click
    /// </summary>
    public void ReturnToGameButton()
    {
        gameObject.SetActive(false);
        menuClickBlockPanel.SetActive(false);
        timeManager.UnpauseGame();
    }

    /// <summary>
    /// A handler for the Save Game button click
    /// </summary>
    public void SaveGameButton()
    {
        saveWindowClickBlockPanel.SetActive(true);
        saveWindow.gameObject.SetActive(true);
        saveWindow.ResetContent();
    }

    /// <summary>
    /// A handler for the Back button in the save menu
    /// </summary>
    public void SaveMenuBackButton()
    {
        saveWindowClickBlockPanel.SetActive(false);
        saveWindow.gameObject.SetActive(false);
    }

    /// <summary>
    /// A handler for the Load Game button click
    /// </summary>
    public void LoadGameButton()
    {
        loadWindowClickBlockPanel.SetActive(true);
        loadWindow.gameObject.SetActive(true);
        loadWindow.ResetContent();
    }

    /// <summary>
    /// A handler for the Back button in the load menu
    /// </summary>
    public void LoadMenuBackButton()
    {
        loadWindowClickBlockPanel.SetActive(false);
        loadWindow.gameObject.SetActive(false);
    }

    /// <summary>
    /// A handler for the Settings button click
    /// </summary>
    public void SettingsButton()
    {

    }

    /// <summary>
    /// A handler for the Exit to Main Menu button click
    /// </summary>
    public void ExitToMainMenuButton()
    {
        // Ask the user if he really wants to quit to main menu
        PopUpWindowManager.Instance.EnableTwoButtonWindow(
            "Are you sure you want to quit to the main menu?",
            () => {
                SceneManager.LoadScene("Assets/Scenes/MainMenu.unity");
            },
            () => {

            }
        );
    }

    /// <summary>
    /// A handler for the Exit to desktop button click
    /// </summary>
    public void ExitToDesktopButton()
    {
        // Ask the user if he really wants to exit the game
        PopUpWindowManager.Instance.EnableTwoButtonWindow(
            "Are you sure you want to quit to desktop?",
            () => {
                Debug.Log("exiting");
                Application.Quit();
            },
            () => {

            }
        );
    }

}
