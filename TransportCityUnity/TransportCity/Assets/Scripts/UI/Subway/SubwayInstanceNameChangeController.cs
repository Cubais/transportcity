﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SubwayInstanceNameChangeController : MonoBehaviour
{
    // The subway train instance to change the name of
    public SubwayInstanceManager.SubwayTrainInstance SubwayTrainInstance { get; set; }

    // If other subway train line is being edited, disable the possibility to edit this one
    public bool OtherEditing { get; set; }

    // What is the max delay between clicks to be considered a double click
    public float doubleClickDelaySeconds = 0.5f;

    // The name text of the subway train instance
    public Text nameText;

    // The input field to be activated via double click
    public InputField nameInputField;

    private float lastClickTime = 0f;
    private bool editing = false;

    private void Start()
    {
        OtherEditing = false;
    }

    private void Update()
    {
        // If not editing, ignore key presses
        if (!editing)
        {
            return;
        }

        // Enter to confirm, escape to discard changes
        if (Input.GetKeyDown(KeyCode.Return))
        {
            if (nameInputField.text.Length == 0)
            {
                // Ignore empty names
                return;
            }

            SubwayTrainInstance.name = nameInputField.text;
            nameText.text = nameInputField.text;
            StopEditing();
        }
        else if (Input.GetKeyDown(KeyCode.Escape))
        {
            StopEditing();
        }
    }

    public void NameClick()
    {
        if (!OtherEditing && (Time.unscaledTime - lastClickTime < doubleClickDelaySeconds))
        {
            // Double click!
            StartEditing();
        }

        lastClickTime = Time.unscaledTime;
    }

    private void StartEditing()
    {
        editing = true;
        nameText.gameObject.SetActive(false);
        nameInputField.gameObject.SetActive(true);
        nameInputField.Select();

        // Let other rows know that they can not be edited
        SubwayTrainWindowManager.Instance.DisableNamesEditing();

        // Disable camera movement
        UI_API.GetInstance().NameEditing = true;
    }

    private void StopEditing()
    {
        editing = false;
        nameText.gameObject.SetActive(true);
        nameInputField.gameObject.SetActive(false);

        // Let other rows know that they can be edited again
        SubwayTrainWindowManager.Instance.EnableNamesEditing();

        // Enable camera movement
        UI_API.GetInstance().NameEditing = false;
    }
}
