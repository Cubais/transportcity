﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using static Constants;

public class SubwayLinesWindowManager : MonoBehaviour
{
    // Various GameObjects from the UI
    public GameObject contentPanel;
    public GameUIController controller;
    public GameObject window;
    public GameObject endSubwayLinePathEditButton;

    // The subway line manager
    private SubwayLineManager lineManager;

    // A prefab of a UI panel for each line
    private GameObject listViewItem;

    // UI API reference
    private UI_API uiApi;

    // Rect transform of the window
    private RectTransform rectTransform;

    private void Awake()
    {
        lineManager = SubwayLineManager.GetInstance();
    }

    private void Start()
    {
        uiApi = UI_API.GetInstance();
        listViewItem = Resources.Load<GameObject>($"Prefabs/UI/Subway/SubwayLineItem");
        rectTransform = GetComponent<RectTransform>();
        PopulateScrollView();

        uiApi.LineEditing = false;
    }

    private void Update()
    {
        // If the player presses escape, he stops editing the subway line.
        if (uiApi.LineEditing && Input.GetKeyDown(KeyCode.Escape))
        {
            EndSubwayLinePathEditButton();
        }

        // Hide the window if the user left-clicks outside
        if (Input.GetMouseButtonDown(0) &&
            !uiApi.MenuUp &&
            gameObject.activeSelf &&
            !RectTransformUtility.RectangleContainsScreenPoint(
                rectTransform,
                Input.mousePosition,
                null)
            )
        {
            gameObject.SetActive(false);
        }
    }

    private void OnEnable()
    {
        RepopulateScrollView();
    }

    /// <summary>
    /// A handler for the "new subway line" button
    /// </summary>
    public void AddLineButton()
    {
        if (subwayLineCost > MoneyManager.instance.Money)
        {
            MoneyManager.instance.ThrowNotEnoughMoneyPopUp();
            return;
        }
        MoneyManager.instance.SpendMoneyOneTime(subwayLineCost, $"One subway line bought for ${subwayLineCost}.");
        
        lineManager.CreateNewSubwayLine();
        RepopulateScrollView();
    }

    /// <summary>
    /// A handler for the "close subway lines window" button
    /// </summary>
    public void WindowCloseButton()
    {
        window.SetActive(false);
    }

    /// <summary>
    /// A handler for the "end subway line path edit" button
    /// </summary>
    public void EndSubwayLinePathEditButton()
    {
        uiApi.LineEditing = false;
        endSubwayLinePathEditButton.SetActive(false);
        window.SetActive(true);
        uiApi.ServeSubwayPathEditEndCallback();
    }

    // Delete all scroll view elements and create new ones
    private void RepopulateScrollView()
    {
        foreach (Transform child in contentPanel.transform)
        {
            Destroy(child.gameObject);
        }
        PopulateScrollView();
    }

    // Create UI panels for the scroll view, one panel for each subway line
    private void PopulateScrollView()
    {
        foreach (SubwayLineManager.SubwayLine subwayLine in lineManager.GetSubwayLines())
        {
            // Instantiate panel
            GameObject go = Instantiate(listViewItem, contentPanel.transform);

            // Get text and button
            Text nameText = go.transform.GetChild(0).gameObject.GetComponent<Text>();
            Button pathButton = go.transform.GetChild(1).gameObject.GetComponent<Button>();
            SubwayIntervalPicker intervalPicker = go.transform.GetChild(2).gameObject.GetComponent<SubwayIntervalPicker>();
            Text numOfTrainsText = go.transform.GetChild(3).gameObject.GetComponent<Text>();

            // Set text
            nameText.text = subwayLine.Name;

            // Set the SubwayLine for interval picker to edit
            intervalPicker.Line = subwayLine;

            // Set number of assigned trains text
            numOfTrainsText.text = subwayLine.Vehicles.Count.ToString();

            // Set button action, copy SubwayTrainLine reference for lambda capture
            SubwayLineManager.SubwayLine subwayLineCopy = subwayLine;
            pathButton.onClick.AddListener(() => {
                EditSubwayLine(subwayLineCopy);
            });
        }
    }

    // A function to enter editing the path of a subway line, called from the subway lines list
    private void EditSubwayLine(SubwayLineManager.SubwayLine subwayLine)
    {
        uiApi.LineEditing = true;
        uiApi.ServeSubwayPathEditCallback(subwayLine);
        endSubwayLinePathEditButton.SetActive(true);
        window.SetActive(false);

        if (SubwayLineManager.GetInstance().LinesDisplayed)
        {
            SubwayLineManager.GetInstance().ToggleAllLines(false);
        }

        subwayLine.ToggleVisibility(true, false);
    }
}
