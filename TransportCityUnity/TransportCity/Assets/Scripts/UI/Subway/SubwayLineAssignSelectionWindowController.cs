﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class SubwayLineAssignSelectionWindowController : MonoBehaviour
{
    public SubwayLineManager subwayLineManager;
    public NotificationManager notificationManager;

    // The rect transform of the selection window
    private RectTransform rectTransform;

    // A prefab of the list view item (the button)
    private GameObject listViewItem;

    // A cache for spacing of the vertical layout group
    private float verticalLayoutGroupSpacing;

    // UI API instance
    private UI_API uiApi;

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        listViewItem = Resources.Load<GameObject>($"Prefabs/UI/Buses/BusLineAssignSelectionItem");
        verticalLayoutGroupSpacing = GetComponent<VerticalLayoutGroup>().spacing;
        subwayLineManager = SubwayLineManager.GetInstance();

        uiApi = UI_API.GetInstance();
    }

    private void Update()
    {
        // Hide the window if the user left-clicks outside
        if (Input.GetMouseButtonDown(0) &&
            !uiApi.MenuUp &&
            gameObject.activeSelf &&
            !RectTransformUtility.RectangleContainsScreenPoint(
                rectTransform,
                Input.mousePosition,
                null)
            )
        {
            gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Moves the SubwayLine assign selection window to specified location and updates its contents (list of SubwayLines)
    /// </summary>
    /// <param name="location">The location to move the window to. The upper left corner of the window will be at this location</param>
    /// <param name="trainInstance">The SubwayTrainInstance which is going to be assigned to the specific SubwayLine when the button is pressed</param>
    public void MoveToLocationAndUpdate(Vector2 location, SubwayInstanceManager.SubwayTrainInstance trainInstance)
    {
        gameObject.SetActive(true);

        // Destroy all children
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }

        // We need to calculate the total height of the added elements because the height of the UI element
        // is not updated until the next frame.
        float itemHeight = -1f;
        float totalHeight = 0f;

        // If there are no subway lines, notify the player
        if (!subwayLineManager.GetLines().Any(x => x.Path.Count > 1))
        {
            notificationManager.AddNotification("You must first create a subway line with at least two stops", UI_API.NotificationLevel.WARN);
        }

        // Create new children, one child for each train line
        foreach (SubwayLineManager.SubwayLine subwayLine in subwayLineManager.GetSubwayLines())
        {
            // Do not show lines with empty path
            if (subwayLine.Path.Count <= 1)
            {
                continue;
            }

            GameObject item = Instantiate(listViewItem, transform);

            if (itemHeight < 0f)
            {
                // Calculate the item height once
                itemHeight = item.GetComponent<LayoutElement>().preferredHeight;
            }
            else
            {
                // Add spacing when we are not in the first iteration
                totalHeight += verticalLayoutGroupSpacing;
            }
            totalHeight += itemHeight;

            // Set the button text to the name of the train line
            Text itemText = item.transform.GetChild(0).GetComponent<Text>();
            itemText.text = subwayLine.Name;

            // Set the button action to assign the train to the line, also copy loop variable for lambda capture
            var subwayLineCopy = subwayLine;
            item.GetComponent<Button>().onClick.AddListener(() => {
                subwayLineManager.AddTrainInstanceToSubwayLine(trainInstance, subwayLineCopy);
                SubwayTrainWindowManager.Instance.RepopulateScrollView();
                gameObject.SetActive(false);
            });
        }

        // Shift the UI panel lower by the calculated height
        rectTransform.position = new Vector2(location.x, location.y - totalHeight * transform.lossyScale.y);
    }
}
