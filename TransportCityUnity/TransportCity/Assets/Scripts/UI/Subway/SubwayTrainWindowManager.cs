﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static Constants;

public class SubwayTrainWindowManager : MonoBehaviour
{
    // Various GameObjects from the UI
    public GameObject contentPanel;
    public GameUIController controller;
    public GameObject window;
    public SubwayLineAssignSelectionWindowController lineAssignSelectionWindow;

    public SubwayInstanceManager instanceManager;
    public SubwayLineManager lineManager;
        
    /// <summary>
    /// The singleton instance of this class
    /// </summary>
    public static SubwayTrainWindowManager Instance { get; private set; }

    // A prefab of a UI panel for each line
    private GameObject listViewItem;

    // UI API reference
    private UI_API uiApi;

    // Rect transform of the window
    private RectTransform rectTransform;

    // A cache of references for updating the health texts of subway trains
    private List<(SubwayTrain, Text)> healthTextsCache;

    // A cache of name change controllers
    private List<SubwayInstanceNameChangeController> nameChangeControllersCache;

    private void Awake()
    {
        // Singleton
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        uiApi = UI_API.GetInstance();
        listViewItem = Resources.Load<GameObject>($"Prefabs/UI/Subway/SubwayInstanceItem");
        rectTransform = GetComponent<RectTransform>();
    }

    private void Update()
    {
        // Update all health texts on the screen
        UpdateAllHealthTexts();

        // Hide the window if the user left-clicks outside
        if (Input.GetMouseButtonDown(0) &&
            !uiApi.MenuUp &&
            !uiApi.SubwayAssignWindowUp &&
            gameObject.activeSelf &&
            !RectTransformUtility.RectangleContainsScreenPoint(
                rectTransform,
                Input.mousePosition,
                null)
            )
        {
            gameObject.SetActive(false);
        }
    }

    private void OnEnable()
    {
        RepopulateScrollView();
    }

    /// <summary>
    /// Delete all scroll view elements and create new ones
    /// </summary>
    public void RepopulateScrollView()
    {
        foreach (Transform child in contentPanel.transform)
        {
            Destroy(child.gameObject);
        }
        PopulateScrollView();
    }

    public void EnableNamesEditing()
    {
        foreach (SubwayInstanceNameChangeController nameChangeController in nameChangeControllersCache)
        {
            nameChangeController.OtherEditing = false;
        }
    }

    public void DisableNamesEditing()
    {
        foreach (SubwayInstanceNameChangeController nameChangeController in nameChangeControllersCache)
        {
            nameChangeController.OtherEditing = true;
        }
    }

    /// <summary>
    /// A handler for the + button in the top bar
    /// </summary>
    public void AddTrainButton()
    {
        if (subwayTrainCost > MoneyManager.instance.Money)
        {
            MoneyManager.instance.ThrowNotEnoughMoneyPopUp();
            return;
        }
        MoneyManager.instance.SpendMoneyOneTime(subwayTrainCost, $"One subway train bought for ${subwayTrainCost}.");

        instanceManager.CreateNewSubwayTrainInstance();
        RepopulateScrollView();
    }

    /// <summary>
    /// A handler for the "close subway trains window" button
    /// </summary>
    public void WindowCloseButton()
    {
        window.SetActive(false);
    }

    // Update all health texts in the open window
    private void UpdateAllHealthTexts()
    {
        if (healthTextsCache == null)
        {
            return;
        }

        foreach ((SubwayTrain train, Text text) in healthTextsCache)
        {
            float health = train.GetHealth();
            // Same color as all the text in the window (= 50/256)
            Color textColor = new Color(0.1953125f, 0.1953125f, 0.1953125f);

            // If the health is below zero or zero, round it to zero and set the text color to red
            if (health <= 0)
            {
                health = 0;
                textColor = Color.red;
            }

            text.text = health.ToString("F2");
            text.color = textColor;
        }
    }

    // Create UI panels for the scroll view, one panel for each subway line
    private void PopulateScrollView()
    {
        healthTextsCache = new List<(SubwayTrain, Text)>();
        nameChangeControllersCache = new List<SubwayInstanceNameChangeController>();

        foreach (var trainInstance in instanceManager.GetSubwayTrainInstances())
        {
            // Instantiate panel
            GameObject go = Instantiate(listViewItem, contentPanel.transform);

            // Get text and button references from the newly instantiated list item
            Text nameText = go.transform.GetChild(0).gameObject.GetComponent<Text>();
            Text lineText = go.transform.GetChild(1).gameObject.GetComponent<Text>();
            Text healthText = go.transform.GetChild(2).gameObject.GetComponent<Text>();
            Button repairButton = go.transform.GetChild(3).gameObject.GetComponent<Button>();
            Button sellButton = go.transform.GetChild(4).gameObject.GetComponent<Button>();
            Button unassignButton = go.transform.GetChild(5).gameObject.GetComponent<Button>();
            Button assignButton = go.transform.GetChild(6).gameObject.GetComponent<Button>();

            // Set name text and line text
            nameText.text = trainInstance.name;
            lineText.text = trainInstance.line == null ? "None" : trainInstance.line.Name;

            // Add the repair text to the cache to be updated via Update
            if (trainInstance.train != null)
            {
                healthTextsCache.Add((trainInstance.train, healthText));
            }
            else
            {
                healthText.text = "";
            }

            SubwayInstanceNameChangeController nameChangeController = go.GetComponent<SubwayInstanceNameChangeController>();
            nameChangeController.SubwayTrainInstance = trainInstance;
            nameChangeControllersCache.Add(nameChangeController);

            // Set buttons actions, copy SubwawyTrainInstance reference for lambda capture
            SubwayInstanceManager.SubwayTrainInstance trainInstanceCopy = trainInstance;

            // Set up button on click listneres via lambdas
            repairButton.onClick.AddListener(() => {
                if (trainInstanceCopy.train == null)
                {
                    return;
                }

                int cost = (int)(subwayTrainCost * repairFromCostMultiplier * (1 - (trainInstanceCopy.train.GetHealth() / 100f)));
                if (cost > MoneyManager.instance.Money)
                {
                    MoneyManager.instance.ThrowNotEnoughMoneyPopUp();
                    return;
                }
                MoneyManager.instance.SpendMoneyFlow(cost);
                trainInstanceCopy.train.Repair();
            });

            sellButton.onClick.AddListener(() => {
                int cost = (int)(subwayTrainCost * sellFromCostMultiplier);
                MoneyManager.instance.GainMoneyOneTime(cost, $"Sold one subway train for {cost}$.");

                instanceManager.DeleteTrainInstance(trainInstanceCopy);
                RepopulateScrollView();
            });

            unassignButton.onClick.AddListener(() => {
                trainInstanceCopy.UnassignFromLine();
                RepopulateScrollView();
            });

            assignButton.onClick.AddListener(() => {
                // Calculate some numbers for the selection window update method call
                RectTransform buttonRectTransform = assignButton.GetComponent<RectTransform>();
                Vector3 buttonPosition = buttonRectTransform.position;
                Rect buttonRect = buttonRectTransform.rect;
                Vector3 worldUiScale = assignButton.transform.lossyScale;

                // Update the selection window
                lineAssignSelectionWindow.MoveToLocationAndUpdate(
                    new Vector2(
                        buttonPosition.x + (buttonRect.width / 2) * worldUiScale.x,
                        buttonPosition.y + (buttonRect.height / 2) * worldUiScale.y
                    ),
                    trainInstanceCopy
                );
            });
        }

        // At the end, update all health texts
        UpdateAllHealthTexts();
    }
}
