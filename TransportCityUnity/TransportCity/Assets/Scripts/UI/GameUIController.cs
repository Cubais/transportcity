﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameUIController : MonoBehaviour
{
    // UI API reference
    public UI_API uiApi;

    // Various references to UI GameObjects
    public GameObject smallPanel;
    public GameObject bigPanel;
    public GameObject buttonGroup1;
    public GameObject buttonGroup2;
    public GameObject buttonGroup3;
    public GameObject buttonGroup4;
    public GameObject statisticsPanel;
    public GameObject statisticsTriggerOnButton;
    public Slider carUsageSlider, happinessSlider;
    public Text timeText, moneyText;
    public GameObject busLinesWindow;
    public GameObject busManagerWindow;
    public GameObject subwayLinesWindow;
    public GameObject subwayTrainsWindow;
    public GameObject statisticsGraphsWindow;
    public GameObject menuPanel;
    public GameObject clickBlockPanelForMenu;
    public GameObject savePanel;
    public GameObject saveClickBlockPanel;
    public GameObject loadPanel;
    public GameObject loadClickBlockPanel;

    // How fast the lower panel rolls up and down
    public float panelRollSpeed;

    // Time manager for pausing the game with escape
    public TimeManager timeManager;

    // Enums for lower UI bar status
    private LowerBarState lowerBarState;
    private MovingState movingState;

    // The Y coordinate of the lower panel in its rolled down or up state
    private float bigPanelLowY;
    private float bigPanelHighY;

    // Current position of the lower panel between 0.0 and 1.0
    private float curPos = 0f;

    // Transforms of the two panels that make up the lower panel
    private RectTransform bigPanelTransform;
    private RectTransform smallPanelTransform;

    // The state of the lower bar which says which four buttones are selected
    enum LowerBarState
    {
         CLOSED,
         ONE,
         TWO,
         THREE,
         FOUR
    }

    // The moving state of the lower bar
    enum MovingState
    {
        MOVING_UP,
        MOVING_DOWN,
        NOT_MOVING
    }

    private void Start()
    {
        lowerBarState = LowerBarState.CLOSED;
        movingState = MovingState.NOT_MOVING;

        // Grab the transforms in advance
        bigPanelTransform = bigPanel.GetComponent<RectTransform>();
        smallPanelTransform = smallPanel.GetComponent<RectTransform>();

        // Calculate the Y positions of the lower panel in its rolled up and down state
        bigPanelLowY = bigPanelTransform.anchoredPosition.y;
        bigPanelHighY = bigPanelLowY + bigPanelTransform.rect.height;
    }

    private void Update()
    {
        // Toggle menu when the player presses escape and is not currently editing a name
        // If a pop up window is up, it will be closed instead
        if (Input.GetKeyDown(KeyCode.Escape) && !uiApi.NameEditing)
        {
            // If a pop up window is up, close all pop up windows, else show or hide the menu
            if (uiApi.PopUpWindowUp)
            {
                PopUpWindowManager.Instance.CloseAllPopUpWindows();
            }
            else if (savePanel.activeSelf)
            {
                savePanel.SetActive(false);
                saveClickBlockPanel.SetActive(false);
            }
            else if (loadPanel.activeSelf)
            {
                loadPanel.SetActive(false);
                loadClickBlockPanel.SetActive(false);
            }
            else
            {
                // Pause/Unpause game
                if (menuPanel.activeSelf)
                {
                    timeManager.UnpauseGame();
                }
                else
                {
                    timeManager.PauseGame();
                }

                menuPanel.SetActive(!menuPanel.activeSelf);
                clickBlockPanelForMenu.SetActive(!clickBlockPanelForMenu.activeSelf);
            }
        }

        // Take care of moving the lower bar when needed
        if (movingState == MovingState.NOT_MOVING)
        {
            return;
        }

        // Calculate how much the lower bar should move
        float curPosDelta = 0;
        if (movingState == MovingState.MOVING_UP)
        {
            curPosDelta = panelRollSpeed * Time.deltaTime;
        }
        else if (movingState == MovingState.MOVING_DOWN)
        {
            curPosDelta = -panelRollSpeed * Time.deltaTime;
        }

        // Calculate the new position, Clamp it and find out whether we should stop moving the lower bar
        curPos += curPosDelta;
        curPos = Mathf.Clamp(curPos, 0f, 1f);
        if (curPos == 0f || curPos == 1f)
        {
            movingState = MovingState.NOT_MOVING;
        }

        // Actually move the lower panel
        float newBigPanelY = Mathf.Lerp(bigPanelLowY, bigPanelHighY, curPos);
        bigPanelTransform.anchoredPosition = new Vector2(bigPanelTransform.anchoredPosition.x, newBigPanelY);
    }

    /// <summary>
    /// The handler for the first button in the smaller lower bar
    /// </summary>
    public void Button1()
    {
        if (!isUiActive())
        {
            return;
        }
        
        if (lowerBarState == LowerBarState.ONE)
        {
            RollPanelDown();
            return;
        }

        DisableAllBut(buttonGroup1);

        if (lowerBarState == LowerBarState.CLOSED)
        {
            RollPanelUp();
        }

        lowerBarState = LowerBarState.ONE;
    }

    /// <summary>
    /// The handler for the second button in the smaller lower bar
    /// </summary>
    public void Button2()
    {
        if (!isUiActive())
        {
            return;
        }

        if (lowerBarState == LowerBarState.TWO)
        {
            RollPanelDown();
            return;
        }

        DisableAllBut(buttonGroup2);

        if (lowerBarState == LowerBarState.CLOSED)
        {
            RollPanelUp();
        }

        lowerBarState = LowerBarState.TWO;
    }

    /// <summary>
    /// The handler for the third button in the smaller lower bar
    /// </summary>
    public void Button3()
    {
        if (!isUiActive())
        {
            return;
        }

        if (lowerBarState == LowerBarState.THREE)
        {
            RollPanelDown();
            return;
        }

        DisableAllBut(buttonGroup3);

        if (lowerBarState == LowerBarState.CLOSED)
        {
            RollPanelUp();
        }

        lowerBarState = LowerBarState.THREE;
    }

    /// <summary>
    /// The handler for the fourth button in the smaller lower bar
    /// </summary>
    public void Button4()
    {
        if (!isUiActive())
        {
            return;
        }

        if (lowerBarState == LowerBarState.FOUR)
        {
            RollPanelDown();
            return;
        }

        DisableAllBut(buttonGroup4);

        if (lowerBarState == LowerBarState.CLOSED)
        {
            RollPanelUp();
        }

        lowerBarState = LowerBarState.FOUR;
    }

    /// <summary>
    /// A handler for the first button in the first button group in the bigger lower bar
    /// </summary>
    public void LowerButton1_1()
    {
        if (!isUiActive())
        {
            return;
        }

        busLinesWindow.SetActive(true);
    }

    /// <summary>
    /// A handler for the second button in the first button group in the bigger lower bar
    /// </summary>
    public void LowerButton1_2()
    {
        if (!isUiActive())
        {
            return;
        }

        busManagerWindow.SetActive(true);
    }

    /// <summary>
    /// A handler for the third button in the first button group in the bigger lower bar
    /// </summary>
    public void LowerButton1_3()
    {
        if (!isUiActive())
        {
            return;
        }
    }

    /// <summary>
    /// A handler for the fourth button in the first button group in the bigger lower bar
    /// </summary>
    public void LowerButton1_4()
    {
        if (!isUiActive())
        {
            return;
        }
    }

    /// <summary>
    /// A handler for the first button in the second button group in the bigger lower bar
    /// </summary>
    public void LowerButton2_1()
    {
        if (!isUiActive())
        {
            return;
        }

        subwayLinesWindow.SetActive(true);
    }

    /// <summary>
    /// A handler for the second button in the second button group in the bigger lower bar
    /// </summary>
    public void LowerButton2_2()
    {
        if (!isUiActive())
        {
            return;
        }

        subwayTrainsWindow.SetActive(true);
    }

    /// <summary>
    /// A handler for the third button in the second button group in the bigger lower bar
    /// </summary>
    public void LowerButton2_3()
    {
        if (!isUiActive())
        {
            return;
        }
    }

    /// <summary>
    /// A handler for the fourth button in the second button group in the bigger lower bar
    /// </summary>
    public void LowerButton2_4()
    {
        if (!isUiActive())
        {
            return;
        }
    }

    /// <summary>
    /// A handler for the first button in the third button group in the bigger lower bar
    /// </summary>
    public void LowerButton3_1()
    {
        if (!isUiActive())
        {
            return;
        }

        statisticsGraphsWindow.SetActive(true);
    }

    /// <summary>
    /// A handler for the second button in the third button group in the bigger lower bar
    /// </summary>
    public void LowerButton3_2()
    {
        if (!isUiActive())
        {
            return;
        }
    }

    /// <summary>
    /// A handler for the third button in the third button group in the bigger lower bar
    /// </summary>
    public void LowerButton3_3()
    {
        if (!isUiActive())
        {
            return;
        }
    }

    /// <summary>
    /// A handler for the fourth button in the third button group in the bigger lower bar
    /// </summary>
    public void LowerButton3_4()
    {
        if (!isUiActive())
        {
            return;
        }
    }

    /// <summary>
    /// A handler for the "show statistics" button
    /// </summary>
    public void StatisticsTriggerOnButton()
    {
        if (!isUiActive())
        {
            return;
        }
        statisticsTriggerOnButton.SetActive(false);
        statisticsPanel.SetActive(true);
    }

    /// <summary>
    /// A handler for the "hide statistics" button
    /// </summary>
    public void StatisticsTriggerOffButton()
    {
        if (!isUiActive())
        {
            return;
        }
        statisticsPanel.SetActive(false);
        statisticsTriggerOnButton.SetActive(true);
    }

    /// <summary>
    /// A handler for the first statistics button
    /// </summary>
    public void StatisticsButton1()
    {
        if (!isUiActive())
        {
            return;
        }
        uiApi.ServeStatisticsCallback(1);
    }

    /// <summary>
    /// A handler for the second statistics button
    /// </summary>
    public void StatisticsButton2()
    {
        if (!isUiActive())
        {
            return;
        }
        uiApi.ServeStatisticsCallback(2);
    }

    /// <summary>
    /// A handler for the third statistics button
    /// </summary>
    public void StatisticsButton3()
    {
        if (!isUiActive())
        {
            return;
        }
        uiApi.ServeStatisticsCallback(3);
    }

    /// <summary>
    /// A handler for the fourth statistics button
    /// </summary>
    public void StatisticsButton4()
    {
        if (!isUiActive())
        {
            return;
        }
        uiApi.ServeStatisticsCallback(4);
    }

    /// <summary>
    /// A handler for the first time button
    /// </summary>
    public void TimeButton0()
    {
        if (!isUiActive())
        {
            return;
        }
        uiApi.ServeTimeCallback(0);
    }

    /// <summary>
    /// A handler for the second time button
    /// </summary>
    public void TimeButton1()
    {
        if (!isUiActive())
        {
            return;
        }
        uiApi.ServeTimeCallback(1);
    }

    /// <summary>
    /// A handler for the third time button
    /// </summary>
    public void TimeButton2()
    {
        if (!isUiActive())
        {
            return;
        }
        uiApi.ServeTimeCallback(2);
    }

    /// <summary>
    /// A handler for the fourth time button
    /// </summary>
    public void TimeButton3()
    {
        if (!isUiActive())
        {
            return;
        }
        uiApi.ServeTimeCallback(3);
    }

    /// <summary>
    /// A handler for the bulldozer button
    /// </summary>
    public void BulldozerButton()
    {
        if (!isUiActive())
        {
            return;
        }
        uiApi.ServeBulldozerCallback();
    }

    /// <summary>
    /// A method for setting the happiness slider
    /// </summary>
    public void SetHappinessSliderValue(float value)
    {
        if (value < happinessSlider.minValue || value > happinessSlider.maxValue)
        {
            throw new UI_API.UIException($"happiness slider value must be between {happinessSlider.minValue} and {happinessSlider.maxValue}: value is {value}");
        }

        happinessSlider.value = value;
        AdjustSliderColor(happinessSlider, false);
    }

    /// <summary>
    /// A method for setting the car usage slider
    /// </summary>
    public void SetCarUsageSliderValue(float value)
    {
        if (value < carUsageSlider.minValue || value > carUsageSlider.maxValue)
        {
            throw new UI_API.UIException($"car usage slider value must be between {carUsageSlider.minValue} and {carUsageSlider.maxValue}: value is {value}");
        }
        
        carUsageSlider.value = value;
        AdjustSliderColor(carUsageSlider, true);
    } 
    
    /// <summary>
    /// Ajusts color of the slider
    /// </summary>
    /// <param name="slider">Slider to adjust</param>
    /// <param name="reversed">Green to red?</param>
    private void AdjustSliderColor(Slider slider, bool reversed)
    {
        if (!slider)
            return;

        // Green color in HSV (100, 255, 255)
        var greenColor = 100 / 255f;
        var sliderFill = slider.fillRect.gameObject.GetComponent<Image>();

        if (reversed)
        {
            sliderFill.color = Color.HSVToRGB(greenColor - slider.value * greenColor, 1, 1);
        }
        else
        {
            sliderFill.color = Color.HSVToRGB(slider.value * greenColor, 1, 1);
        }
    }

    /// <summary>
    /// Can be used to slowly move slider
    /// </summary>
    /// <param name="slider">Slider to change</param>
    /// <param name="beginValue">Start value of slider</param>
    /// <param name="endValue">Target value of slider</param>
    /// <param name="fillSpeed">Speed at which slider should move</param>
    /// <returns></returns>
    IEnumerator SliderValue(Slider slider, float beginValue, float endValue, float fillSpeed)
    {
        if (slider.value != endValue)
        {
            slider.value = Mathf.MoveTowards(beginValue, endValue, Time.deltaTime * fillSpeed);
            yield return null;
        }
    }

    /// <summary>
    /// A method for setting the time text
    /// </summary>
    public void SetTimeText(string text)
    {
        timeText.text = text;
    }

    /// <summary>
    /// A method for setting the money text
    /// </summary>
    public void SetMoneyText(string text)
    {
        moneyText.text = text;
    }

    // Sets the variables to make the lower panel roll down
    private void RollPanelDown()
    {
        movingState = MovingState.MOVING_DOWN;
        lowerBarState = LowerBarState.CLOSED;
        curPos = 1.0f;
    }

    // Sets the variables to make the lower panel roll up
    private void RollPanelUp()
    {
        movingState = MovingState.MOVING_UP;
        curPos = 0.0f;
    }

    // Disables all but the correct button group in the lower panel
    private void DisableAllBut(GameObject buttonGroup)
    {
        buttonGroup1.SetActive(false);
        buttonGroup2.SetActive(false);
        buttonGroup3.SetActive(false);
        buttonGroup4.SetActive(false);

        buttonGroup.SetActive(true);
    }

    // Returns whether the UI should be active, which means that
    // the player is not currently editing a bus line or something else
    private bool isUiActive()
    {
        return !uiApi.LineEditing;
    }

}
