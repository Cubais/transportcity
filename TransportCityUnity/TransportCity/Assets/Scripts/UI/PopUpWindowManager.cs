﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopUpWindowManager : MonoBehaviour
{
    public Transform oneButtonPopUpWindow, oneButtonClickBlockPanel, twoButtonPopUpWindow, twoButtonClickBlockPanel;

    /// <summary>
    /// Singleton instance
    /// </summary>
    public static PopUpWindowManager Instance { get; private set; }

    /// <summary>
    /// Tells whether a pop up window is currently open
    /// </summary>
    public bool IsActive { get => oneButtonPopUpWindow.gameObject.activeSelf || twoButtonPopUpWindow.gameObject.activeSelf; }

    private Text oneButtonWindowText, twoButtonWindowText;
    private Button oneButtonWindowButton, twoButtonWindowConfirmButton, twoButtonWindowDeclineButton;
    private Text oneButtonWindowButtonText, twoButtonWindowConfirmButtonText, twoButtonWindowDeclineButtonText;

    private void Awake()
    {
        // Singleton assignment
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        // Ignore this in review pls, it is just getting references to valid components
        oneButtonWindowText = oneButtonPopUpWindow.GetChild(0).GetComponent<Text>();
        oneButtonWindowButton = oneButtonPopUpWindow.GetChild(1).GetChild(0).GetComponent<Button>();
        oneButtonWindowButtonText = oneButtonPopUpWindow.GetChild(1).GetChild(0).GetChild(0).GetComponent<Text>();
        twoButtonWindowText = twoButtonPopUpWindow.GetChild(0).GetComponent<Text>();
        twoButtonWindowConfirmButton = twoButtonPopUpWindow.GetChild(1).GetChild(0).GetComponent<Button>();
        twoButtonWindowDeclineButton = twoButtonPopUpWindow.GetChild(1).GetChild(1).GetComponent<Button>();
        twoButtonWindowConfirmButtonText = twoButtonPopUpWindow.GetChild(1).GetChild(0).GetChild(0).GetComponent<Text>();
        twoButtonWindowDeclineButtonText = twoButtonPopUpWindow.GetChild(1).GetChild(1).GetChild(0).GetComponent<Text>();
    }

    /// <summary>
    /// Show a pop up window with one button
    /// The button text is "OK" by default
    /// </summary>
    /// <param name="windowText">The text to show above the button</param>
    /// <param name="buttonHandler">A handler for the button click</param>
    public void EnableOneButtonWindow(string windowText, UI_API.ButtonHandler buttonHandler)
    {
        EnableOneButtonWindow(windowText, "OK", buttonHandler);
    }

    /// <summary>
    /// Show a pop up window with one button
    /// </summary>
    /// <param name="windowText">The text to show above the button</param>
    /// <param name="buttonText">The text to show on the button</param>
    /// <param name="buttonHandler">A handler for the button click</param>
    public void EnableOneButtonWindow(string windowText, string buttonText, UI_API.ButtonHandler buttonHandler)
    {
        if (IsActive)
        {
            throw new UI_API.UIException("A pop up window is already active");
        }

        // Enable GameObjects
        oneButtonPopUpWindow.gameObject.SetActive(true);
        oneButtonClickBlockPanel.gameObject.SetActive(true);

        // Change window text
        oneButtonWindowText.text = windowText;

        // Set up the button
        oneButtonWindowButtonText.text = buttonText;
        oneButtonWindowButton.onClick.RemoveAllListeners();
        oneButtonWindowButton.onClick.AddListener(() => buttonHandler());
        oneButtonWindowButton.onClick.AddListener(() => {
            // Add a listener to disable the pop up window afterwards
            oneButtonPopUpWindow.gameObject.SetActive(false);
            oneButtonClickBlockPanel.gameObject.SetActive(false);
        });
    }

    /// <summary>
    /// Show a pop up window with two buttons, one for confirm and one for decline
    /// The confirm button text is "Yes" by default
    /// The decline button text is "No" by default
    /// </summary>
    /// <param name="windowText">The text to show above the buttons</param>
    /// <param name="confirmButtonText">The text of the confirm button</param>
    /// <param name="confirmButtonHandler">A handler for the confirm button</param>
    /// <param name="declineButtonText">The text of the decline button</param>
    /// <param name="declineButtonHandler">A handler for the decline button</param>
    public void EnableTwoButtonWindow(string windowText, UI_API.ButtonHandler confirmButtonHandler, UI_API.ButtonHandler declineButtonHandler)
    {
        EnableTwoButtonWindow(windowText, "Yes", confirmButtonHandler, "No", declineButtonHandler);
    }

    /// <summary>
    /// Show a pop up window with two buttons, one for confirm and one for decline
    /// </summary>
    /// <param name="windowText">The text to show above the buttons</param>
    /// <param name="confirmButtonText">The text of the confirm button</param>
    /// <param name="confirmButtonHandler">A handler for the confirm button</param>
    /// <param name="declineButtonText">The text of the decline button</param>
    /// <param name="declineButtonHandler">A handler for the decline button</param>
    public void EnableTwoButtonWindow(
        string windowText, 
        string confirmButtonText, 
        UI_API.ButtonHandler confirmButtonHandler, 
        string declineButtonText, 
        UI_API.ButtonHandler declineButtonHandler
    )
    {
        if (IsActive)
        {
            throw new UI_API.UIException("A pop up window is already active");
        }

        // Enable GameObjects
        twoButtonPopUpWindow.gameObject.SetActive(true);
        twoButtonClickBlockPanel.gameObject.SetActive(true);

        // Change window text
        twoButtonWindowText.text = windowText;

        // Set up the confirm button
        twoButtonWindowConfirmButtonText.text = confirmButtonText;
        twoButtonWindowConfirmButton.onClick.RemoveAllListeners();
        twoButtonWindowConfirmButton.onClick.AddListener(() => confirmButtonHandler());
        twoButtonWindowConfirmButton.onClick.AddListener(() => {
            // Add a listener to disable the pop up window afterwards
            twoButtonPopUpWindow.gameObject.SetActive(false);
            twoButtonClickBlockPanel.gameObject.SetActive(false);
        });

        // Set up the decline button
        twoButtonWindowDeclineButtonText.text = declineButtonText;
        twoButtonWindowDeclineButton.onClick.RemoveAllListeners();
        twoButtonWindowDeclineButton.onClick.AddListener(() => declineButtonHandler());
        twoButtonWindowDeclineButton.onClick.AddListener(() => {
            // Add a listener to disable the pop up window afterwards
            twoButtonPopUpWindow.gameObject.SetActive(false);
            twoButtonClickBlockPanel.gameObject.SetActive(false);
        });
    }
    
    /// <summary>
    /// Closes all currently open pop up windows
    /// </summary>
    public void CloseAllPopUpWindows()
    {
        oneButtonPopUpWindow.gameObject.SetActive(false);
        oneButtonClickBlockPanel.gameObject.SetActive(false);
        twoButtonPopUpWindow.gameObject.SetActive(false);
        twoButtonClickBlockPanel.gameObject.SetActive(false);
    }
}
