﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

public class UI_API : MonoBehaviour
{
    /// <summary>
    /// Importance level of a notification
    /// </summary>
    public enum NotificationLevel
    {
        INFO,
        WARN,
        CRITICAL
    }

    /// <summary>
    /// A universal exception with everything related to UI
    /// </summary>
    public class UIException : System.Exception {
        public UIException(string message) : base(message) { }
    }

    /// <summary>
    /// A handler delegate for group buttons
    /// </summary>
    /// <param name="buttonIndex">Index of the button in the group</param>
    public delegate void GroupButtonsHandler(int buttonIndex);

    /// <summary>
    /// A handler delegate for single buttons
    /// </summary>
    public delegate void ButtonHandler();

    /// <summary>
    /// A handler delegate for editing bus lines
    /// </summary>
    /// <param name="busLine"></param>
    public delegate void BusLineEditButtonHandler(BusLineManager.BusLine busLine);

    /// <summary>
    /// A handler delegate for editing subway lines
    /// </summary>
    /// <param name="busLine"></param>
    public delegate void SubwayLineEditButtonHandler(SubwayLineManager.SubwayLine busLine);

    /// <summary>
    /// Tells whether a line is currently being edited
    /// </summary>
    public bool LineEditing { get; set; }

    /// <summary>
    /// Tells whether the bus instance window currently shows bus types or not
    /// Only valid when the bus instance is currently active
    /// </summary>
    public bool BusInstanceWindowShowingBusTypes { get; set; }

    /// <summary>
    /// Tells whether the escape menu is currently displayed
    /// </summary>
    public bool MenuUp { get => menuPanel.activeSelf; }

    /// <summary>
    /// Tells whether a pop up window is active
    /// </summary>
    public bool PopUpWindowUp { get => PopUpWindowManager.Instance.IsActive; }

    /// <summary>
    /// Tells whether the bus line assign selection window is up
    /// </summary>
    public bool BusAssignWindowUp { get => busAssignPanel.activeSelf; }

    /// <summary>
    /// Tells whether the subway train line assign selection window is up
    /// </summary>
    public bool SubwayAssignWindowUp { get => subwayAssignPanel.activeSelf; }

    /// <summary>
    /// Tells whether a scrollable window is currently open
    /// Useful for disabling scrolling when these windows are up
    /// </summary>
    public bool ScrollableWindowUp { get => busLineWindow.activeSelf || busInstanceWindow.activeSelf || subwayLineWindow.activeSelf || subwayTrainWindow.activeSelf; }

    /// <summary>
    /// Whether a name is being edited. This is used for disabling camera movement when
    /// the player is editing a name.
    /// </summary>
    public bool NameEditing { get; set; }

    public GameUIController controller;
    public NotificationManager notificationManager;
    public GameObject menuPanel;
    public GameObject busAssignPanel;
    public GameObject subwayAssignPanel;
    public GameObject busLineWindow, busInstanceWindow, subwayLineWindow, subwayTrainWindow;

    // Handlers for events
    private event GroupButtonsHandler statsHandler;
    private event GroupButtonsHandler timeHandler;
    private event ButtonHandler bulldozerHandler;
    private event BusLineEditButtonHandler busPathEditHandler;
    private event ButtonHandler busPathEditEndHandler;
    private event SubwayLineEditButtonHandler subwayPathEditHandler;
    private event ButtonHandler subwayPathEditEndHandler;

    // Current money and money flow for display in UI
    private int currentMoney;
    private int currentMoneyFlow;

    // Singleton instance
    private static UI_API instance;

    void Awake()
    {
        // Singleton assignment
        instance = this;
    }

    /// <summary>
    /// Get the singleton instance of this class
    /// </summary>
    /// <returns>A singleton instance</returns>
    public static UI_API GetInstance()
    {
        return instance;
    }

    /// <summary>
    /// Display a notification in the lower left part of the screen
    /// </summary>
    /// <param name="text">The text of the notification</param>
    /// <param name="level">The importance level of the notification (color)</param>
    public void DisplayNotification(string text, NotificationLevel level)
    {
        notificationManager.AddNotification(text, level);
    }

    /// <summary>
    /// Register a handler for statistics buttons
    /// </summary>
    /// <param name="function">The handler to register</param>
    public void RegisterForStatisticsButtons(GroupButtonsHandler function)
    {
        statsHandler += function;
    }

    /// <summary>
    /// Register a handler for bulldozer button
    /// </summary>
    /// <param name="function">The handler to register</param>
    public void RegisterForBulldozer(ButtonHandler function)
    {
        bulldozerHandler += function;
    }

    /// <summary>
    /// Register a handler for time buttons
    /// </summary>
    /// <param name="function">The handler to register</param>
    public void RegisterForTimeButtons(GroupButtonsHandler function)
    {
        timeHandler += function;
    }

    /// <summary>
    /// Register a handler for bus line path edit buttons
    /// </summary>
    /// <param name="function">The handler to register</param>
    public void RegisterForBusPathEditButtons(BusLineEditButtonHandler function)
    {
        busPathEditHandler += function;
    }

    /// <summary>
    /// Register a handler for bus line path edit end buttons
    /// </summary>
    /// <param name="function">The handler to register</param>
    public void RegisterForBusPathEditEndButton(ButtonHandler function)
    {
        busPathEditEndHandler += function;
    }

    /// <summary>
    /// Register a handler for subway line path edit buttons
    /// </summary>
    /// <param name="function">The handler to register</param>
    public void RegisterForSubwayPathEditButtons(SubwayLineEditButtonHandler function)
    {
        subwayPathEditHandler += function;
    }

    /// <summary>
    /// Register a handler for bus line path edit end buttons
    /// </summary>
    /// <param name="function">The handler to register</param>
    public void RegisterForSubwayPathEditEndButton(ButtonHandler function)
    {
        subwayPathEditEndHandler += function;
    }

    /// <summary>
    /// Set the value of the happiness slider in the UI
    /// </summary>
    /// <param name="value">The value between -1.0 and 1.0</param>
    public void SetHappinessSlider(float value)
    {
        controller.SetHappinessSliderValue(value);
    }

    /// <summary>
    /// Set the value of the car usage slider in the UI
    /// </summary>
    /// <param name="value">The value between -1.0 and 1.0</param>
    public void SetCarUsageSlider(float value)
    {
        controller.SetCarUsageSliderValue(value);
    }

    /// <summary>
    /// Set the UI money text
    /// </summary>
    /// <param name="value">The number to be displayed, is converted to int</param>
    public void SetCurrentMoney(float value)
    {
        currentMoney = (int)value;
        UpdateMoneyText();
    }

    /// <summary>
    /// Set the UI money flow text
    /// </summary>
    /// <param name="value">The number to be displayed, is converted to int</param>
    public void SetCurrentMoneyFlow(float value)
    {
        currentMoneyFlow = (int)value;
        UpdateMoneyText();
    }

    /// <summary>
    /// Set the time text in the UI
    /// </summary>
    /// <param name="value">The text to be displayed</param>
    public void SetTimeText(string value)
    {
        controller.SetTimeText(value);
    }

    /// <summary>
    /// Invoke the handler of the statistics button event
    /// </summary>
    /// <param name="index">The index of the button pressed</param>
    public void ServeStatisticsCallback(int index)
    {
        if (statsHandler == null)
        {
            throw new UIException("You have to register a statistics handler first");
        }

        statsHandler(index);
    }

    /// <summary>
    /// Invoke the handler of the time button event
    /// </summary>
    /// <param name="index">The index of the button pressed</param>
    public void ServeTimeCallback(int index)
    {
        if (timeHandler == null)
        {
            throw new UIException("You have to register a time handler first");
        }

        timeHandler(index);
    }

    /// <summary>
    /// Invoke the handler of the bulldozer button event
    /// </summary>
    public void ServeBulldozerCallback()
    {
        if (bulldozerHandler == null)
        {
            throw new UIException("You have to register a bulldozer handler first");
        }

        bulldozerHandler();
    }

    /// <summary>
    /// Invoke the handler of the bus line path edit button event
    /// </summary>
    /// <param name="busLine">The bus line object to be changed</param>
    public void ServeBusPathEditCallback(BusLineManager.BusLine busLine)
    {
        if (busPathEditHandler == null)
        {
            throw new UIException("You have to register a path edit handler first");
        }

        busPathEditHandler(busLine);
    }

    /// <summary>
    /// Invoke the handler of the bus line path edit end button event
    /// </summary>
    public void ServeBusPathEditEndCallback()
    {
        if (busPathEditEndHandler == null)
        {
            throw new UIException("You have to register a path edit end handler first");
        }

        busPathEditEndHandler();
    }

    /// <summary>
    /// Invoke the handler of the subway line path edit button event
    /// </summary>
    /// <param name="subwayLine">The subway line object to be changed</param>
    public void ServeSubwayPathEditCallback(SubwayLineManager.SubwayLine subwayLine)
    {
        if (subwayPathEditHandler == null)
        {
            throw new UIException("You have to register a path edit handler first");
        }

        subwayPathEditHandler(subwayLine);
    }

    /// <summary>
    /// Invoke the handler of the subway line path edit end button event
    /// </summary>
    public void ServeSubwayPathEditEndCallback()
    {
        if (subwayPathEditEndHandler == null)
        {
            throw new UIException("You have to register a path edit end handler first");
        }

        subwayPathEditEndHandler();
    }

    private void UpdateMoneyText()
    {
        // Set the color of the money text
        string currentMoneyTextColor = currentMoney >= 0 ? "green" : "red";
        // Add color tag for Unity rich text
        string currentMoneyText = $"<color={currentMoneyTextColor}>${currentMoney}</color>";

        // The same for money flow, but we add + prefix too if needed (- is automatically included)
        string currentMoneyFlowTextColor = currentMoneyFlow >= 0 ? "green" : "red";
        string currentMoneyFlowPrefix = currentMoneyFlow >= 0 ? "+" : "";
        string currentMoneyFlowText = $"<color={currentMoneyFlowTextColor}>{currentMoneyFlowPrefix}${currentMoneyFlow}</color>";

        // Put it together
        string moneyText = $"{currentMoneyText} ({currentMoneyFlowText})";

        controller.SetMoneyText(moneyText);
    }
}
