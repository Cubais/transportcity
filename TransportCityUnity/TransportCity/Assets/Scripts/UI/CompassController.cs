﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompassController : MonoBehaviour
{
    public GameObject mainCamera;

    void Update()
    {
        transform.eulerAngles = new Vector3(
            transform.rotation.eulerAngles.x,
            transform.rotation.eulerAngles.y,
            mainCamera.transform.rotation.eulerAngles.y
        );
    }
}
