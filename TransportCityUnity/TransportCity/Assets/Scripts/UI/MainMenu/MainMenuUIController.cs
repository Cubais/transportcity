﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuUIController : MonoBehaviour
{
    // Some public UI elements for the controller to control
    public ConfirmWindowController confirmWindow;
    public GameObject mainButtonsPanel;
    public GameObject playButtonsPanel;
    public LoadGamePanelController loadGamePanel;
    public GameObject loadingScreen;

    /// <summary>
    /// A handler for the Play Game button click
    /// </summary>
    public void PlayButtonClick()
    {
        mainButtonsPanel.SetActive(false);
        playButtonsPanel.SetActive(true);
    }

    /// <summary>
    /// A handler for the Settings button click
    /// </summary>
    public void SettingsButtonClick()
    {

    }

    /// <summary>
    /// A handler for the About button click
    /// </summary>
    public void AboutButtonClick()
    {

    }

    /// <summary>
    /// A handler for the Quit Game button click
    /// </summary>
    public void QuitButtonClick()
    {
        // Ask the user if he really wants to quit
        confirmWindow.EnableAndSetUp(
            "Are you sure you want to exit the game?",
            () => {
                Debug.Log("exiting");
                Application.Quit();
            },
            () => {

            }
        );
    }

    /// <summary>
    /// A handler for the New Game button click
    /// </summary>
    public void NewGameButtonClick()
    {
        // Ask the user if he really wants to start a new game since it takes a long time
        confirmWindow.EnableAndSetUp(
            "Are you sure you want to start a new game?",
            () => {
                loadingScreen.SetActive(true);
                SceneManager.LoadScene("Assets/Scenes/GameScene.unity");
            },
            () => {

            }
        );
    }

    /// <summary>
    /// A handler for the Load Game button click
    /// </summary>
    public void LoadGameButtonClick()
    {
        playButtonsPanel.SetActive(false);
        loadGamePanel.gameObject.SetActive(true);
        loadGamePanel.ResetContent();
    }

    /// <summary>
    /// A handler for the Back button in the play menu click
    /// </summary>
    public void PlayMenuBackButtonClick()
    {
        playButtonsPanel.SetActive(false);
        mainButtonsPanel.SetActive(true);
    }

    /// <summary>
    /// A handler for the Back button in the load game menu click
    /// </summary>
    public void LoadMenuBackButtonClick()
    {
        loadGamePanel.gameObject.SetActive(false);
        playButtonsPanel.SetActive(true);
    }
}
