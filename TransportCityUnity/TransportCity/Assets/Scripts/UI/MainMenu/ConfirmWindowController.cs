﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// A controller for a generic Confirmation Window
/// </summary>
public class ConfirmWindowController : MonoBehaviour
{
    // Public UI elements for controlling their content
    public Text windowText;
    public GameObject clickBlockPanel;

    // Handlers for Yes/No clicks
    private UI_API.ButtonHandler confirmHandler;
    private UI_API.ButtonHandler declineHandler;

    // The RectTransform of the window
    private RectTransform rectTransform;

    private void Start()
    {
        rectTransform = GetComponent<RectTransform>();
    }

    private void Update()
    {
        // Hide the window if the user left-clicks outside
        if (Input.GetMouseButtonDown(0) &&
            gameObject.activeSelf &&
            !RectTransformUtility.RectangleContainsScreenPoint(
                rectTransform,
                Input.mousePosition,
                null)
            )
        {
            gameObject.SetActive(false);
            clickBlockPanel.SetActive(false);
        }
    }

    /// <summary>
    /// Display the confirmation window and set up its contents
    /// </summary>
    /// <param name="text">The text to be displayed above the yes/no buttons</param>
    /// <param name="ConfirmAction">The method that will be called when the yes button is pressed</param>
    /// <param name="DeclineAction">The method that will be called when the no button is pressed</param>
    public void EnableAndSetUp(string text, UI_API.ButtonHandler ConfirmAction, UI_API.ButtonHandler DeclineAction)
    {
        gameObject.SetActive(true);
        clickBlockPanel.SetActive(true);
        windowText.text = text;
        confirmHandler = ConfirmAction;
        declineHandler = DeclineAction;
    }

    /// <summary>
    /// A handler for the yes button click, disables the window and calls the handler for the yes button
    /// </summary>
    public void ConfirmButtonClick()
    {
        confirmHandler();
        gameObject.SetActive(false);
        clickBlockPanel.SetActive(false);
    }

    /// <summary>
    /// A handler for the no button click, disables the window and calls the handler for the no button
    /// </summary>
    public void DeclineButtonClick()
    {
        declineHandler();
        gameObject.SetActive(false);
        clickBlockPanel.SetActive(false);
    }
}
