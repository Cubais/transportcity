﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadGamePanelController : MonoBehaviour
{
    // Various UI elements references
    public GameObject loadItemPrefab;
    public GameObject contentPanel;
    public GameObject clickBlockPanel;
    public GameObject menuPanel;
    public GameObject menuClickBlockPanel;
    public GameObject loadingScreen;

    public SaveManager saveManager;

    private List<string> fileNames;
    private RectTransform rectTransform;
    private string fileToLoad = null;
    private AsyncOperation asyncOp = null;

    private void Start()
    {
        rectTransform = GetComponent<RectTransform>();
    }

    private void Update()
    {
        // Hide the window if the user left-clicks outside, but only in the Main Game scene
        // since this window and this script is shared with the Main Menu scene
        if (Input.GetMouseButtonDown(0) &&
            gameObject.activeSelf &&
            SceneManager.GetActiveScene().name == "GameScene" &&
            !RectTransformUtility.RectangleContainsScreenPoint(
                rectTransform,
                Input.mousePosition,
                null)
            )
        {
            clickBlockPanel.SetActive(false);
            gameObject.SetActive(false);
        }

        if (fileToLoad != null && (asyncOp == null || asyncOp.isDone))
        {
            if (asyncOp != null) { 
                saveManager.Awake();
                saveManager.Start();

                saveManager.roadGraphGeneration = GameObject.Find("RoadMapGeneration").GetComponent<RoadGraphGeneration>();
                saveManager.heightMapApplier = GameObject.Find("TerrainGeneration").GetComponent<HeightMapApplier>();
                saveManager.buildingCreator = GameObject.Find("RoadMapGeneration").GetComponent<BuildingCreator>();
                saveManager.zoneController = GameObject.Find("RoadMapGeneration").GetComponent<ZoneController>();
                saveManager.peopleManager = GameObject.Find("PeopleManager").GetComponent<PeopleManager>();
                saveManager.cityCouncil = GameObject.Find("CityCouncil").GetComponent<CityCouncil>();
                saveManager.loadingScreen = GameObject.Find("GameUI").transform.Find("LoadingScreenPanel").gameObject;
                saveManager.savingScreen = GameObject.Find("GameUI").transform.Find("SavingScreenPanel").gameObject;
            }

            saveManager.LoadGame(fileToLoad);
            clickBlockPanel.SetActive(false);
            gameObject.SetActive(false);
            if (menuPanel != null)
            {
                // We are not in the main menu, hide the in-game menu
                menuPanel.SetActive(false);
                menuClickBlockPanel.SetActive(false);
            }
            else
            {
                // We were in the main menu, hide the main menu UI
                transform.parent.gameObject.SetActive(false);
            }

            fileToLoad = null;
            asyncOp = null;
        }
    }

    /// <summary>
    /// Resets the content of the Load Game window
    /// </summary>
    public void ResetContent()
    {
        // Destroy all children
        foreach (Transform child in contentPanel.transform)
        {
            Destroy(child.gameObject);
        }

        // Reset filenames cache
        fileNames = new List<string>();

        // Create UI buttons for each save, also fill filenames cache
        foreach ((string fileName, DateTime lastEdit) in saveManager.EnumerateSaves())
        {
            GameObject go = Instantiate(loadItemPrefab, contentPanel.transform);
            Text fileNameText = go.transform.GetChild(0).GetComponent<Text>();
            Text lastEditText = go.transform.GetChild(1).GetComponent<Text>();

            fileNameText.text = fileName;
            lastEditText.text = lastEdit.ToString();

            fileNames.Add(fileName);
        }
    }

    /// <summary>
    /// The handler for the UI "Load Game" button
    /// </summary>
    public void LoadGameButton()
    {
        // Find out the index of the selected button and then
        // load the file name from the cache
        int i = 0;
        bool found = false;
        foreach (Transform child in contentPanel.transform)
        {
            if (child.GetComponent<LoadItem>().selected)
            {
                found = true;
                break;
            }
            i++;
        }

        if (!found)
        {
            return;
        }

        if (menuPanel == null)
        {
            // We are in the main menu, change scenes
            loadingScreen.SetActive(true);
            Canvas.ForceUpdateCanvases();
            DontDestroyOnLoad(transform.parent);
            DontDestroyOnLoad(saveManager);
            asyncOp = SceneManager.LoadSceneAsync("Assets/Scenes/GameScene.unity");
        }

        fileToLoad = fileNames[i];
    }
}
