﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadItem : MonoBehaviour
{
    // Is this one item selected
    public bool selected = false;

    // The color that the selected item has
    public Color selectedColor;

    /// <summary>
    /// The handler for when the user clicks the item in the saved games list
    /// </summary>
    public void ButtonClick()
    {
        // Unselect all other items and change their color to transparent
        foreach (Transform sibling in transform.parent)
        {
            sibling.GetComponent<LoadItem>().selected = false;

            Button siblingButton = sibling.GetComponent<Button>();
            ColorBlock siblingColors = siblingButton.colors;
            siblingColors.normalColor = new Color(0, 0, 0, 0);
            siblingColors.selectedColor = new Color(0, 0, 0, 0);
            siblingButton.colors = siblingColors;
        }

        // Select this item and change its color to the one selected
        selected = true;

        Button button = GetComponent<Button>();
        ColorBlock colors = button.colors;
        colors.normalColor = selectedColor;
        colors.selectedColor = selectedColor;
        button.colors = colors;
    }
}
