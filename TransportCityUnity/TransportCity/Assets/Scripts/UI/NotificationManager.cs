﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NotificationManager : MonoBehaviour
{
    public GameObject notificationPanel;

    // How long should the notifications stay on the screen in seconds
    public float notificationTimeout;

    // Max number of notifications that can be displayed at once
    public int maxNumberOfNotifications;

    // All notifications
    private Queue<NotificationItem> notifications;

    // Notifications in the process of destruction
    private Queue<NotificationItem> toDestroy;

    private GameObject notificationPrefab;

    // Time of the last check of expired notifications
    private float lastCheck;

    void Start()
    {
        notificationPrefab = Resources.Load<GameObject>($"Prefabs/UI/notificationPrefab");

        notifications = new Queue<NotificationItem>();
        toDestroy = new Queue<NotificationItem>();

        lastCheck = 0;
    }

    void Update()
    {
        if (Time.unscaledTime - lastCheck < 1)
        {
            return;
        }
        lastCheck = Time.unscaledTime;

        // For notifications in toDestroy, wait 15 seconds before destroying the GameObject
        while (toDestroy.Count > 0)
        {
            NotificationItem notification = toDestroy.Peek();
            if (Time.unscaledTime - notification.time > 15)
            {
                toDestroy.Dequeue();
                Destroy(notification.rectTransform.gameObject);
            }
            else
            {
                break;
            }
        }

        // For old notifications, fade them if they are old enough
        while (notifications.Count > 0)
        {
            NotificationItem notification = notifications.Peek();
            if (Time.unscaledTime - notification.time > notificationTimeout)
            {
                FadeLastNotification();
            }
            else
            {
                break;
            }
        }
        
    }

    /// <summary>
    /// Create a new notification
    /// </summary>
    /// <param name="text">The text of the notification</param>
    /// <param name="level">The importance level of the notification</param>
    public void AddNotification(string text, UI_API.NotificationLevel level)
    {
        GameObject notification = Instantiate(notificationPrefab, notificationPanel.transform);

        // Move old notifications out of the way
        ShiftNotificationsUp();

        // Set the text and color of the notification
        notification.transform.GetChild(0).GetComponent<Text>().text = text;
        switch (level) {
            case UI_API.NotificationLevel.INFO:
                notification.transform.GetComponent<Image>().color = new Color(1, 1, 1);
                break;
            case UI_API.NotificationLevel.WARN:
                notification.transform.GetComponent<Image>().color = new Color(1, 1, 0);
                break;
            case UI_API.NotificationLevel.CRITICAL:
                notification.transform.GetComponent<Image>().color = new Color(1, 0, 0);
                break;
            default:
                throw new UI_API.UIException("weird notification level");
        }

        notifications.Enqueue(new NotificationItem(notification.GetComponent<RectTransform>()));

        // Check if we exceeded the max number of notifications
        if (notifications.Count > maxNumberOfNotifications)
        {
            FadeLastNotification();
        }
    }

    // Move all notifications up one step
    private void ShiftNotificationsUp() 
    {
        foreach (NotificationItem notification in notifications)
        {
            notification.rectTransform.position = new Vector2(
                notification.rectTransform.position.x, 
                notification.rectTransform.position.y + notification.rectTransform.rect.height * notificationPanel.transform.lossyScale.y
            );
        }
    }

    // Trigger the fade animation of the last notification in queue and move it into the toDestroy queue
    private void FadeLastNotification()
    {
        NotificationItem notification = notifications.Dequeue();
        toDestroy.Enqueue(notification);
        notification.ResetTime();
        notification.rectTransform.GetComponent<Animator>().SetTrigger("fade trigger");
    }

    // A private class for representing a notification and its age in a queue
    private class NotificationItem {
        public RectTransform rectTransform;
        public float time;

        public NotificationItem(RectTransform rect)
        {
            rectTransform = rect;
            ResetTime();
        }

        public void ResetTime()
        {
            time = Time.unscaledTime;
        }
    }

}
