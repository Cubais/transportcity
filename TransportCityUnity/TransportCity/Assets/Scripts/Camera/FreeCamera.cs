﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreeCamera : MonoBehaviour
{
    public float buttonSpeed;
    public float mouseSpeed;
    public float xRotationLimit;
    public float terrainHeight;

    void Update()
    {
        // Y plane movement
        float forwardBackward = Input.GetAxis("Vertical");
        float leftRight = Input.GetAxis("Horizontal");
        transform.Translate(Vector3.forward * forwardBackward * Time.deltaTime * buttonSpeed);
        transform.Translate(Vector3.right * leftRight * Time.deltaTime * buttonSpeed);

        // Up down
        float upDown = Input.GetAxis("UpDown");
        transform.position += Vector3.up * upDown * Time.deltaTime * buttonSpeed;

        // Clamp camera y position to avoid going below terrain
        // In the future perhaps fire a raycast down to see the y coordinates of the terrain below
        float clampedY = Mathf.Clamp(transform.position.y, terrainHeight, 1000);
        transform.position = new Vector3(transform.position.x, clampedY, transform.position.z);

        // Mouse camera rotation
        if (Input.GetKey(KeyCode.Mouse1))
        {
            float mouseX = Input.GetAxis("Mouse X");
            float mouseY = Input.GetAxis("Mouse Y");
            float xCoeficient = Mathf.Sqrt(Mathf.Abs(Mathf.Cos(transform.rotation.eulerAngles.x)));
            transform.Rotate(new Vector3(-mouseY * Time.deltaTime * mouseSpeed, mouseX * Time.deltaTime * mouseSpeed * xCoeficient, 0));

            // Clamp X rotation so that it cannot go above or below certain threshold
            // Also set Z rotation to 0 to avoid camera body roll
            float xRotation = transform.rotation.eulerAngles.x;
            if (xRotation > 180f)
            {
                xRotation -= 360f;
            }

            float limit = 90f - xRotationLimit;
            float clampedX = Mathf.Clamp(xRotation, -limit, limit);
            transform.rotation = Quaternion.Euler(clampedX, transform.rotation.eulerAngles.y, 0);
        }
    }
}
