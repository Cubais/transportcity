﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixedCamera : MonoBehaviour
{
    public float buttonSpeed;
    public float xMouseSpeed;
    public float yMouseSpeed;
    public float minHeightAboveTerrain;
    public float xRotationLimit;
    public float wheelSpeed;
    public float shiftSpeedMultiplier;


    public float camerahighLimitAboveCity = 150;
    public float camerasAllowedOverlapXZ = 20;

    private UI_API uiApi;

    private void Start()
    {
        uiApi = UI_API.GetInstance();
    }

    void Update()
    {
        // Disable camera movement when the menu is up or when a name is being edited
        if (uiApi.MenuUp || uiApi.NameEditing)
        {
            return;
        }

        float shiftMultiplier = 1f;

        // If shift is pressed, set speed multiplier
        if (Input.GetKey(KeyCode.LeftShift))
        {
            shiftMultiplier = shiftSpeedMultiplier;
        }

        // Y plane movement with fixed y
        float forwardBackward = Input.GetAxisRaw("Vertical");
        float leftRight = Input.GetAxisRaw("Horizontal");
        Vector3 previousPosition = transform.position;

        // The camera would move slower depending on its x angle, therefore we calculate this coefficient to smooth it out
        float angleCoefficient = 1 / Mathf.Cos(transform.rotation.eulerAngles.x * Mathf.Deg2Rad);

        transform.Translate(Vector3.forward * forwardBackward * Time.unscaledDeltaTime * buttonSpeed * shiftMultiplier * angleCoefficient);
        transform.Translate(Vector3.right * leftRight * Time.unscaledDeltaTime * buttonSpeed * shiftMultiplier);
        transform.position = new Vector3(transform.position.x, previousPosition.y, transform.position.z);

        // Up down
        float upDown = Input.GetAxisRaw("UpDown");
        transform.position += Vector3.up * upDown * Time.unscaledDeltaTime * buttonSpeed * shiftMultiplier;

        // Forward backward with mousewheel, disable when a scrollable window is up
        if (!uiApi.ScrollableWindowUp)
        {
            float mouseWheel = Input.GetAxisRaw("Mouse ScrollWheel");
            transform.Translate(Vector3.forward * mouseWheel * Time.unscaledDeltaTime * wheelSpeed * shiftMultiplier);
        }

        // Fire a raycast down to see if we are still above terrain
        bool raycastHit = Physics.Raycast(transform.position, Vector3.down, out RaycastHit raycastInfo, Mathf.Infinity, ~(1 << 8));

        if (!raycastHit)
        {
            // If we are below terrain (raycast did not hit), fire a new raycast up and set camera position to
            // the hit point plus min distance height
            Physics.Raycast(transform.position, Vector3.up, out raycastInfo, Mathf.Infinity, ~(1 << 8));
            transform.position = new Vector3(raycastInfo.point.x, raycastInfo.point.y + minHeightAboveTerrain, raycastInfo.point.z);
        }
        else if (raycastInfo.distance < minHeightAboveTerrain)
        {
            // We are below min distance above terrain, but we are still above terrain
            // Set position to the correct min distance above the hit point
            transform.position = new Vector3(raycastInfo.point.x, raycastInfo.point.y + minHeightAboveTerrain, raycastInfo.point.z);
        }

        // Mouse camera rotation
        if (Input.GetKey(KeyCode.Mouse1))
        {
            float mouseX = Input.GetAxisRaw("Mouse X");
            float mouseY = Input.GetAxisRaw("Mouse Y");

            // Horizontal camera rotation tends to be very quick when looking directly down/up,
            // therefore we set this coefficient to slow it down.
            // This was essentially done using trial and error so do not try to find any logic in this.
            float xCoeficient = Mathf.Sqrt(Mathf.Abs(Mathf.Cos(transform.rotation.eulerAngles.x * Mathf.Deg2Rad)));

            transform.Rotate(new Vector3(
                -mouseY * Time.unscaledDeltaTime * yMouseSpeed,
                mouseX * Time.unscaledDeltaTime * xMouseSpeed * xCoeficient,
                0
            ));

            // Clamp X rotation so that it cannot go above or below certain threshold
            // Also set Z rotation to 0 to avoid camera body roll
            float xRotation = transform.rotation.eulerAngles.x;
            if (xRotation > 180f)
            {
                xRotation -= 360f;
            }

            float limit = 90f - xRotationLimit;
            float clampedX = Mathf.Clamp(xRotation, -limit, limit);
            transform.rotation = Quaternion.Euler(clampedX, transform.rotation.eulerAngles.y, 0);
        }

        float x = 0, y = 0, z = 0;
        y = Math.Min(transform.position.y, RoadGraphGeneration.instance.cityBaseHeight + camerahighLimitAboveCity);
        
        x = Math.Max(transform.position.x, RoadGraphGeneration.instance.downLeftCorner.x - camerasAllowedOverlapXZ);
        x = Math.Min(x, RoadGraphGeneration.instance.downLeftCorner.x + RoadGraphGeneration.instance.totalCitySize.x + camerasAllowedOverlapXZ);
        
        z = Math.Max(transform.position.z, RoadGraphGeneration.instance.downLeftCorner.y - camerasAllowedOverlapXZ);
        z = Math.Min(z, RoadGraphGeneration.instance.downLeftCorner.y + RoadGraphGeneration.instance.totalCitySize.y + camerasAllowedOverlapXZ);

        transform.position = new Vector3(x, y, z);
    }
}
