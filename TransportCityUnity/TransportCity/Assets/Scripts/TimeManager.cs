﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeManager : MonoBehaviour
{
    // Singleton
    public static TimeManager instance;

    // Delegates for handlers
    public delegate void PartOfDayChangeHandler(Constants.PartOfDay newPartOfDay);
    public delegate void FutureCallback();

    // Public variables for easy editing of possible game speeds in the inspector
    public float speed0;
    public float speed1;
    public float speed2;
    public float speed3;

    // How much faster is game time than real time, we wrote 30 in specification
    public float gameToRealTimeMultiplier;

    // The date of the start of the time simulation
    public int startDay;
    public int startMonth;
    public int startYear;
    public int startHour;
    public int startMinute;

    // Format of the time shown in the upper left corner
    public string dateTimeFormat;

    /// <summary>
    /// Current game time as a DateTime object
    /// </summary>
    public DateTime CurrentDateTime { get; internal set; }

    // Colors of the time buttons to be set when the player changes the simulation speed
    public Color timeSpeedButtonHighlightColor;
    public Color timeSpeedButtonRegularColor;

    // The parent of the time buttons, needed to be able to highlight the buttons according to the selected timespeed
    public Transform timeButtonsPanel;

    /// <summary>
    /// Comparer for comparing two keys in order to allow duplicate items in SortedList
    /// </summary>
    /// <typeparam name="TKey">The key type</typeparam>
    public class DuplicateKeyComparer<TKey> : IComparer<TKey>
        where TKey : IComparable
    {
        public int Compare(TKey x, TKey y)
        {
            int result = x.CompareTo(y);

            if (result == 0)
                return -1;
            else
                return result;
        }
    }

    /// <summary>
    /// Says whether the game is currently paused or not
    /// </summary>
    public bool Paused { get; private set; }

    internal event PartOfDayChangeHandler partOfDayChangeHandler;
    internal event FutureCallback dayChangeHandler;

    private Constants.PartOfDay? lastPartOfDay;

    // UI API reference
    private UI_API uiApi;

    // A sorted list of alarm clock callbacks to be called
    // The first type argument is datetime represented as a UNIX timestamp
    private SortedList<long, FutureCallback> alarmClocks;

    // Current game simulation speed, represented as 0, 1, 2 or 3
    private int currentSpeed = 1;

    // An array holding references to the time speed buttons for changing their color
    private Button[] timeSpeedButtons;

    private float fixedDeltaDefault;

    private void Awake()
    {
        // Create DateTime object
        CurrentDateTime = new DateTime(day: startDay, month: startMonth, year: startYear, hour: startHour, minute: startMinute, second: 0);

        alarmClocks = new SortedList<long, FutureCallback>(new DuplicateKeyComparer<long>());

        if (instance == null)
        {
            instance = this;
        }

        fixedDeltaDefault = Time.fixedDeltaTime;
    }

    private void Start()
    {
        // Register the handler for UI buttons
        uiApi = UI_API.GetInstance();
        uiApi.RegisterForTimeButtons(TimeSpeedButtonsHandler);

        // Get time speed buttons references
        timeSpeedButtons = new Button[timeButtonsPanel.childCount];
        int i = 0;
        foreach (Transform timeSpeedButton in timeButtonsPanel)
        {
            timeSpeedButtons[i++] = timeSpeedButton.GetComponent<Button>();
        }

        // Highlight the "1" time speed button
        HighlightTimeSpeedButton(timeSpeedButtons[1]);
    }

    private void Update()
    {
        int previousDay = CurrentDateTime.Day;

        // Add time delta and update UI text
        CurrentDateTime = CurrentDateTime.AddSeconds(gameToRealTimeMultiplier * Time.deltaTime);
        uiApi.SetTimeText(CurrentDateTime.ToString(dateTimeFormat));

        // Check if the day changed, if it did, call the change handler
        if (previousDay != CurrentDateTime.Day)
        {
            CallDayChangeHandler();
        }

        // Check if the last part of day was the same and if it was, call the change handler
        if (lastPartOfDay == null)
        {
            lastPartOfDay = GetCurrentPartOfDay();
        }
        else
        {
            Constants.PartOfDay curPartOfDay = GetCurrentPartOfDay();
            if (curPartOfDay != lastPartOfDay)
            {
                lastPartOfDay = curPartOfDay;
                CallPartOfDayChangeHandler(curPartOfDay);
            }
        }

        long currentDateTimeInMilliseconds = ConvertDateTimeToMilliseconds(CurrentDateTime);

        // Call all alarm clock callbacks that are already in the past
        while (alarmClocks.Count > 0 && currentDateTimeInMilliseconds >= alarmClocks.Keys[0])
        {
            alarmClocks.Values[0]();

            // RemoveAt() is an O(n) operation, therefore there is space for future optimisations.
            alarmClocks.RemoveAt(0);
        }

        // If the in-game menu is shown, disable keyboard buttons to control time
        if (uiApi.MenuUp)
        {
            return;
        }

        // Set time speeds when user clicks physical buttons above the keyboard.
        // "else" just in case the player clicks more buttons at once.
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            TimeSpeedButtonsHandler(1);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            TimeSpeedButtonsHandler(2);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            //TimeSpeedButtonsHandler(3);
        }
        // If the player presses SPACE, pause/unpause the game
        else if (Input.GetKeyDown(KeyCode.Space))
        {
            // If the player pressed the space when the game is already stopped, continue with speed 1
            if (currentSpeed == 0)
            {
                TimeSpeedButtonsHandler(1);
                return;
            }

            if (Paused)
            {
                // The game is paused but the current speed is not 0 => The game was paused by space and is being unpaused by space.
                UnpauseGame();
            }
            else
            {
                PauseGame();
            }
        }
    }

    internal void ClearHandlers()
    {
        partOfDayChangeHandler = null;
        dayChangeHandler = null;
    }


    /// <summary>
    /// Register a function to be called at a specific in-game time.
    /// Expected usage is to get the current in-game time from the CurrentDateTime attribute of this class,
    /// call the desired Add... method on it, and then pass the result as the when parameter.
    /// </summary>
    /// <param name="when">The in-game time when the method should be called</param>
    /// <param name="callback">The method to be called</param>
    /// <exception cref="ArgumentException">Thrown when the "when" argument is in the past</exception>
    public void RegisterAlarmClock(DateTime when, FutureCallback callback)
    {
        if (when < CurrentDateTime)
        {
            throw new ArgumentException("The specified time is in the past!");
        }

        long whenInMilliseconds = ConvertDateTimeToMilliseconds(when);
        alarmClocks.Add(whenInMilliseconds, callback);
    }

    /// <summary>
    /// Register a method that will be called when the part of day changes
    /// </summary>
    /// <param name="handler">The handler method</param>
    public void RegisterPartOfDayChangeHandler(PartOfDayChangeHandler handler)
    {
        partOfDayChangeHandler += handler;
    }

    /// <summary>
    /// Unregister a method from PartOfDayChangeHandler
    /// </summary>
    /// <param name="handler">The handler method</param>
    public void UnregisterPartOfDayChangeHandler(PartOfDayChangeHandler handler)
    {
        try
        {
            partOfDayChangeHandler -= handler;
        }
        catch (Exception)
        { }
    }

    /// <summary>
    /// Register a method to be called at midnight, when the day changes
    /// </summary>
    /// <param name="handler">The handler method</param>
    public void RegisterDayChangeHandler(FutureCallback handler)
    {
        dayChangeHandler += handler;
    }

    /// <summary>
    /// Returns the current part of day
    /// </summary>
    /// <returns>Current part of day</returns>
    public Constants.PartOfDay GetCurrentPartOfDay()
    {
        int currentHour = CurrentDateTime.Hour;

        // A lot of ifs to get the intervals for parts of day
        if (currentHour >= Constants.nightStartHour || currentHour < Constants.morningRushStartHour)
        {
            return Constants.PartOfDay.NIGHT;
        }

        if (currentHour < Constants.middayStartHour)
        {
            return Constants.PartOfDay.MORNING_RUSH;
        }

        if (currentHour < Constants.afternoonRushStartHour)
        {
            return Constants.PartOfDay.MIDDAY;
        }

        if (currentHour < Constants.eveningStartHour)
        {
            return Constants.PartOfDay.AFTERNOON_RUSH;
        }

        return Constants.PartOfDay.EVENING;
    }

    public void PauseGame()
    {
        // Do everything as if the player clicked the pause button in UI,
        // but remember the current speed for unpause.
        int currentSpeedCopy = currentSpeed;
        TimeSpeedButtonsHandler(0);
        currentSpeed = currentSpeedCopy;
    }

    public void UnpauseGame()
    {
        TimeSpeedButtonsHandler(currentSpeed);
    }

    /// <summary>
    /// A handler for time button clicks, registered at the UI API
    /// </summary>
    /// <param name="buttonIndex">The index of the button pressed</param>
    public void TimeSpeedButtonsHandler(int buttonIndex)
    {
        if (buttonIndex < 0 || buttonIndex > 3)
        {
            throw new UI_API.UIException("Time speed button handler was called with a weird button index: " + buttonIndex);
        }

        // Set the current speed and highlight the button.
        // Exception has not been thrown, therefore we know it is valid.
        currentSpeed = buttonIndex;
        HighlightTimeSpeedButton(timeSpeedButtons[buttonIndex]);

        // Set the Paused variable
        Paused = buttonIndex == 0;

        // Set time speed based on the button pressed
        switch (buttonIndex)
        {
            case 0:
                SetTimeSpeed(speed0);
                break;
            case 1:
                SetTimeSpeed(speed1);
                break;
            case 2:
                SetTimeSpeed(speed2);
                break;
            case 3:
                SetTimeSpeed(speed3);
                break;
        }
    }

    /// <summary>
    /// Sets the speed of the time simulation
    /// </summary>
    /// <param name="speed">The time multiplier to use. Default is 1.0. Anything below 0.03 pauses the game.</param>
    private void SetTimeSpeed(float speed)
    {
        if (speed < 0.03f)
        {
            Time.timeScale = 0;            
        }
        else
        {
            Time.timeScale = speed;
            Time.fixedDeltaTime = fixedDeltaDefault / speed;
        }
    }

    // A method for calling the part of day change handler
    private void CallPartOfDayChangeHandler(Constants.PartOfDay partOfDay)
    {
        if (partOfDayChangeHandler == null)
        {
            return;
        }

        partOfDayChangeHandler(partOfDay);
    }

    /// <summary>
    /// Can be used to manually raise partOfDayChangeHandler
    /// </summary>
    /// <param name="partOfDay">Which PartOfDay happens</param>
    public void CallPartOfDayChangeHandlerDebug(Constants.PartOfDay partOfDay)
    {
        CallPartOfDayChangeHandler(partOfDay);
    }

    /// <summary>
    /// A helper method for converting DateTime to UNIX timestamp
    /// </summary>
    /// <param name="dateTime">The DateTime to convert</param>
    /// <returns></returns>
    public long ConvertDateTimeToMilliseconds(DateTime dateTime)
    {
        return (long)(dateTime - new DateTime(1970, 1, 1)).TotalMilliseconds;
    }

    // A method for calling the day change handler
    private void CallDayChangeHandler()
    {
        if (dayChangeHandler == null)
        {
            return;
        }

        dayChangeHandler();
    }

    // Colors all time buttons as disabled but the one in the parameter
    private void HighlightTimeSpeedButton(Button activeButton)
    {
        // Set all others as regular
        foreach(Button b in timeSpeedButtons)
        {
            ColorBlock colorBlock = b.colors;
            colorBlock.normalColor = timeSpeedButtonRegularColor;
            colorBlock.highlightedColor = timeSpeedButtonRegularColor;
            colorBlock.selectedColor = timeSpeedButtonRegularColor;
            b.colors = colorBlock;
        }

        // Set the parameter button as highlighted
        ColorBlock activeColorBlock = activeButton.colors;
        activeColorBlock.normalColor = timeSpeedButtonHighlightColor;
        activeColorBlock.highlightedColor = timeSpeedButtonHighlightColor;
        activeColorBlock.selectedColor = timeSpeedButtonHighlightColor;
        activeButton.colors = activeColorBlock;
    }
}
