﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CoroutineContext<MB> where MB : MonoBehaviour
{
    public bool isRunning = false;

    public abstract void Start(MB mb);
}

public interface IResumable
{
    void Resume();
}

public struct CoroutineContextPair<MB> : IResumable where MB : MonoBehaviour 
{
    public CoroutineContextPair(CoroutineContext<MB> coroutineContext, MB monoBehaviour)
    {
        this.coroutineContext = coroutineContext;
        this.monoBehaviour = monoBehaviour;
    }

    public CoroutineContext<MB> coroutineContext;
    public MB monoBehaviour;

    public void Resume()
    {
        if (coroutineContext.isRunning && monoBehaviour.isActiveAndEnabled)
        {
            coroutineContext.Start(monoBehaviour);
        }
    }
}

public class ChangeCarSpeedCoroutineContext : CoroutineContext<CarNavigation>
{
    public float targetSpeed;
    public float speedChangePerSecond;

    public override void Start(CarNavigation carNavigation)
    {
        carNavigation.StartCoroutine(carNavigation.ChangeSpeed(targetSpeed, speedChangePerSecond));
    }
}
public class CreateAgentsCoroutineContext : CoroutineContext<PeopleManager>
{
    public int peopleCount;

    public override void Start(PeopleManager peopleManager)
    {
        peopleManager.StartCoroutine(peopleManager.CreateAgents(peopleCount));
    }
}
public class AddCarCoroutineContext : CoroutineContext<Intersection>
{
    [NonSerialized]
    public IntersectionWaypoint intersectionWaypoint;
    [NonSerialized]
    public CarNavigation carNavigation;

    // For serialization purposes
    public Vector3 intersectionWaypointPosition;
    public string carNavID;

    public override void Start(Intersection intersection)
    {
        intersection.StartCoroutine(intersection.AddCar(intersectionWaypoint, carNavigation));
    }
}
public class ChangeSubwayTrainSpeedCoroutineContext : CoroutineContext<SubwayTrain>
{
    public float targetSpeed;
    public float speedChangePerSecond;

    public override void Start(SubwayTrain subwayTrain)
    {
        subwayTrain.StartCoroutine(subwayTrain.ChangeSpeed(targetSpeed, speedChangePerSecond));
    }
}
public class ChooseTransportTypeCoroutineContext : CoroutineContext<Person>
{
    public Vector3 destination;

    public override void Start(Person person)
    {
        person.StartCoroutine(person.ChooseTransportType(destination));
    }
}
public class SpawnBusesCoroutineContext : CoroutineContext<BusStop>
{
    public override void Start(BusStop busStop)
    {
        busStop.StartCoroutine(busStop.SpawnBuses());
    }
}
public class BusHandlePassengerCoroutineContext : CoroutineContext<Bus>
{
    public override void Start(Bus bus)
    {
        bus.StartCoroutine(bus.HandlePassengers());
    }
}
public class SubwayTrainHandlePassengerCoroutineContext : CoroutineContext<SubwayTrain>
{
    public override void Start(SubwayTrain subwayTrain)
    {
        subwayTrain.StartCoroutine(subwayTrain.HandlePassengers());
    }
}
