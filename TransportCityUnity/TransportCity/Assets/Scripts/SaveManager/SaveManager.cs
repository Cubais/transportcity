﻿#define TryCatch

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SceneManagement;
using VoxelBusters.Serialization;
using static Constants;
using SaveData = SaveManager.SaveData_1_0_0; // Curent newest version

public class SaveManager : MonoBehaviour
{
    #region Instances
    public RoadGraphGeneration roadGraphGeneration;
    public HeightMapApplier heightMapApplier;
    public BuildingCreator buildingCreator;
    public ZoneController zoneController;
    public PeopleManager peopleManager;
    public CityCouncil cityCouncil;
    public GameObject loadingScreen;
    public GameObject savingScreen;

    private TimeManager timeManager;

    private BuildingManager buildingManager;

    private BusInstanceManager busInstanceManager;
    private BusLineManager busLineManager;
    private BusLineCreator busLineCreator;

    private SubwayInstanceManager subwayInstanceManager;
    private SubwayLineManager subwayLineManager;
    private SubwayLineCreator subwayLineCreator;

    private MoneyManager moneyManager;
    private StatisticsDataManager statisticsDataManager;
    #endregion

    public const string saveFileExtension = "bytes";
    private const float waypointColliderRange = 0.1f;

    public static int numberOfBlockers = 0;

    /// <summary>
    /// This field represents the path to a save folder
    /// The actual path is setup through unity editor in VoxelBusters setting
    /// </summary>
    public string SaveDirPath { get; private set; }

    public void Awake()
    {
        SaveDirPath = Path.Combine(Application.persistentDataPath, "Saves");
        if (!Directory.Exists(SaveDirPath))
            Directory.CreateDirectory(SaveDirPath);
    }

    public void Start()
    {
        timeManager = TimeManager.instance;
        buildingManager = BuildingManager.instance;

        busInstanceManager = BusInstanceManager.GetInstance();
        busLineManager = BusLineManager.GetInstance();
        busLineCreator = BusLineCreator.GetInstance();

        subwayInstanceManager = SubwayInstanceManager.GetInstance();
        subwayLineManager = SubwayLineManager.GetInstance();
        subwayLineCreator = SubwayLineCreator.GetInstance();

        moneyManager = MoneyManager.instance;
        statisticsDataManager = StatisticsDataManager.instance;
    }

    public void SaveGame(string saveName) => StartCoroutine(SaveLoadCoroutine(SaveGame_, saveName, true));
    public void LoadGame(string saveName) => StartCoroutine(SaveLoadCoroutine(LoadGame_, saveName, false));

    private IEnumerator SaveLoadCoroutine(SaveLoadDelegate saveLoadDelegate, string saveName, bool saving)
    {
        if (saving)
        {
            savingScreen.SetActive(true);
        }
        else
        {
            loadingScreen.SetActive(true);
        }

        timeManager.PauseGame();

        yield return new WaitForEndOfFrame();

        if (saving)
        {
            // Wait while blockers finish their job
            int failCounter = 500;
            while (numberOfBlockers > 0)
            {
                if (failCounter-- <= 0)
                {
                    UI_API.GetInstance().DisplayNotification("SaveManager waited too long, try it again later", UI_API.NotificationLevel.WARN);
                    timeManager.UnpauseGame();
                    savingScreen.SetActive(false);
                    loadingScreen.SetActive(false);
                    yield break;
                }
                yield return new WaitForEndOfFrame();
            }
        }

        Intersection.isTriggerActive = false;
        BuildingManager.areOnDestroyMethodsEnabled = false;

        yield return new WaitForEndOfFrame();

        saveLoadDelegate(saveName);

        yield return new WaitForEndOfFrame();

        Intersection.isTriggerActive = true;
        BuildingManager.areOnDestroyMethodsEnabled = true;

        timeManager.UnpauseGame();
        savingScreen.SetActive(false);
        loadingScreen.SetActive(false);
    }


    /// <summary>
    /// Saves game state into file
    /// </summary>
    /// <param name="saveName">name of the save file without extension</param>
    private void SaveGame_(string saveName)
    {
        try
        {
            #region ========  SETUP DICTIONARIES  ========

            // Roads
            var dictRoadToID = new Dictionary<GameObject, string>();
            var roadGOs = GameObject.FindGameObjectsWithTag("Road");
            for (int i = 0; i < roadGOs.Length; i++)
            {
                dictRoadToID.Add(roadGOs[i], $"road_{i}");
            }

            // Districts
            var dictDistrictToID = new Dictionary<DistrictData, string>();
            for (int i = 0; i < roadGraphGeneration.allDistricts.Count; i++)
            {
                dictDistrictToID.Add(roadGraphGeneration.allDistricts[i], $"district_{i}");
            }

            // Buildings
            var buildingGOs = GameObject.FindGameObjectsWithTag("Building");
            var subwayStops = new List<SubwayStop>();
            var dictBuildingToID = new Dictionary<Building, string>();
            for (int i = 0; i < buildingGOs.Length; i++)
            {
                dictBuildingToID.Add(buildingGOs[i].GetComponent<Building>(), $"building_{i}");

                var subwayStop = buildingGOs[i].GetComponent<SubwayStop>();
                if (subwayStop != null)
                {
                    subwayStops.Add(subwayStop);
                }
            }

            // Cars
            var carNavs = CarNavigation.allCars;
            var dictCarNavToID = new Dictionary<CarNavigation, string>();
            for (int i = 0; i < carNavs.Count; i++)
            {
                dictCarNavToID.Add(carNavs[i], $"carNav_{i}");
            }

            // BusStops
            var dictBusStopToID = new Dictionary<IStop, string>();
            var busStopGOs = GameObject.FindGameObjectsWithTag("BusStop");
            for (int i = 0; i < busStopGOs.Length; i++)
            {
                dictBusStopToID.Add(busStopGOs[i].GetComponent<BusStop>(), $"busStop_{i}");
            }

            // BusLines
            var busLines = busLineManager.GetLines();
            var dictBusLineToID = new Dictionary<ILine, string>();
            for (int i = 0; i < busLines.Count; i++)
            {
                dictBusLineToID.Add(busLines[i], $"busLine_{i}");
            }

            // BusInstances
            var dictBusInstanceToID = new Dictionary<BusInstanceManager.BusInstance, string>();
            for (int i = 0; i < busInstanceManager.buses.Count; i++)
            {
                dictBusInstanceToID.Add(busInstanceManager.buses[i], $"busInstance_{i}");
            }

            // SubwayStops
            var dictSubwayStopToID = new Dictionary<IStop, string>();
            for (int i = 0; i < subwayStops.Count; i++)
            {
                dictSubwayStopToID.Add(subwayStops[i], $"subwayStop_{i}");
            }

            // SubwayLines
            var subwayLines = subwayLineManager.GetLines();
            var dictSubwayLineToID = new Dictionary<ILine, string>();
            for (int i = 0; i < subwayLines.Count; i++)
            {
                dictSubwayLineToID.Add(subwayLines[i], $"subwayLine_{i}");
            }

            // SubwayTrainInstances
            var dictSubwayTrainInstanceToID = new Dictionary<SubwayInstanceManager.SubwayTrainInstance, string>();
            for (int i = 0; i < subwayInstanceManager.trains.Count; i++)
            {
                dictSubwayTrainInstanceToID.Add(subwayInstanceManager.trains[i], $"subwayTrainInstance_{i}");
            }

            // IStops and ILines
            var dictIStopToID = new Dictionary<IStop, string>();
            foreach (var bsid in dictBusStopToID)
            {
                dictIStopToID.Add(bsid.Key, bsid.Value);
            }
            foreach (var ssid in dictSubwayStopToID)
            {
                dictIStopToID.Add(ssid.Key, ssid.Value);
            }
            var dictILineToID = new Dictionary<ILine, string>();
            foreach (var blid in dictBusLineToID)
            {
                dictILineToID.Add(blid.Key, blid.Value);
            }
            foreach (var slid in dictSubwayLineToID)
            {
                dictILineToID.Add(slid.Key, slid.Value);
            }

            // People
            var people = peopleManager.people;
            var dictPersonToID = new Dictionary<Person, string>();
            for (int i = 0; i < people.Count; i++)
            {
                dictPersonToID.Add(people[i], $"person_{i}");
            }
            #endregion


            #region ========  SERIALIZE DATA  ========
            SerializationManager.BeginSerializeGroup(saveName);

            SerializationManager.Serialize("saveVersion", SaveData.saveVersion);

            var saveData = new SaveData();

            // Camera
            saveData.cameraData = new SaveData.CameraData()
            {
                position = Camera.main.transform.position,
                rotation = Camera.main.transform.rotation,
            };


            // Time
            saveData.timeData = new SaveData.TimeData()
            {
                currentDateTime = timeManager.CurrentDateTime,
            };


            // Terrain
            var terrain = Terrain.activeTerrain;
            var heightmap = terrain.terrainData.GetHeights(0, 0, terrain.terrainData.heightmapResolution, terrain.terrainData.heightmapResolution);
            saveData.terrainDataData = new SaveData.TerrainDataData()
            {
                heightmap = heightmap,
                thickness = heightMapApplier.thickness,
                scale = heightMapApplier.scale,
                power = heightMapApplier.power,
                offsetX = heightMapApplier.offsetX,
                offsetY = heightMapApplier.offsetY,

                treeDataList = new List<SaveData.TreeData>(),
            };

            var trees = GameObject.FindGameObjectsWithTag("Tree");
            foreach (var tree in trees)
            {
                saveData.terrainDataData.treeDataList.Add(new SaveData.TreeData()
                {
                    position = tree.transform.position,
                    rotation = tree.transform.rotation,
                });
            }


            // Road Graph
            var roadGraphData = saveData.roadGraphData = new SaveData.RoadGraphData()
            {
                heightLimit = roadGraphGeneration.heightLimit,
                blockCount = roadGraphGeneration.blockCount,
                terrainFlatteningExtension = roadGraphGeneration.terrainFlatteningExtension,

                terrainSize = roadGraphGeneration.terrainSize,
                terrainCenter = roadGraphGeneration.terrainCenter,
                cityCenter = roadGraphGeneration.cityCenter,
                totalCitySize = roadGraphGeneration.totalCitySize,
                downLeftCorner = roadGraphGeneration.downLeftCorner,
                southWestCorner = roadGraphGeneration.southWestCorner,

                cityBaseHeight = roadGraphGeneration.cityBaseHeight,

                supermodels = new string[roadGraphGeneration.supermodels.GetLength(0), roadGraphGeneration.supermodels.GetLength(1)],
                districts = new string[roadGraphGeneration.districts.GetLength(0), roadGraphGeneration.districts.GetLength(1)],
            };

            for (int x = 0; x < roadGraphGeneration.supermodels.GetLength(0); x++)
            {
                for (int y = 0; y < roadGraphGeneration.supermodels.GetLength(1); y++)
                {
                    roadGraphData.supermodels[x, y] = SafeGet(dictRoadToID, roadGraphGeneration.supermodels[x, y]);
                }
            }
            for (int x = 0; x < roadGraphGeneration.districts.GetLength(0); x++)
            {
                for (int y = 0; y < roadGraphGeneration.districts.GetLength(1); y++)
                {
                    roadGraphData.districts[x, y] = SafeGet(dictDistrictToID, roadGraphGeneration.districts[x, y]);
                }
            }

            // Districts
            roadGraphData.districtDataDataList = new List<SaveData.DistrictDataData>();
            for (int i = 0; i < roadGraphGeneration.allDistricts.Count; i++)
            {
                var districtData = roadGraphGeneration.allDistricts[i];

                var districtDataData = new SaveData.DistrictDataData()
                {
                    districtID = dictDistrictToID[districtData],

                    blockCount = districtData.blockCount,
                    blockSize = districtData.blockSize,

                    downLeftCorner = districtData.downLeftCorner,
                    citySize = districtData.citySize,
                    cityCenter = districtData.cityCenter,

                    roads = districtData.roads,
                    junctions = districtData.junctions,
                    crosswalks = districtData.crosswalks,
                    roadIDmap = new string[districtData.citySize.x, districtData.citySize.y],

                    directionFromMainDistrict = districtData.directionFromMainDistrict,
                    districtState = districtData.districtState,
                };

                for (int x = 0; x < districtData.citySize.x; x++)
                {
                    for (int y = 0; y < districtData.citySize.y; y++)
                    {
                        districtDataData.roadIDmap[x, y] = SafeGet(dictRoadToID, districtData.models[x, y]);
                    }
                }

                if (districtData == roadGraphGeneration.centerDistrict)
                {
                    roadGraphData.centerDistrict = districtDataData.districtID;
                }

                roadGraphData.districtDataDataList.Add(districtDataData);
            }


            // Roads
            roadGraphData.roadDataList = new List<SaveData.RoadData>();
            foreach (var roadGO in roadGOs)
            {
                var roadSegment = roadGO.GetComponent<RoadSegment>();
                var roadID = dictRoadToID[roadGO];

                var roadData = new SaveData.RoadData()
                {
                    roadID = roadID,
                    roadType = roadSegment.type,

                    position = roadGO.transform.position,
                    rotation = roadGO.transform.rotation,
                };
                roadGraphData.roadDataList.Add(roadData);

                // Intersection
                var intersection = roadGO?.GetComponent<Intersection>();
                if (intersection != null)
                {
                    var intersectionData = new SaveData.IntersectionData();

                    intersectionData.buffers = new Dictionary<Vector3, List<string>>();
                    foreach (var buffer in intersection.buffers)
                    {
                        intersectionData.buffers.Add(
                            buffer.Key.transform.position,
                            new List<string>(from carNav in buffer.Value select dictCarNavToID[carNav])
                        );
                    }

                    intersectionData.pendingVehicles = new List<string>(from carNav in intersection.pendingVehicles select dictCarNavToID[carNav]);

                    var intersectionDirector = intersection.director;
                    if (intersectionDirector is BasicDirector basicDirector)
                    {
                        intersectionData.intersectionDirectorType = SaveData.IntersectionDirectorType.BasicDirector;

                        intersectionData.directionIndicators = new Dictionary<Vector3, int>();
                        foreach (var directionIndicator in basicDirector.directionIndicators)
                        {
                            intersectionData.directionIndicators.Add(directionIndicator.Key.transform.position, directionIndicator.Value);
                        }

                        intersectionData.occupiedClusters = new Dictionary<string, List<Vector3>>();
                        foreach (var occupiedCluster in basicDirector.occupiedClusters)
                        {
                            intersectionData.occupiedClusters.Add(
                                dictCarNavToID[occupiedCluster.Key],
                                new List<Vector3>(from wp in occupiedCluster.Value select wp.transform.position)
                            );
                        }
                    }
                    else
                    {
                        throw new VoxelBusters.Serialization.SerializationException("Unsupported IntersectionDirector");
                    }

                    var addCarCC = intersection.addCarCC;
                    addCarCC.intersectionWaypointPosition = addCarCC.intersectionWaypoint != null ? addCarCC.intersectionWaypoint.transform.position : Vector3.positiveInfinity;
                    addCarCC.carNavID = SafeGet(dictCarNavToID, addCarCC.carNavigation);
                    intersectionData.addCarCC = addCarCC;

                    roadData.intersectionData = intersectionData;
                }

                // Crosswalk
                var crosswalk = roadGO?.GetComponent<Crosswalk>();
                if (crosswalk != null)
                {
                    var crosswalkData = new SaveData.CrosswalkData()
                    {
                        timeSinceLastLeftNotification = crosswalk.timeSinceLastLeftNotification,
                        timeSinceLastRightNotification = crosswalk.timeSinceLastRightNotification,

                        vehicleIDsInRightTrigger = new List<string>(from carNav in crosswalk.vehiclesInRightTrigger select SafeGet(dictCarNavToID, carNav)),
                        vehicleIDsInLeftTrigger = new List<string>(from carNav in crosswalk.vehiclesInLeftTrigger select SafeGet(dictCarNavToID, carNav)),

                        leftOccupied = crosswalk.leftOccupied,
                        rightOccupied = crosswalk.rightOccupied,
                    };


                    roadData.crosswalkData = crosswalkData;
                }
            }


            // Waypoints
            var waypointDataList = saveData.roadGraphData.waypointDataList = new List<SaveData.WaypointData>();
            var waypointGOs = GameObject.FindGameObjectsWithTag("CarWaypoint");
            foreach (var waypointGO in waypointGOs)
            {
                var waypoint = waypointGO.GetComponent<Waypoint>();

                var waypointData = new SaveData.WaypointData()
                {
                    position = waypoint.transform.position,
                    occupied = waypoint.occupied,
                    occupiers = new List<string>(from carNav in waypoint.occupiers select SafeGet(dictCarNavToID, carNav)),
                    discarded = waypoint.discarded,
                };

                var intersectionExitWaypoint = waypointGO.GetComponent<IntersectionExitWaypoint>();
                if (intersectionExitWaypoint != null)
                {
                    waypointData.numOfCollidersInTrigger = intersectionExitWaypoint.numOfCollidersInTrigger;
                    waypointData.prevBlocked = intersectionExitWaypoint.prevBlocked;
                }

                waypointDataList.Add(waypointData);
            }


            // Building Manager
            var buildingManagerData = saveData.buildingManagerData = new SaveData.BuildingManagerData()
            {
                isInitialized = buildingManager.IsInitialized,
                totalLivingCapacity = buildingManager.totalLivingCapacity,
                schools = new List<string>(from building in buildingManager.schools select dictBuildingToID[building]),

                buildingZonesAverageOccupancyRate = buildingManager.buildingZonesAverageOccupancyRate,
                buildingZonesTotalOccupancy = buildingManager.buildingZonesTotalOccupancy,
                treePlantation = buildingManager.treePlantation,
            };

            var buildingMap = buildingCreator.buildingMap;
            buildingManagerData.buildingIDMap = new string[buildingMap.GetLength(0), buildingMap.GetLength(1)];
            for (int x = 0; x < buildingMap.GetLength(0); x++)
            {
                for (int y = 0; y < buildingMap.GetLength(1); y++)
                {
                    buildingManagerData.buildingIDMap[x, y] = SafeGet(dictBuildingToID, buildingMap[x, y]);
                }
            }

            var blockMap = buildingCreator.blockMap;
            buildingManagerData.blockDataDataList = new List<SaveData.BlockDataData>();
            buildingManagerData.blockMapSize = new Vector2Int(blockMap.GetLength(0), blockMap.GetLength(1));
            for (int x = 0; x < blockMap.GetLength(0); x++)
            {
                for (int y = 0; y < blockMap.GetLength(1); y++)
                {
                    var blockData = blockMap[x, y];

                    if (blockData != null)
                    {
                        var blockDataData = new SaveData.BlockDataData()
                        {
                            position = blockData.Position,
                            worldPosition = blockData.WorldPosition,
                            zoneType = blockData.Type,
                            blockState = blockData.State,
                            neighboringRoads = blockData.neighboringRoads != null ? (from neighboringRoad in blockData.neighboringRoads select neighboringRoad != null ? dictRoadToID[neighboringRoad] : null).ToArray() : null,
                            areNeighboringRoadsSet = blockData.areNeighboringRoadsSet,
                            isThereNeighboringRoad = blockData.IsThereNeighboringRoad,
                            road = SafeGet(dictRoadToID, blockData.Road),
                        };
                        buildingManagerData.blockDataDataList.Add(blockDataData);
                    }
                }
            }


            // Buildings
            buildingManagerData.buildingDataList = new List<SaveData.BuildingData>();
            foreach (var buildingGO in buildingGOs)
            {
                var building = buildingGO.GetComponent<Building>();

                var buildingData = new SaveData.BuildingData()
                {
                    buildingID = dictBuildingToID[building],
                    buildingName = GetPrefabName(buildingGO),

                    position = building.transform.position,
                    rotation = building.transform.rotation,

                    mapPosition = building.MapPosition,
                    firstMapPosition = building.FirstMapPosition,
                    lastMapPosition = building.LastMapPosition,
                    size = building.size,
                    capacity = building.capacity,
                    zoneType = building.Type,
                    closestWaypointPos = building.ClosestWaypoint.transform.position,
                    doorDirection = building.DoorDirection,
                    facingRoadID = SafeGet(dictRoadToID, building.FacingRoad),
                    isSchool = building.isSchool,

                    residentsIDs = new List<string>(from person in building.residents select dictPersonToID[person]),
                };
                buildingManagerData.buildingDataList.Add(buildingData);
            }


            // Zone Controller
            var zoneControllerData = saveData.zoneControllerData = new SaveData.ZoneControllerData();

            zoneControllerData.zoneMap = new Dictionary<string, int[,]>();
            foreach (var zm in zoneController.zoneMap)
            {
                zoneControllerData.zoneMap.Add(dictRoadToID[zm.Key], zm.Value);
            }

            zoneControllerData.roadPositions = new Dictionary<Vector3, string>();
            foreach (var rp in zoneController.roadPositions)
            {
                zoneControllerData.roadPositions.Add(rp.Key, dictRoadToID[rp.Value]);
            }

            zoneControllerData.zoneTypes = new Dictionary<string, ZoneType>();
            foreach (var zt in zoneController.zoneTypes)
            {
                zoneControllerData.zoneTypes.Add(dictRoadToID[zt.Key], zt.Value);
            }

            zoneControllerData.cityPartTypes = zoneController.cityPartTypes;
            zoneControllerData.zoneColors = zoneController.zoneColors;


            // Cars
            var carManagerData = saveData.carManagerData = new SaveData.CarManagerData()
            {
                carDataList = new List<SaveData.CarData>(),
                activeCars = new List<string>(),
            };

            var carDataList = saveData.carManagerData.carDataList;
            foreach (var carNav in carNavs)
            {
                var carData = new SaveData.CarData()
                {
                    carName = GetPrefabName(carNav.gameObject),
                    vehicleType = carNav.vehicleType,
                    active = carNav.gameObject.activeSelf,

                    position = carNav.gameObject.transform.position,
                    rotation = carNav.gameObject.transform.rotation,

                    carNavID = dictCarNavToID[carNav],
                    speed = carNav.speed,
                    defaultSpeed = carNav.defaultSpeed,
                    path = carNav.path?.ToArray(),
                    nextOrCurrentWaypoint_pos = carNav.nextOrCurrentWaypoint != null ? carNav.nextOrCurrentWaypoint.transform.position : Vector3.positiveInfinity,
                    destination = carNav.destination,
                    centre = carNav.centre,
                    radius = carNav.radius,
                    nextDir = carNav.nextDir,
                    angularSpeed = carNav.angularSpeed,
                    carAtDestination = carNav.carAtDestination,
                    angle = carNav.angle,
                    correctionAngle = carNav.correctionAngle,
                    stoppingWP_pos = carNav.stoppingWP != null ? carNav.stoppingWP.transform.position : Vector3.positiveInfinity,
                    blockers = new List<string>(from cN in carNav.blockers select SafeGet(dictCarNavToID, cN)),
                    lookahead_pos = carNav.lookahead != null ? carNav.lookahead.transform.position : Vector3.positiveInfinity,
                    stopping = carNav.stopping,
                    accelerating = carNav.accelerating,
                    length = carNav.length,
                    stoppedAtIntersection = carNav.stoppedAtIntersection,

                    coroutineContext = carNav.changeSpeedCC,
                };
                carDataList.Add(carData);
            }

            foreach (var carNav in CarNavigation.activeCars)
            {
                saveData.carManagerData.activeCars.Add(SafeGet(dictCarNavToID, carNav));
            }

            // Bus Managers
            var busManagerData = saveData.busManagerData = new SaveData.BusManagerData()
            {
                nextBusIndex = busInstanceManager.nextBusIndex,
                nextLineIndex = busLineManager.nextLineIndex,
            };

            // Bus Stops
            var busStopDataList = busManagerData.busStopDataList = new List<SaveData.BusStopData>();
            foreach (var busStopGO in busStopGOs)
            {
                var busStop = busStopGO.GetComponent<BusStop>();
                var busStopData = new SaveData.BusStopData()
                {
                    busStopID = dictBusStopToID[busStop],
                    busStopName = GetPrefabName(busStop.gameObject),

                    position = busStop.transform.position,
                    rotation = busStop.transform.rotation,

                    adjacentWaypointPos = busStop.adjacentWaypoint.transform.position,
                    entranceWaypointPos = busStop.entranceWaypoint.transform.position,

                    lines = busStop.Lines,

                    next = busStop.next.ToDictionary(kv => kv.Key, kv => SafeGet(dictBusStopToID, kv.Value)),
                    previous = busStop.previous.ToDictionary(kv => kv.Key, kv => SafeGet(dictBusStopToID, kv.Value)),
                    nextPath = busStop.nextPath.ToDictionary(kv => kv.Key, kv => kv.Value.ToArray()),

                    occupied = busStop.occupied,
                    peopleAtStop = new List<string>(from person in busStop.peopleAtStop select SafeGet(dictPersonToID, person)),
                    busInstancesIDsToSpawn = new List<string>(from busInstance in busStop.instancesToSpawn select SafeGet(dictBusInstanceToID, busInstance)),
                    blockedEntrance = busStop.blockedEntrance,
                    lastToEnterCarNavID = SafeGet(dictCarNavToID, busStop.lastToEnter),
                    vehiclesInTrigger = new List<string>(from carNav in busStop.vehiclesInTrigger select SafeGet(dictCarNavToID, carNav)),
                    spawnBusesCC = busStop.spawnBusesCC,
                };

                busStopDataList.Add(busStopData);
            }

            // Bus Lines
            var busLineDataList = busManagerData.busLineDataList = new List<SaveData.BusLineData>();
            foreach (var line in busLines)
            {
                if (line is BusLineManager.BusLine busLine)
                {
                    var busLineData = new SaveData.BusLineData()
                    {
                        busLineID = dictBusLineToID[busLine],

                        ID = busLine.Id,
                        name = busLine.Name,
                        color = busLine.Color,

                        vehicles = new List<string>(from vehicle in busLine.Vehicles select dictBusInstanceToID[(vehicle as Bus).instance]),
                        path = new List<string>(from stop in busLine.Path select dictBusStopToID[stop]),

                        interval = busLine.schedule.Interval,

                        closed = busLine.Closed,
                        nextStopIndex = busLine.nextStopIndex,
                    };
                    busLineDataList.Add(busLineData);
                }
                else
                {
                    throw new System.Runtime.Serialization.SerializationException($"Unsupported ILine type: {line.GetType().Name}");
                }
            }


            // Bus Instances
            var busInstanceDataList = busManagerData.busInstanceDataList = new List<SaveData.BusInstanceData>();
            foreach (var busInstance in busInstanceManager.buses)
            {
                var busInstanceData = new SaveData.BusInstanceData()
                {
                    busInstanceID = dictBusInstanceToID[busInstance],

                    ID = busInstance.id,
                    name = busInstance.name,
                    busType = busInstance.type,
                    busLineID = SafeGet(dictBusLineToID, busInstance.line),
                };

                var bus = busInstance.bus;
                if (bus)
                {
                    busInstanceData.busCarNavID = dictCarNavToID[bus];

                    busInstanceData.damageMax = bus.damageMax;
                    busInstanceData.initialThreshold = bus.initialThreshold;
                    busInstanceData.thresholdStep = bus.thresholdStep;
                    busInstanceData.currentStopID = SafeGet(dictBusStopToID, bus.currentStop as BusStop);
                    busInstanceData.justStopped = bus.justStopped;
                    busInstanceData.timeToWait = bus.timeToWait;
                    busInstanceData.damage = bus.damage;
                    busInstanceData.prevPos = bus.prevPos;
                    busInstanceData.broken = bus.broken;
                    busInstanceData.threshold = bus.threshold;
                    busInstanceData.busHandlePassengerCC = bus.busHandlePassengerCC;
                }

                busInstanceDataList.Add(busInstanceData);
            }


            // Subway Manager
            var subwayManagerData = saveData.subwayManagerData = new SaveData.SubwayManagerData()
            {
                nextTrainIndex = subwayInstanceManager.nextTrainIndex,
                nextLineIndex = subwayLineManager.nextLineIndex,
            };


            // Subway Stops
            var subwayStopDataList = subwayManagerData.subwayStopDataList = new List<SaveData.SubwayStopData>();
            foreach (var subwayStop in subwayStops)
            {
                var subwayStopData = new SaveData.SubwayStopData()
                {
                    subwayStopID = dictSubwayStopToID[subwayStop],
                    buildingID = dictBuildingToID[subwayStop],

                    lines = subwayStop.Lines,

                    nextStop = subwayStop.nextStop.ToDictionary(kv => kv.Key, kv => SafeGet(dictSubwayStopToID, kv.Value)),
                    previousStop = subwayStop.previousStop.ToDictionary(kv => kv.Key, kv => SafeGet(dictSubwayStopToID, kv.Value)),
                };

                subwayStopDataList.Add(subwayStopData);
            }


            // Subway Lines
            var subwayLineDataList = subwayManagerData.subwayLineDataList = new List<SaveData.SubwayLineData>();
            foreach (var line in subwayLineManager.subwayLines)
            {
                if (line is SubwayLineManager.SubwayLine subwayLine)
                {
                    var subwayLineData = new SaveData.SubwayLineData()
                    {
                        subwayLineID = dictSubwayLineToID[subwayLine],

                        ID = subwayLine.Id,
                        name = subwayLine.Name,
                        color = subwayLine.Color,

                        vehicles = new List<string>(from vehicle in subwayLine.Vehicles select dictSubwayTrainInstanceToID[(vehicle as SubwayTrain).instance]),
                        path = new List<string>(from stop in subwayLine.Path select dictSubwayStopToID[stop]),

                        interval = subwayLine.schedule.Interval,

                        nextStopIndex = subwayLine.nextStopIndex,
                        direction = subwayLine.direction,
                    };

                    subwayLineDataList.Add(subwayLineData);
                }
                else
                {
                    throw new System.Runtime.Serialization.SerializationException($"Unsupported ILine type: {line.GetType().Name}");
                }
            }


            // Subway Train Instances
            var subwayTrainInstancesDataList = subwayManagerData.subwayTrainInstancesDataList = new List<SaveData.SubwayTrainInstanceData>();
            foreach (var subwayTrainInstance in subwayInstanceManager.trains)
            {
                var subwayTrainInstanceData = new SaveData.SubwayTrainInstanceData()
                {
                    subwayTrainInstanceID = dictSubwayTrainInstanceToID[subwayTrainInstance],

                    ID = subwayTrainInstance.id,
                    name = subwayTrainInstance.name,
                    lineID = SafeGet(dictSubwayLineToID, subwayTrainInstance.line),
                };

                var subwayTrain = subwayTrainInstance.train;
                if (subwayTrainInstanceData.trainSet = subwayTrain != null)
                {
                    subwayTrainInstanceData.position = subwayTrain.transform.position;
                    subwayTrainInstanceData.rotation = subwayTrain.transform.rotation;

                    subwayTrainInstanceData.timeToWait = subwayTrain.timeToWait;
                    subwayTrainInstanceData.defaultSpeed = subwayTrain.defaultSpeed;
                    subwayTrainInstanceData.speed = subwayTrain.speed;

                    subwayTrainInstanceData.currentStopID = SafeGet(dictSubwayStopToID, subwayTrain.currentStop);
                    subwayTrainInstanceData.destination = subwayTrain.destination;

                    subwayTrainInstanceData.stopping = subwayTrain.stopping;
                    subwayTrainInstanceData.accelerating = subwayTrain.accelerating;
                    subwayTrainInstanceData.goingBack = subwayTrain.goingBack;
                    subwayTrainInstanceData.waitingForUpdate = subwayTrain.waitingForUpdate;

                    subwayTrainInstanceData.passedThroughStopIDs = new List<string>(from stop in subwayTrain.passedThrough select SafeGet(dictSubwayStopToID, stop));
                    subwayTrainInstanceData.passengers = new List<string>(from passenger in subwayTrain.passengers select SafeGet(dictPersonToID, passenger));
                    subwayTrainInstanceData.preceedingTrains = new List<string>(from train in subwayTrain.preceedingTrains select SafeGet(dictSubwayTrainInstanceToID, train.instance));

                    subwayTrainInstanceData.broken = subwayTrain.broken;
                    subwayTrainInstanceData.threshold = subwayTrain.threshold;
                    subwayTrainInstanceData.damage = subwayTrain.damage;

                    subwayTrainInstanceData.changeSpeedCC = subwayTrain.changeSpeedCC;
                    subwayTrainInstanceData.subwayTrainHandlePassengerCC = subwayTrain.subwayTrainHandlePassengerCC;
                    subwayTrainInstanceData.prevPos = subwayTrain.prevPos;
                }

                subwayTrainInstancesDataList.Add(subwayTrainInstanceData);
            }


            // People Manager
            var peopleManagerData = saveData.peopleManagerData = new SaveData.PeopleManagerData()
            {
                destination = peopleManager.destination,

                childrenCount = peopleManager.childrenCount,
                adultCount = peopleManager.adultCount,
                pensionerCount = peopleManager.pensionerCount,

                createOnClick = peopleManager.createOnClick,
                debugRush = peopleManager.debugRush,

                peopleOwnedCarTotal = peopleManager.peopleOwnedCarTotal,
                peopleUsingCarCount = peopleManager.peopleUsingCarCount,
                peopleHappiness = peopleManager.peopleHappiness.ToDictionary(kv => SafeGet(dictPersonToID, kv.Key), kv => kv.Value),
                personsHappinessSum = peopleManager.personsHappinessSum,

                createAgentsCC = peopleManager.createAgentsCC,
            };

            peopleManagerData.personDataList = new List<SaveData.PersonData>();
            foreach (var person in people)
            {
                var personData = new SaveData.PersonData()
                {
                    personID = dictPersonToID[person],
                    personName = GetPrefabName(person.gameObject),

                    position = person.transform.position,
                    rotation = person.transform.rotation,

                    pathStops = person.PathStops != null ? (from stop in person.PathStops select SafeGet(dictIStopToID, stop)).ToArray() : null,
                    pathLines = person.PathLines != null ? (from line in person.PathLines select SafeGet(dictILineToID, line)).ToArray() : null,

                    isInsideVehicle = person.isInsideVehicle,

                    livingPlaceBuildingID = SafeGet(dictBuildingToID, person.livingPlace),
                    workingPlaceBuildingID = SafeGet(dictBuildingToID, person.workingPlace),
                    shoppingPlaceBuildingID = SafeGet(dictBuildingToID, person.shoppingPlace),

                    personType = person.personType,
                    ownedCarNavID = SafeGet(dictCarNavToID, person.ownedCar),
                    drivingCar = person.drivingCar,

                    currentDestination = person.currentDestination,
                    currentBuilding = SafeGet(dictBuildingToID, person.currentBuilding),
                    currentTransportType = person.currentTransportType,

                    isInsideBuilding = person.isInsideBuilding,
                    isComputingTransportType = person.isComputingTransportType,

                    personHappiness = person.personHappiness,
                    estimatedTravellingTime = person.estimatedTravellingTime,
                    startJourneyTime = person.startJourneyTime,
                    timer = person.timer,
                    rnd = person.rnd,
                    prevCarPos = person.prevCarPos,

                    chooseTransportTypeCC = person.chooseTransportTypeCC,
                };

                peopleManagerData.personDataList.Add(personData);
            }


            // Events
            saveData.eventsData = new SaveData.EventsData()
            {
                events = new List<SaveData.EventDataData>(
                    from e in cityCouncil.events
                    select new SaveData.EventDataData()
                    {
                        id = e.id,
                        startingNumber = e.startingNumber,
                    }),
                nextGoals = new Dictionary<string, object>()
            {
                { "time", CityCouncilEvents.TimeEvent.nextGoalTime },
                { "money", CityCouncilEvents.TimeEvent.nextGoalMoney },
            },
            };


            // Money
            saveData.moneyManagerData = new SaveData.MoneyManagerData()
            {
                money = moneyManager.Money,
                moneyFlow = moneyManager.MoneyFlow,

                hourlyGain = moneyManager.hourlyGain,
                thisHourGain = moneyManager.thisHourGain,

                moneyChanged = moneyManager.moneyChanged,
                moneyFlowChanged = moneyManager.moneyFlowChanged,
            };


            // Statistics
            saveData.statisticsDataManagerData = new SaveData.StatisticsDataManagerData()
            {
                lastStatisticsUpdateTime = statisticsDataManager.lastStatisticsUpdateTime,
                numericSeries = statisticsDataManager.NumericSeries,
            };


            SerializationManager.Serialize("saveData", saveData);

            SerializationManager.EndSerializeGroup();

            #endregion


            print($"Saved {saveName}");
        }
        catch (Exception ex)
        {
            PopUpWindowManager.Instance.EnableOneButtonWindow($"Saving game failed!", () => { });
            Debug.LogError($"Exception occured while serializing save {saveName}:\n{ex.Message}");
            return;
        }
    }

    /// <summary>
    /// Loads game from file
    /// </summary>
    /// <param name="saveName">name of the save file without extension</param>
    private void LoadGame_(string saveName)
    {
        #region ======== DESERIALIZE DATA ========

        SaveData saveData = null;

        try
        {
            SerializationManager.BeginDeserializeGroup(saveName);

            // Version
            var saveVersion = SerializationManager.Deserialize<string>("saveVersion");

            if (saveVersion == SaveData_1_0_0.saveVersion)
            {
                saveData = SerializationManager.Deserialize<SaveData_1_0_0>("saveData");
            }
            else
            {
                PopUpWindowManager.Instance.EnableOneButtonWindow($"Unsupported save version: {saveVersion}", () => { });
                throw new Exception($"Unsupported save version: {saveVersion}");
            }

            // --- Older saveData shall be converted to newest version here --- 

            SerializationManager.EndDeserializeGroup();

        }
        catch (Exception ex)
        {
            Debug.LogError($"Exception occured while deserializing save {saveName}:\n{ex.Message}");
            return;
        }
        #endregion

        try
        {
            #region ======== DESTROY CURRENT SCENE ========


            // Stop all coroutines
            var allMonoBehaviours = FindObjectsOfType<MonoBehaviour>();
            foreach (var monoBehaviour in allMonoBehaviours)
            {
                if (!(monoBehaviour is SaveManager))
                {
                    monoBehaviour.StopAllCoroutines();
                }
            }

            // Clear Time handlers
            timeManager.ClearHandlers();

            // Hide overlays
            busLineManager.ToggleAllLines(false);
            subwayLineManager.ToggleAllLines(false);
            zoneController.ToggleZones(false);

            // Terrain
            var currentTrees = GameObject.FindGameObjectsWithTag("Tree");
            foreach (var tree in currentTrees)
            {
                DestroyImmediate(tree);
            }

            // Road Graph
            var currentRoadGOs = GameObject.FindGameObjectsWithTag("Road");
            foreach (var roadGO in currentRoadGOs)
            {
                DestroyImmediate(roadGO);
            }
            foreach (var districtData in roadGraphGeneration.allDistricts)
            {
                Array.Clear(districtData.models, 0, districtData.models.Length);
            }
            roadGraphGeneration.allDistricts?.Clear();

            // Buildings
            var currentBuildingGOs = GameObject.FindGameObjectsWithTag("Building");
            foreach (var buildingGO in currentBuildingGOs)
            {
                DestroyImmediate(buildingGO);
            }
            Array.Clear(buildingCreator.buildingMap, 0, buildingCreator.buildingMap.Length);
            Array.Clear(buildingCreator.blockMap, 0, buildingCreator.blockMap.Length);
            buildingCreator.buildings?.Clear();
            foreach (var buildingsOfZones in BuildingManager.instance.buildingsOfZones ?? new List<HashSet<Building>>())
            {
                buildingsOfZones?.Clear();
            }
            Array.Clear(BuildingManager.instance.buildingZonesAverageOccupancyRate, 0, BuildingManager.instance.buildingZonesAverageOccupancyRate.Length);
            Array.Clear(BuildingManager.instance.buildingZonesTotalOccupancy, 0, BuildingManager.instance.buildingZonesTotalOccupancy.Length);

            // Cars
            var currentCarNavs = CarNavigation.allCars;
            foreach (var carNav in currentCarNavs)
            {
                if (carNav != null)
                {
                    DestroyImmediate(carNav.gameObject);
                }
            }
            CarNavigation.allCars.Clear();
            CarNavigation.activeCars.Clear();

            // Zone Controller
            zoneController.zoneMap.Clear();
            zoneController.roadPositions.Clear();
            zoneController.zoneTypes.Clear();
            Array.Clear(zoneController.cityPartTypes, 0, zoneController.cityPartTypes.Length);

            // Buses
            var currentBusStopGOs = GameObject.FindGameObjectsWithTag("BusStop");
            foreach (var busStopGO in currentBusStopGOs)
            {
                DestroyImmediate(busStopGO);
            }
            busLineManager.busLines.Clear();
            busInstanceManager.buses.Clear();
            // - buses destroyed as cars
            busLineManager.linesDisplayed = false;

            // Subway
            // - subway stops destroyed as buildings
            subwayLineManager.subwayLines.Clear();
            foreach (var subwayInstance in subwayInstanceManager.trains)
            {
                if (subwayInstance.train != null)
                {
                    DestroyImmediate(subwayInstance.train.gameObject);
                }
            }
            subwayInstanceManager.trains.Clear();
            subwayLineManager.LinesDisplayed = false;

            // People Manager
            var peopleClone = new List<Person>(peopleManager.people.Count);
            foreach (var person in peopleManager.people)
                peopleClone.Add(person);

            foreach (var person in peopleClone)
            {
                if (person != null) // - people that are driving, are destroyed with their car
                {
                    DestroyImmediate(person.gameObject);
                }
            }
            peopleManager.people.Clear();

            // Events
            cityCouncil.events.Clear();

            #endregion


            #region ======== INSTANTIATE OBJECTS ========

            // Terrrain
            var treePrefab = Resources.Load<GameObject>($"{folderPathBuildingPrefabs}/Tree");
            foreach (var treeData in saveData.terrainDataData.treeDataList)
            {
                Instantiate(treePrefab, treeData.position, treeData.rotation, BuildingManager.instance.buildingParentObject.transform);
            }

            // Roads
            var dictIDToRoad = new Dictionary<string, GameObject>();
            foreach (var roadData in saveData.roadGraphData.roadDataList)
            {
                dictIDToRoad[roadData.roadID] = Instantiate(
                    Resources.Load<GameObject>($"Prefabs/Roads/{roadData.roadType}"),
                    roadData.position,
                    roadData.rotation,
                    roadGraphGeneration.roadParentObject.transform
                );
            }
            roadGraphGeneration.BakeRoadNavmesh();

            var dictIDToDistrict = new Dictionary<string, DistrictData>();
            foreach (var districtDataData in saveData.roadGraphData.districtDataDataList)
            {
                dictIDToDistrict[districtDataData.districtID] = new DistrictData(districtDataData.blockCount, districtDataData.blockSize, districtDataData.cityCenter, districtDataData.districtState);
            }

            // Cars
            var dictIDToCarNav = new Dictionary<string, CarNavigation>();
            foreach (var carData in saveData.carManagerData.carDataList)
            {
                dictIDToCarNav[carData.carNavID] = Instantiate(
                    Resources.Load<GameObject>($"Prefabs/{GetVehiclePrefabFolderName(carData.vehicleType)}/{carData.carName}"),
                    carData.position,
                    carData.rotation,
                    CarCreator.instance.transform
                ).GetComponent<CarNavigation>();
            }

            // Buildings
            var dictIDToBuilding = new Dictionary<string, Building>();
            foreach (var buildingData in saveData.buildingManagerData.buildingDataList)
            {
                dictIDToBuilding[buildingData.buildingID] = Instantiate(
                    Resources.Load<GameObject>($"Prefabs/Buildings/{buildingData.buildingName}"),
                    buildingData.position,
                    buildingData.rotation,
                    buildingCreator.buildingParentObject.transform
                ).GetComponent<Building>();
            }

            // Bus Stops
            var dictIDToBusStop = new Dictionary<string, BusStop>();
            foreach (var busStopData in saveData.busManagerData.busStopDataList)
            {
                dictIDToBusStop[busStopData.busStopID] = Instantiate(
                    Resources.Load<GameObject>($"Prefabs/Stops/{busStopData.busStopName}"),
                    busStopData.position,
                    busStopData.rotation
                // TODO add parent object to busstop
                ).GetComponent<BusStop>();
            }

            // Bus Lines
            var dictIDToBusLine = new Dictionary<string, BusLineManager.BusLine>();
            foreach (var busLineData in saveData.busManagerData.busLineDataList)
            {
                dictIDToBusLine[busLineData.busLineID] = new BusLineManager.BusLine();
            }

            // Bus Instances
            var dictIDToBusInstance = new Dictionary<string, BusInstanceManager.BusInstance>();
            foreach (var busInstanceData in saveData.busManagerData.busInstanceDataList)
            {
                dictIDToBusInstance[busInstanceData.busInstanceID] = new BusInstanceManager.BusInstance()
                {
                    bus = SafeGet(dictIDToCarNav, busInstanceData.busCarNavID) as Bus
                };
            }

            // Subway Stop
            var dictIDToSubwayStop = new Dictionary<string, SubwayStop>();
            foreach (var subwayStopData in saveData.subwayManagerData.subwayStopDataList)
            {
                dictIDToSubwayStop.Add(subwayStopData.subwayStopID, dictIDToBuilding[subwayStopData.buildingID] as SubwayStop);
            }

            // Subway Line
            var dictIDToSubwayLine = new Dictionary<string, SubwayLineManager.SubwayLine>();
            foreach (var subwayLineData in saveData.subwayManagerData.subwayLineDataList)
            {
                dictIDToSubwayLine.Add(subwayLineData.subwayLineID, new SubwayLineManager.SubwayLine());
            }

            // Subway Train Instance
            var dictIDToSubwayTrainInstance = new Dictionary<string, SubwayInstanceManager.SubwayTrainInstance>();
            var subwayTrainPrefab = Resources.Load<GameObject>("Prefabs/Subway/SubwayTrain");
            foreach (var subwayTrainInstanceData in saveData.subwayManagerData.subwayTrainInstancesDataList)
            {
                var subwayTrainInstance = new SubwayInstanceManager.SubwayTrainInstance();
                dictIDToSubwayTrainInstance.Add(subwayTrainInstanceData.subwayTrainInstanceID, subwayTrainInstance);
                if (subwayTrainInstanceData.trainSet)
                {
                    subwayTrainInstance.train = Instantiate(
                        subwayTrainPrefab,
                        subwayTrainInstanceData.position,
                        subwayTrainInstanceData.rotation
                    ).GetComponent<SubwayTrain>();
                }
            }

            // IStops and ILines
            var dictIDToIStop = new Dictionary<string, IStop>();
            foreach (var idbs in dictIDToBusStop)
            {
                dictIDToIStop.Add(idbs.Key, idbs.Value);
            }
            foreach (var idss in dictIDToSubwayStop)
            {
                dictIDToIStop.Add(idss.Key, idss.Value);
            }

            var dictIDToILine = new Dictionary<string, ILine>();
            foreach (var idbl in dictIDToBusLine)
            {
                dictIDToILine.Add(idbl.Key, idbl.Value);
            }
            foreach (var idsl in dictIDToSubwayLine)
            {
                dictIDToILine.Add(idsl.Key, idsl.Value);
            }

            // People
            var dictIDToPerson = new Dictionary<string, Person>();
            foreach (var personData in saveData.peopleManagerData.personDataList)
            {
                var personGO = Instantiate(
                    Resources.Load<GameObject>($"Prefabs/People/{personData.personType}/{personData.personName}"),
                    personData.position,
                    personData.rotation,
                    peopleManager.personParent.transform
                );

                switch (personData.personType)
                {
                    case PersonType.Child:
                        personGO.gameObject.AddComponent<BasicChild>();
                        break;
                    case PersonType.Adult:
                        personGO.gameObject.AddComponent<BasicAdult>();
                        break;
                    case PersonType.Pensioner:
                        personGO.gameObject.AddComponent<BasicPensioner>();
                        break;
                    default:
                        break;
                }

                dictIDToPerson[personData.personID] = personGO.GetComponent<Person>();
            }

            #endregion


            #region ======== SETUP NEW SCENE ========

            var coroutineContextPairs = new List<IResumable>();

            // Camera
            var cameraData = saveData.cameraData;
            Camera.main.transform.position = cameraData.position;
            Camera.main.transform.rotation = cameraData.rotation;


            // Time
            var timeData = saveData.timeData;
            timeManager.CurrentDateTime = timeData.currentDateTime;


            // Terrain
            var terrainDataData = saveData.terrainDataData;
            heightMapApplier.thickness = terrainDataData.thickness;
            heightMapApplier.scale = terrainDataData.scale;
            heightMapApplier.power = terrainDataData.power;
            heightMapApplier.offsetX = terrainDataData.offsetX;
            heightMapApplier.offsetY = terrainDataData.offsetY;

            TerrainData newData = Terrain.activeTerrain.terrainData;

            newData.heightmapResolution = terrainDataData.heightmap.GetLength(0);
            newData.size = new Vector3(terrainDataData.heightmap.GetLength(0), terrainDataData.thickness, terrainDataData.heightmap.GetLength(1));
            newData.SetHeights(0, 0, terrainDataData.heightmap);

            Terrain.activeTerrain.terrainData = newData;


            // Road Graph
            var roadGraphData = saveData.roadGraphData;
            roadGraphGeneration.heightLimit = roadGraphData.heightLimit;
            roadGraphGeneration.blockCount = roadGraphData.blockCount;
            roadGraphGeneration.terrainFlatteningExtension = roadGraphData.terrainFlatteningExtension;
            roadGraphGeneration.cityBaseHeight = roadGraphData.cityBaseHeight;

            roadGraphGeneration.terrainSize = roadGraphData.terrainSize;
            roadGraphGeneration.terrainCenter = roadGraphData.terrainCenter;
            roadGraphGeneration.cityCenter = roadGraphData.cityCenter;
            roadGraphGeneration.totalCitySize = roadGraphData.totalCitySize;
            roadGraphGeneration.downLeftCorner = roadGraphData.downLeftCorner;
            roadGraphGeneration.southWestCorner = roadGraphData.southWestCorner;

            roadGraphGeneration.supermodels = new GameObject[roadGraphData.supermodels.GetLength(0), roadGraphData.supermodels.GetLength(1)];
            for (int x = 0; x < roadGraphData.supermodels.GetLength(0); x++)
            {
                for (int y = 0; y < roadGraphData.supermodels.GetLength(1); y++)
                {
                    roadGraphGeneration.supermodels[x, y] = SafeGet(dictIDToRoad, roadGraphData.supermodels[x, y]);
                }
            }

            roadGraphGeneration.districts = new DistrictData[roadGraphData.districts.GetLength(0), roadGraphData.districts.GetLength(1)];
            for (int x = 0; x < roadGraphData.districts.GetLength(0); x++)
            {
                for (int y = 0; y < roadGraphData.districts.GetLength(1); y++)
                {
                    roadGraphGeneration.districts[x, y] = SafeGet(dictIDToDistrict, roadGraphData.districts[x, y]);
                }
            }

            foreach (var districtDataData in roadGraphData.districtDataDataList)
            {
                var districtData = dictIDToDistrict[districtDataData.districtID];

                districtData.downLeftCorner = districtDataData.downLeftCorner;
                districtData.citySize = districtDataData.citySize;
                districtData.roads = districtDataData.roads;
                districtData.junctions = districtDataData.junctions;
                districtData.crosswalks = districtDataData.crosswalks;
                districtData.directionFromMainDistrict = districtDataData.directionFromMainDistrict;

                if (districtDataData.districtID == roadGraphData.centerDistrict)
                {
                    roadGraphGeneration.centerDistrict = districtData;
                }

                districtData.models = new GameObject[districtData.citySize.x, districtData.citySize.y];
                for (int x = 0; x < districtData.citySize.x; x++)
                {
                    for (int y = 0; y < districtData.citySize.y; y++)
                    {
                        districtData.models[x, y] = SafeGet(dictIDToRoad, districtDataData.roadIDmap[x, y]);
                    }
                }
            }

            // Roads
            foreach (var roadData in saveData.roadGraphData.roadDataList)
            {
                var roadGO = dictIDToRoad[roadData.roadID];

                // Intersection
                var intersectionData = roadData.intersectionData;
                if (intersectionData != null)
                {
                    var intersection = roadGO.GetComponent<Intersection>();

                    foreach (var buffer in intersectionData.buffers)
                    {
                        var waypoint = GetWaypoint(buffer.Key).GetComponent<IntersectionWaypoint>();
                        intersection.buffers[waypoint] = new List<CarNavigation>(from carNavID in buffer.Value select dictIDToCarNav[carNavID]);
                    }

                    intersection.pendingVehicles = new List<CarNavigation>(from carNavID in intersectionData.pendingVehicles select dictIDToCarNav[carNavID]);

                    if (intersectionData.intersectionDirectorType == SaveData.IntersectionDirectorType.BasicDirector)
                    {
                        var director = intersection.director as BasicDirector ?? throw new SerializationDataInconsistencyException();

                        foreach (var directionIndicator in intersectionData.directionIndicators)
                        {
                            var wp = GetWaypoint(directionIndicator.Key);
                            if (director.directionIndicators.ContainsKey(wp))
                            {
                                director.directionIndicators[wp] = directionIndicator.Value;
                            }
                            else
                            {
                                director.directionIndicators.Add(wp, directionIndicator.Value);
                            }
                        }

                        foreach (var occupiedCluster in intersectionData.occupiedClusters)
                        {
                            var carNav = dictIDToCarNav[occupiedCluster.Key];
                            var waypoints = new List<Waypoint>(from pos in occupiedCluster.Value select GetWaypoint(pos));

                            director.occupiedClusters.Add(carNav, waypoints);
                        }
                    }

                    var addCarCC = intersectionData.addCarCC;
                    addCarCC.carNavigation = SafeGet(dictIDToCarNav, addCarCC.carNavID);
                    addCarCC.intersectionWaypoint = GetWaypoint(addCarCC.intersectionWaypointPosition)?.GetComponent<IntersectionWaypoint>();
                    intersection.addCarCC = addCarCC;

                    coroutineContextPairs.Add(new CoroutineContextPair<Intersection>(addCarCC, intersection));
                }

                // Crosswalk
                var crosswalkData = roadData.crosswalkData;
                if (crosswalkData != null)
                {
                    var crosswalk = roadGO.GetComponent<Crosswalk>();

                    crosswalk.timeSinceLastLeftNotification = crosswalkData.timeSinceLastLeftNotification;
                    crosswalk.timeSinceLastRightNotification = crosswalkData.timeSinceLastRightNotification;

                    crosswalk.vehiclesInRightTrigger = new List<CarNavigation>(from carNavID in crosswalkData.vehicleIDsInRightTrigger select SafeGet(dictIDToCarNav, carNavID));
                    crosswalk.vehiclesInLeftTrigger = new List<CarNavigation>(from carNavID in crosswalkData.vehicleIDsInLeftTrigger select SafeGet(dictIDToCarNav, carNavID));

                    crosswalk.leftOccupied = crosswalkData.leftOccupied;
                    crosswalk.rightOccupied = crosswalkData.rightOccupied;
                }
            }


            // Link Straight waypoints
            var wpos = GameObject.FindGameObjectsWithTag("CarWaypoint");
            foreach (var wpo in wpos)
            {
                var wp = wpo.GetComponent<Waypoint>();
                if (wp is StraightWaypoint swp)
                {
                    swp.FindNextWaypoint();
                }
            }

            // Waypoints
            var waypointDataList = saveData.roadGraphData.waypointDataList;
            foreach (var waypointData in waypointDataList)
            {
                var waypoint = GetWaypoint(waypointData.position);

                waypoint.occupied = waypointData.occupied;
                waypoint.occupiers = new List<CarNavigation>(from carNavID in waypointData.occupiers select SafeGet(dictIDToCarNav, carNavID));

                var intersectionExitWaypoint = waypoint.GetComponent<IntersectionExitWaypoint>();
                if (intersectionExitWaypoint != null)
                {
                    intersectionExitWaypoint.numOfCollidersInTrigger = waypointData.numOfCollidersInTrigger;
                    intersectionExitWaypoint.prevBlocked = waypointData.prevBlocked;
                }
            }


            // Cars
            var carManagerData = saveData.carManagerData;

            for (int i = 0; i < carManagerData.carDataList.Count; i++)
            {
                var carData = carManagerData.carDataList[i];
                var carNav = dictIDToCarNav[carData.carNavID];

                CarNavigation.allCars.Add(carNav);

                carNav.vehicleType = carData.vehicleType;
                carNav.speed = carData.speed;
                carNav.defaultSpeed = carData.defaultSpeed;
                carNav.path = carData.path != null ? new Stack<Direction>(carData.path.Reverse()) : null;
                carNav.nextOrCurrentWaypoint = GetWaypoint(carData.nextOrCurrentWaypoint_pos);
                carNav.destination = carData.destination;
                carNav.centre = carData.centre;
                carNav.radius = carData.radius;
                carNav.nextDir = carData.nextDir;
                carNav.angularSpeed = carData.angularSpeed;
                carNav.carAtDestination = carData.carAtDestination;
                carNav.angle = carData.angle;
                carNav.correctionAngle = carData.correctionAngle;
                carNav.stoppingWP = GetWaypoint(carData.stoppingWP_pos);
                carNav.blockers = new List<CarNavigation>(from carNavID in carData.blockers select SafeGet(dictIDToCarNav, carNavID));
                carNav.lookahead = GetWaypoint(carData.lookahead_pos);
                carNav.stopping = carData.stopping;
                carNav.accelerating = carData.accelerating;
                carNav.length = carData.length;
                carNav.stoppedAtIntersection = carData.stoppedAtIntersection;

                carNav.changeSpeedCC = carData.coroutineContext;

                coroutineContextPairs.Add(new CoroutineContextPair<CarNavigation>(carNav.changeSpeedCC, carNav));
                carNav.gameObject.SetActive(carData.active);
            }

            foreach (var carNavID in carManagerData.activeCars)
            {
                var carNav = SafeGet(dictIDToCarNav, carNavID);

                if (carNav != null)
                {
                    CarNavigation.activeCars.Add(carNav);
                }
            }


            // Buildings
            var buildingDataList = saveData.buildingManagerData.buildingDataList;
            foreach (var buildingData in buildingDataList)
            {
                var building = dictIDToBuilding[buildingData.buildingID];

                building.MapPosition = buildingData.mapPosition;
                building.FirstMapPosition = buildingData.firstMapPosition;
                building.LastMapPosition = buildingData.lastMapPosition;
                building.size = buildingData.size;
                building.capacity = buildingData.capacity;
                building.Type = buildingData.zoneType;
                building.ClosestWaypoint = GetWaypoint(buildingData.closestWaypointPos).gameObject;
                building.DoorDirection = buildingData.doorDirection;
                building.FacingRoad = SafeGet(dictIDToRoad, buildingData.facingRoadID);
                building.isSchool = buildingData.isSchool;

                building.residents.AddRange(from personID in buildingData.residentsIDs select dictIDToPerson[personID]);

                buildingCreator.buildings.Add(building);
                BuildingManager.instance.buildingsOfZones[(int)building.Type].Add(building);
            }


            // Building Manager
            var buildingManagerData = saveData.buildingManagerData;

            buildingManager.IsInitialized = buildingManagerData.isInitialized;
            buildingManager.totalLivingCapacity = buildingManagerData.totalLivingCapacity;
            buildingManager.schools = new List<Building>(from buildingID in buildingManagerData.schools select dictIDToBuilding[buildingID]);

            buildingManager.buildingZonesAverageOccupancyRate = buildingManagerData.buildingZonesAverageOccupancyRate;
            buildingManager.buildingZonesTotalOccupancy = buildingManagerData.buildingZonesTotalOccupancy;
            buildingManager.treePlantation = buildingManagerData.treePlantation;

            buildingCreator.buildingMap = new Building[buildingManagerData.buildingIDMap.GetLength(0), buildingManagerData.buildingIDMap.GetLength(1)];
            for (int x = 0; x < buildingManagerData.buildingIDMap.GetLength(0); x++)
            {
                for (int y = 0; y < buildingManagerData.buildingIDMap.GetLength(1); y++)
                {
                    var buildingID = buildingManagerData.buildingIDMap[x, y];
                    buildingCreator.buildingMap[x, y] = SafeGet(dictIDToBuilding, buildingID);
                }
            }
            buildingCreator.blockMap = new BlockData[buildingManagerData.blockMapSize.x, buildingManagerData.blockMapSize.y];

            foreach (var blockDataData in buildingManagerData.blockDataDataList)
            {
                var blockData = buildingCreator.blockMap[blockDataData.position.x, blockDataData.position.y] = new BlockData()
                {
                    Position = blockDataData.position,
                    WorldPosition = blockDataData.worldPosition,
                    Type = blockDataData.zoneType,
                    State = blockDataData.blockState,
                    neighboringRoads = blockDataData.neighboringRoads != null ? (from neighboringRoadID in blockDataData.neighboringRoads select neighboringRoadID != null ? dictIDToRoad[neighboringRoadID] : null).ToArray() : null,
                    areNeighboringRoadsSet = blockDataData.areNeighboringRoadsSet,
                    IsThereNeighboringRoad = blockDataData.isThereNeighboringRoad,
                    Road = SafeGet(dictIDToRoad, blockDataData.road),
                };
            }
            BuildingManager.instance.LoadSyncManager(buildingCreator.blockMap, buildingCreator.buildingMap, buildingCreator.buildings);


            // Zone Controller
            var zoneControllerData = saveData.zoneControllerData;

            zoneController.zoneMap = new Dictionary<GameObject, int[,]>();
            foreach (var zm in zoneControllerData.zoneMap)
            {
                zoneController.zoneMap.Add(dictIDToRoad[zm.Key], zm.Value);
            }

            zoneController.roadPositions = new Dictionary<Vector3, GameObject>();
            foreach (var rp in zoneControllerData.roadPositions)
            {
                zoneController.roadPositions.Add(rp.Key, dictIDToRoad[rp.Value]);
            }

            zoneController.zoneTypes = new Dictionary<GameObject, ZoneType>();
            foreach (var zt in zoneControllerData.zoneTypes)
            {
                zoneController.zoneTypes.Add(dictIDToRoad[zt.Key], zt.Value);
            }

            zoneController.cityPartTypes = zoneControllerData.cityPartTypes;
            zoneController.zoneColors = zoneControllerData.zoneColors;

            var roads = GameObject.FindGameObjectsWithTag("Road");
            foreach (var road in roads) // Code inspired from last part of method 'ZoneController.ChooseTypeForZones()'
            {
                var type = zoneController.zoneTypes[road];

                var roadZone = road.GetComponent<RoadZone>();
                roadZone.InitializeZoneMarkerList();
                roadZone.SetZoneMarkersColour(zoneController.zoneColors[type]);
            }

            // Bus Manager
            var busManagerData = saveData.busManagerData;

            busInstanceManager.nextBusIndex = busManagerData.nextBusIndex;
            busLineManager.nextLineIndex = busManagerData.nextLineIndex;

            // Bus Stops
            foreach (var busStopData in busManagerData.busStopDataList)
            {
                var busStop = dictIDToBusStop[busStopData.busStopID];

                busStop.adjacentWaypoint = GetWaypoint(busStopData.adjacentWaypointPos);
                busStop.entranceWaypoint = GetWaypoint(busStopData.entranceWaypointPos);

                busStop.Lines = busStopData.lines;

                busStop.next = busStopData.next.ToDictionary(kv => kv.Key, kv => SafeGet(dictIDToBusStop, kv.Value));
                busStop.previous = busStopData.previous.ToDictionary(kv => kv.Key, kv => SafeGet(dictIDToBusStop, kv.Value));
                busStop.nextPath = busStopData.nextPath.ToDictionary(kv => kv.Key, kv => new Stack<Direction>(kv.Value.Reverse()));

                busStop.occupied = busStopData.occupied;
                busStop.peopleAtStop = new List<Person>(from personID in busStopData.peopleAtStop select dictIDToPerson[personID]);
                busStop.instancesToSpawn = new Queue<BusInstanceManager.BusInstance>(from busInstanceID in busStopData.busInstancesIDsToSpawn select dictIDToBusInstance[busInstanceID]);
                busStop.blockedEntrance = busStopData.blockedEntrance;
                busStop.lastToEnter = SafeGet(dictIDToCarNav, busStopData.lastToEnterCarNavID);
                busStop.vehiclesInTrigger = new List<CarNavigation>(from carNavID in busStopData.vehiclesInTrigger select SafeGet(dictIDToCarNav, carNavID));

                busStop.spawnBusesCC = busStopData.spawnBusesCC;
                coroutineContextPairs.Add(new CoroutineContextPair<BusStop>(new SpawnBusesCoroutineContext(), busStop));
            }

            // Bus Lines
            foreach (var busLineData in busManagerData.busLineDataList)
            {
                var busLine = dictIDToBusLine[busLineData.busLineID];

                busLine.Id = busLineData.ID;
                busLine.Name = busLineData.name;
                busLine.Color = busLineData.color;

                busLine.Vehicles = new List<IVehicle>(from busInstanceID in busLineData.vehicles select dictIDToBusInstance[busInstanceID].bus);
                busLine.Path = new List<IStop>(from busStopID in busLineData.path select dictIDToBusStop[busStopID]);

                var busSchedule = busLine.schedule = new BusSchedule(busLine);
                busSchedule.Interval = busLineData.interval;

                busLine.Closed = busLineData.closed;
                busLine.gradient = busLineManager.GetGradientFromColor(busLine.Color);
                busLine.nextStopIndex = busLineData.nextStopIndex;

                foreach (var stop in busLine.Path)
                {
                    var busStop = stop as BusStop;
                    var nextBusStop = SafeGet(busStop.next, busLine.Id);

                    var busStopSpriteRenderer = busStop.GetComponentInChildren<SpriteRenderer>();
                    busStopSpriteRenderer.enabled = false;
                    busStopSpriteRenderer.color = busLine.Color;
                    busStop.transform.Find("Sprite").transform.position = busStop.transform.Find("Sprite").transform.position + busLineCreator.drawnLineYOffset;

                    if (nextBusStop != null)
                    {
                        var lineRenderer = Instantiate(Resources.Load<LineRenderer>(lineRendererPath), busStop.transform);

                        lineRenderer.colorGradient = busLine.gradient;
                        busLineCreator.DrawPath(busStop.adjacentWaypoint, nextBusStop.adjacentWaypoint, new Stack<Direction>(busStop.nextPath[busLine.Id].ToArray().Reverse()), lineRenderer);

                        lineRenderer.gameObject.SetActive(false);
                        busStop.lineRenderers.Add(busLine.Id, lineRenderer);
                    }
                }

                busLineManager.busLines.Add(busLine);
            }

            // Bus Instances
            foreach (var busInstanceData in busManagerData.busInstanceDataList)
            {
                var busInstance = dictIDToBusInstance[busInstanceData.busInstanceID];

                busInstance.id = busInstanceData.ID;
                busInstance.name = busInstanceData.name;
                busInstance.type = busInstanceData.busType;
                busInstance.line = SafeGet(dictIDToBusLine, busInstanceData.busLineID);
                var bus = busInstance.bus;

                if (bus != null)
                {
                    bus.instance = busInstance;
                    bus.damageMax = busInstanceData.damageMax;
                    bus.initialThreshold = busInstanceData.initialThreshold;
                    bus.thresholdStep = busInstanceData.thresholdStep;
                    bus.currentStop = SafeGet(dictIDToBusStop, busInstanceData.currentStopID);
                    bus.justStopped = busInstanceData.justStopped;
                    bus.timeToWait = busInstanceData.timeToWait;
                    bus.damage = busInstanceData.damage;
                    bus.prevPos = busInstanceData.prevPos;
                    bus.broken = busInstanceData.broken;
                    bus.threshold = busInstanceData.threshold;
                    bus.busHandlePassengerCC = busInstanceData.busHandlePassengerCC;
                }

                busInstanceManager.buses.Add(busInstance);
            }


            // Subway Manager
            var subwayManagerData = saveData.subwayManagerData;
            subwayInstanceManager.nextTrainIndex = subwayManagerData.nextTrainIndex;
            subwayLineManager.nextLineIndex = subwayManagerData.nextLineIndex;

            // Subway Stops
            foreach (var subwayStopData in subwayManagerData.subwayStopDataList)
            {
                var subwayStop = dictIDToSubwayStop[subwayStopData.subwayStopID];

                subwayStop.Lines = subwayStopData.lines;

                subwayStop.nextStop = subwayStopData.nextStop.ToDictionary(kv => kv.Key, kv => SafeGet(dictIDToSubwayStop, kv.Value));
                subwayStop.previousStop = subwayStopData.previousStop.ToDictionary(kv => kv.Key, kv => SafeGet(dictIDToSubwayStop, kv.Value));
            }

            // Subway Lines
            foreach (var subwayLineData in subwayManagerData.subwayLineDataList)
            {
                var subwayLine = dictIDToSubwayLine[subwayLineData.subwayLineID];

                subwayLine.Id = subwayLineData.ID;
                subwayLine.Name = subwayLineData.name;
                subwayLine.Color = subwayLineData.color;

                subwayLine.Vehicles = new List<IVehicle>(from vehicleID in subwayLineData.vehicles select dictIDToSubwayTrainInstance[vehicleID].train);
                subwayLine.Path = new List<IStop>(from stopID in subwayLineData.path select dictIDToSubwayStop[stopID]);

                subwayLine.schedule = new SubwaySchedule(subwayLine)
                {
                    Interval = subwayLineData.interval,
                };

                subwayLine.nextStopIndex = subwayLineData.nextStopIndex;
                subwayLine.direction = subwayLineData.direction;

                foreach (var stop in subwayLine.Path)
                {
                    var subwayStop = stop as SubwayStop;
                    var nextSubwayStop = SafeGet(subwayStop.nextStop, subwayLine.Id);

                    if (nextSubwayStop != null)
                    {
                        var lineRenderer = Instantiate(Resources.Load<LineRenderer>(lineRendererPath), subwayStop.transform);

                        lineRenderer.startWidth = 4;
                        lineRenderer.endWidth = 4;

                        lineRenderer.positionCount = 2;
                        lineRenderer.SetPosition(0, subwayStop.transform.position - subwayLineCreator.lineRendererYShift);
                        lineRenderer.SetPosition(1, nextSubwayStop.transform.position - subwayLineCreator.lineRendererYShift);

                        lineRenderer.gameObject.SetActive(false);
                        subwayStop.lineRenderers.Add(subwayLine.Id, lineRenderer);
                    }
                }

                subwayLineManager.subwayLines.Add(subwayLine);
            }

            // Subway Train Instances
            foreach (var subwayTrainInstanceData in subwayManagerData.subwayTrainInstancesDataList)
            {
                var subwayTrainInstance = dictIDToSubwayTrainInstance[subwayTrainInstanceData.subwayTrainInstanceID];

                subwayTrainInstance.id = subwayTrainInstanceData.ID;
                subwayTrainInstance.name = subwayTrainInstanceData.name;
                subwayTrainInstance.line = SafeGet(dictIDToSubwayLine, subwayTrainInstanceData.lineID);

                if (subwayTrainInstanceData.trainSet)
                {
                    var subwayTrain = subwayTrainInstance.train;
                    subwayTrain.instance = subwayTrainInstance;
                    subwayTrain.ToggleVisibility(false);

                    subwayTrain.timeToWait = subwayTrainInstanceData.timeToWait;
                    subwayTrain.defaultSpeed = subwayTrainInstanceData.defaultSpeed;
                    subwayTrain.speed = subwayTrainInstanceData.speed;

                    subwayTrain.currentStop = SafeGet(dictIDToSubwayStop, subwayTrainInstanceData.currentStopID);
                    subwayTrain.destination = subwayTrainInstanceData.destination;

                    subwayTrain.stopping = subwayTrainInstanceData.stopping;
                    subwayTrain.accelerating = subwayTrainInstanceData.accelerating;
                    subwayTrain.goingBack = subwayTrainInstanceData.goingBack;
                    subwayTrain.waitingForUpdate = subwayTrainInstanceData.waitingForUpdate;

                    subwayTrain.passedThrough = new List<IStop>(from stopID in subwayTrainInstanceData.passedThroughStopIDs select SafeGet(dictIDToSubwayStop, stopID));
                    subwayTrain.passengers = new List<Person>(from passengerID in subwayTrainInstanceData.passengers select SafeGet(dictIDToPerson, passengerID));
                    subwayTrain.preceedingTrains = new List<SubwayTrain>(from trainID in subwayTrainInstanceData.preceedingTrains select SafeGet(dictIDToSubwayTrainInstance, trainID)?.train);

                    subwayTrain.broken = subwayTrainInstanceData.broken;
                    subwayTrain.threshold = subwayTrainInstanceData.threshold;
                    subwayTrain.damage = subwayTrainInstanceData.damage;

                    subwayTrain.changeSpeedCC = subwayTrainInstanceData.changeSpeedCC;
                    coroutineContextPairs.Add(new CoroutineContextPair<SubwayTrain>(subwayTrain.changeSpeedCC, subwayTrain));
                    subwayTrain.subwayTrainHandlePassengerCC = subwayTrainInstanceData.subwayTrainHandlePassengerCC;
                    coroutineContextPairs.Add(new CoroutineContextPair<SubwayTrain>(subwayTrain.subwayTrainHandlePassengerCC, subwayTrain));
                    subwayTrain.prevPos = subwayTrainInstanceData.prevPos;

                    if (subwayTrainInstance.line != null)
                    {
                        foreach (var material in subwayTrain.GetComponent<MeshRenderer>().materials)
                        {
                            material.SetColor("_Color1", subwayTrainInstance.line.Color);
                        }
                    }
                }

                subwayInstanceManager.trains.Add(subwayTrainInstance);
            }


            // People Manager
            var peopleManagerData = saveData.peopleManagerData;

            peopleManager.destination = peopleManagerData.destination;

            peopleManager.childrenCount = peopleManagerData.childrenCount;
            peopleManager.adultCount = peopleManagerData.adultCount;
            peopleManager.pensionerCount = peopleManagerData.pensionerCount;

            peopleManager.createOnClick = peopleManagerData.createOnClick;
            peopleManager.debugRush = peopleManagerData.debugRush;

            peopleManager.peopleOwnedCarTotal = peopleManagerData.peopleOwnedCarTotal;
            peopleManager.peopleUsingCarCount = peopleManagerData.peopleUsingCarCount;
            peopleManager.peopleHappiness = peopleManagerData.peopleHappiness.ToDictionary(kv => SafeGet(dictIDToPerson, kv.Key), kv => kv.Value);
            peopleManager.personsHappinessSum = peopleManagerData.personsHappinessSum;

            peopleManager.createAgentsCC = peopleManagerData.createAgentsCC;

            foreach (var personData in peopleManagerData.personDataList)
            {
                var person = dictIDToPerson[personData.personID];

                person.PathStops = personData.pathStops != null ? new Stack<IStop>(from stop in personData.pathStops.Reverse() select SafeGet(dictIDToIStop, stop)) : null;
                person.PathLines = personData.pathLines != null ? new Stack<ILine>(from line in personData.pathLines.Reverse() select SafeGet(dictIDToILine, line)) : null;

                person.isInsideVehicle = personData.isInsideVehicle;

                person.livingPlace = SafeGet(dictIDToBuilding, personData.livingPlaceBuildingID);
                person.workingPlace = SafeGet(dictIDToBuilding, personData.workingPlaceBuildingID);
                person.shoppingPlace = SafeGet(dictIDToBuilding, personData.shoppingPlaceBuildingID);

                person.personType = personData.personType;
                person.ownedCar = SafeGet(dictIDToCarNav, personData.ownedCarNavID);
                person.drivingCar = personData.drivingCar;

                person.currentDestination = personData.currentDestination;
                person.currentBuilding = SafeGet(dictIDToBuilding, personData.currentBuilding);
                person.currentTransportType = personData.currentTransportType;

                person.isInsideBuilding = personData.isInsideBuilding;
                person.isComputingTransportType = personData.isComputingTransportType;

                person.personHappiness = personData.personHappiness;
                person.estimatedTravellingTime = personData.estimatedTravellingTime;
                person.startJourneyTime = personData.startJourneyTime;
                person.timer = personData.timer;
                person.rnd = personData.rnd;
                person.prevCarPos = personData.prevCarPos;

                person.chooseTransportTypeCC = personData.chooseTransportTypeCC;
                //coroutineContextPairs.Add(new CoroutineContextPair<Person>(person.chooseTransportTypeCC, person));

                peopleManager.people.Add(person);

                if (person.drivingCar)
                {
                    person.transform.SetParent(person.ownedCar.transform);
                    person.agent.enabled = false;
                    person.personCollider.enabled = false;
                    person.meshRenderer.enabled = false;
                }

                if (person.isActiveAndEnabled && !person.drivingCar)
                {
                    person.UpdateDestination(person.currentBuilding);
                }
            }

            coroutineContextPairs.Add(new CoroutineContextPair<PeopleManager>(peopleManager.createAgentsCC, peopleManager));


            // Events
            var nextGoals = saveData.eventsData.nextGoals;
            CityCouncilEvents.TimeEvent.nextGoalTime = (int)nextGoals["time"];
            CityCouncilEvents.TimeEvent.nextGoalMoney = (int)nextGoals["money"];

            foreach (var eventDataData in saveData.eventsData.events)
            {
                var eventData = CityCouncilEvents.dictIDToEventDataCreator[eventDataData.id]();
                eventData.startingNumber = eventDataData.startingNumber;

                cityCouncil.events.Add(eventData);
            }


            // Money
            var moneyManagerData = saveData.moneyManagerData;
            moneyManager.Money = moneyManagerData.money;
            moneyManager.MoneyFlow = moneyManagerData.moneyFlow;

            moneyManager.hourlyGain = moneyManagerData.hourlyGain;
            moneyManager.thisHourGain = moneyManagerData.thisHourGain;

            moneyManager.moneyChanged = moneyManagerData.moneyChanged;
            moneyManager.moneyFlowChanged = moneyManagerData.moneyFlowChanged;


            // Statistics
            var statisticsDataManagerData = saveData.statisticsDataManagerData;
            statisticsDataManager.lastStatisticsUpdateTime = statisticsDataManagerData.lastStatisticsUpdateTime;
            statisticsDataManager.NumericSeries = statisticsDataManagerData.numericSeries;


            // Resume Coroutines
            foreach (var coroutineContextPair in coroutineContextPairs)
            {
                coroutineContextPair.Resume();
            }

            #endregion


            print($"Loaded {saveName}");
        }
        catch (Exception ex)
        {
            PopUpWindowManager.Instance.EnableOneButtonWindow($"Loading game failed!", () => {
                SceneManager.LoadScene("Assets/Scenes/MainMenu.unity");
            });
            Debug.LogError($"Exception occured while loading save {saveName}:\n{ex.Message}");
        }
    }


    /// <summary>
    /// Finds closest waypoint to the given position.
    /// </summary> 
    /// <returns>Closest waypoint to the given position, null = no waypoint was found</returns>
    protected Waypoint GetWaypoint(Vector3 position)
    {
        if (position == Vector3.positiveInfinity)
        {
            return null;
        }

        Waypoint closestWP = null;
        float closestDistance = float.MaxValue;

        // Creates collider in order to find the waypoint
        var overlapingObjects = Physics.OverlapSphere(position, waypointColliderRange);

        foreach (var item in overlapingObjects)
        {
            if (item.gameObject.CompareTag("CarWaypoint"))
            {
                var waypoint = item.gameObject.GetComponent<Waypoint>();

                float distance = Vector3.Distance(waypoint.transform.position, position);
                if (distance < closestDistance)
                {
                    closestDistance = distance;
                    closestWP = waypoint;
                }
            }
        }

        return closestWP;
    }

    /// <summary>
    /// Enumerates through Names and LastAccessTimes of all save files
    /// </summary>
    public IEnumerable<(string, DateTime)> EnumerateSaves()
        => from save in Directory.EnumerateFiles(SaveDirPath)
           select (Path.GetFileNameWithoutExtension(save), File.GetLastAccessTime(save));

    public static string GetPrefabName(GameObject gameObject)
        => gameObject.name.Replace("(Clone)", "");

    /// <summary>
    /// Returns fallbackValue when key is not present in the dictionary
    /// </summary>
    /// <typeparam name="TKey">Dictionary Key Type</typeparam>
    /// <typeparam name="TValue">Dictionary Value Type</typeparam>
    /// <param name="dictionary">Dictionary to be queried</param>
    /// <param name="key">Key to the dictionary</param>
    /// <param name="fallbackValue">Value to be returned when dictionary is null or key is not present in the dictionary</param>
    /// <returns></returns>
    public static TValue SafeGet<TKey, TValue>(Dictionary<TKey, TValue> dictionary, TKey key, TValue fallbackValue = default)
        => dictionary != null && key != null && dictionary.ContainsKey(key) ? dictionary[key] : fallbackValue;

    /// <summary>
    /// Returns full path to a given save file name
    /// </summary>
    /// <param name="saveFileName">name of the save file without extension</param>
    public string GetSaveFilePath(string saveFileName)
        => $"{Path.Combine(SaveDirPath, saveFileName)}.{saveFileExtension}";


    public delegate void SaveLoadDelegate(string saveName);


    #region Custom classes used for bulk data serialization
    // Note that
    //  - all fields has to be marked as public for the serialization to work
    //  - custom multidimensional array cannot be serialized (internal asset bug)
    public class SaveData_1_0_0
    {
        public const string saveVersion = "1.0.0";

        //  ============ DATA ============
        public CameraData cameraData;
        public TimeData timeData;
        public TerrainDataData terrainDataData;
        public RoadGraphData roadGraphData;
        public CarManagerData carManagerData;
        public BuildingManagerData buildingManagerData;
        public BusManagerData busManagerData;
        public SubwayManagerData subwayManagerData;
        public ZoneControllerData zoneControllerData;
        public PeopleManagerData peopleManagerData;
        public EventsData eventsData;
        public MoneyManagerData moneyManagerData;
        public StatisticsDataManagerData statisticsDataManagerData;


        //  ============ CLASSES ============

        //  ------------ CAMERA ------------
        public class CameraData
        {
            public Vector3 position;
            public Quaternion rotation;
        }

        //  ------------ TIME ------------
        public class TimeData
        {
            public DateTime currentDateTime;
        }

        //  ------------ TERRAIN ------------
        public class TerrainDataData
        {
            public float[,] heightmap;
            public int thickness;
            public float scale;
            public float power;
            public float offsetX;
            public float offsetY;

            public List<TreeData> treeDataList;
        }
        public class TreeData
        {
            public Vector3 position;
            public Quaternion rotation;
        }

        //  ------------ ROADS ------------
        public class RoadGraphData
        {
            public float heightLimit;
            public Vector2Int blockCount;
            public int terrainFlatteningExtension;

            public Vector2Int terrainSize;
            public Vector2Int terrainCenter;
            public Vector2Int cityCenter;
            public Vector2Int totalCitySize;
            public Vector2Int downLeftCorner;
            public Vector2Int southWestCorner;

            public float cityBaseHeight;

            public string centerDistrict;
            public List<DistrictDataData> districtDataDataList;

            public string[,] supermodels;
            public string[,] districts;

            // Roads
            public List<RoadData> roadDataList;
            public List<WaypointData> waypointDataList;
        }
        public class DistrictDataData
        {
            public string districtID;

            public Vector2Int blockCount;
            public int blockSize;

            public Vector2Int downLeftCorner;

            public Vector2Int citySize;
            public Vector2 cityCenter;

            public bool[,] roads;
            public bool[,] junctions;
            public bool[,] crosswalks;
            public string[,] roadIDmap;

            public CardinalDirection directionFromMainDistrict;
            public DistrictState districtState;
        }
        public class RoadData
        {
            public string roadID;
            public SegmentType roadType;

            public Vector3 position;
            public Quaternion rotation;

            public CrosswalkData crosswalkData;
            public IntersectionData intersectionData;
        }
        public class IntersectionData
        {
            // public int posX, posY;
            public Dictionary<Vector3, List<string>> buffers;
            public List<string> pendingVehicles;

            public AddCarCoroutineContext addCarCC;

            public IntersectionDirectorType intersectionDirectorType;
            // BasicDirector
            public Dictionary<Vector3, int> directionIndicators;
            public Dictionary<string, List<Vector3>> occupiedClusters;
        }
        public enum IntersectionDirectorType { BasicDirector }
        public class CrosswalkData
        {
            public float timeSinceLastLeftNotification;
            public float timeSinceLastRightNotification;

            public List<string> vehicleIDsInRightTrigger;
            public List<string> vehicleIDsInLeftTrigger;

            public bool leftOccupied;
            public bool rightOccupied;
        }
        public class WaypointData
        {
            // Transform
            public Vector3 position;

            // Waypoint
            public int occupied;
            public List<string> occupiers;
            public bool discarded;

            // IntersectionExitWaypoint
            public int numOfCollidersInTrigger;
            public bool prevBlocked;
        }

        //  ------------ BUILDINGS ------------
        public class BuildingManagerData
        {
            public bool isInitialized;
            public int totalLivingCapacity;
            public List<string> schools;

            public float[] buildingZonesAverageOccupancyRate;
            public int[] buildingZonesTotalOccupancy;
            public bool[,] treePlantation;

            // Builder
            public string[,] buildingIDMap;
            public Vector2Int blockMapSize;
            public List<BlockDataData> blockDataDataList;

            public List<BuildingData> buildingDataList;
        }
        public class BuildingData
        {
            public string buildingID;
            public string buildingName;

            // Transform
            public Vector3 position;
            public Quaternion rotation;

            // Building
            public Vector2Int mapPosition;
            public Vector2Int firstMapPosition;
            public Vector2Int lastMapPosition;
            public int size;
            public int capacity;
            public ZoneType zoneType;
            public Vector3 closestWaypointPos;
            public CardinalDirection doorDirection;
            public string facingRoadID;
            public bool isSchool;

            public List<string> residentsIDs;
        }
        public class BlockDataData
        {
            public Vector2Int position;
            public Vector3 worldPosition;
            public ZoneType zoneType;
            public BlockState blockState;
            public string[] neighboringRoads;
            public bool areNeighboringRoadsSet;
            public bool[] isThereNeighboringRoad;
            public string road;
        }

        //  ------------ ZONES ------------
        public class ZoneControllerData
        {
            public Dictionary<string, int[,]> zoneMap;
            public Dictionary<Vector3, string> roadPositions;
            public Dictionary<string, ZoneType> zoneTypes;
            public ZoneType[] cityPartTypes;
            public Dictionary<ZoneType, Color> zoneColors;
        }

        //  ------------ CARS ------------
        public class CarManagerData
        {
            public List<CarData> carDataList;
            public List<string> activeCars;
        }
        public class CarData
        {
            // Car
            public string carName;
            public bool active;

            // Transform
            public Vector3 position;
            public Quaternion rotation;

            // CarNavigation
            public string carNavID;
            public VehicleType vehicleType;
            public float speed;
            public float defaultSpeed;
            public Direction[] path;
            public Vector3 nextOrCurrentWaypoint_pos;
            public Vector3 destination;
            public Vector2 centre;
            public float radius;
            public Direction nextDir;
            public float angularSpeed;
            public float angle;
            public float correctionAngle;
            public bool carAtDestination;
            public Vector3 stoppingWP_pos;
            public List<string> blockers;
            public Vector3 lookahead_pos;
            public bool stopping;
            public bool accelerating;
            public float length;
            public bool stoppedAtIntersection;
            public ChangeCarSpeedCoroutineContext coroutineContext;
        }

        //  ------------ BUSES ------------
        public class BusManagerData
        {
            public int nextBusIndex;
            public int nextLineIndex;

            public List<BusInstanceData> busInstanceDataList;
            public List<BusLineData> busLineDataList;
            public List<BusStopData> busStopDataList;
        }
        public class BusLineData
        {
            public string busLineID;

            public int ID;
            public string name;
            public Color color;

            public List<string> vehicles;
            public List<string> path;

            // Schedule
            public int interval;

            public bool closed;
            public int nextStopIndex;
        }
        public class BusStopData
        {
            public string busStopID;
            public string busStopName;

            public Vector3 position;
            public Quaternion rotation;

            public Vector3 adjacentWaypointPos;
            public Vector3 entranceWaypointPos;

            public List<int> lines;

            public Dictionary<int, string> next;
            public Dictionary<int, string> previous;
            public Dictionary<int, Direction[]> nextPath;

            public bool occupied;
            public List<string> peopleAtStop;
            public List<string> busInstancesIDsToSpawn;
            public bool blockedEntrance;
            public string lastToEnterCarNavID;
            public List<string> vehiclesInTrigger;
            public SpawnBusesCoroutineContext spawnBusesCC;
        }
        public class BusInstanceData
        {
            public string busInstanceID;

            // Instance
            public int ID;
            public string name;
            public BusInstanceManager.BusType busType;
            public string busCarNavID;
            public string busLineID;

            // Bus
            public float damageMax;
            public float initialThreshold;
            public float thresholdStep;
            public string currentStopID;
            public bool justStopped;
            public float timeToWait;
            public float damage;
            public Vector3 prevPos;
            public bool broken;
            public float threshold;

            public BusHandlePassengerCoroutineContext busHandlePassengerCC;
        }

        //  ------------ SUBWAY ------------
        public class SubwayManagerData
        {
            public int nextLineIndex;
            public int nextTrainIndex;

            public List<SubwayTrainInstanceData> subwayTrainInstancesDataList;
            public List<SubwayLineData> subwayLineDataList;
            public List<SubwayStopData> subwayStopDataList;
        }
        public class SubwayLineData
        {
            public string subwayLineID;

            public int ID;
            public string name;
            public Color color;

            public List<string> vehicles;
            public List<string> path;

            // Schedule
            public int interval;

            public int nextStopIndex;
            public bool direction;

        }
        public class SubwayStopData
        {
            public string subwayStopID;
            public string buildingID;

            public List<int> lines;

            public Dictionary<int, string> nextStop;
            public Dictionary<int, string> previousStop;
        }
        public class SubwayTrainInstanceData
        {
            public string subwayTrainInstanceID;

            // Instance
            public int ID;
            public string name;
            public string lineID;

            // SubwayTrain
            public bool trainSet;
            public Vector3 position;
            public Quaternion rotation;

            public float timeToWait;
            public float defaultSpeed;
            public float speed;

            public string currentStopID;
            public Vector3 destination;
            public bool stopping;
            public bool accelerating;
            public bool goingBack;
            public bool waitingForUpdate;

            public List<string> passedThroughStopIDs;
            public List<string> passengers;
            public List<string> preceedingTrains;

            public bool broken;
            public float threshold;
            public float damage;

            public ChangeSubwayTrainSpeedCoroutineContext changeSpeedCC;
            public SubwayTrainHandlePassengerCoroutineContext subwayTrainHandlePassengerCC;
            public Vector3 prevPos;
        }

        //  ------------ PEOPLE ------------
        public class PeopleManagerData
        {
            public Vector3 destination;

            public int childrenCount;
            public int adultCount;
            public int pensionerCount;

            public bool createOnClick;
            public int debugRush;
            public int peopleOwnedCarTotal;
            public int peopleUsingCarCount;

            public Dictionary<string, float> peopleHappiness;
            public float personsHappinessSum;

            public CreateAgentsCoroutineContext createAgentsCC;

            public List<PersonData> personDataList;
        }
        public class PersonData
        {
            public string personID;
            public string personName; // Name of the prefab

            // Transform
            public Vector3 position;
            public Quaternion rotation;

            // Person
            public string[] pathStops;
            public string[] pathLines;

            public bool isInsideVehicle;

            public string livingPlaceBuildingID;
            public string workingPlaceBuildingID;
            public string shoppingPlaceBuildingID;

            public PersonType personType;
            public string ownedCarNavID;
            public bool drivingCar;
            public Vector3 currentDestination;

            public string currentBuilding;
            public bool isInsideBuilding;

            public bool isComputingTransportType;
            public TransportType currentTransportType;

            public float personHappiness;
            public float estimatedTravellingTime;
            public float startJourneyTime;
            public float timer;
            public float rnd;
            public Vector3 prevCarPos;

            public ChooseTransportTypeCoroutineContext chooseTransportTypeCC;
        }

        //  ------------ EVENTS ------------
        public class EventsData
        {
            public List<EventDataData> events;
            public Dictionary<string, object> nextGoals;
        }
        public class EventDataData
        {
            public string id;
            public long startingNumber;
        }

        //  ------------ MONEY ------------
        public class MoneyManagerData
        {
            public int money;
            public int moneyFlow;
            
            public int[] hourlyGain;
            public int thisHourGain;

            public bool moneyChanged;
            public bool moneyFlowChanged;
        }


        //  ------------ STATISTICS ------------
        public class StatisticsDataManagerData
        {
            public float lastStatisticsUpdateTime;
            public Dictionary<string, List<(long, float)>> numericSeries;
        }
    }
    #endregion
}