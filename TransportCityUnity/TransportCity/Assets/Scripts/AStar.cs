﻿using System.Collections.Generic;
using CarPathFinding;

public class AStar
{
    /// <summary>
    /// Returns the end path point which now has a link to its preceding path point
    /// (and that is linked to its preceding path point and so on).
    /// </summary>
    /// <typeparam name="T"> The type used to express direction from one path point to another. </typeparam>
    /// <typeparam name="S"> The type of the wrapped object. </typeparam>
    public IPathPoint<T, S> GetPath<T, S>(IPathPoint<T, S> start, IPathPoint<T, S> end)
    {
        if (start.Equals(end))
        {
            return null;
        }

        IPathPoint<T, S> currentNode = start;

        // The list of nodes that are to be processsed.
        var openList = new SortedList<float, IPathPoint<T, S>>(new FloatComparer());
        // The list already visited nodes.
        var closedList = new List<IPathPoint<T, S>>();

        openList.Add(0, currentNode);
        currentNode.Previous = null;
        currentNode.Metric = 0f;

        // While some nodes are still waiting to be processed.
        while (openList.Count > 0)
        {
            // Pop the first node from the open list and add it to the closed list.
            currentNode = openList.Values[0];
            openList.RemoveAt(0);
            closedList.Add(currentNode);

            // If the current node's waypoint is the final destination, break the cycle.
            if (currentNode.Equals(end))
            {
                break;
            }

            // For each of the node's successors try to create a neighbour node.
            for (int i = 0; i < currentNode.NumOfSuccessors; i++)
            {
                IPathPoint<T, S> neighbour = currentNode.GetNextPoint(i, end);

                if (neighbour != null)
                {
                    if (closedList.Contains(neighbour))
                    {
                        continue;
                    }

                    // If openList contains the new neighbour, check if the new path to it was better.
                    // If not, continue to the next iteration. If yes, remove the previous version and finish this iteration.
                    if (openList.ContainsValue(neighbour))
                    {
                        IPathPoint<T, S> firstNeighbour = openList.Values[openList.IndexOfValue(neighbour)];
                        if (firstNeighbour.Metric <= neighbour.Metric)
                        {
                            continue;
                        }
                        else
                        {
                            openList.RemoveAt(openList.IndexOfValue(neighbour));
                        }
                    }

                    var metricToTarget = neighbour.MetricTo(end);
                    openList.Add(neighbour.Metric + metricToTarget, neighbour);
                }
            }
        }

        if (currentNode.Equals(end))
        {
            return currentNode;
        }
        else
        {
            return null;
        }
    }
}
