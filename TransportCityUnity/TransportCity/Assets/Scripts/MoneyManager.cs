﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using static Constants;

public class MoneyManager : MonoBehaviour
{
    // Singleton
    public static MoneyManager instance;

    /// <summary>
    /// Money the player has
    /// </summary>
    public int Money { get; internal set; }

    /// <summary>
    /// How much money did player gained by regular daily activities like bus maintenance or people buying tickets
    /// </summary>
    public int MoneyFlow { get; internal set; }

    internal int[] hourlyGain;
    internal int thisHourGain;

    internal bool moneyChanged;
    internal bool moneyFlowChanged;

    private UI_API ui;

    // Start is called before the first frame update

    void Awake()
    {
        ui = UI_API.GetInstance();
        hourlyGain = new int[hoursInDay];

        if (instance == null)
            instance = this;
    }

    void Start()
    {
        Money = startingMoney;

        if (ui == null)
        {
            ui = UI_API.GetInstance();
        }

        // assign starting money
        ui.SetCurrentMoney(Money);
        ui.SetCurrentMoneyFlow(MoneyFlow);

        RegisterToCallNextHour();
        RegisterToCallEachDay();
    }

    // Update is called once per frame
    void Update()
    {
        if (moneyFlowChanged)
        {
            moneyFlowChanged = false;
            RecalculateMoneyFlow();

            ui.SetCurrentMoneyFlow(MoneyFlow);
        }

        if (moneyChanged)
        {
            moneyChanged = false;

            ui.SetCurrentMoney(Money);
        }

        /*
        if (Input.GetKey(KeyCode.M))
        {
            GainMoneyOneTime(5000, "Congratulations! You have won a lottery!");
        }

        if (Input.GetKey(KeyCode.N))
        {
            SpendMoneyOneTime(5000, "Congratulations! You have lost a lottery!");
        }

        if (Input.GetKey(KeyCode.O))
        {
            // Passenger has bought a ticket
            GainMoneyFlow(5);
        }
        */
    }

    /// <summary>
    /// Recalculates money flow 
    /// </summary>
    private void RecalculateMoneyFlow()
    {
        MoneyFlow = hourlyGain.Sum();
    }

    /// <summary>
    /// Adds this one money gain as a regular one.
    /// </summary>
    /// <param name="gain">Amount of money gained, counts aas a regular gain.</param>
    public void GainMoneyFlow(int gain)
    {
        Money += gain;
        thisHourGain += gain;
        moneyFlowChanged = true;
        moneyChanged = true;
    }

    /// <summary>
    /// Adds this money loss as a regular one
    /// </summary>
    /// <param name="loss">Amount of money spent, counts as a regular spending.</param>
    public void SpendMoneyFlow(int loss)
    {
        Money -= loss;
        thisHourGain -= loss;
        moneyFlowChanged = true;
        moneyChanged = true;
    }

    /// <summary>
    /// Adds this money to the current amount of money.
    /// </summary>
    /// <param name="gain">Amount of money gained</param>
    /// <param name="text"*>Text in the notification bar to show.</param>
    public void GainMoneyOneTime(int gain, string text = "")
    {
        Money += gain;
        moneyChanged = true;

        if (text != "")
        {
            ui.DisplayNotification(text, UI_API.NotificationLevel.INFO);
        }
    }

    /// <summary>
    /// Acknowleges this money loss.
    /// </summary>
    /// <param name="loss">Amount of money lost/spent</param>
    /// <param name="text"*>Text in the notification bar to show.</param>
    public void SpendMoneyOneTime(int loss, string text = "")
    {
        Money -= loss;
        moneyChanged = true;

        if (text != "")
        {
            ui.DisplayNotification(text, UI_API.NotificationLevel.INFO);
        }
    }

    /// <summary>
    /// Throws pop-up window when there is not enough money to complete payment.
    /// </summary>
    /// <param name="buttonHandler"></param>
    public void ThrowNotEnoughMoneyPopUp(UI_API.ButtonHandler buttonHandler = null)
    {
        if (buttonHandler != null)
        {

            PopUpWindowManager.Instance.EnableOneButtonWindow(notEnoughMoney, buttonHandler);
        }
        else 
        { 
            PopUpWindowManager.Instance.EnableOneButtonWindow(notEnoughMoney, () => { }); 
        }
    }

    /// <summary>
    /// Ends an hour for money manager -> puts hourly gain to the daily gain and deletes the oldest hourly gain in the daily gain.
    /// </summary>
    public void EndOfHour()
    {
        for (int i = 1; i < hoursInDay; i++)
        {
            hourlyGain[i - 1] = hourlyGain[i];
        }
        hourlyGain[hoursInDay - 1] = thisHourGain;

        thisHourGain = 0;
        moneyFlowChanged = true;

        // Set Time Manager to call next hour
        RegisterToCallNextHour();
    }

    /// <summary>
    /// Registers to time manager's alarm clock to the beginning of the next hour to recalculate money flow.
    /// </summary>
    private void RegisterToCallNextHour()
    {
        DateTime nextHour = TimeManager.instance.CurrentDateTime.AddHours(1);
        TimeManager.instance.RegisterAlarmClock(nextHour, EndOfHour);
    }

    /// <summary>
    /// Ends a day -> adds maintenance costs into money flow
    /// </summary>
    public void EndOfDay()
    {
        int cost = (int)(SubwayLineManager.GetInstance().LinesCount * subwayLineMaintenanceMultiplier * subwayLineCost);
        SpendMoneyFlow(cost);

        RegisterToCallEachDay();
    }

    /// <summary>
    /// Registers to time manager's alarm clock to the beginning of the next day (adds everyday maintenance costs).
    /// </summary>
    private void RegisterToCallEachDay()
    { 
        DateTime nextDay = TimeManager.instance.CurrentDateTime.AddDays(1);
        TimeManager.instance.RegisterAlarmClock(nextDay, EndOfDay);
    }
}
