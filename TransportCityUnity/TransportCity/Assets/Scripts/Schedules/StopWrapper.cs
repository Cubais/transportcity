﻿using UnityEngine;
using System.Linq;
using static Constants;

/// <summary>
/// A wrapper for any kind of stop. Used in the Scheduler's A* algorithm.
/// </summary>
public class StopWrapper : IPathPoint<int, IStop>
{
    // The wrapper of the stop from which the algorithm got to this stop.
    public IPathPoint<int, IStop> Previous { get; set; }
    
    // An approximation of the time it will take to get from the start to this stop.
    public float Metric { get; set; }

    // The direction which leads from the previous waypoint to this one.
    public int Direction { get; set; }
    
    // Number of the wrapped stop's successors. Set to be the number of all the other stops on the map + 1 (for the end path point).
    // (Unless the wrapped stop is the start or end path point in which case the end isn't present in its neighbours.)
    public int NumOfSuccessors { get; set; }
     
    // The wrapped stop.
    public IStop WrappedObject { get; set; }

    // The line by which the algorithm got from the previous waypoint to this one.
    // (Null means that the algorithm used walking instead.)
    public ILine currentLine;

    public StopWrapper(IStop stop)
    {
        WrappedObject = stop;

        if (WrappedObject is StopSkeleton)
        {
            NumOfSuccessors = stop.Neighbours().Count;
        }
        else
        {
            NumOfSuccessors = stop.Neighbours().Count + 1;
        }
    }

    /// <summary>
    /// Returns the stop's ith successor.
    /// </summary>
    public IPathPoint<int, IStop> GetNextPoint(int i, IPathPoint<int, IStop> end)
    {
        StopWrapper next;

        if (i < NumOfSuccessors - 1 || WrappedObject is StopSkeleton)
        {
            next = new StopWrapper(WrappedObject.Neighbours()[i]);
        }
        else
        {
            next = new StopWrapper(end.WrappedObject);
        }

        next.Previous = this;

        float time = float.MaxValue;
        ILine bestLine = null;

        // If both this stop and the next stop are subway stops or bus stops, check if there is a line connecting them.
        if ((WrappedObject is SubwayStop && next.WrappedObject is SubwayStop) || (WrappedObject is BusStop && next.WrappedObject is BusStop))
        {
            // If the algorithm already used a line to get to this stop, first check if this line is usable.
            if (currentLine != null)
            {
                if (next.WrappedObject.Lines.Contains(currentLine.Id))
                {
                    time = Scheduler.GetInstance().GetApproximateLineDistance(currentLine, WrappedObject, next.WrappedObject) / avgVehicleSpeed;
                    bestLine = currentLine;
                }
            }

            var intersection = WrappedObject.Lines.Intersect(next.WrappedObject.Lines);

            // If there are other lines connecting the two stops, check those as well (while taking into account the waiting time).
            if (intersection.Count() != 0)
            {
                foreach (int id in intersection)
                {
                    ILine newLine = null;
                    if (WrappedObject is SubwayStop)
                    {
                        newLine = SubwayLineManager.GetInstance().GetLines()[id];
                    }
                    else
                    {
                        newLine = BusLineManager.GetInstance().GetLines()[id];
                    }

                    float newTime = Scheduler.GetInstance().GetApproximateLineTravelTime(newLine, WrappedObject, next.WrappedObject);

                    if (newTime < time)
                    {
                        time = newTime;
                        bestLine = newLine;
                    }
                }
            }
        }

        // Compute how long it would take to get to the next stop on foot.
        float time2 = Scheduler.GetInstance().GetWalkingTime(WrappedObject.GetPosition(), next.WrappedObject.GetPosition());

        if (time < time2)
        {
            next.currentLine = bestLine;
            next.Metric = time + Metric;
        }
        else
        {
            next.currentLine = null;
            next.Metric = time2 + Metric;
        }

        next.Direction = i;

        return next;
    }

    /// <summary>
    /// Computes an approximation of the time it would take to get to another stop from this one.
    /// </summary>
    public float MetricTo(IPathPoint<int, IStop> other)
    {        
        return Vector3.Distance(WrappedObject.GetPosition(), other.WrappedObject.GetPosition()) / avgVehicleSpeed;
    }

    public override bool Equals(object obj)
    {
        return this.Equals(obj as StopWrapper);
    }

    public bool Equals(IPathPoint<int, IStop> other)
    {
        if (other == null) return false;
        if (other.WrappedObject == WrappedObject) return true;
        return false;
    }

    public override int GetHashCode()
    {
        return WrappedObject.GetHashCode();
    }

    public static bool operator ==(StopWrapper leftSide, StopWrapper rightSide)
    {
        // Check for null on left side.
        if (System.Object.ReferenceEquals(leftSide, null))
        {
            if (System.Object.ReferenceEquals(rightSide, null))
            {
                // null == null = true.
                return true;
            }

            return false;
        }
        return leftSide.Equals(rightSide);
    }

    public static bool operator !=(StopWrapper lhs, StopWrapper rhs)
    {
        return !(lhs == rhs);
    }
}
