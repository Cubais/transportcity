﻿using System.Collections.Generic;
using UnityEngine;
using static Constants;

public class Scheduler : MonoBehaviour
{
    // An object with a method for computing the A* algorithm.
    private AStar aStar;

    // Singleton.
    private static Scheduler instance;

    private void Awake()
    {
        instance = this;
        aStar = new AStar();
    }

    public static Scheduler GetInstance()
    {
        return instance;
    }

    /// <summary>
    /// Computes the best line to take if one wants to get from startPosition to endPosition.
    /// </summary>
    public (Stack<IStop>, Stack<ILine>, float) GetBestLine(Vector3 startPosition, Vector3 endPosition)
    {
        IPathPoint<int, IStop> res = aStar.GetPath<int, IStop>(
            new StopWrapper(new StopSkeleton(startPosition)),
            new StopWrapper(new StopSkeleton(endPosition))
            );

        if (res == null)
        {
            return (null, null, float.MaxValue);
        }
        else
        {
            Stack<IStop> stops = new Stack<IStop>();
            Stack<ILine> lines = new Stack<ILine>();
            float time = 0f;

            IPathPoint<int, IStop> currentNode = res.Previous;
            ILine prevLine = null;

            // Compute the path by backtracking from the end of the path.
            while (currentNode.Previous != null)
            {
                if (prevLine == null || prevLine != ((StopWrapper)currentNode).currentLine)
                {
                    stops.Push(currentNode.WrappedObject);

                    if (((StopWrapper)currentNode).currentLine != null)
                    {
                        lines.Push(((StopWrapper)currentNode).currentLine);
                    }
                }
                time += currentNode.Metric;
                prevLine = ((StopWrapper)currentNode).currentLine;
                currentNode = currentNode.Previous;
            }

            // This can happen if the quickest way to get to the given destination is on foot.
            if (stops.Count > 1 && lines.Count > 0)
            {
                return (stops, lines, time);
            }
            else
            {
                return (null, null, float.MaxValue);
            }
        }
    }

    /// <summary>
    /// Gets the the stop from a given line that is nearest to a given point.
    /// </summary>
    public IStop GetNearestStop(Vector3 point, ILine line)
    {
        IStop result = default;
        float min = float.MaxValue;

        foreach (IStop stop in line.Path)
        {
            float dist = GetStopDistance(stop, point);
            if (dist < min)
            {
                min = dist;
                result = stop;
            }
        }

        return result;
    }

    /// <summary>
    /// Returns distance between a given stop and a given point.
    /// </summary>
    public float GetStopDistance(IStop stop, Vector3 point)
    {
        return Vector3.Distance(stop.GetPosition(), point);
    }

    /// <summary>
    /// Computes the aproximate length of the whole line.
    /// </summary>
    public float GetApproximateLineDistance(ILine line)
    {
        List<IStop> path = line.Path;
        float result = 0;

        for (int i = 1; i < path.Count; i++)
        {
            result += Vector3.Distance(path[i - 1].GetPosition(), path[i].GetPosition());
        }        

        // If the line is a bus line, the path isn't straight and it is circular.
        if (path.Count > 0 && path[0] is BusStop)
        {
            result += Vector3.Distance(path[0].GetPosition(), path[path.Count - 1].GetPosition());
            result *= approximationCoefficient;
        }

        return result;
    }

    /// <summary>
    /// Computes an approximate distance from between stops start and end.
    /// </summary>
    public float GetApproximateLineDistance(ILine line, IStop start, IStop end)
    {
        // Subway lines and bus lines have to be handled differently because subway lines aren't circular
        // and they go straight from one stop to another.
        if (line is BusLineManager.BusLine)
        {
            List<IStop> path = line.Path;
            float result = 0;

            int i = path.IndexOf(start);
            int pathLength = path.Count;

            while (path[i] != end)
            {
                result += Vector3.Distance(path[i].GetPosition(), path[(i + 1) % pathLength].GetPosition());
                i = (i + 1) % pathLength;
            }

            result *= approximationCoefficient;

            return result;
        }
        else
        {
            List<IStop> path = line.Path;
            float result = 0;

            int i = Mathf.Min(path.IndexOf(start), path.IndexOf(end));
            IStop finish = path.IndexOf(start) != i ? start : end;
            i++;

            while (path[i] != finish)
            {
                result += Vector3.Distance(path[i - 1].GetPosition(), path[i].GetPosition());
                i++;
            }

            return result;
        }
    }

    /// <summary>
    /// Returns an estimate of the time it will take for a vehicle that belongs to a given line to arrive at a stop.
    /// </summary>
    /// <param name="distance"> The length of the line. </param>
    /// <param name="speed"> The speed of the subway train. </param>
    public float GetApproximateWaitingTime(ILine line, float distance, float speed)
    {
        if (line is SubwayLineManager.SubwayLine)
        {
            distance *= 2f;
        }

        int numOfVehicles = line.Vehicles.Count;

        if (numOfVehicles == 0)
        {
            return float.MaxValue;
        }

        float cycleTime = distance / speed;
        float avgTimePerVehicle = cycleTime / 2;

        return avgTimePerVehicle / numOfVehicles;
    }

    /// <summary>
    /// Computes time from speed and distance.
    /// </summary>
    public float GetTravelTime(float distance, float speed)
    {
        return distance / speed;
    }

    /// <summary>
    /// Computes an approximation of how long it will take to get from start to end.
    /// </summary>
    public float GetWalkingTime(Vector3 start, Vector3 end)
    {
        return GetTravelTime(Vector3.Distance(start, end) * approximationCoefficient, avgPersonSpeed);
    }

    /// <summary>
    /// Computes an approximation of how long it will take to get from the starting to the ending stop using the given line.
    /// </summary>
    public float GetApproximateLineTravelTime(ILine line, IStop start, IStop end)
    {
        return GetApproximateLineDistance(line, start, end) / avgVehicleSpeed +
            GetApproximateWaitingTime(line, GetApproximateLineDistance(line), avgVehicleSpeed);
    }
}
