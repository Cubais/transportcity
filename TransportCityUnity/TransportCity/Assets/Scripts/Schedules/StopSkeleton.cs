﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/// <summary>
/// A script used by the A* algorithm in the Scheduler. It wraps a position in a basic implementation of a script that inherits from IStop,
/// so that the position can be passed to the A* algorithm.
/// </summary>
public class StopSkeleton : IStop
{
    public List<int> Lines { get; set; }

    // The wrapped position.
    private Vector3 position;

    public StopSkeleton(Vector3 position)
    {
        this.position = position;
        Lines = new List<int>();
    }

    /// <summary>
    /// Returns all the stops on the map.
    /// </summary>
    public List<IStop> Neighbours()
    {
        HashSet<IStop> result = new HashSet<IStop>();

        foreach (ILine line in SubwayLineManager.GetInstance().GetLines())
        {
            if (line.Closed)
            {
                result.UnionWith(line.Path);
            }
        }

        foreach (ILine line in BusLineManager.GetInstance().GetLines())
        {
            if (line.Closed)
            {
                result.UnionWith(line.Path);
            }
        }

        return result.ToList();
    }

    public Waypoint GetAdjacentWaypoint()
    {
        return null;
    }

    public Vector3 GetPosition()
    {
        return position;
    }

    public void AddStop(IStop stop, int id, bool nextOrPrevious) {}

    public IStop GetNextStop(int id)
    {
        return null;
    }

    public IStop GetPreviousStop(int id)
    {
        return null;
    }

    public void RemoveStop(int id, bool nextOrPrevious) {}

    public List<Person> GetPeopleForLine(ILine line)
    {
        return new List<Person>();
    }

    public Vector3 EntryPoint()
    {
        return position;
    }

    public bool Exists()
    {
        return true;
    }
}
