﻿using UnityEngine;

public class HeightMapGenerator : MonoBehaviour
{
    private const int width = 4096;
    private const int height = 4096;

    private float offsetX;
    private float offsetY;
    private float power = 7;
    private float scale = 1;

    /// <summary>
    /// HeightMapGenerator needs to be initialized.
    /// </summary>
    /// <param name="scale">The lower the scale, the more zoomed-in the map seems to be.</param>
    /// <param name="power">Exponent power, makes lower points lower, 
    /// highlights the difference between higher and lower points.</param>
    public void Init(float scale, float power)
    {
        this.scale = scale;
        this.power = power;
    }

    /// <summary>
    /// Generates array of floats meant as heights for terrain.
    /// </summary>
    /// <returns>Height map (array of floats)</returns>
    public float[,] GenerateHeights(float offsetX, float offsetY)
    {
        this.offsetX = offsetX;
        this.offsetY = offsetY;

        float[,] heights = new float[width, height];

        float[,] cache = CachePerlin();

        // Cache has dimensions (width + 2) and (height + 2), 
        // as we average out values from around of that point for smoothness
        // that's why x,y start at 1, and at width/height + 1 and save result to [x-1,y-1]
        for (int x = 1; x < width + 1; x++)
        {
            for (int y = 1; y < height + 1; y++)
            {
                float val = cache[x, y];
                val += cache[x + 1, y];
                val += cache[x, y + 1];
                val += cache[x + 1, y + 1];
                val += cache[x - 1, y];
                val += cache[x, y - 1];
                val += cache[x - 1, y - 1];
                val += cache[x - 1, y + 1];
                val += cache[x + 1, y - 1];
                val /= 9;

                heights[x - 1, y - 1] = val;
            }
        }
        return heights;
    }

    /// <summary>
    /// Calculates height for single point on terrain.
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns></returns>
    private float CalculateHeight(int x, int y)
    {
        float xCoord = x / (float) width * scale + offsetX;
        float yCoord = y / (float) height * scale + offsetY;

        float perlin = Mathf.PerlinNoise(xCoord, yCoord);
        return Mathf.Pow(perlin,power);
    }

    /// <summary>
    /// Caching values of Perlin noise calculation, so we can smooth land and not calculate every value couple times.
    /// </summary>
    /// <returns></returns>
    private float[,] CachePerlin()
    {
        var basicHeights = new float[width + 2, height + 2];
        for (int x = 0; x < width + 2; x++)
        {
            for (int y = 0; y < height + 2; y++) 
            {
                basicHeights[x, y] = CalculateHeight(x, y);
            }
        }
        return basicHeights;
    }
}
