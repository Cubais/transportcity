﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeightMapApplier : MonoBehaviour
{
    private Terrain terrain;
    private HeightMapGenerator generator;
    
    // The thickness of the terrain. (The other two dimensions are taken from the height map.)
    public int thickness = 950;

    public float scale = 2;
    public float power = 7;

    // Defining offsets for heightmaps here (will be generated in Start() randomly)
    // We could then ask for next values when we need next tile
    // Next tile should have offset: offset_ + scale
    internal float offsetX;
    internal float offsetY;
    private void Awake()
    {
        offsetX = Random.Range(0, 4096);
        offsetY = Random.Range(0, 4096);
        //offsetX = 99;
        //offsetY = 2.5f;

        terrain = GetComponent<Terrain>();
        generator = GetComponent<HeightMapGenerator>();
        generator.Init(scale, power);
        var map = generator.GenerateHeights(offsetX, offsetY);

        ApplyHeightMap(map);
    }

    /// <summary>
    /// Applies a given height map to the terrain. Note that, for this to cover the whole map, its size needs to be a power of 2.
    /// This is due to the fact that the resolution of a height map can only be a power of 2 + 1. 
    /// </summary>
    /// <param name="map"></param>
    internal void ApplyHeightMap(float[,] map)
    {
        TerrainData newData = terrain.terrainData;

        newData.heightmapResolution = map.GetLength(0) + 1;
        newData.size = new Vector3(map.GetLength(0), thickness, map.GetLength(1));
        newData.SetHeights(0, 0, map);

        terrain.terrainData = newData;
    }
}
