﻿using System;

/// <summary>
/// Wrapper for an object that functions as a graph vertex in the A* algorithm.
/// </summary>
/// <typeparam name="T"> The type used to express direction from one path point to another. </typeparam>
/// <typeparam name="S"> The type of the wrapped object. </typeparam>
public interface IPathPoint<T, S> : IEquatable<IPathPoint<T, S>>
{
    /// <summary>
    /// The path point that precedes this one.
    /// </summary>
    IPathPoint<T, S> Previous {get; set;}

    /// <summary>
    /// The "distance" of this path point from the starting path point in a given metric.
    /// </summary>
    float Metric { get; set; }

    /// <summary>
    /// The direction through which to get from the previous point to this one.
    /// </summary>
    T Direction { get; set; }

    /// <summary>
    /// Number of the points successors.
    /// </summary>
    int NumOfSuccessors { get; set; }

    /// <summary>
    /// The object that this wrapper is supposed to represent.
    /// </summary>
    S WrappedObject { get; set; }

    /// <summary>
    /// Returns the next path point in direction i.
    /// </summary>
    IPathPoint<T, S> GetNextPoint(int i, IPathPoint<T, S> end);

    /// <summary>
    /// Computes the "distance" to another path point in the metric of this instance of the algorithm.
    /// </summary>
    float MetricTo(IPathPoint<T, S> other);
}
