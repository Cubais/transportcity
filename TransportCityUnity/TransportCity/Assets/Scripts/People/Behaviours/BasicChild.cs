﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicChild : Person
{
    private void Start()
    {        
        personType = Constants.PersonType.Child;
    }

    public override void Init()
    {
        base.Init();

        FindSchool();
        GoHome();
    }

    /// <summary>
    /// Basic Child goes to school
    /// </summary>
    protected override void OnMorningRushRoutine()
    {
        FindSchool();

        if (!workingPlace)
            workingPlace = BuildingManager.instance.GetRandomBuilding(Constants.ZoneType.Work);
     
        // For children workingPlace = school
        UpdateDestination(workingPlace);        
    }

    /// <summary>
    /// Basic Child stays at school
    /// </summary>
    protected override void OnMiddayRoutine()
    {
        // Do nothing
        var randDestination = BuildingManager.instance.GetRandomBuilding(Constants.ZoneType.Commercial);

        if (!randDestination)
            UpdateDestination(randDestination);
    }

    /// <summary>
    /// Basic Child goes out with friends somewhere
    /// </summary>
    protected override void OnAfternoonRushRoutine()
    {
        // For now it's random living place but we need to add PoI later on
        var POIDestination = BuildingManager.instance.GetRandomBuilding(Constants.ZoneType.Residential);

        UpdateDestination(POIDestination);

        // Register to go back home in two hours
        TimeManager.instance.RegisterAlarmClock(TimeManager.instance.CurrentDateTime.AddHours(2), GoHome);
    }

    /// <summary>
    /// Basic Child stays at home at evening
    /// </summary>
    protected override void OnEveningRoutine()
    {
        // Do nothing
        var randDestination = BuildingManager.instance.GetRandomBuilding(Constants.ZoneType.Commercial);

        if (!randDestination)
            UpdateDestination(randDestination);
    }

    /// <summary>
    /// Basic Child sleeps at home
    /// </summary>
    protected override void OnNightRoutine()
    {
        // Do nothing
    }
}
