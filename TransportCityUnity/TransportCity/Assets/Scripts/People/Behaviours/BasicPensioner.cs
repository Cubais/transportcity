﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicPensioner : Person
{
    private void Start()
    {        
        personType = Constants.PersonType.Pensioner;
    }

    public override void Init()
    {
        base.Init();

        GoHome();
    }

    /// <summary>
    ///  Basic Pensioner goes shopping at the morning
    /// </summary>
    protected override void OnMorningRushRoutine()
    {
        if (!shoppingPlace)
            FindShoppingPlace();

        UpdateDestination(shoppingPlace);
    }

    /// <summary>
    ///  Basic Pensioner goes sleeping at lunch
    /// </summary>
    protected override void OnMiddayRoutine()
    {
        if (!livingPlace)
            FindLivingPlace();

        UpdateDestination(livingPlace);
    }

    /// <summary>
    /// Basic Pensioner goes for a walk to the park
    /// </summary>
    protected override void OnAfternoonRushRoutine()
    {
        var POIDestination = BuildingManager.instance.GetRandomBuilding(Constants.ZoneType.Work);

        UpdateDestination(POIDestination);

        var timeManager = TimeManager.instance;

        // Register to go back home in 3 hours
        timeManager.RegisterAlarmClock(timeManager.CurrentDateTime.AddHours(1), GoHome);
    }

    /// <summary>
    /// Basic Pensioner stays at home at evening
    /// </summary>
    protected override void OnEveningRoutine()
    {
        if (!shoppingPlace)
            FindShoppingPlace();

        // Do nothing
        UpdateDestination(shoppingPlace);
    }

    /// <summary>
    /// Basic Pensioner sleeps at home
    /// </summary>
    protected override void OnNightRoutine()
    {
        // Do nothing
    }
}
