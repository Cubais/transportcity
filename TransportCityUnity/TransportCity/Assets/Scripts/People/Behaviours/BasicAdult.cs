﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicAdult : Person
{
    private void Start()
    {        
        personType = Constants.PersonType.Adult;
    }

    public override void Init()
    {
        base.Init();

        SetOwnedCar();
        GoHome();
    }

    /// <summary>
    ///  Basic Adult goes to work
    /// </summary>
    protected override void OnMorningRushRoutine()
    {
        if (!workingPlace)
            FindWorkingPlace();

        UpdateDestination(workingPlace);
    }

    /// <summary>
    ///  Basic Adult goes for lunch
    /// </summary>
    protected override void OnMiddayRoutine()
    {
        var lunchDestination = BuildingManager.instance.GetRandomBuilding(Constants.ZoneType.Commercial);

        UpdateDestination(lunchDestination);

        var timeManager = TimeManager.instance;

        // Register to go Back to work in two hours
        timeManager.RegisterAlarmClock(timeManager.CurrentDateTime.AddHours(2), OnMorningRushRoutine);
    }

    /// <summary>
    /// Basic Adult goes shopping after work
    /// </summary>
    protected override void OnAfternoonRushRoutine()
    {
        if (!shoppingPlace)
            FindShoppingPlace();

        UpdateDestination(shoppingPlace);
    }

    /// <summary>
    /// Basic Adult goes home at evening
    /// </summary>
    protected override void OnEveningRoutine()
    {
        if (!livingPlace)
            FindLivingPlace();

        UpdateDestination(livingPlace);
    }

    /// <summary>
    /// Basic Adult sleeps at home
    /// </summary>
    protected override void OnNightRoutine()
    {
        // Do nothing
        var randDestination = BuildingManager.instance.GetRandomBuilding(Constants.ZoneType.Commercial);

        if (!randDestination)
            UpdateDestination(randDestination);
    }
}
