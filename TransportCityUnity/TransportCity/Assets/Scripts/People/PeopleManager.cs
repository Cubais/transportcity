﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

using static Constants;

public class PeopleManager : MonoBehaviour
{
    // Singleton
    public static PeopleManager instance;

    public GameObject roadParent;
    public GameObject personParent;

    internal UI_API ui;

    // The destination to which the people should head after pressing L.
    public Vector3 destination;

    // List of all persons
    internal List<Person> people;

    /// <summary>
    /// Number of people living in the city right now
    /// </summary>
    public int PeopleCount => people.Count;

    // Number of children spawned in the city
    internal int childrenCount;

    // Number of all adults spawned in the city
    internal int adultCount;

    // Number of all pensioners spawned in the city
    internal int pensionerCount;

    // Indicates whether people should be created
    internal bool createOnClick = false;

    internal int debugRush = 0;
        
    internal int peopleOwnedCarTotal = 0;

    internal int peopleUsingCarCount = 0;

    // Stores happiness value for each person
    internal Dictionary<Person, float> peopleHappiness = new Dictionary<Person, float>();

    // Sum of the happiness of all persons
    internal float personsHappinessSum = 0;

    internal CreateAgentsCoroutineContext createAgentsCC = new CreateAgentsCoroutineContext();

    private void Awake()
    {
        people = new List<Person>();

        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(this.gameObject);
        }        

        ui = UI_API.GetInstance();        
    }

    public void GeneratePeopleOnStart()
    {
        RegisterToCallEachDay();

        var peopleNumber = Constants.peopleRatioAtStart * BuildingManager.instance.totalLivingCapacity;
        StartCoroutine(CreateAgents((int)peopleNumber));

    }

    public void GeneratePeopleDaily()
    {
        if (BuildingManager.instance.buildingZonesAverageOccupancyRate[(int)ZoneType.Residential] <= maxOccupancyRateForNewPeople)
        {
            RegisterToCallEachDay();

            var peopleNumber = Constants.peopleRatioDaily * BuildingManager.instance.totalLivingCapacity;
            if (peopleNumber + PeopleCount >= BuildingManager.instance.totalLivingCapacity)
            {
                peopleNumber = BuildingManager.instance.totalLivingCapacity - PeopleCount;
                //Debug.Log($"The city is going to be full! The date is just {TimeManager.instance.CurrentDateTime}");
            }

            StartCoroutine(CreateAgents((int)peopleNumber));
        }
        BuildingManager.instance.AdjustOccupancyRatio(ZoneType.Residential);
        BuildingManager.instance.AdjustOccupancyRatio(ZoneType.Commercial);
        BuildingManager.instance.AdjustOccupancyRatio(ZoneType.Work);
    }

    /// <summary>
    /// Registers daily people generating to time manager's alarm clock to the beginning of the next day).
    /// </summary>
    private void RegisterToCallEachDay()
    {
        DateTime nextDay = TimeManager.instance.CurrentDateTime
            .AddDays(1);
        TimeManager.instance.RegisterAlarmClock(nextDay, GeneratePeopleDaily);
    }

    /// <summary>
    /// Creates people at random road
    /// </summary>    
    public IEnumerator CreateAgents(int peopleCount)
    {
        createAgentsCC.isRunning = true;
        createAgentsCC.peopleCount = peopleCount;

        for (int i = 0; i < peopleCount; i++)
        {
            InstantiatePersonOfType(GetRandomRoadPosition(), DeterminePersonType());
            createAgentsCC.peopleCount--;

            // Creates 50 persons at once
            if (i % 50 == 0)
            {
                yield return new WaitForSeconds(2);
            }
        }

        Debug.Log(peopleCount + " people generated");
        createAgentsCC.isRunning = false;
    }

    /// <summary>
    /// Get random position within roads
    /// </summary>
    /// <returns>Random position on the pathwalk</returns>
    public Vector3 GetRandomRoadPosition()
    {
        var validRoad = false;
        int roadIndex = 0;
        while (!validRoad)
        {
            roadIndex = UnityEngine.Random.Range(0, roadParent.transform.childCount - 1);
            var roadType = roadParent.transform.GetChild(roadIndex).GetComponent<RoadSegment>().type;
            validRoad = roadType != SegmentType.IRoadC;
        }

        return roadParent.transform.GetChild(roadIndex).position;
    }

    private Constants.PersonType DeterminePersonType()
    {
        var currentChildrenRatio = childrenCount / (people.Count + 1f);
        var currentAdultsRatio = adultCount / (people.Count + 1f);
        var currentPensionersRatio = pensionerCount / (people.Count + 1f);

        if (currentChildrenRatio < Constants.childrenRatio)
        {
            return Constants.PersonType.Child;
        }

        if (currentPensionersRatio < Constants.pensionerRatio)
        {
            return Constants.PersonType.Pensioner;
        }

        return Constants.PersonType.Adult;
    }

    /// <summary>
    /// Creates one person on the place where the player is pointing.
    /// </summary>
    private void CreateOnClick()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            if (hit.transform.tag == "Road")
            {
                InstantiatePersonOfType(hit.transform.position, DeterminePersonType());
            }
        }
    }

    private void InstantiatePersonOfType(Vector3 pos, Constants.PersonType personType)
    {
        var personList = Resources.LoadAll<GameObject>("Prefabs/People/" + personType);
        var personObject = personList[UnityEngine.Random.Range(0, personList.Length)];

        var go = Instantiate(personObject, pos, Quaternion.identity, personParent.transform);

        switch (personType)
        {
            case Constants.PersonType.Child:
                childrenCount++;
                go.AddComponent<BasicChild>();
                break;
            case Constants.PersonType.Adult:
                adultCount++;
                go.AddComponent<BasicAdult>();
                break;
            case Constants.PersonType.Pensioner:
                pensionerCount++;
                go.AddComponent<BasicPensioner>();
                break;
            default:
                break;
        }

        var person = go.GetComponent<Person>();
        person.personType = personType;

        person.Init();
        peopleHappiness.Add(person, person.personHappiness);
        personsHappinessSum += person.personHappiness;

        people.Add(person);
    }

    /// <summary>
    /// Finds a destination on the navmesh that is close to where the player is pointing.
    /// </summary>
    private void SetDestination()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            if (hit.transform.tag == "Road")
            {
                NavMeshHit nMHit = new NavMeshHit();
                NavMesh.SamplePosition(hit.transform.position, out nMHit, 5, NavMesh.AllAreas);
                destination = nMHit.position;
            }
        }
    }

    public Vector3 GetDestination()
    {
        if (destination == null)
            destination = GetRandomRoadPosition();

        return destination;
    }

    /// <summary>
    /// Adjust car usage and change slider
    /// </summary>
    /// <param name="carUsed">Is person using car and he didn't in the previous way? or vice versa</param>
    public void PersonUsingCar(bool carUsed)
    {        
        // Person wants to use a car in his journey
        if (carUsed)
        {
            peopleUsingCarCount++;
        }
        else
        {
            peopleUsingCarCount--;
        }

        if (peopleUsingCarCount > peopleOwnedCarTotal || peopleUsingCarCount < 0)
            Debug.LogError("PeopleUsing car wrong value >" + peopleUsingCarCount);
        
        ui.SetCarUsageSlider(peopleUsingCarCount / (float)peopleOwnedCarTotal);        
    }

    /// <summary>
    /// Register car that is in the city and will be used
    /// </summary>
    public void RegisterCar(Person person)
    {
        peopleOwnedCarTotal++;
        peopleUsingCarCount += (person.lastJourneyCarUsed) ? 1 : 0;

        ui.SetCarUsageSlider(peopleUsingCarCount / (float)peopleOwnedCarTotal);
    }

    /// <summary>
    /// Adjust happiness slider based on people happiness
    /// </summary>
    /// <param name="person">Person, who happiness changed</param>
    /// <param name="personHappiness">Persons current happiness value</param>
    public void AdjustHappiness(Person person, float personHappiness)
    {
        if (peopleHappiness.ContainsKey(person))
        {
            // Remove old happ. value from sum and add the new one (faster than make a sum throught all dictionary)
            personsHappinessSum -= peopleHappiness[person];
            personsHappinessSum += personHappiness;
            personsHappinessSum = (personsHappinessSum < 0) ? 0 : personsHappinessSum;

            // Store new happ. value
            peopleHappiness[person] = personHappiness;

            // Change slider acording to average happ. value
            ui.SetHappinessSlider(personsHappinessSum / peopleHappiness.Count);
        }
    }

    /// <summary>
    /// Cleans up references and adjust values after person is destroyed
    /// </summary>
    public void OnPersonDestroy(Person person)
    {
        // Adjust car usage if person had a car
        if (person.ownedCar)
        {
            peopleUsingCarCount -= (person.lastJourneyCarUsed) ? 1 : 0; 
            peopleOwnedCarTotal -= (peopleOwnedCarTotal > 1) ? 1 : 0;
                        
            ui.SetCarUsageSlider(CalculateCarUsage());
        }

        // Adjust people happinnes
        if (peopleHappiness.ContainsKey(person))
        {
            personsHappinessSum -= peopleHappiness[person];
            personsHappinessSum = (personsHappinessSum < 0) ? 0 : personsHappinessSum;

            peopleHappiness.Remove(person);

            ui.SetHappinessSlider(CalculateHappiness());
        }

        people.Remove(person);
        if (person.livingPlace == null)
        {
            person.OnMoveOut(person.livingPlace, true);
        }

        TimeManager.instance.UnregisterPartOfDayChangeHandler(person.OnPartOfDayChange);
    }

    /// <summary>
    /// Calculates and returns the car usage index
    /// </summary>
    /// <returns>The car usage index as a float between 0 and 1</returns>
    public float CalculateCarUsage()
    {
        return peopleUsingCarCount / (float)peopleOwnedCarTotal;
    }

    /// <summary>
    /// Calculates and returns the happiness index
    /// </summary>
    /// <returns>The happiness index as a float between 0 and 1</returns>
    public float CalculateHappiness()
    {
        // To not divide by zero
        var totalPeople = (peopleHappiness.Count != 0) ? peopleHappiness.Count : 1;
        return personsHappinessSum / totalPeople;
    }
}
