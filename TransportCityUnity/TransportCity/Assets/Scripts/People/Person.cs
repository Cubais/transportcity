﻿using System;
using System.Collections;
using System.Collections.Generic;
using static Constants;

#if UNITY_EDITOR

using UnityEditor.Rendering;

#endif

using UnityEngine;
using UnityEngine.AI;

public enum TransportType { Walk, Car, PublicTransport};
public enum HappinessImpact { Large, Small};

public abstract class Person : MonoBehaviour
{
    // The stops that the person needs to go through to get to his destination.
    public Stack<IStop> PathStops { get; internal set; }

    // The lines that the person needs to go through to get to his destination.
    public Stack<ILine> PathLines { get; internal set; }

    public bool isInsideVehicle = false;

    [SerializeField]
    public Building livingPlace;

    [SerializeField]
    public Building workingPlace;

    [SerializeField]
    public Building shoppingPlace;

    internal NavMeshAgent agent;

    protected PeopleManager peopleManager;

    internal Collider personCollider;

    private Animator animatorController;

    internal SkinnedMeshRenderer meshRenderer;

    [SerializeField]
    internal Constants.PersonType personType;

    [SerializeField]
    internal CarNavigation ownedCar;

    [SerializeField]
    internal bool drivingCar = false;

    // Indicates whether person used the car in the previous journey
    [SerializeField]
    internal bool lastJourneyCarUsed = true;

    [SerializeField]
    internal Vector3 currentDestination;

    // Building where the person is heading or building where the person is
    [SerializeField]
    internal Building currentBuilding;

    [SerializeField]
    internal bool isInsideBuilding = false;

    [SerializeField]
    internal bool isComputingTransportType = false;

    [SerializeField]
    internal TransportType currentTransportType;

    // In range of [0, 1], where 0 = sad, 1 = happy
    [SerializeField]
    internal float personHappiness = 0.5f;

    // Estimated travelling time of current journey
    [SerializeField]
    internal float estimatedTravellingTime = 0;

    // Start time of journey
    [SerializeField]
    internal float startJourneyTime = 0;

    // The minimum time that a person should wait in a traffic jam before getting out of the car.
    [SerializeField]
    internal float minWaitTime = 45f;

    // The maximum time that a person should wait in a traffic jam before getting out of the car.
    [SerializeField]
    internal float maxWaitTime = 180f;

    // The number of seconds that the person has been waiting in a traffic jam.
    [SerializeField]
    internal float timer = 0;

    // A random number which is randomly generated every time a person gets into a car. It determines
    // how long a person will wait before getting out of his car.
    internal float rnd;

    // The previous position of the person's car.
    internal Vector3 prevCarPos = Vector3.positiveInfinity;

    internal ChooseTransportTypeCoroutineContext chooseTransportTypeCC = new ChooseTransportTypeCoroutineContext();

    void Awake()
    {
        peopleManager = PeopleManager.instance;
        agent = GetComponent<NavMeshAgent>();
        personCollider = GetComponent<Collider>();
        animatorController = GetComponent<Animator>();
        meshRenderer = GetComponent<SkinnedMeshRenderer>();

        currentTransportType = TransportType.Walk;

        animatorController.SetBool("Walking", false);

        TimeManager.instance.RegisterPartOfDayChangeHandler(OnPartOfDayChange);
    }

    private void Update()
    {
        if (isComputingTransportType)
            return;

        if (drivingCar)
        {
            // Should drive a car, but looking for better place to spawn a car
            if (currentTransportType == TransportType.Walk)
            {
                animatorController.SetBool("Walking", !agent.pathPending && !isInsideBuilding && (agent.velocity.magnitude > 0.01));
                meshRenderer.enabled = !agent.pathPending && !isInsideBuilding && (agent.velocity.magnitude > 0.01);

                DriveACarToDestination(currentDestination);
            }
            else
            {
                // Check whether the person's car has been stuck in a traffic jam for too long.
                if ((ownedCar.transform.position - prevCarPos).magnitude == 0)
                {
                    timer += Time.deltaTime;

                    if (timer >= minWaitTime)
                    {                        
                        if (rnd <= (timer - minWaitTime) / (maxWaitTime - minWaitTime))
                        {
                            LowerHappiness(HappinessImpact.Small);
                            EjectPerson(FindClosestWaypointWithinRange(transform.position, 5, new List<SegmentType> { SegmentType.IRoadC }).transform.position);
                        }
                    }
                }
                else
                {
                    timer = 0;
                }

                prevCarPos = ownedCar.transform.position;

                transform.position = ownedCar.transform.position;
                if (ownedCar.carAtDestination)
                {
                    EjectPerson();
                }
                else
                {
                    return;
                }
            }

            return;
        }
        
        animatorController.SetBool("Walking", !agent.pathPending && !isInsideBuilding && (agent.velocity.magnitude > 0.01));
        meshRenderer.enabled = !agent.pathPending && !isInsideBuilding;

        if (currentTransportType == TransportType.Walk)
        {
            // Person is in front of the door of the building
            if (agent.enabled && (Vector3.Distance(transform.position, currentDestination) <= 2f) && currentBuilding && !isInsideBuilding)
            {
                TravellingTimeHappiness();
                EnterBuilding(currentBuilding);
            }
        }
        else if (currentTransportType == TransportType.PublicTransport)
        {
            if (!PathStops.Peek().Exists())
            {
                if (chooseTransportTypeCC.isRunning == false)
                {
                    UpdateDestination(currentBuilding);
                }
            }
            // If the person is in front of the subway that leads to a subway line he wants to take, he should enter it.
            else if (agent.enabled && (Vector3.Distance(transform.position, agent.destination) <= 2f) && PathStops.Peek() is SubwayStop && !isInsideBuilding)
            {
                EnterBuilding((SubwayStop)PathStops.Peek());
            }
        }
    }

    public virtual void Init()
    {
        FindLivingPlace();
        FindShoppingPlace();
        FindWorkingPlace(); 
    }

    protected abstract void OnMorningRushRoutine();
    protected abstract void OnMiddayRoutine();
    protected abstract void OnAfternoonRushRoutine();
    protected abstract void OnEveningRoutine();
    protected abstract void OnNightRoutine();
    internal virtual void OnPartOfDayChange(Constants.PartOfDay partOfDay)
    {
        switch (partOfDay)
        {
            case Constants.PartOfDay.MORNING_RUSH:
                PostponeRush(3, OnMorningRushRoutine);  
                break;
            case Constants.PartOfDay.MIDDAY:
                PostponeRush(4, OnMiddayRoutine);
                break;
            case Constants.PartOfDay.AFTERNOON_RUSH:
                PostponeRush(3, OnAfternoonRushRoutine);
                break;
            case Constants.PartOfDay.EVENING:
                PostponeRush(3, OnEveningRoutine);
                break;
            case Constants.PartOfDay.NIGHT:
                PostponeRush(5, OnNightRoutine);
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// Postpones rush call
    /// </summary>
    /// <param name="MaxPostponeHours">Max times in hours to be postpone</param>
    /// <param name="callback">Rush function to be postponed</param>
    void PostponeRush(int MaxPostponeHours, TimeManager.FutureCallback callback)
    {
        var dateTime = TimeManager.instance.CurrentDateTime;
        var hoursAdded = UnityEngine.Random.Range(0, MaxPostponeHours);
        var minsAdded = UnityEngine.Random.Range(0, 60);

        if (MaxPostponeHours > 0)
        {
            dateTime = dateTime.AddHours(hoursAdded).AddMinutes(minsAdded);
            Debug.Log("HoursAdde " + hoursAdded + " Mins " + minsAdded + " RESULT TIME " + dateTime);
        }

        TimeManager.instance.RegisterAlarmClock(dateTime, callback);
    }

    public void UpdateDestination(Building buildingDestination)
    {
        if (buildingDestination == null)
        {
            GoHome();
            return;
        }

        // Go out if inside the building
        if (isInsideBuilding)
        {
            ExitBuilding(currentBuilding);
        }

        if (buildingDestination.inFrontOfDoor == null)
            Debug.Log(buildingDestination.gameObject.name + " does not have frontDoor assigned");

        // Update currentDestination position and building where the person is heading to
        currentDestination = buildingDestination.inFrontOfDoor.transform.position;
        currentBuilding = buildingDestination;

        StartCoroutine(ChooseTransportType(currentDestination));
    }

    public void EnterBuilding(Building building)
    {
        isInsideBuilding = true;

        if (agent.enabled)
        {
            agent.ResetPath();
            agent.enabled = false;
        }

        personCollider.enabled = false;
        transform.position = building.transform.position;
        meshRenderer.enabled = false;

        building.peopleInBuilding.Add(this);
    }

    public void ExitBuilding(Building building)
    {        
        isInsideBuilding = false;

        if (building)
        {
            building.peopleInBuilding.Remove(this);
        }

        if (this == null)
            return;

        if (NavMesh.SamplePosition(transform.position, out NavMeshHit hit, 10, NavMesh.AllAreas))
        {
            transform.position = hit.position;
            agent.enabled = true;
            agent.Warp(hit.position);
        }        

        personCollider.enabled = true;
        meshRenderer.enabled = true;
    }

    protected void GoHome()
    {
        if (isInsideBuilding)
        {
            ExitBuilding(currentBuilding);
        }

        UpdateDestination(livingPlace);
    }

    /// <summary>
    /// Move out person from given building
    /// </summary>
    /// <param name="building">Building, from which person should be moved out</param>
    /// <param name="permanent">Move out permanentaly? (used when destroying person)</param>
    public void OnMoveOut(Building building, bool permanent = false)
    {
        if (!permanent)
        {
            FindNewPlace(building.Type);
            LowerHappiness(HappinessImpact.Large);
        }

        building.ResidentMovedOut(this);
        if (isInsideBuilding)
        {
            ExitBuilding(building);
        }
    }

    protected void FindNewPlace(Constants.ZoneType zoneType)
    {
        switch (zoneType)
        {
            case Constants.ZoneType.Residential:
                FindLivingPlace();
                break;
            case Constants.ZoneType.Commercial:
                FindShoppingPlace();
                break;
            case Constants.ZoneType.Work:
                FindWorkingPlace();
                break;
        }
    }

    /// <summary>
    /// Finds living place for person
    /// </summary>
    protected void FindLivingPlace()
    {
        livingPlace = BuildingManager.instance.GetBuildingOfZone(Constants.ZoneType.Residential);

        if (!livingPlace)
            Debug.LogError("Living place is NULL");

        livingPlace.ResidentMovedIn(this);
    }

    /// <summary>
    /// Finds working place/ school based on person type
    /// </summary>
    protected void FindWorkingPlace()
    {
        workingPlace = BuildingManager.instance.GetBuildingOfZone(Constants.ZoneType.Work);

        if (!workingPlace)
            Debug.LogError("Working place is NULL");

        workingPlace.ResidentMovedIn(this);
    }

    /// <summary>
    /// Finds shopping place for person
    /// </summary>
    protected void FindShoppingPlace()
    {
        shoppingPlace = BuildingManager.instance.GetBuildingOfZone(Constants.ZoneType.Commercial);

        if (!shoppingPlace)
            Debug.LogError("Shopping place is NULL");

        shoppingPlace.ResidentMovedIn(this);
    }

    protected void FindSchool()
    {
        workingPlace = BuildingManager.instance.GetSchool();

        if (!workingPlace)
            Debug.LogError("School place is NULL");

        workingPlace.ResidentMovedIn(this);
    }

    /// <summary>
    /// Person choose the best possible transport type to given destination
    /// </summary>
    /// <param name="destination">Destination, where person wants to go</param>
    internal IEnumerator ChooseTransportType(Vector3 destination)
    {
        chooseTransportTypeCC.isRunning = true;
        chooseTransportTypeCC.destination = destination;

        while (drivingCar)
            yield return null;

        isComputingTransportType = true;

        // Check whether agent is enabled
        while (!agent.isActiveAndEnabled)
            yield return new WaitForEndOfFrame();

        // Tells the person to go to the destination in order to judge the remaining distance.
        agent.SetDestination(destination);

        float publicTransportTime;
        (PathStops, PathLines, publicTransportTime) = Scheduler.GetInstance().GetBestLine(transform.position, destination);

        // Wait for path to be computed
        if (agent.pathPending || agent.pathStatus == NavMeshPathStatus.PathInvalid)
            yield return new WaitForFixedUpdate();

        // Path, which was computed is updated in next Update cycle, so wait for it 1 frame
        yield return new WaitForEndOfFrame();

        // Decide best transport type
        float carTime = -1;
        var walkTime = Scheduler.GetInstance().GetWalkingTime(transform.position, destination);
        var bestTransportType = EvaluateTransportType(publicTransportTime, walkTime, destination, ref carTime);

        // Adjust car usage if necessary
        AdjustCarUsage(bestTransportType);

        if (bestTransportType == TransportType.Car)
        {
            PathStops = null;
            PathLines = null;

            DriveACarToDestination(destination);
            // Estimated time is measured in ingame minutes, we want real seconds (in-game minute = 3 realtime seconds)
            estimatedTravellingTime = carTime * 3;            
        }
        else if (bestTransportType == TransportType.PublicTransport)
        {
            agent.SetDestination(PathStops.Peek().EntryPoint());
            currentTransportType = TransportType.PublicTransport;
            estimatedTravellingTime = publicTransportTime;

            RaiseHappiness(HappinessImpact.Small);
        }
        else
        {
            PathStops = null;
            PathLines = null;

            currentTransportType = TransportType.Walk;
            estimatedTravellingTime = walkTime;
        }

        isComputingTransportType = false;
        startJourneyTime = Time.realtimeSinceStartup;
        chooseTransportTypeCC.isRunning = false;
    }

    /// <summary>
    /// Get current path length of the agent
    /// </summary>
    /// <returns>Path length in Unity units</returns>
    private float PathLength()
    {
        if (agent.pathPending ||
            agent.pathStatus == NavMeshPathStatus.PathInvalid ||
            agent.path.corners.Length == 0)
            return float.MaxValue;

        float distance = 0.0f;
        for (int i = 0; i < agent.path.corners.Length - 1; ++i)
        {
            distance += Vector3.Distance(agent.path.corners[i], agent.path.corners[i + 1]);
        }

        return distance;
    }

    /// <summary>
    /// Evalutaing the best transport type to take
    /// </summary>
    /// <param name="lsst">Route using public transport</param>
    /// <param name="walkingPathDistance">Walking distance of agent</param>
    /// <param name="destination">Destination of the agent</param>
    /// <returns>The best possible TransportType</returns>
    protected TransportType EvaluateTransportType(float publicTransportTime, float walkingTime, Vector3 destination, ref float carTime)
    {
        // Compute walking cost
        float walkCost = walkingTime * GetTransportCost(TransportType.Walk);

        // Walking cost is really cheap, walk anyway
        if (walkCost <= 20f)
        {          
            return TransportType.Walk;
        }

        float carCost = 0;
        if (ownedCar)
        {
            // Compute car cost based on real car path
            var startWaypoint = FindClosestWaypointWithinRange(transform.position, 1, null);

            var endWaypoint = FindClosestWaypointWithinRange(destination, 1, null);

            var radius = 1f;
            while (endWaypoint == null)
            {
                radius += 0.5f;
                endWaypoint = FindClosestWaypointWithinRange(destination, radius, null);                
            }

            var carPath = PathManager.GetInstance().GetPath(startWaypoint, endWaypoint);

            // Shouldn't happen, but keep it for sure
            if (carPath == null)
            {
                carCost = float.MaxValue;
                Debug.LogError("NUll path, endW " + endWaypoint + " startW " + startWaypoint);
            }
            else
            {
                carTime = (startWaypoint.transform.position - endWaypoint.transform.position).magnitude * approximationCoefficient / avgVehicleSpeed;
                carCost = carTime * GetTransportCost(TransportType.Car);
            }
        }
        else
        {
            carCost = float.MaxValue;
        }

        // Compute public transport cost        
        var metroBusCost = publicTransportTime * GetTransportCost(TransportType.PublicTransport);

        // Decide what type is the best
        if (walkCost <= carCost)
        {
            if (walkCost <= metroBusCost)
            {
                return TransportType.Walk;
            }
            else
            {
                return TransportType.PublicTransport;
            }
        }
        else
        {
            if (metroBusCost <= carCost)
            {
                return TransportType.PublicTransport;
            }
            else
            {
                return TransportType.Car;
            }
        }
    }

    /// <summary>
    /// Returns estimated cost of given transport type
    /// </summary>    
    /// <returns>Cost of given transportType</returns>
    private float GetTransportCost(TransportType transportType)
    {
        switch (transportType)
        {
            case TransportType.Walk:
                return 1;

            case TransportType.Car:
                return 3.0f;

            case TransportType.PublicTransport:
                return 1.2f;
        }

        throw new MissingComponentException("Not defined transport type was inserted");
    }

    /// <summary>
    /// Notify PeopleManager whether person want to use a car
    /// Notify only, if the car was used in the previous journey and now it's not, and vice versa
    /// </summary>
    void AdjustCarUsage(TransportType chosenTransportType)
    {
        // If car usage would remain unchange from previous journey or the person doesn't own a car => skip
        if ((chosenTransportType == TransportType.Car && lastJourneyCarUsed) || (chosenTransportType != TransportType.Car && !lastJourneyCarUsed) || !ownedCar)
            return;

        var usingCar = chosenTransportType == TransportType.Car;
        peopleManager.PersonUsingCar(usingCar);
        lastJourneyCarUsed = usingCar;
    }


    /// <summary>
    /// Instantiate random car and assignes it to the person
    /// </summary>
    internal void SetOwnedCar()
    {
        // If already has a car
        if (ownedCar)
        {            
            return;
        }

        // Choose random car
        ownedCar = CarCreator.instance.CreateRandomCar(transform.position, Quaternion.identity);
        ownedCar.gameObject.SetActive(false);

        // Register car before using it
        peopleManager.RegisterCar(this);        
    }

    /// <summary>
    /// Finds closest waypoint to the given position.    
    /// </summary> 
    /// <param name="range">Find waypoint within range</param>
    /// <param name="restrictedRoads">Waypoint shouldnt be on these road types</param>
    /// <returns>Closest waypoint to the given position, null = no waypoint was found on allowed road types</returns>
    protected Waypoint FindClosestWaypointWithinRange(Vector3 position, float range, List<SegmentType> restrictedRoads)
    {
        // Creates collider in order to find the road
        var overlapingObjects = Physics.OverlapSphere(position, range);
        foreach (var item in overlapingObjects)
        {
            if (item.gameObject.CompareTag("Road"))
            {
                var road = item.gameObject.GetComponent<RoadSegment>();

                // If road is not restricted return waypoint on it
                if (restrictedRoads == null || !restrictedRoads.Contains(road.type))
                {
                    return road.GetClosestWaypointToPosition(position);
                }
            }
        }

        return null;
    }

    /// <summary>
    /// Activates pocket car and make a person drive a car.
    /// If person cannot place a car, he walks further until it's possible to place a car
    /// </summary>
    /// <param name="destination">Destiantion where person should drive a car</param>
    protected void DriveACarToDestination(Vector3 destination)
    {
        if (ownedCar)
        {
            drivingCar = true;

            var startWaypoint = FindClosestWaypointWithinRange(transform.position, 1, new List<SegmentType>() { SegmentType.XRoad, SegmentType.TRoad, SegmentType.IRoadC });

            // If no suitable starting waypoint was found or waypoint is occupied, walk for now
            if (startWaypoint == null || startWaypoint.CarOnTheWaypoint())
            {
                currentTransportType = TransportType.Walk;
                return;
            }

            var endWaypoint = FindClosestWaypointWithinRange(destination, 1, new List<SegmentType>());
            var range = 1f;

            while (endWaypoint == null)
            {
                range += 0.5f;
                endWaypoint = FindClosestWaypointWithinRange(destination, range, new List<SegmentType>());
            }

            //Setup car position, start and destination + make car visible
            ownedCar.transform.position = startWaypoint.transform.position;

            // Make the car rotates toward next waypoint
            ownedCar.transform.LookAt(startWaypoint.GetNextWaypoint().transform.position);

            ownedCar.SetStartingWaypoint(startWaypoint);
            ownedCar.GoTo(endWaypoint);

            ownedCar.gameObject.SetActive(true);

            rnd = UnityEngine.Random.Range(0, 1);
            prevCarPos = ownedCar.transform.position;
            GetIntoVehicle(ownedCar.gameObject, TransportType.Car);
        }
        else
        {
            drivingCar = false;
            currentTransportType = TransportType.Walk;
        }
    }

    /// <summary>
    /// Makes the person get out of the carand places him at his current destination.
    /// </summary>
    protected void EjectPerson()
    {
        EjectPerson(currentDestination);
    }

    /// <summary>
    /// Makes the person get out of the carand places him at the given position.
    /// </summary>
    protected void EjectPerson(Vector3 pos)
    {
        ownedCar.gameObject.SetActive(false);

        drivingCar = false;

        ExitVehicle(pos);
    }

    /// <summary>
    /// Makes the person get into a vehicle.
    /// </summary>
    /// <param name="vehicle"> The vehicle that the person is supposed to get into. </param>
    /// <param name="type"> The type of transport that this vehicle is a part of. </param>
    public void GetIntoVehicle(GameObject vehicle, TransportType type)
    {
        if (agent.enabled)
        {
            // Agent is inside a vehicle, no need to calculating path or use agent
            agent.ResetPath();
            agent.enabled = false;
        }

        // Disable collider due to crosswalks and meshrenderer to not be able to see person
        personCollider.enabled = false;
        transform.position = vehicle.transform.position;
        meshRenderer.enabled = false;

        // Set transform parent in order to move with vehicle automatically
        transform.SetParent(vehicle.transform);

        // Agent is driving a car
        currentTransportType = type;

        // If the person is taking some form of public transport, then getting into a vehicle means that he was at a stop
        // and that he is getting on a new line, so he should pop both his stops stack and his lines stack.
        if (type == TransportType.PublicTransport)
        {
            PathStops.Pop();
            PathLines.Pop();
        }

        isInsideVehicle = true;
        isInsideBuilding = false;

        animatorController.SetBool("Walking", false);        
    }

    public void ExitVehicle(Vector3 newPosition)
    {
        // Make person visible and activates collider        
        personCollider.enabled = true;
        agent.enabled = true;
        meshRenderer.enabled = true;

        // Sets person to the destination
        agent.Warp(newPosition);

        transform.SetParent(peopleManager.personParent.transform);

        GetComponent<Collider>().enabled = true;

        // If the person is currently using public transport, then getting out of vehicle means that he should either look for the next
        // stop he wants to go to, or that he should walk to his final destination.
        if (currentTransportType == TransportType.PublicTransport && PathStops != null && PathStops.Count != 0)
        {
            agent.SetDestination(PathStops.Peek().EntryPoint());
        }
        else
        {
            agent.SetDestination(currentDestination);
            currentTransportType = TransportType.Walk;
        }

        isInsideVehicle = false;
    }

    /// <summary>
    /// Raises happiness
    /// </summary>
    /// <param name="largeImpact">This raise have a big impact</param>
    public void RaiseHappiness(HappinessImpact impact)
    {
        var raiseCoef = (impact == HappinessImpact.Large) ? 0.2f : 0.1f;
        personHappiness = Mathf.Clamp01(personHappiness + raiseCoef);

        peopleManager.AdjustHappiness(this, personHappiness);
    }

    /// <summary>
    /// Lowers happiness
    /// </summary>
    /// <param name="largeImpact">This raise have a big impact</param>
    public void LowerHappiness(HappinessImpact impact)
    {
        var raiseCoef = (impact == HappinessImpact.Large) ? 0.2f : 0.1f;
        personHappiness = Mathf.Clamp01(personHappiness - raiseCoef);

        peopleManager.AdjustHappiness(this, personHappiness);
    }

    /// <summary>
    /// Adjusts happinnes of the person based on estimated and real travelling time
    /// </summary>
    private void TravellingTimeHappiness()
    {
        var realTime = Time.realtimeSinceStartup - startJourneyTime;
        if (estimatedTravellingTime > realTime)
        {            
            RaiseHappiness(HappinessImpact.Large);
        }
        else
        {
            var diff = realTime - estimatedTravellingTime;
            var ratioDelay = diff / estimatedTravellingTime;
                        
            // If delay was bigger than 30% of estimated value, lower happiness by bigger amount
            LowerHappiness((ratioDelay >= 0.3) ? HappinessImpact.Large : HappinessImpact.Small);
        }
    }

    private void OnDestroy()
    {
        peopleManager.OnPersonDestroy(this);
    }

    public Building GetCurrentBuildingDestination()
    {
        return currentBuilding;
    }
}
