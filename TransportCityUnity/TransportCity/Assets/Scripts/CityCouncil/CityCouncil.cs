﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using static Constants;
using static CityCouncilEvents;

public class CityCouncil : MonoBehaviour
{
    /// <summary>
    /// List of events we want to check
    /// </summary>
    public List<EventData> events;

    public BusInstanceManager busInstanceManager;

    public SubwayInstanceManager subwayInstanceManager;

    private Queue<EventData> toPopUp;
    
    void Awake()
    {
        events = new List<EventData>();

        toPopUp = new Queue<EventData>();
    }

    void Start()
    {
        AddStartingEvents();

        RegisterCallToTimeManager();
    }

    /// <summary>
    /// Functions as update function, calls TimeManager to call it every few in-game minutes
    /// </summary>
    public void UpdateMe()
    {
        var toRemove = new List<EventData>();
        for (int i = events.Count - 1; i >= 0; i--)
        {
            CheckEvent(events[i]);
            if (events[i].IsFinished)
            {
                toRemove.Add(events[i]);
            }
        }

        foreach(var e in toRemove)
        {
            events.Remove(e);
        }

        RegisterCallToTimeManager();

        PopUpQueue();
    }

    private void CheckEvent(EventData e)
    {
        if (e.Evaluate())
        {
            e.Complete();
            if (e.isRepetitive)
            {
                events.Add(e.Repeat());
            }

            MoneyManager.instance.GainMoneyOneTime(e.amount, e.notificationText);

            toPopUp.Enqueue(e);
        }

    }

    private void AddStartingEvents()
    {
        events.Add(TimeEvent.Create());
        events.Add(BusLineEvent.Create());
        events.Add(BusEvent.Create());
        events.Add(SubwayLineEvent.Create());
        events.Add(SubwayTrainEvent.Create());
        events.Add(QuickBusLineBuildingEvent.Create());
    }

    private void RegisterCallToTimeManager()
    {
        DateTime nextTime = TimeManager.instance.CurrentDateTime.AddMinutes(cityCouncilUpdateFreq);
        TimeManager.instance.RegisterAlarmClock(nextTime, UpdateMe);
    }

    /// <summary>
    /// Pops up one pop-up window of the finished event (only when there is such event and no other pop-up window is active).
    /// </summary>
    private void PopUpQueue()
    {
        if (toPopUp.Count > 0 && !PopUpWindowManager.Instance.IsActive)
        {
            var e = toPopUp.Dequeue();

            PopUpWindowManager.Instance.EnableOneButtonWindow($"<b>{e.title}:</b>\n {e.text}", ()=> { });
        }
    }

}
