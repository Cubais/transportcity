﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class EventData
{
    /// <summary>
    /// Unique identifier
    /// </summary>
    public string id;

    /// <summary>
    /// Title for the UI
    /// </summary>
    public string title;

    /// <summary>
    /// Text for the UI
    /// </summary>
    public string text;

    /// <summary>
    /// Text to be shown in notification bar (along with the amount)
    /// </summary>
    public string notificationText;

    /// <summary>
    /// What to do to finish this event.
    /// </summary>
    public string achievementText;

    /// <summary>
    /// Amount of money given to player by council (negative amount means council wants this money from player)
    /// </summary>
    public int amount;

    /// <summary>
    /// New events to be checked after finishing this one (for example - build 10 more lines)
    /// </summary>
    public List<EventData> postFinishNewEvents;

    /// <summary>
    /// Some number we're comparing to (doesn't have to be used/filled)
    /// </summary>
    public long startingNumber;

    /// <summary>
    /// Function to evaluate, returns true when accomplished
    /// </summary>
    public event Predicate<long> Event;

    /// <summary>
    /// Contains information whether has this event passed at least once
    /// </summary>
    public bool IsFinished { get; private set; }

    /// <summary>
    /// Is this event repetitive?
    /// </summary>
    public bool isRepetitive;

    /// <summary>
    /// If event is repetitive, this attribute contains delegate to a function which will repeatedly register it to the city council events.
    /// </summary>
    public event Func<EventData> RepeatCall;

    /// <summary>
    /// Action that is called, when Event is completed
    /// </summary>
    public event Action OnComplete;

    /// <summary>
    /// Called when the event is completed
    /// </summary>
    public void Complete()
    {
        OnComplete?.Invoke();
    }

    /// <summary>
    /// Evaulates predicate, returns true of false depending on if the predicate is accomplished.
    /// </summary>
    /// <param name="current">Parameter for the predicate, value we want to compare to.</param>
    /// <returns>True, if predicate passes, false otherwise</returns>
    public bool Evaluate()
    {
        if (Event.Invoke(startingNumber))
        {
            IsFinished = true;

            return true;
        }

        return false;
    }

    /// <summary>
    /// Repeats repetitive event call, call only when isRepetitive is true, otherwise it will fail with an exception.
    /// </summary>
    /// <returns></returns>
    public EventData Repeat()
    {
        return RepeatCall.Invoke();
    }
}
