﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Class defining all events and full of constants
/// </summary>
public static class CityCouncilEvents
{
    public static readonly Dictionary<string, Func<EventData>> dictIDToEventDataCreator = new Dictionary<string, Func<EventData>>()
    {
        { nameof(TimeEvent), () => TimeEvent.Create() },
        { nameof(BusLineEvent), () => BusLineEvent.Create() },
        { nameof(BusEvent), () => BusEvent.Create() },
        { nameof(SubwayLineEvent), () => SubwayLineEvent.Create() },
        { nameof(SubwayTrainEvent), () => SubwayTrainEvent.Create() },
        { nameof(QuickBusLineBuildingEvent), () => QuickBusLineBuildingEvent.Create() },
    };


    public static class TimeEvent
    {
        public const int checkTimeEventHours = 1;

        public const int timeMultiplier = 4;
        public const int moneyMultiplier = 2;

        internal static int nextGoalTime = 1;
        internal static int nextGoalMoney = 1;

        public static EventData Create()
        {
            EventData data = new EventData()
            {
                id = nameof(TimeEvent),
                title = "Praise from the mayor",
                text = "The mayor likes your patience, keep it up.",
                notificationText = "The mayor likes what you're doing.",
                achievementText = $"Play a game for another {checkTimeEventHours * nextGoalTime} in-game hours.",
                amount = 5000 * nextGoalMoney,
                startingNumber = TimeManager.instance.CurrentDateTime.Ticks,
                isRepetitive = true
            };
            data.Event += Check;
            data.RepeatCall += Create;

            data.OnComplete += () => {           
                nextGoalTime *= timeMultiplier;
                nextGoalMoney *= moneyMultiplier;
            };

			if (nextGoalMoney == 1)
            {
                data.title = "The mayor believes in you";
                data.text = "The mayor himself has persuaded the city coucil to give you some starting money.";
                data.notificationText = "The mayor likes your confidence.";
                data.achievementText = $"Play a game for the first {checkTimeEventHours} in-game hour{( (checkTimeEventHours == 1)? "" : "s" )}.";
            }

            return data;
        }
        public static bool Check(long starting) => new DateTime(TimeManager.instance.CurrentDateTime.Ticks) > new DateTime(starting).AddHours(checkTimeEventHours * nextGoalTime);
    }

    public static class BusLineEvent
    {
        public const int checkBusLinesCount = 2;
        public static EventData Create()
        {
            EventData data = new EventData()
            {
                id = nameof(BusLineEvent),
                title = "First step into the transport life",
                text = "The city council likes your idea of bus lines. They say it looks interesting and they want to help.",
                notificationText = "The city council has decided to help with building bus lines.",
                achievementText = $"Build your first {checkBusLinesCount} bus line{((checkBusLinesCount > 1) ? "s" : "")}.",
                amount = 40000,
                startingNumber = 0
            };
            data.Event += Check;

            return data;
        }
        public static bool Check(long starting) => checkBusLinesCount <= BusLineManager.GetInstance().LinesCount - starting;
    }

    public static class BusEvent
    {
        public const int wantedBusCount = 10;
        public static EventData Create()
        {
            EventData data = new EventData()
            {
                id = nameof(BusEvent),
                title = "There's no city transport without buses",
                text = "The city council wants to invest in your buses.",
                notificationText = "The city council wants to help you with buying buses.",
                achievementText = $"Buy your first {wantedBusCount} bus{((wantedBusCount > 1)? "es":"")}.",
                amount = 50000,
                startingNumber = 0
            };
            data.Event += Check;

            return data;
        }
        public static bool Check(long starting) => wantedBusCount <= BusInstanceManager.GetInstance().BusCount - starting;
    }

    public static class SubwayLineEvent
    {
        public const int wantedSubwayLinesCount = 1;

        public static EventData Create()
        {
            EventData data = new EventData()
            {
                id = nameof(SubwayLineEvent),
                title = "Underground transportation? Like land submarines?",
                text = "´The city council is sceptical about your idea of subways, but they are interested and want to fund further development of the city.",
                notificationText = "The city council likes your subway project.",
                achievementText = $"Build your first {wantedSubwayLinesCount} subway line{((wantedSubwayLinesCount > 1) ? "s" : "")}.",
                amount = 100000,
                startingNumber = 0
            };
            data.Event += Check;

            return data;
        }

        public static bool Check(long starting) => wantedSubwayLinesCount <= SubwayLineManager.GetInstance().LinesCount - starting;
    }

    public static class SubwayTrainEvent
    {
        public const int wantedTrainsCount = 5;
        public static EventData Create()
        {
            EventData data = new EventData()
            {
                id = nameof(SubwayTrainEvent),
                title = "You can't have subway without the trains",
                text = "The city council has decided to help you with buying trains for subways.",
                notificationText = "The city council wants to invest in your subway trains.",
                achievementText = $"Buy your first {wantedTrainsCount} subway train{((wantedTrainsCount > 1) ? "s" : "")}.",
                amount = 50000,
                startingNumber = 0
            };
            data.Event += Check;

            return data;
        }
        public static bool Check(long starting) => wantedTrainsCount <= SubwayInstanceManager.GetInstance().TrainCount - starting;
    }

    public static class QuickBusLineBuildingEvent
    {
        public const int busLines = 3;
        public const int stops = 5;
        public const int minutesTimespan = 30;

        static int existingBusLineCount = 0;
        static List<BusTime> evalutedLines = new List<BusTime>();
        public static EventData Create()
        {
            EventData data = new EventData()
            {
                id = nameof(QuickBusLineBuildingEvent),
                title = "The fastest hands in the city",
                text = "The city council loves your commitment to build a city transportation service as soon as possible.",
                notificationText = "The city council wants to help you find money for buses quickly.",
                achievementText = $"Build {busLines} new bus lines with at least {stops} stops each in {minutesTimespan} minutes or less.",
                amount = 50000,
                startingNumber = 0
            };
            data.Event += Check;

            return data;
        }

        public static bool Check(long starting)
        {
            var lines = BusLineManager.GetInstance().GetLines();

            // Adding new lines
            if (lines.Count > existingBusLineCount)
            {
                for (int i = existingBusLineCount; i < lines.Count; i++)
                {
                    evalutedLines.Add(
                        new BusTime()
                        {
                            creationTime = TimeManager.instance.CurrentDateTime,
                            busLine = lines[i]
                        });
                }

                existingBusLineCount = lines.Count;
            }

            // Deleting old lines
            for (int i = evalutedLines.Count - 1; i >= 0; i--) 
            {
                if (evalutedLines[i].creationTime.AddMinutes(minutesTimespan) < TimeManager.instance.CurrentDateTime)
                {
                    evalutedLines.RemoveAt(i);
                }
            }

            // Evaluating 
            // (we look at all the "new" bus lines (created within last x in-game minutes) and we count all those that have at least y stops and compare it to objective)
            return evalutedLines.Where(b => b.busLine.Path.Count > stops).Count() > busLines;
        }

        private class BusTime
        {
            // Creation time is just approximate from city council point of view, it doesn't have to be accurate.
            public DateTime creationTime;
            public ILine busLine;
        }
    }
}
