﻿using System.Collections.Generic;
using UnityEngine;

public class RoadZone : MonoBehaviour
{
    // The mesh renderers of all the zone markers attached to the road.
    private List<MeshRenderer> zoneMarkers = new List<MeshRenderer>();

    public void InitializeZoneMarkerList()
    {
        foreach (Transform marker in transform.Find("Zone Markers"))
        {
            zoneMarkers.Add(marker.GetComponent<MeshRenderer>());
        }
    }

    /// <summary>
    /// Displays or disables showing the zone markers.
    /// </summary>
    /// <param name="onOrOff"> True <=> on. </param>
    public void ToggleZoneMarkers(bool onOrOff)
    {
        foreach (MeshRenderer marker in zoneMarkers)
        {
            marker.enabled = onOrOff;
        }
    }

    public void SetZoneMarkersColour(Color c)
    {
        foreach (MeshRenderer marker in zoneMarkers)
        {
            marker.material.color = c;
        }
    }

    /// <summary>
    /// Notifies the zone markers that a block has been deleted.
    /// </summary>
    public void UpdateZoneMarkers(GameObject block)
    {
        foreach (MeshRenderer marker in zoneMarkers)
        {
            marker.GetComponent<ZoneMarker>().UpdateZoneMarker(block);
        }
    }
}
