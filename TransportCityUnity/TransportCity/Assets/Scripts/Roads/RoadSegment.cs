﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SegmentType { IRoad, IRoadC, LRoad, TRoad, XRoad, URoad };

/// <summary>
/// A script that is attached to every road. It stores the road's type and can determine
/// which of its colliders is left and which is right.
/// </summary>
public class RoadSegment : MonoBehaviour
{
    public SegmentType type;
    public Collider left;
    public Collider right;
    
    public bool GetColliderSide(Collider c)
    {
        if (c == left)
        {
            return false;
        }
        return true;
    }

    /// <summary>
    /// Find closest waypoint on the road to the given position
    /// </summary>
    /// <param name="position">Position on the road segment</param>
    /// <returns>Closest waypoint to the given position</returns>
    public Waypoint GetClosestWaypointToPosition(Vector3 position)
    {
        float distance = float.MaxValue;
        GameObject closestWaypoint = null;

        foreach (Transform waypoint in transform.Find("CarWaypoints"))
        {
            var currentWaypointDistance = Vector3.Distance(waypoint.position, position);
            if (currentWaypointDistance < distance)
            {
                distance = currentWaypointDistance;
                closestWaypoint = waypoint.gameObject;
            }
        }

        return closestWaypoint?.GetComponent<Waypoint>();
    }
}