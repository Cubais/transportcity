﻿using System.Collections.Generic;
using UnityEngine;

public class Crosswalk : MonoBehaviour
{
    // The waypoints that are to be blocked on the right and left side when the colliders are intersected.
    private Waypoint rightWP, leftWP;

    // Time since the script has last been notified that there is something blocking the left side of the crosswalk.
    public float timeSinceLastLeftNotification = float.MaxValue;

    // Time since the script has last been notified that there is something blocking the right side of the crosswalk.
    public float timeSinceLastRightNotification = float.MaxValue;

    // Lists of vehicles in the right and left trigger colliders of the crosswalk.
    internal List<CarNavigation> vehiclesInRightTrigger = new List<CarNavigation>();
    internal List<CarNavigation> vehiclesInLeftTrigger = new List<CarNavigation>();

    // Time after which the crosswalk is supposed to stop being blocked if it gets no signals from its colliders.
    private float threshold = 0.3f;

    // These booleans determine whether the left and right side of the crosswalk are occupied.
    internal bool leftOccupied = false;
    internal bool rightOccupied = false;

    private void Awake()
    {
        rightWP = transform.GetChild(0).GetChild(0).GetComponent<Waypoint>();
        leftWP = transform.GetChild(0).GetChild(2).GetComponent<Waypoint>();
    }

    // Keeps checking the time since the last notification came from either side of the crosswalk.
    // Once the time exceeds a certain threshold, the side is marked as unoccupied.
    private void Update()
    {
        timeSinceLastLeftNotification += Time.deltaTime;
        timeSinceLastRightNotification += Time.deltaTime;

        if (timeSinceLastRightNotification >= threshold && rightOccupied)
        {
            rightWP.ChangeOccupied(false);
            rightOccupied = false;
            foreach(CarNavigation cn in vehiclesInRightTrigger)
            {
                rightWP.occupiers.Remove(cn);
            }
            vehiclesInRightTrigger.Clear();
        }

        if (timeSinceLastLeftNotification >= threshold && leftOccupied)
        {
            leftWP.ChangeOccupied(false);
            leftOccupied = false;
            foreach (CarNavigation cn in vehiclesInLeftTrigger)
            {
                leftWP.occupiers.Remove(cn);
            }
            vehiclesInLeftTrigger.Clear();
        }
    }

    /// <summary>
    /// Sets both timers to 0.
    /// </summary>
    public void ResetBothTimers()
    {
        if (!rightOccupied)
        {
            rightWP.ChangeOccupied(true);
            rightOccupied = true;
        }

        if (!leftOccupied)
        {
            leftWP.ChangeOccupied(true);
            leftOccupied = true;
        }

        timeSinceLastRightNotification = 0f;
        timeSinceLastLeftNotification = 0f;
    }

    /// <summary>
    /// Sets either the left or the right timer to zero.
    /// </summary>
    /// <param name="rightOrLeft"> true <=> reset the right timer </param>
    public void ResetOneTimer(bool rightOrLeft, CarNavigation blocker)
    {
        if (rightOrLeft)
        {
            timeSinceLastRightNotification = 0f;

            if (!rightOccupied)
            {
                rightWP.ChangeOccupied(true);
                rightOccupied = true;
            }

            if (!vehiclesInRightTrigger.Contains(blocker))
            {
                vehiclesInRightTrigger.Add(blocker);
                rightWP.occupiers.Add(blocker);
            }
        }
        else
        {
            timeSinceLastLeftNotification = 0f;

            if (!leftOccupied)
            {
                leftWP.ChangeOccupied(true);
                leftOccupied = true;
            }

            if (!vehiclesInLeftTrigger.Contains(blocker))
            {
                vehiclesInLeftTrigger.Add(blocker);
                leftWP.occupiers.Add(blocker);
            }
        }
    }
}
