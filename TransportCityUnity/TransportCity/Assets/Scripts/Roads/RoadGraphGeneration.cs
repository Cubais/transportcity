﻿using System;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR

using UnityEditor;

#endif

using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.AI;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

using static Constants;
using System.Linq;

public class RoadGraphGeneration : MonoBehaviour
{
    /// <summary>
    /// Roads are not generated above this limit
    /// </summary>
    public float heightLimit = Constants.heightLimit;

    /// <summary>
    /// Number of blocks in the city
    /// </summary>
    public Vector2Int blockCount = new Vector2Int(15, 15);

    /// <summary>
    /// Increases range around roads and city
    /// </summary>
    public int terrainFlatteningExtension = 32;

    /// <summary>
    /// Minimum size of a rectangle made from roads
    /// </summary>
    public int minRect = minRectangleSize;

    /// <summary>
    /// Maximum size of a rectangle made from roads
    /// </summary>
    public int maxRect = maxRectangleSize;

    public GameObject loadingScreen;

    public static RoadGraphGeneration instance;

    [SerializeField]
    internal GameObject roadParentObject;

    private const string folderPathRoadPrefabs = "Prefabs/Roads";
    private const int roadSize = roadPrefabSize;
    private const int blockSize = 3;

    private System.Random rand;
    private Terrain terrain;

    internal Vector2Int terrainSize;
    internal Vector2Int terrainCenter;
    internal Vector2Int cityCenter;
    internal Vector2Int totalCitySize;
    public Vector2Int downLeftCorner;
    public Vector2Int southWestCorner;

    public DistrictData centerDistrict;
    public List<DistrictData> allDistricts;
    public DistrictData[,] districts;

    internal float cityBaseHeight = 0;

    ZoneController zoneController;
    BuildingCreator buildingCreator;
    BuildingManager buildingManager;

    public GameObject[,] supermodels;

    // A list of all the waypoints that are diconnected from roads. Note that one entry in this list
    // is a parent transform of multiple waypoints.
    internal List<Transform> isolatedWaypoints = new List<Transform>();

    void Awake()
    {
        instance = this;

        // Assert
        Debug.Assert(blockCount.x >= 0);
        Debug.Assert(blockCount.y >= 0);

        Debug.Assert(heightLimit >= 0);
        Debug.Assert(terrainFlatteningExtension >= 0);

        // Init
        rand = new System.Random();
        terrain = Terrain.activeTerrain;

        terrainSize = new Vector2Int((int)terrain.terrainData.size.x, (int)terrain.terrainData.size.z);
        terrainCenter = terrainSize / 2;

        centerDistrict = new DistrictData(blockCount, blockSize, DistrictState.There);

        totalCitySize = centerDistrict.citySize * roadSize;

        buildingManager = GetComponent<BuildingManager>();
        zoneController = GetComponent<ZoneController>();
        buildingCreator = GetComponent<BuildingCreator>();

        allDistricts = new List<DistrictData>
        {
            centerDistrict
        };

        districts = new DistrictData[districtSizeRatio, districtSizeRatio];
        for (int x = 0; x < districtSizeRatio; x++)
        {
            for (int y = 0; y < districtSizeRatio; y++)
            {
                districts[x, y] = centerDistrict;
            }
        }

        // Warning
        if (terrain.terrainData.size.x + 1 != terrain.terrainData.heightmapResolution)
        {
            Debug.LogWarning("Terrain size in x axis does not match heightmap resolution");
        }
        if (terrain.terrainData.size.z + 1 != terrain.terrainData.heightmapResolution)
        {
            Debug.LogWarning("Terrain size in z axis does not match heightmap resolution");
        }
    }

    void Start()
    {
        SetCityLocation();

        CreateWholeCity(centerDistrict);

        Debug.Log("Navmesh Baking");
        BakeRoadNavmesh();

        SetCameraPosition();
        Debug.Log("All Setup");

        supermodels = centerDistrict.models;
    }

    void Update()
    {
        /*
        if (Input.GetKeyDown(KeyCode.H))
        {
            StartCoroutine(AddDistrict());
        }
		*/

        if (PeopleManager.instance.PeopleCount / (float)BuildingManager.instance.totalLivingCapacity > residentsCapacityDistrictThreshold + newDistrictThreshold * (allDistricts.Count - 1))
        {
            StartCoroutine(AddDistrict());
        }
    }

    /// <summary>
    /// Creates the whole city - inits district data, generates roads, junctions, crosswalks, instantiates correct prefabs and flattens whole terrain under.
    /// </summary>
    /// <param name="district">New district about to be created</param>
    private void CreateWholeCity(DistrictData district)
    {
        // We need to make sure that the new, generated roadmap has at least one road 
        // on both the first and last row and column in roads array before building roads there
        bool okay = false;
        while (!okay)
        {
            district.DistrictArraysInit();

            GenereateRoadGraph(district);
            PostProcessRoads(district);

            okay = IsDistrictOk(district);
        }

        SetRoadModels(district);
        FlattenTerrainUnderWholeCity(district);
    }
    
    /// <summary>
    /// Bake NavMesh on roads at runtime
    /// </summary>
    internal void BakeRoadNavmesh()
    {
        // Disable terrain to not baking navmesh on the terrain
        terrain.enabled = false;

        // Bake NavMesh for each road
        for (int i = 0; i < roadParentObject.transform.childCount; i++)
        {
            var road = roadParentObject.transform.GetChild(i).gameObject;
            var navMeshSurface = road.GetComponent<NavMeshSurface>();

            if (navMeshSurface)
            {
                navMeshSurface.BuildNavMesh();
                break;
            }
        }

        // Enable terrain after baking navmesh on roads
        terrain.enabled = true;
    }

    private void SetCityLocation()
    {
        const int sampleSpacing = 128;

        // Fill sample map with heights from terrain heightmap
        var sampleMapSize = terrainSize / sampleSpacing;
        var sampleMap = new float[sampleMapSize.x, sampleMapSize.y];
        for (int x = 0; x < sampleMapSize.x; x++)
        {
            for (int y = 0; y < sampleMapSize.y; y++)
            {
                var pos = new Vector3(x, 0, y) * sampleSpacing;

                sampleMap[x, y] = terrain.SampleHeight(pos);
            }
        }

        // Offset, so the city is not so close to the edge of the terrain
        var offset = ElementwiseMax(totalCitySize / 2 + Vector2Int.one * 10, terrainSize / 3) / sampleSpacing;
        var radius = totalCitySize / sampleSpacing;

        // Maps for different parameters of a part of the terrain with center at equivalent coordinates and the radius (set above)
        var paramMapSize = sampleMapSize - 2 * offset;
        var deltaMap = new float[paramMapSize.x, paramMapSize.y];
        var minMap = new float[paramMapSize.x, paramMapSize.y];
        var maxMap = new float[paramMapSize.x, paramMapSize.y];
        var averageMap = new float[paramMapSize.x, paramMapSize.y];

        var bestPosInSampleMap = Vector2Int.zero;
        float smallestDelta = float.MaxValue;

        for (int x = 0; x < sampleMapSize.x - 2 * offset.x; x++)
        {
            for (int y = 0; y < sampleMapSize.y - 2 * offset.y; y++)
            {
                var posInSampleMap = new Vector2Int(x, y) + offset;

                float min = float.MaxValue;
                float max = float.MinValue;
                float sum = 0;

                for (int dx = -radius.x + 1; dx < radius.x; dx++)
                {
                    for (int dy = -radius.y + 1; dy < radius.y; dy++)
                    {
                        int fx = posInSampleMap.x + dx;
                        int fy = posInSampleMap.y + dy;

                        if (fx < 0 || fx >= sampleMapSize.x || fy < 0 || fy >= sampleMapSize.y)
                        {
                            continue;
                        }

                        float val = sampleMap[fx, fy];
                        sum += val;
                        if (val < min)
                        {
                            min = val;
                        }
                        if (val > max)
                        {
                            max = val;
                        }
                    }
                }

                float delta = max - min;
                float average = sum / (4 * radius.x * radius.y);

                deltaMap[x, y] = delta;
                minMap[x, y] = min;
                maxMap[x, y] = max;
                averageMap[x, y] = average;

                if (delta < smallestDelta)
                {
                    smallestDelta = delta;
                    bestPosInSampleMap = posInSampleMap;
                }
            }
        }

        // Set city center and base height to the best found position
        cityCenter = bestPosInSampleMap * sampleSpacing;
        var posInParamMap = bestPosInSampleMap - offset;
        cityBaseHeight = minMap[posInParamMap.x, posInParamMap.y];

        southWestCorner = cityCenter - totalCitySize / 2 + Vector2Int.one * roadSize / 2;
        downLeftCorner = southWestCorner;
        centerDistrict.SetCorner(southWestCorner);
    }

    /// <summary>
    /// Generates roads and then removes roads, where terrain is higher than maximum allowed to make roads and after this, it removes isolated roads in certain district.
    /// </summary>
    private void GenereateRoadGraph(DistrictData district)
    {
        GenerateRoads(district);

        RemoveRoadsAboveLimit(district);
        RemoveIsolatedRoads(district);
    }

    /// <summary>
    /// Finds junctions and generates crosswalks between every two of them (for certain district).
    /// </summary>
    private void PostProcessRoads(DistrictData district)
    {
        FindJunctions(district);
        GenerateCrosswalks(district);
    }

    /// <summary>
    /// Generates roads into the bool roads array of certain district.
    /// </summary>
    private void GenerateRoads(DistrictData district)
    {
        var roads = district.roads;
        var blockCount = district.blockCount;
        var blockSize = district.blockSize;

        // Iteratively draw rectangles with roads
        var rectangleCount = blockCount.x * blockCount.y / 2;
        for (int i = 0; i < rectangleCount; i++)
        {
            var sizeX = rand.Next(minRect, maxRect);
            var sizeY = rand.Next(minRect, maxRect);

            var posXfrom = rand.Next(blockCount.x - sizeX + 1) * blockSize;
            var posYfrom = rand.Next(blockCount.y - sizeY + 1) * blockSize;

            var dx = sizeX * blockSize;
            var dy = sizeY * blockSize;

            // Remove all roads inside the rectangle
            for (int x = 1; x < dx; x++)
            {
                for (int y = 1; y < dy; y++)
                {
                    roads[posXfrom + x, posYfrom + y] = false;
                }
            }

            // Draw outline
            for (int x = 0; x <= dx; x++)
            {
                roads[posXfrom + x, posYfrom] = true;
            }
            for (int x = 0; x <= dx; x++)
            {
                roads[posXfrom + x, posYfrom + dy] = true;
            }
            for (int y = 0; y <= dy; y++)
            {
                roads[posXfrom, posYfrom + y] = true;
            }
            for (int y = 0; y <= dy; y++)
            {
                roads[posXfrom + dx, posYfrom + y] = true;
            }
        }
    }

    /// <summary>
    /// Removes roads which would be generated into the terrain above maximum height.
    /// </summary>
    private void RemoveRoadsAboveLimit(DistrictData district)
    {
        var citySize = district.citySize;
        var roads = district.roads;
        var downLeftCorner = district.downLeftCorner;

        // Foreach road
        for (int x = 0; x < citySize.x; x++)
        {
            for (int y = 0; y < citySize.y; y++)
            {
                if (roads[x, y])
                {
                    var pos = new Vector2Int(x, y);
                    var positionInWorld = ConvertTo3DAtCityHeight(downLeftCorner + pos * roadSize);

                    // Preserve only when under heightLimit
                    roads[x, y] = terrain.SampleHeight(positionInWorld) < cityBaseHeight + heightLimit;
                }
            }
        }
    }

    /// <summary>
    /// Uses DFS to find isolated roads and then removes them in this district
    /// </summary>
    private void RemoveIsolatedRoads(DistrictData district)
    {
        var citySize = district.citySize;
        var roads = district.roads;

        var right = new Vector2Int(1, 0);
        var up = new Vector2Int(0, 1);
        var left = new Vector2Int(-1, 0);
        var down = new Vector2Int(0, -1);

        // Spiral search to select a road from the city center
        var pos = new Vector2Int(citySize.x, citySize.y) / 2;
        var delta = new Vector2Int[4] { right, up, left, down };
        int shorterCitySide = Math.Min(citySize.x, citySize.y);
        bool found = false;
        for (int n = 1; n < shorterCitySide && !found; n++)
        {
            for (int i = 0; i < n; i++)
            {
                if (roads[pos.x, pos.y])
                {
                    found = true;
                    break;
                }
                pos += delta[n % delta.Length];
            }
        }

        if (!found)
        {
            Debug.LogWarning("No road found with spiral search, potential isolated roads were not removed");
            return;
        }

        // Preserve only roads reachable from the selected road
        var roadsNew = new bool[citySize.x, citySize.y];

        var stack = new Stack<Vector2Int>();
        stack.Push(pos);

        /* Roads in both arrays 'roads' and 'roadsNew', are expanded and processed
         * Roads in only array 'roads' that
         *    - are in stack, are waiting to be expanded
         *    - are not in stack, are waiting to be discorevered (and they many not be)
         */

        // DFS
        while (stack.Count > 0)
        {
            pos = stack.Pop();

            if (roadsNew[pos.x, pos.y]) // already expanded
            {
                continue;
            }
            else
            {
                roadsNew[pos.x, pos.y] = true;
            }

            // Left
            if (pos.x > 0)
            {
                var posNew = pos + left;
                if (roads[posNew.x, posNew.y] && !roadsNew[posNew.x, posNew.y])
                {
                    stack.Push(posNew);
                }
            }
            // Down
            if (pos.y > 0)
            {
                var posNew = pos + down;
                if (roads[posNew.x, posNew.y] && !roadsNew[posNew.x, posNew.y])
                {
                    stack.Push(posNew);
                }
            }
            // Right
            if (pos.x < roads.GetLength(0) - 1)
            {
                var posNew = pos + right;
                if (roads[posNew.x, posNew.y] && !roadsNew[posNew.x, posNew.y])
                {
                    stack.Push(posNew);
                }
            }
            // Up
            if (pos.y < roads.GetLength(1) - 1)
            {
                var posNew = pos + up;
                if (roads[posNew.x, posNew.y] && !roadsNew[posNew.x, posNew.y])
                {
                    stack.Push(posNew);
                }
            }
        }

        roads = roadsNew;
    }

    /// <summary>
    /// Finds every junction/crossroad in the city and fills junction array for a certain district.
    /// </summary>
    private void FindJunctions(DistrictData district)
    {
        var citySize = district.citySize;
        var roads = district.roads;
        var junctions = district.junctions;

        // Foreach road
        for (int x = 0; x < citySize.x; x++)
        {
            for (int y = 0; y < citySize.y; y++)
            {
                if (roads[x, y])
                {
                    bool roadLeft = x > 0 && roads[x - 1, y];
                    bool roadDown = y > 0 && roads[x, y - 1];
                    bool roadRight = x < citySize.x - 1 && roads[x + 1, y];
                    bool roadUp = y < citySize.y - 1 && roads[x, y + 1];

                    junctions[x, y] = !( // NOT
                           roadLeft && roadRight && !roadUp && !roadDown     // Horizontal I
                        || !roadLeft && !roadRight && roadUp && roadDown);   // Vertical I
                }
            }
        }
    }

    /// <summary>
    /// Generates crosswalks between every two junctions of a certain district (LRoads are counted as junctions)
    /// </summary>
    /// <param name="district"></param>
    private void GenerateCrosswalks(DistrictData district)
    {
        var citySize = district.citySize;
        var roads = district.roads;
        var junctions = district.junctions;
        var crosswalks = district.crosswalks;

        // Foreach junction
        for (int x = 0; x < citySize.x; x++)
        {
            for (int y = 0; y < citySize.y; y++)
            {
                if (junctions[x, y])
                {
                    /* Search how long the road line is to the left an up
                     * Pick one of the roads in the line and make it crosswalk
                     */

                    // Left
                    int n = 1;
                    while (x - n > 0 && roads[x - n, y] && !junctions[x - n, y])
                    {
                        n += 1;
                    }
                    if (n > 1)
                    {
                        int r = rand.Next(1, n);
                        crosswalks[x - r, y] = true;
                    }

                    // Up
                    n = 1;
                    while (y - n > 0 && roads[x, y - n] && !junctions[x, y - n])
                    {
                        n += 1;
                    }
                    if (n > 1)
                    {
                        int r = rand.Next(1, n);
                        crosswalks[x, y - r] = true;
                    }
                }
            }
        }
    }

    /// <summary>
    /// Instantiates all road models from roads array in a certain district
    /// </summary>
    internal void SetRoadModels(DistrictData district)
    {
        var citySize = district.citySize;
        var roads = district.roads;
        var models = district.models;
        var downLeftCorner = district.downLeftCorner;

        // Foreach road
        for (int x = 0; x < citySize.x; x++)
        {
            for (int y = 0; y < citySize.y; y++)
            {
                if (roads[x, y] && models[x, y] == null)
                {
                    var pos = new Vector2Int(x, y);
                    var positionInWorld = ConvertTo3DAtCityHeight(downLeftCorner + pos * roadSize);

                    SetRoadModel(x, y, positionInWorld, district);
                }
            }
        }
    }

    /// <summary>
    /// Instantiates one road on a specific coordinates and for specific district
    /// </summary>
    /// <param name="x">X coordinate of roads array in its district</param>
    /// <param name="y">Y coordinate of roads array in its district</param>
    /// <param name="positionInWorld">World position</param>
    /// <param name="district">District the road belongs to</param>
    private void SetRoadModel(int x, int y, Vector3 positionInWorld, DistrictData district)
    {
        var roads = district.roads;
        var crosswalks = district.crosswalks;
        var models = district.models;

        bool roadLeft = x > 0 && roads[x - 1, y];
        bool roadDown = y > 0 && roads[x, y - 1];
        bool roadRight = x < roads.GetLength(0) - 1 && roads[x + 1, y];
        bool roadUp = y < roads.GetLength(1) - 1 && roads[x, y + 1];

        int count = (roadUp ? 1 : 0)
                  + (roadDown ? 1 : 0)
                  + (roadRight ? 1 : 0)
                  + (roadLeft ? 1 : 0);


        GameObject model = null;

        if (count == 4) // XRoad
        {
            model = Instantiate(GetRoadPrefab(Constants.XRoadName), positionInWorld, Quaternion.identity, roadParentObject.transform);
        }
        else if (count == 3) // TRoad
        {
            if (!roadDown)
            {
                model = Instantiate(GetRoadPrefab(Constants.TRoadName), positionInWorld, Quaternion.Euler(0, 90, 0), roadParentObject.transform);
            }
            else if (!roadUp)
            {
                model = Instantiate(GetRoadPrefab(Constants.TRoadName), positionInWorld, Quaternion.Euler(0, -90, 0), roadParentObject.transform);
            }
            else if (!roadLeft)
            {
                model = Instantiate(GetRoadPrefab(Constants.TRoadName), positionInWorld, Quaternion.Euler(0, 180, 0), roadParentObject.transform);
            }
            else if (!roadRight)
            {
                model = Instantiate(GetRoadPrefab(Constants.TRoadName), positionInWorld, Quaternion.identity, roadParentObject.transform);
            }
        }
        else if (count == 2) // LRoad or IRoad(C)
        {
            if (roadDown)
            {
                if (roadUp)
                {
                    model = Instantiate(GetRoadPrefab(crosswalks[x, y] ? Constants.IRoadCName : Constants.IRoadName), positionInWorld, Quaternion.identity, roadParentObject.transform);
                }
                else if (roadLeft)
                {
                    model = Instantiate(GetRoadPrefab(Constants.LRoadName), positionInWorld, Quaternion.Euler(0, 180, 0), roadParentObject.transform);
                }
                else if (roadRight)
                {
                    model = Instantiate(GetRoadPrefab(Constants.LRoadName), positionInWorld, Quaternion.Euler(0, 90, 0), roadParentObject.transform);
                }
            }
            else if (roadLeft)
            {
                if (roadRight)
                {
                    model = Instantiate(GetRoadPrefab(crosswalks[x, y] ? Constants.IRoadCName : Constants.IRoadName), positionInWorld, Quaternion.Euler(0, 90, 0), roadParentObject.transform);
                }
                else if (roadUp)
                {
                    model = Instantiate(GetRoadPrefab(Constants.LRoadName), positionInWorld, Quaternion.Euler(0, -90, 0), roadParentObject.transform);
                }
            }
            else if (roadUp && roadRight)
            {
                model = Instantiate(GetRoadPrefab(Constants.LRoadName), positionInWorld, Quaternion.identity, roadParentObject.transform);
            }
        }
        else if (count == 1) // URoad
        {
            if (roadRight)
            {
                model = Instantiate(GetRoadPrefab(Constants.URoadName), positionInWorld, Quaternion.Euler(0, -90, 0), roadParentObject.transform);
            }
            else if (roadLeft)
            {
                model = Instantiate(GetRoadPrefab(Constants.URoadName), positionInWorld, Quaternion.Euler(0, 90, 0), roadParentObject.transform);
            }
            else if (roadDown)
            {
                model = Instantiate(GetRoadPrefab(Constants.URoadName), positionInWorld, Quaternion.identity, roadParentObject.transform);
            }
            else if (roadUp)
            {
                model = Instantiate(GetRoadPrefab(Constants.URoadName), positionInWorld, Quaternion.Euler(0, 180, 0), roadParentObject.transform);
            }
        }

        models[x, y] = model;
    }

    /// <summary>
    /// Flattens the terrain in a certain distance around the city
    /// </summary>
    /// <param name="district"></param>
    private void FlattenTerrainUnderWholeCity(DistrictData district)
    {
        var citySize = district.citySize;
        var roads = district.roads;
        var downLeftCorner = district.downLeftCorner;

        // Search outer most rectangle for non-road positions
        var stack = new Stack<Vector2Int>();
        bool[,] cityOutside = new bool[citySize.x, citySize.y];

        for (int x = 0; x < citySize.x; x++)
        {
            if (!roads[x, 0])
            {
                stack.Push(new Vector2Int(x, 0));
                cityOutside[x, 0] = true;
            }
        }
        for (int y = 1; y < citySize.y - 1; y++)
        {
            if (!roads[0, y])
            {
                stack.Push(new Vector2Int(0, y));
                cityOutside[0, y] = true;
            }
        }
        for (int x = 0; x < citySize.x; x++)
        {
            if (!roads[x, citySize.y - 1])
            {
                stack.Push(new Vector2Int(x, citySize.y - 1));
                cityOutside[x, citySize.y - 1] = true;
            }
        }
        for (int y = 1; y < citySize.y - 1; y++)
        {
            if (!roads[citySize.x - 1, y])
            {
                stack.Push(new Vector2Int(citySize.x - 1, y));
                cityOutside[citySize.x - 1, y] = true;
            }
        }

        // Fill outside using DFS
        SpreadOutsideMarks(cityOutside, stack, district);

        // Search for hills inside the city
        for (int x = 0; x < citySize.x; x++)
        {
            for (int y = 0; y < citySize.y; y++)
            {
                if (!cityOutside[x, y]) // Inside
                {
                    var pos = new Vector2Int(x, y);
                    var positionInWorld = ConvertTo3DAtCityHeight(downLeftCorner + pos * roadSize);

                    if (terrain.SampleHeight(positionInWorld) > cityBaseHeight + heightLimit)
                    {
                        stack.Push(new Vector2Int(citySize.x - 1, y));
                        cityOutside[citySize.x - 1, y] = true;
                    }
                }
            }
        }

        // Again fill outside using DFS
        SpreadOutsideMarks(cityOutside, stack, district);


        // Find Outline
        bool[,] cityOutline = new bool[citySize.x, citySize.y];
        for (int x = 0; x < citySize.x; x++)
        {
            for (int y = 0; y < citySize.y; y++)
            {
                if (!cityOutside[x, y]) // Inside
                {
                    cityOutline[x, y] =
                        x == 0 || cityOutside[x - 1, y]
                     || y == 0 || cityOutside[x, y - 1]
                     || x == citySize.x - 1 || cityOutside[x + 1, y]
                     || y == citySize.y - 1 || cityOutside[x, y + 1];
                }
            }
        }

        // Clone heightmap
        var heightmap = terrain.terrainData.GetHeights(0, 0, terrain.terrainData.heightmapResolution, terrain.terrainData.heightmapResolution);

        // Foreach road inside
        for (int x = 0; x < citySize.x; x++)
        {
            for (int y = 0; y < citySize.y; y++)
            {
                if (!cityOutside[x, y]) // Inside
                {
                    var pos = new Vector2Int(x, y);
                    var positionInWorld = ConvertTo3DAtCityHeight(downLeftCorner + pos * roadSize);

                    FlattenTerrainUnderRoad(heightmap, positionInWorld, cityOutline[x, y] ? terrainFlatteningExtension : 0);
                }
            }
        }

        // Write heightmap
        terrain.terrainData.SetHeights(0, 0, heightmap);
    }

    internal void DestroyAllRoads()
    {
        foreach (var district in allDistricts)
        {
            var citySize = district.citySize;
            var models = district.models;

            // Foreach road
            for (int x = 0; x < citySize.x; x++)
            {
                for (int y = 0; y < citySize.y; y++)
                {
                    if (models[x, y] != null)
                    {
                        Destroy(models[x, y]);
                        models[x, y] = null;
                    }
                }
            }
        }
    }

    private void SpreadOutsideMarks(bool[,] cityOutside, Stack<Vector2Int> stack, DistrictData district)
    {
        while (stack.Count > 0)
        {
            var pos = stack.Pop();

            var dps = new Vector2Int[] { Vector2Int.left, Vector2Int.up, Vector2Int.down, Vector2Int.right };

            foreach (var dp in dps)
            {
                var p = pos + dp;
                if (p.x >= 0 && p.y >= 0 && p.x < district.citySize.x && p.y < district.citySize.y)
                {
                    if (!district.roads[p.x, p.y] && !cityOutside[p.x, p.y])
                    {
                        cityOutside[p.x, p.y] = true;
                        stack.Push(new Vector2Int(p.x, p.y));
                    }
                }
            }
        }
    }

    /// <summary>
    /// Flattens terrain in a certain distance around a road
    /// </summary>
    /// <param name="heightmap">Terrain heightmap</param>
    /// <param name="positionInWorld">World position</param>
    /// <param name="extension">How much around each road do we want to flatten the heightmap</param>
    private void FlattenTerrainUnderRoad(float[,] heightmap, Vector3 positionInWorld, int extension)
    {
        float height = positionInWorld.y / terrain.terrainData.size.y;

        // Translate terrain position to heightmap position
        int posXfrom = (int)Math.Floor((positionInWorld.x - roadSize / 2f - extension) / terrain.terrainData.size.x * terrain.terrainData.heightmapResolution);
        int posZfrom = (int)Math.Floor((positionInWorld.z - roadSize / 2f - extension) / terrain.terrainData.size.z * terrain.terrainData.heightmapResolution);

        int posXto = (int)Math.Floor((positionInWorld.x + roadSize / 2f + extension) / terrain.terrainData.size.x * terrain.terrainData.heightmapResolution);
        int posZto = (int)Math.Floor((positionInWorld.z + roadSize / 2f + extension) / terrain.terrainData.size.z * terrain.terrainData.heightmapResolution);

        for (int x = posXfrom; x <= posXto; x++)
        {
            for (int z = posZfrom; z <= posZto; z++)
            {
                if (x < 0 || x > heightmap.GetLength(0) || z < 0 || z > heightmap.GetLength(1))
                {
                    //Debug.Log($"{posXfrom}-{posXto},{posZfrom}-{posZto}. heightmap[{z},{x}] from {heightmap.GetLength(0)}, {heightmap.GetLength(1)}");
                    continue;
                }

                heightmap[z, x] = height;
            }
        }
    }

    private Vector2Int ElementwiseMax(Vector2Int a, Vector2Int b)
    {
        return new Vector2Int(Math.Max(a.x, b.x), Math.Max(a.y, b.y));
    }

    /// <summary>
    /// Adds base city height as a third dimension to make a correct world position of the city (ground is at this world position).
    /// </summary>
    /// <param name="vector">World position of x and z coordinates</param>
    /// <returns>World position with correct y axis</returns>
    public Vector3 ConvertTo3DAtCityHeight(Vector2Int vector)
    {
        return new Vector3(vector.x, cityBaseHeight, vector.y);
    }

    private static GameObject GetRoadPrefab(string roadName)
    {
        return Resources.Load<GameObject>($"{folderPathRoadPrefabs}/{roadName}");
    }

    /// <summary>
    /// Positions the camera above the first child of the Road object, effectively placing it in a corner
    /// of the roadmap.
    /// </summary>
    private void SetCameraPosition()
    {
        Vector3 pos = roadParentObject.transform.GetChild(0).transform.position;
        pos.y += 100;
        Camera.main.transform.position = pos;
    }

    /// <summary>
    /// Adds new district to the city
    /// </summary>
    internal IEnumerator AddDistrict()
    {
        SaveManager.numberOfBlockers += 1;
        loadingScreen.SetActive(true);

        // Pause the game so that the collider computations don't get messed up.
        TimeManager.instance.PauseGame();

        var stopwatch = new System.Diagnostics.Stopwatch();
        stopwatch.Start();

        var newBlockCount = new Vector2Int((int)((blockCount.x / (float)districtSizeRatio) - 1), (int)((blockCount.y / (float)districtSizeRatio) - 1));
        var newDistrict = new DistrictData(newBlockCount, blockSize, DistrictState.There);
        allDistricts.Add(newDistrict);

        yield return new WaitForEndOfFrame();

        // Choose new position for the district and orientation from main district
        (Vector2Int downLeftCorner, List<CardinalDirection> directions) = ChooseNewDistrictPosition(newDistrict);

        newDistrict.directionFromMainDistrict = directions[0];
        newDistrict.SetCorner(downLeftCorner);

        // Generate the district
        CreateWholeCity(newDistrict);

        AddToSupermodels(newDistrict);

        var one = stopwatch.Elapsed;

        // At last, connect district to main district
        for (int i = 0; i < directions.Count; i++)
        {
            ConnectDistricts(newDistrict, directions[i]);
        }

        zoneController.UpdateZoneMap(supermodels);
        buildingCreator.UpdateBuildingMap();
        buildingManager.BuildNewBuildings();

        CalculateCityParameters();

        var two = stopwatch.Elapsed;

        BakeRoadNavmesh();

        stopwatch.Stop();

        Debug.Log($"Times: after district - {one}, after buildings - {two}, after navmesh - {stopwatch.Elapsed}");

        yield return new WaitForEndOfFrame();

        // Make all the cars recompute their paths so that they expect the new intersections.
        foreach (CarNavigation cn in CarNavigation.activeCars)
        {
            cn.RecomputePath();
        }

        yield return new WaitForEndOfFrame();

        TimeManager.instance.UnpauseGame();

		loadingScreen.SetActive(false);
        SaveManager.numberOfBlockers -= 1;        
    }

    /// <summary>
    /// Chooses new position for the district, returns neighbouring districts.
    /// </summary>
    /// <param name="district">Initial data of the district</param>
    /// <returns>World position of the new district and list of neigbouring districts to connect to</returns>
    private (Vector2Int, List<CardinalDirection>) ChooseNewDistrictPosition(DistrictData district)
    {
        // Clone heightmap
        var heightmap = terrain.terrainData.GetHeights(0, 0, terrain.terrainData.heightmapResolution, terrain.terrainData.heightmapResolution);

        var downLeftCorner = new Vector2Int(0, 0);

        List<DistrictData> neighbours = new List<DistrictData>();
        List<CardinalDirection> neighbourDirections = new List<CardinalDirection>();

        // Is an array full? (Do we want to make it bigger?)
        bool districtArrayFull = districts.Cast<DistrictData>().All(x => x != null);
        if (districtArrayFull)
        {
            DistrictData[,] newDistr = districts;
            districts = new DistrictData[newDistr.GetLength(0) + 2, newDistr.GetLength(1) + 2];

            for (int i = 0; i < newDistr.GetLength(0); i++)
            {
                for (int j = 0; j < newDistr.GetLength(1); j++)
                {
                    districts[i + 1, j + 1] = newDistr[i, j];
                }
            }

            MakeMapBigger();
        }

        // Is an array full except corners?
        bool corners = false;

        bool onlyCornersLeft = districts.Cast<DistrictData>()
            .Select((x, i) => new { i, x })
            .Where(x => x.x == null)
            .All(x => x.i == 0 || x.i == districts.GetLength(0) * districts.GetLength(1) - 1 || x.i == districts.GetLength(0) - 1 || x.i == (districts.GetLength(0) - 1) * districts.GetLength(1));

        if (onlyCornersLeft)
        {
            // Only corners left
            corners = true;
        }

        // Choose if corner
        if (corners)
        {
            downLeftCorner = ChooseCorner(district, neighbourDirections, heightmap);
        }
        else
        {
            downLeftCorner = ChooseClassicDistrictPosition(district, neighbourDirections, heightmap);
        }

        return (downLeftCorner, neighbourDirections);
    }

    private void CalculateCityParameters()
    {
        var downLeftCorner = new Vector2Int(int.MaxValue, int.MaxValue);
        var upRightCorner = new Vector2Int(int.MinValue, int.MinValue);

        foreach (var district in allDistricts)
        {
            downLeftCorner = Vector2Int.Min(downLeftCorner, district.downLeftCorner);
            upRightCorner = Vector2Int.Max(upRightCorner, district.downLeftCorner + district.citySize * roadSize);
        }

        this.downLeftCorner = downLeftCorner;
        totalCitySize = upRightCorner - downLeftCorner;
    }

    /// <summary>
    /// When new position should be corner, it chooses random one.
    /// </summary>
    private Vector2Int ChooseCorner(DistrictData district, List<CardinalDirection> neighbourDirections, float[,] heightmap)
    {
        var downLeftCorner = new Vector2Int(0, 0);

        int x = 0;
        int y = 0;
        bool okay = false;

        CardinalDirection northsouth = CardinalDirection.None;
        CardinalDirection eastwest = CardinalDirection.None;

        while (!okay)
        {
            switch (rand.Next(4))
            {
                case 0:
                    x = 0;
                    y = 0;
                    northsouth = CardinalDirection.South;
                    eastwest = CardinalDirection.West;
                    downLeftCorner = southWestCorner +
                        new Vector2Int(
                            x * (int)(centerDistrict.citySize.x / districtSizeRatio) * roadPrefabSize,
                            y * (int)(centerDistrict.citySize.y / districtSizeRatio) * roadPrefabSize
                            );
                    break;
                case 1:
                    x = 0;
                    y = districts.GetLength(1) - 1;
                    northsouth = CardinalDirection.South;
                    eastwest = CardinalDirection.East;
                    downLeftCorner = southWestCorner +
                        new Vector2Int(
                            x * (int)(centerDistrict.citySize.x / districtSizeRatio) * roadPrefabSize,
                            (y * (int)(centerDistrict.citySize.y / districtSizeRatio) + districtDistance * blockSize) * roadPrefabSize
                            );
                    break;
                case 2:
                    x = districts.GetLength(0) - 1;
                    y = 0;
                    northsouth = CardinalDirection.North;
                    eastwest = CardinalDirection.West;
                    downLeftCorner = southWestCorner +
                        new Vector2Int(
                            (x * (int)(centerDistrict.citySize.x / districtSizeRatio) + districtDistance * blockSize) * roadPrefabSize,
                            y * (int)(centerDistrict.citySize.y / districtSizeRatio) * roadPrefabSize
                            );
                    break;
                case 3:
                    x = districts.GetLength(0) - 1;
                    y = districts.GetLength(1) - 1;
                    northsouth = CardinalDirection.North;
                    eastwest = CardinalDirection.East;
                    downLeftCorner = southWestCorner +
                        new Vector2Int(
                            (x * (int)(centerDistrict.citySize.x / districtSizeRatio) + districtDistance * blockSize) * roadPrefabSize,
                            (y * (int)(centerDistrict.citySize.y / districtSizeRatio) + districtDistance * blockSize) * roadPrefabSize
                            );
                    break;
            }

            okay = AreDownLeftCoordsOkay(downLeftCorner, heightmap, district);
            if (okay)
            {
                if (districts[x, y] != null)
                {
                    okay = false;
                }
                else if (AreBothNotThere(x, y, northsouth, eastwest))
                {
                    districts[x, y] = new DistrictData(new Vector2Int(0, 0), 0, DistrictState.NotThere);
                    okay = false;

                    Debug.LogError($"Attempting to be not there.");
                }
            }
            else
            {
                Debug.Log($"AreDownLeftCoordsOkay said no :(");
                districts[x, y] = new DistrictData(new Vector2Int(0, 0), 0, DistrictState.NotThere);
            }
        }

        neighbourDirections.Add(northsouth);
        neighbourDirections.Add(eastwest);
        districts[x, y] = district;

        return downLeftCorner;
    }

    /// <summary>
    /// Choose district position if it's not a corner one.
    /// </summary>
    private Vector2Int ChooseClassicDistrictPosition(DistrictData district, List<CardinalDirection> neighbourDirections, float[,] heightmap)
    {
        var downLeftCorner = new Vector2Int(0, 0);
        CardinalDirection direction = CardinalDirection.None;

        bool isOkay = false;
        Vector2Int connectTo = new Vector2Int(0, 0);
        int coordX = 0;
        int coordY = 0;

        int loopity = 0;
        // Choose random position around the city until it's free and there can another district
        while (!isOkay)
        {
            // Here we assume that we are using squares (on each side of the city will be the same amount of districts)
            var x = rand.Next(4 * (districts.GetLength(0) - 2));
            switch (x % 4)
            {
                case 0:
                    coordX = 0;
                    coordY = x / 4 + 1;
                    connectTo = new Vector2Int(1, coordY);
                    direction = CardinalDirection.South;
                    downLeftCorner = southWestCorner +
                        new Vector2Int(
                            coordX * (int)(centerDistrict.citySize.x / districtSizeRatio) * roadPrefabSize,
                            coordY * (int)(centerDistrict.citySize.y / districtSizeRatio) * roadPrefabSize
                            );
                    break;
                case 1:
                    coordX = districts.GetLength(0) - 1;
                    coordY = x / 4 + 1;
                    connectTo = new Vector2Int(coordX - 1, coordY);
                    direction = CardinalDirection.North;
                    downLeftCorner = southWestCorner +
                        new Vector2Int(
                            (coordX * (int)(centerDistrict.citySize.x / districtSizeRatio) + districtDistance * blockSize) * roadPrefabSize,
                            coordY * (int)(centerDistrict.citySize.y / districtSizeRatio) * roadPrefabSize
                            );
                    break;

                case 2:
                    coordX = x / 4 + 1;
                    coordY = districts.GetLength(1) - 1;
                    connectTo = new Vector2Int(coordX, coordY - 1);
                    direction = CardinalDirection.East;
                    downLeftCorner = southWestCorner +
                        new Vector2Int(
                            coordX * (int)(centerDistrict.citySize.x / districtSizeRatio) * roadPrefabSize,
                            (coordY * (int)(centerDistrict.citySize.y / districtSizeRatio) + districtDistance * blockSize) * roadPrefabSize
                            );
                    break;

                case 3:
                    coordX = x / 4 + 1;
                    coordY = 0;
                    connectTo = new Vector2Int(coordX, 1);
                    direction = CardinalDirection.West;
                    downLeftCorner = southWestCorner +
                        new Vector2Int(
                            coordX * (int)(centerDistrict.citySize.x / districtSizeRatio) * roadPrefabSize,
                            coordY * (int)(centerDistrict.citySize.y / districtSizeRatio) * roadPrefabSize
                            );
                    break;
            }

            //Debug.Log($"Creating new district with corner at {downLeftCorner}");
            isOkay = AreDownLeftCoordsOkay(downLeftCorner, heightmap, district);

            if (isOkay)
            {
                if (districts[coordX, coordY] != null)
                {
                    isOkay = false;
                }
                // if a place we pointed at would be connected to unexisted district, fill the place with new "NotThere" district
                else if (districts[connectTo.x, connectTo.y] != null && districts[connectTo.x, connectTo.y].districtState == DistrictState.NotThere)
                {
                    districts[coordX, coordY] = new DistrictData(new Vector2Int(0, 0), 0, DistrictState.NotThere);
                    isOkay = false;

                    Debug.LogError($"Attempting to be not there.");
                }
            }
            // if not isOkay -> AreDownLeftCoordsOkay returned false, therefore we cannot build district there
            else
            {
                Debug.Log($"AreDownLeftCoordsOkay said no :(");
                districts[coordX, coordY] = new DistrictData(new Vector2Int(0, 0), 0, DistrictState.NotThere);
            }

            if (loopity > 1000)
            {
                Debug.Log($"Too much iterations!");
                return Vector2Int.zero;
            }

        }

        // Add this direction and district to array
        neighbourDirections.Add(direction);

        // Add this district to the districts
        districts[coordX, coordY] = district;

        // Look for neighbours
        FindNeighbours(coordX, coordY, connectTo, neighbourDirections);

        return downLeftCorner;
    }

    /// <summary>
    /// Finds new neighbour districts to the current one given the coordinates.
    /// </summary>
    /// <param name="alreadyKnown">Coordinates of the district we already know that's neighbouring and therefore it's added</param>
    private void FindNeighbours(int x, int y, Vector2Int alreadyKnown, List<CardinalDirection> neighbourDirections)
    {
        if ((x + 1 != alreadyKnown.x || y != alreadyKnown.y) &&
            InBounds(x + 1, y, districts) &&
            districts[x + 1, y] != null)
        {
            neighbourDirections.Add(CardinalDirection.South);
        }

        if ((x - 1 != alreadyKnown.x || y != alreadyKnown.y) &&
            InBounds(x - 1, y, districts) &&
            districts[x - 1, y] != null)
        {
            neighbourDirections.Add(CardinalDirection.North);
        }

        if ((x != alreadyKnown.x || y - 1 != alreadyKnown.y) &&
            InBounds(x, y - 1, districts) &&
            districts[x, y - 1] != null)
        {
            neighbourDirections.Add(CardinalDirection.East);
        }

        if ((x != alreadyKnown.x || y + 1 != alreadyKnown.y) &&
            InBounds(x, y + 1, districts) &&
            districts[x, y + 1] != null)
        {
            neighbourDirections.Add(CardinalDirection.West);
        }
    }

    /// <summary>
    /// Checks coordinates of the potential new district if the district can be here
    /// </summary>
    private bool AreDownLeftCoordsOkay(Vector2Int downLeftCorner, float[,] heightmap, DistrictData district)
    {
        Vector2 minAccPos = new Vector2(
            roadSize / 2f + terrainFlatteningExtension,
            roadSize / 2f + terrainFlatteningExtension
            );

        Vector2 maxAccPos = new Vector2(
            (heightmap.GetLength(0) / terrain.terrainData.size.x * terrain.terrainData.heightmapResolution) - (district.citySize.x + roadSize / 2f + terrainFlatteningExtension),
            (heightmap.GetLength(1) / terrain.terrainData.size.z * terrain.terrainData.heightmapResolution) - (district.citySize.y + roadSize / 2f + terrainFlatteningExtension)
            );

        // TODO: height control

        return downLeftCorner.x > minAccPos.x && downLeftCorner.y > minAccPos.y && downLeftCorner.x < maxAccPos.x && downLeftCorner.y < maxAccPos.y;
    }

    /// <summary>
    /// If districts array is full, we need to make the map bigger
    /// </summary>
    private void MakeMapBigger()
    {
        var oldsupermodels = supermodels;
        supermodels = new GameObject[
            oldsupermodels.GetLength(0) + centerDistrict.citySize.x / districtSizeRatio * 2,
            oldsupermodels.GetLength(1) + centerDistrict.citySize.x / districtSizeRatio * 2
            ];

        for (int i = 0; i < oldsupermodels.GetLength(0); i++)
        {
            for (int j = 0; j < oldsupermodels.GetLength(1); j++)
            {
                supermodels[i + centerDistrict.citySize.x / districtSizeRatio, j + centerDistrict.citySize.x / districtSizeRatio] = oldsupermodels[i, j];
            }
        }

        southWestCorner -= new Vector2Int(
            centerDistrict.citySize.x / districtSizeRatio * roadPrefabSize,
            centerDistrict.citySize.y / districtSizeRatio * roadPrefabSize
            );
    }

    /// <summary>
    /// Adding all new district models to supermodels array
    /// </summary>
    /// <param name="district"></param>
    private void AddToSupermodels(DistrictData district)
    {
        var difference = district.downLeftCorner - southWestCorner;

        var startindex = difference / roadPrefabSize;

        for (int i = 0; i < district.citySize.x; i++)
        {
            for (int j = 0; j < district.citySize.y; j++)
            {
                supermodels[i + startindex.x, j + startindex.y] = district.models[i, j];
            }
        }
    }

    /// <summary>
    /// If both neighbouring districts have NotThere state, the corner will also have a this state.
    /// </summary>
    private bool AreBothNotThere(int x, int y, CardinalDirection northsouth, CardinalDirection eastwest)
    {
        int ns = (northsouth == CardinalDirection.North) ? -1 : 1;
        int ew = (eastwest == CardinalDirection.East) ? -1 : 1;

        return districts[x + ns, y].districtState == DistrictState.NotThere && districts[x, y + ew].districtState == DistrictState.NotThere;
    }

    /// <summary>
    /// Checks if a certain district's roads array is meeting conditions
    /// </summary>
    /// <returns>True, if a district's meeting the conditions, false otherwise</returns>
    private bool IsDistrictOk(DistrictData district)
    {
        // Condition: We need to have at least one road on max/min indices of an road array -> district has at least some size
        bool conditionMet;
        var roads = district.roads;

        bool oneside = false;
        bool otherside = false;

        var maxx = roads.GetLength(0);
        var maxy = roads.GetLength(1);

        for (int x = 0; x < maxx; x++)
        {
            if (roads[x, 0] == true)
            {
                oneside = true;
            }
            if (roads[x, maxy - 1] == true)
            {
                otherside = true;
            }
        }

        conditionMet = oneside && otherside;
        oneside = false;
        otherside = false;

        for (int y = 0; y < maxy; y++)
        {
            if (roads[0, y] == true)
            {
                oneside = true;
            }
            if (roads[maxx - 1, y] == true)
            {
                otherside = true;
            }
        }
        conditionMet = conditionMet && oneside && otherside;

        return conditionMet;
    }

    /// <summary>
    /// After the district is created, we need to connect it to the already created district.
    /// </summary>
    /// <param name="newDistrict">New, just created distric</param>
    /// <param name="directionFromOlder">Direction from the older district here</param>
    private void ConnectDistricts(DistrictData newDistrict, CardinalDirection directionFromOlder)
    {
        // Start index of newDistrict.models in supermodels
        var startindex = (newDistrict.downLeftCorner - southWestCorner) / roadPrefabSize;

        int citySize = (directionFromOlder == CardinalDirection.East || directionFromOlder == CardinalDirection.West) ?
            newDistrict.citySize.y :
            newDistrict.citySize.x;

        Model[] newModels = new Model[citySize];

        int roadRotation = GetRotationFromDirection(directionFromOlder);

        // Go around the closest side of the new district, change every L to T and every T do X
        Vector2Int edge = (directionFromOlder == CardinalDirection.East || directionFromOlder == CardinalDirection.West) ?
            new Vector2Int(1, 0) :
            new Vector2Int(0, 1);

        Vector2Int side = (directionFromOlder == CardinalDirection.North || directionFromOlder == CardinalDirection.East) ? new Vector2Int(0, 0) :
            (directionFromOlder == CardinalDirection.South) ? new Vector2Int(citySize - 1, 0) :
            (directionFromOlder == CardinalDirection.West) ? new Vector2Int(0, citySize - 1) :
            new Vector2Int(0, 0);

        for (int i = 0; i < citySize; i++)
        {
            var coordinate = side + i * edge;
            var model = supermodels[coordinate.x + startindex.x, coordinate.y + startindex.y];
            if (model != null && (model.name.StartsWith(TRoadName) || model.name.StartsWith(LRoadName)))
            {
                newModels[i] = MakeNewModelForConnection(model, coordinate + startindex, roadRotation);
            }
        }

        // List of "road chains", each chain is a List of roads
        // If something fails, we can just delete whole chain and act like nothing has happened
        List<List<Model>> modelsToBuild = new List<List<Model>>();

        Vector2Int backwardsStep = GetBackwardsStep(directionFromOlder);

        // Add crosswalk after crossroad
        for (int i = 0; i < citySize; i++)
        {
            if (newModels[i] != null)
            {
                var coordinate = side + i * edge;
                coordinate += backwardsStep;

                // Adding first road coming from new district and a crosswalk
                List<Model> toBuild = new List<Model>
                {
                    newModels[i],
                    new Model(GetRoadPrefab(IRoadCName), coordinate + startindex, roadRotation)
                };

                modelsToBuild.Add(toBuild);
            }
        }

        List<GameObject> affectedRoads = new List<GameObject>();

        // Then add all the other IRoads
        foreach (var chain in modelsToBuild)
        {
            ConnectChainToDistrict(chain, backwardsStep, directionFromOlder, roadRotation);
        }

        // Instatiate toInstantiate list (road just before connection has to go last for building zone reasons)
        InstantiateConnection(modelsToBuild);
    }

    /// <summary>
    /// Connects already existing chain of road models all the way to the older district
    /// </summary>
    /// <param name="chain">Chain of roads starting in the new district.</param>
    /// <param name="backwardsStep">Step from new district to old district in which we are moving (backwards because it should be opposite step to the direction)</param>
    /// <param name="directionFromOlder">Cardinal direction from the older district to newer one</param>
    /// <param name="roadRotation">Calculated rotation of the road</param>
    private void ConnectChainToDistrict(List<Model> chain, Vector2Int backwardsStep, CardinalDirection directionFromOlder, int roadRotation)
    {
        // restore these variables in case we "turned" them when turning roads
        var step = backwardsStep;
        var direction = directionFromOlder;
        var rotation = roadRotation;

        var model = chain[chain.Count - 1];
        var coordinates = model.supermodelCoords;

        coordinates += step;

        // Continue to add IRoads until there is actually a road (that road needs to be transferred)
        bool turn = false;
        Vector2Int newStep = Vector2Int.zero;
        Model turnRoad = null;
        while (supermodels[coordinates.x, coordinates.y] == null && !(turn = CheckRoadsPerpendicularly(coordinates, step, out newStep, out turnRoad)))
        {
            chain.Add(new Model(GetRoadPrefab(IRoadName), coordinates, rotation));

            coordinates += step;
        }

        // Adding crossroad to the last model in chain
        if (chain.Count > 1 && chain[chain.Count - 2].prefab.name != IRoadCName)
        {
            chain[chain.Count - 1].prefab = GetRoadPrefab(IRoadCName);
        }

        // If road is getting too close to another road in supermodels, do not go further, but turn this road and connect it there
        if (turn)
        {
            chain.Add(turnRoad);

            // After turning the road, there'll be new direction for each step
            step = newStep;
            direction = GetDirectionFromStep(step);
            rotation = GetRotationFromDirection(direction);

            // Add another IRoads, now turned, until the road we are connecting to (affected road)
            coordinates += step;
            while (supermodels[coordinates.x, coordinates.y] == null)
            {
                chain.Add(new Model(GetRoadPrefab(IRoadName), coordinates, rotation));
                coordinates += step;
            }
        }

        // Get road
        GameObject affectedRoad = supermodels[coordinates.x, coordinates.y];

        // Get the opposite direction (as it is the end of connection) and change road
        Model newModel = MakeNewModelForConnection(affectedRoad, coordinates, rotation + 180);
        newModel.destroyDirection = direction;
        newModel.affectedRoad = affectedRoad;

        chain.Add(newModel);
    }

    /// <summary>
    /// Checks if there is a road to the left or the right a few spaces from current position 
    /// (if the generated chain would just miss the earlier connection to the district if we went straight ahead)
    /// </summary>
    /// <param name="coordinates">Coordinates in the supermodels array</param>
    /// <param name="backwardsStep">One step from the newer district to the old one</param>
    /// <param name="newBackwardsStep">Returns new step from the newer district to the old one.</param>
    /// <param name="turn">Returns model of a turn (LRoad) with correct parameters, just ready to build.</param>
    /// <returns>True, if there is a road to which we can connect the chain from this position, but we would need to make make a turn right or left, false otherwise.</returns>
    private bool CheckRoadsPerpendicularly(Vector2Int coordinates, Vector2Int backwardsStep, out Vector2Int newBackwardsStep, out Model turn)
    {
        newBackwardsStep = Vector2Int.zero;
        turn = null;

        var direction = GetDirectionFromStep(backwardsStep);
        var rotation = GetRotationFromDirection(direction);

        var stepRight = new Vector2Int(backwardsStep.y, -backwardsStep.x);
        var stepLeft = new Vector2Int(-backwardsStep.y, backwardsStep.x);

        for (int i = 1; i <= 2; i++)
        {
            var right = new Vector2Int(coordinates.x + i * stepRight.x, coordinates.y + i * stepRight.y);
            if (InBounds(right.x, right.y, supermodels) && supermodels[right.x, right.y] != null)
            {
                newBackwardsStep = stepRight;
                turn = new Model(GetRoadPrefab(LRoadName), coordinates, rotation + 90);
                return true;
            }

            var left = new Vector2Int(coordinates.x + i * stepLeft.x, coordinates.y + i * stepLeft.y);
            if (InBounds(left.x, left.y, supermodels) && supermodels[coordinates.x + i * stepLeft.x, coordinates.y + i * stepLeft.y] != null)
            {
                newBackwardsStep = stepLeft;
                turn = new Model(GetRoadPrefab(LRoadName), coordinates, rotation + 180);
                return true;
            }
        }

        return false;
    }

    /// <summary>
    /// Demolishes buildings around the road in the certain direction. Used when the road is connecting there.
    /// </summary>
    /// <param name="road">Roads around which we would like to demolish buildings-</param>
    /// <param name="destroyDirection">Direction in which to destroy building from the center of a road 
    /// (we cannot just destroy every building belonging to the road, people live there)</param>
    private void DemolishBuildingsAroundRoad(GameObject road, CardinalDirection destroyDirection)
    {
        var step = GetBackwardsStep(destroyDirection);
        step *= -1;

        var buildingZone = road.transform.Find(zoneName);
        for (int i = 0; i < buildingZone.childCount; i++)
        {
            var block = buildingZone.GetChild(i);
            Vector2 preStep = new Vector2(block.position.x - road.transform.position.x, block.position.z - road.transform.position.z);
            Vector2 postStep = new Vector2(block.position.x - (road.transform.position.x + step.x), block.position.z - (road.transform.position.z + step.y));

            // destroy building on this position if it's on the right side of the road
            if (block.CompareTag(blockTag) &&
                Math.Abs(postStep.x) + Math.Abs(postStep.y) < Math.Abs(preStep.x) + Math.Abs(preStep.y))
            {
                Vector2 flatWorldPosition = new Vector2(block.position.x, block.position.z);

                var building = buildingManager.GetBuildingWorldPosition(flatWorldPosition);
                if (building != null)
                {
                    buildingManager.DemolishBuilding(building.gameObject);
                }
            }
        }
    }

    /// <summary>
    /// Gets old existing model with coordinates and rotation and returns model of the road.
    /// </summary>
    private Model MakeNewModelForConnection(GameObject oldModel, Vector2Int supermodelCoords, int rotation)
    {
        Model newModel = null;

        var roadType = oldModel.GetComponent<RoadSegment>().type;

        switch (roadType)
        {
            case SegmentType.IRoadC:
            case SegmentType.IRoad:
                newModel = new Model(GetRoadPrefab(TRoadName), supermodelCoords, rotation + 90);
                break;
            case SegmentType.LRoad:
                var oldRotation = (oldModel.transform.eulerAngles.y % 180 == rotation % 180) ? oldModel.transform.eulerAngles.y + 180 : oldModel.transform.eulerAngles.y + 90;
                newModel = new Model(GetRoadPrefab(TRoadName), supermodelCoords, (int)oldRotation);
                break;
            case SegmentType.TRoad:
                newModel = new Model(GetRoadPrefab(XRoadName), supermodelCoords, 0);
                break;
            case SegmentType.XRoad:
                throw new Exception($"Cannot add another road to XRoad");
        }

        return newModel;
    }

    /// <summary>
    /// Instantiates all roads from the models that were made.
    /// </summary>
    private void InstantiateConnection(List<List<Model>> modelsToBuild)
    {
        foreach (var chain in modelsToBuild)
        {
            // Main thing here is how do we want to connect the roads and how we need to do it

            // We can initialize each road up until last IRoad before new district
            for (int i = 0; i < chain.Count - 2; i++)
            {
                var model = chain[i];

                if (supermodels[model.supermodelCoords.x, model.supermodelCoords.y] != null)
                {
                    supermodels[model.supermodelCoords.x, model.supermodelCoords.y].SetActive(false);
                    Destroy(supermodels[model.supermodelCoords.x, model.supermodelCoords.y]);
                }
                supermodels[model.supermodelCoords.x, model.supermodelCoords.y] =
                    Instantiate(model.prefab, ConvertTo3DAtCityHeight(southWestCorner + model.supermodelCoords * roadSize), model.rotation, roadParentObject.transform);
            }

            // Then we really need destroy and connect the affected / connecting road in the older district (e.g. make TRoad from IRoad, so we could connect)
            // This is needed for the building zones and blocks purposes as it wouldn't correctly calculate zones if we made IRoad and then the transformation itself

            // As to why this isn't in the for cycle before: this step actually deletes road in "working" part of the city with all its waypoints and stuff and may lead to quite a few exceptions
            // I highly recommend to think this through more (When can we add these last roads so the app wouldn't fall?)
            var affectedModel = chain[chain.Count - 1];

            if (supermodels[affectedModel.supermodelCoords.x, affectedModel.supermodelCoords.y] != null)
            {
                // Disconnect the waypoints from the road model so that they aren't destroyed and cars have time to finish using them.
                Transform carWaypoints = supermodels[affectedModel.supermodelCoords.x, affectedModel.supermodelCoords.y].transform.Find("CarWaypoints");
                carWaypoints.parent = null;

                isolatedWaypoints.Add(carWaypoints);

                foreach(Transform wp in carWaypoints)
                {
                    wp.GetComponent<Waypoint>().discarded = true;
                }

                // Destroy the isolated waypoints in 10 seconds. There is no rhyme or reason behind the number, it just seemed
                // like enough time for all cars to get out of the new intersections.
                StartCoroutine(DestroyIsolatedWaypoints(carWaypoints, 20f));

                supermodels[affectedModel.supermodelCoords.x, affectedModel.supermodelCoords.y].SetActive(false);
                Destroy(supermodels[affectedModel.supermodelCoords.x, affectedModel.supermodelCoords.y]);

                // Delete buildings
                DemolishBuildingsAroundRoad(affectedModel.affectedRoad, affectedModel.destroyDirection);
            }
            supermodels[affectedModel.supermodelCoords.x, affectedModel.supermodelCoords.y] =
                Instantiate(affectedModel.prefab, ConvertTo3DAtCityHeight(southWestCorner + affectedModel.supermodelCoords * roadSize), affectedModel.rotation, roadParentObject.transform);

            foreach (Transform t in supermodels[affectedModel.supermodelCoords.x, affectedModel.supermodelCoords.y].transform.Find("CarWaypoints"))
            {
                Waypoint wp = t.GetComponent<Waypoint>();

                if (wp is IntersectionWaypoint)
                {
                    StartCoroutine(wp.FindPreviousWayPoint());
                }
                else if (wp.GetNextWaypoint() == null || wp.GetNextWaypoint().gameObject == null ||
                    wp.GetNextWaypoint().transform.parent.parent != supermodels[affectedModel.supermodelCoords.x, affectedModel.supermodelCoords.y])
                {
                    StartCoroutine(((StraightWaypoint)wp).FindNextWaypointCoroutine());
                }
            }

            var lastModel = chain[chain.Count - 2];

            supermodels[lastModel.supermodelCoords.x, lastModel.supermodelCoords.y] =
                Instantiate(lastModel.prefab, ConvertTo3DAtCityHeight(southWestCorner + lastModel.supermodelCoords * roadSize), lastModel.rotation, roadParentObject.transform);
        }
    }

    /// <summary>
    /// Calculates step in the opposite direction from a certain direction.
    /// </summary>
    /// <param name="direction">Cardinal direction against which we want a step</param>
    /// <returns>Step in the opposite direction</returns>
    private Vector2Int GetBackwardsStep(CardinalDirection direction)
    {
        Vector2Int ret = new Vector2Int(0, 0);
        switch (direction)
        {
            case CardinalDirection.North:
                ret = new Vector2Int(-1, 0);
                break;
            case CardinalDirection.South:
                ret = new Vector2Int(1, 0);
                break;
            case CardinalDirection.East:
                ret = new Vector2Int(0, -1);
                break;
            case CardinalDirection.West:
                ret = new Vector2Int(0, 1);
                break;
        }
        return ret;
    }

    /// <summary>
    /// Reverse to <see cref="GetBackwardsStep(CardinalDirection)"/>.
    /// </summary>
    /// <param name="step">Vector of length one in one cardinal direction (e.g. (0,1))</param>
    /// <returns>Opposite direction from the step</returns>
    private CardinalDirection GetDirectionFromStep(Vector2Int step)
    {
        CardinalDirection direction;

        if (step.y == 0)
        {
            if (step.x == -1)
            {
                direction = CardinalDirection.North;
            }
            else
            {
                direction = CardinalDirection.South;
            }
        }
        else if (step.y == -1)
        {
            direction = CardinalDirection.East;
        }
        else
        {
            direction = CardinalDirection.West;
        }

        return direction;
    }

    /// <summary>
    /// Calculates rotation from a certain direction
    /// </summary>
    /// <param name="direction">Cardinal direction from which to calculate rotation</param>
    /// <returns>Rotation in which we need to turn to face this direction</returns>
    private int GetRotationFromDirection(CardinalDirection direction)
    {
        int rotation = 0;

        switch (direction)
        {
            case CardinalDirection.North:
                rotation = 270;
                break;
            case CardinalDirection.South:
                rotation = 90;
                break;
            case CardinalDirection.East:
                rotation = 180;
                break;
            case CardinalDirection.West:
                rotation = 0;
                break;
        }

        return rotation;
    }

    private bool InBounds(int x, int y, GameObject[,] array)
    {
        return x >= 0 && y >= 0 && x < array.GetLength(0) && y < array.GetLength(1);
    }

    private bool InBounds(int x, int y, DistrictData[,] array)
    {
        return x >= 0 && y >= 0 && x < array.GetLength(0) && y < array.GetLength(1);
    }

    private IEnumerator DestroyIsolatedWaypoints(Transform waypoints, float timeToWait)
    {
        SaveManager.numberOfBlockers += 1;

        yield return new WaitForSeconds(timeToWait);
        isolatedWaypoints.Remove(waypoints);
        Destroy(waypoints.gameObject);

        SaveManager.numberOfBlockers -= 1;
    }

    /// <summary>
    /// Model with all the data necessary to instantiate a prefab
    /// </summary>
    internal class Model
    {
        internal GameObject prefab;
        internal Vector2Int supermodelCoords;
        internal Quaternion rotation;

        /// <summary>
        /// If this models interfere with already existing road, it should contain direction in which to destroy buildings from the old road.
        /// </summary>
        internal CardinalDirection destroyDirection;

        /// <summary>
        /// If this models interfere with already existing road, it should remember this road.
        /// </summary>
        internal GameObject affectedRoad;

        internal Model(GameObject prefab, Vector2Int supermodelCoords, int rotation)
        {
            this.prefab = prefab;
            this.supermodelCoords = supermodelCoords;
            this.rotation = Quaternion.Euler(0, rotation % 360, 0);
        }
    }
}
