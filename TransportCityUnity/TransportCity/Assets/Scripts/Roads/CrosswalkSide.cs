﻿using UnityEngine;

/// <summary>
/// A script used by empty objects with colliders on either side of a crosswalk to notify the crosswalk
/// script when a collider starts/stops intersecting them.
/// </summary>
public class CrosswalkSide : MonoBehaviour
{
    // The crosswalk which the colliders are supposed to notify when they are triggered.
    private Crosswalk cw;

    // Determines whether the collider is on the right or left side.
    public bool rightOrLeft;
    
    private void Awake()
    {
        cw = GetComponentInParent<Crosswalk>();
    }

    // As long as there is something in the trigger, notify the crosswalk script.
    private void OnTriggerStay(Collider other)
    {
        if (!other.isTrigger)
        {
            if (other.CompareTag("Person"))
            {
                cw.ResetBothTimers();
            }
            else if(other.CompareTag("Car"))
            {
                cw.ResetOneTimer(rightOrLeft, other.GetComponent<CarNavigation>());
            }
        }
    }
}
