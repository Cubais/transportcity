﻿using System.Collections.Generic;
using UnityEngine;

public class ZoneMarker : MonoBehaviour
{
    /// <summary>
    /// The cubes which determine the zone marker's length and position.
    /// </summary>
    [SerializeField]
    private List<GameObject> cubes;

    /// <summary>
    /// The amount by which the zone marker's position is to be shifted.
    /// </summary>
    [SerializeField]
    private Vector3 positionShift;

    public void UpdateZoneMarker(GameObject block)
    {
        if (cubes.Contains(block))
        {
            int index = cubes.IndexOf(block);

            // Adjusts the zone marker's scale.
            Vector3 newScale = transform.localScale;
            newScale.y -= 1;
            transform.localScale = newScale;

            // Adjusts the zone marker's position (based on on which side of the zone marker the destroyed block was).
            if (index < cubes.Count / 2)
            {
                transform.localPosition = transform.localPosition + positionShift;
                cubes[index] = null;
            }
            else
            {
                transform.localPosition = transform.localPosition - positionShift;
                cubes[index] = null;
            }           
        }
    }
}
