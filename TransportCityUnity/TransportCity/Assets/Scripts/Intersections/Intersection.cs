﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Intersection : MonoBehaviour
{
    // Holds the different waypoints through which a vehicle can enter the intersection and the vehicles
    // that are currently occupying them.
    internal Dictionary<IntersectionWaypoint, List<CarNavigation>> buffers;

    // A script that decides which cars are allowed to go when.
    internal IIntersectionDirector director;

    // The collider in the centre of the intersection.
    private BoxCollider centreCollider;

    // Determines whether the trigger colliders should work.
    internal static bool isTriggerActive = true;

    // A list of vehicles that are waiting to be processed.
    internal List<CarNavigation> pendingVehicles = new List<CarNavigation>();

    internal AddCarCoroutineContext addCarCC = new AddCarCoroutineContext();

    private void Awake()
    {
        director = GetComponent<IIntersectionDirector>();
        buffers = new Dictionary<IntersectionWaypoint, List<CarNavigation>>();

        foreach (IntersectionWaypoint iwp in GetComponentsInChildren<IntersectionWaypoint>())
        {
            buffers.Add(iwp, new List<CarNavigation>());
            director.AddWaypoint(iwp);
        }

        centreCollider = GetComponent<BoxCollider>();
    }

    private void Update()
    {
        // Goes through all the buffers and checks if the cars are still active (if not, then they are removed).
        foreach (List<CarNavigation> lcn in buffers.Values) {
            for (int i = lcn.Count - 1; i >= 0; i--)
            {
                if (!lcn[i].gameObject.activeSelf)
                {
                    director.FreeCluster(lcn[i]);
                    lcn.RemoveAt(i);
                }
            }
        }

        // Goes through the list of pending vehicles and checks if they are still active (if not, then they are removed).
        for (int i = pendingVehicles.Count - 1; i >= 0; i--)
        {
            if (!pendingVehicles[i].gameObject.activeSelf)
            {
                pendingVehicles.RemoveAt(i);
            }
        }
    }

    /// <summary>
    /// Checks whether the car is already being processed and if not, it starts the AddCar coroutine.
    /// </summary>
    /// <param name="iwp"> The intersection waypoint at which a car is waiting. </param>
    /// <param name="cn"> The car that is waiting. </param>
    public void StartAddCarCoroutine(IntersectionWaypoint iwp, CarNavigation cn)
    {
        if (!buffers[iwp].Contains(cn) && !pendingVehicles.Contains(cn))
        {
            pendingVehicles.Add(cn);
            StartCoroutine(AddCar(iwp, cn));
        }
    }

    /// <summary>
    /// Adds an incoming car to the corresponding buffer and decides whether to let it pass or stop it.
    /// It needs to be a coroutine in case the car is stopping and doesn't yet know where it will go.
    /// </summary>
    /// <param name="wp"> The intersection waypoint to which the car is going. </param>
    /// <param name="cn"> The navigation component of the incoming car. </param>
    internal IEnumerator AddCar(IntersectionWaypoint wp, CarNavigation cn)
    {
        addCarCC.isRunning = true;
        addCarCC.intersectionWaypoint = wp;
        addCarCC.carNavigation = cn;
        
        // While the car car hasn't despawned.
        while (cn.gameObject.activeSelf)
        {
            if (cn.GetNextDirection() != Direction.None && wp.GetNextWaypoint(cn.GetNextDirection()) != null)
            {
                buffers[wp].Add(cn);
                pendingVehicles.Remove(cn);

                // Stop the car if the direction from which it is coming or the direction in which it wants to go isn't free.
                if (!director.IsFree(wp, wp.GetNextWaypoint(cn.GetNextDirection()), GetExitWp(wp, cn.GetNextDirection())))
                {
                    StopCar(cn, wp, GetExitWp(wp, cn.GetNextDirection()).GetComponent<Waypoint>(), 0.5f);
                }
                // Otherwise let the director know that a car is passing through.
                else
                {
                    director.CarAt(cn, wp);
                }

                break;
            }
            else
            {
                yield return new WaitForSeconds(0.1f);
            }
        }

        addCarCC.isRunning = false;
        addCarCC.intersectionWaypoint = null;
        addCarCC.carNavigation = null;
    }

    /// <summary>
    /// Removes the car from the intersection's buffers.
    /// </summary>
    /// <param name="wp"> The intersection waypoint which the car has just left. </param>
    /// <param name="cn"> The navigation component of the leaving car. </param>
    public void RemoveCar(IntersectionWaypoint wp, CarNavigation cn)
    {
        if (buffers[wp].Contains(cn))
        {
            buffers[wp].Remove(cn);
        }
    }

    /// <summary>
    /// Notifies the intersection that there has been a change in which waypoints cars can pass through.
    /// </summary>
    public void Change()
    {
        // Find out whether any of the cars can now cross the intersection.
        foreach (KeyValuePair<IntersectionWaypoint, List<CarNavigation>> kvp in buffers)
        {
            if (
                kvp.Value.Count != 0 &&
                kvp.Value[0].GetNextDirection() != Direction.None &&
                kvp.Key.GetNextWaypoint(kvp.Value[0].GetNextDirection()) != null &&
                director.IsFree(
                    kvp.Key, 
                    kvp.Key.GetNextWaypoint(kvp.Value[0].GetNextDirection()), 
                    GetExitWp(kvp.Key, kvp.Value[0].GetNextDirection())
                    ) &&
                // The third condition is here so that an intersection doesn't send the signal to the same car twice, before it's managed to move.
                kvp.Value[0].stoppedAtIntersection
            )
            {
                SendCarOnItsWay(kvp.Value[0]);
                director.CarAt(kvp.Value[0], kvp.Key);
            }
        }
    }

    private void StopCar(CarNavigation cn, Waypoint wp, Waypoint exitWp, float time)
    {
        cn.stoppedAtIntersection = true;
        cn.Stop(wp, wp.GetNextWaypoint(cn.GetNextDirection()), exitWp, time);
    }

    private void SendCarOnItsWay(CarNavigation cn)
    {
        cn.stoppedAtIntersection = false;
        cn.DiscardStoppingWp();
    }

    /// <summary>
    /// Returns the waypoint through which a car will exit the intersection, given its entry point and direction.
    /// </summary>
    /// <param name="iwp"> The waypoint through which the car enters the intersection. </param>
    /// <param name="dir"> Its direction. </param>
    /// <returns></returns>
    private IntersectionExitWaypoint GetExitWp(IntersectionWaypoint iwp, Direction dir)
    {
        IntersectionExitWaypoint exitWp;

        if (iwp.GetNextWaypoint(dir) == null)
        {
            Debug.Log(dir);
            Debug.Log(iwp.transform.position);
            Debug.Log(buffers[iwp][0].transform.position);
        }

        if (dir == Direction.Straight)
        {
            exitWp = iwp.GetNextWaypoint(dir).GetComponent<IntersectionExitWaypoint>();
        }
        else
        {
            exitWp = iwp.GetNextWaypoint(dir).GetNextWaypoint().GetComponent<IntersectionExitWaypoint>();
        }

        return exitWp;
    }

    // Tells the director to free a cluster blocked by the car that just exited the intersection.
    private void OnTriggerExit(Collider other)
    {
        if (!isTriggerActive)
            return;

        if (!other.isTrigger)
        {
            CarNavigation cn = other.GetComponent<CarNavigation>();
            if (cn)
            {
                foreach(List<CarNavigation> lcn in buffers.Values)
                {
                    if (lcn.Contains(cn))
                    {
                        lcn.Remove(cn);
                    }
                }

                director.FreeCluster(cn);
            }
        }
    }
}
