﻿using System.Collections.Generic;
using UnityEngine;

///<inheritdoc cref="IIntersectionDirector"/>
public class BasicDirector : MonoBehaviour, IIntersectionDirector
{
    // For each waypoint of interest, this dictionary holds an int which determines how
    // many cars are blocking that way.
    internal Dictionary<Waypoint, int> directionIndicators = new Dictionary<Waypoint, int>();

    // The intersection that this director is directing.
    private Intersection intersection;

    // For every car in the intersection, this dictionary holds the list of waypoints that it is blocking.
    internal Dictionary<CarNavigation, List<Waypoint>> occupiedClusters;

    private void Awake()
    {
        intersection = GetComponent<Intersection>();
        occupiedClusters = new Dictionary<CarNavigation, List<Waypoint>>();
    }

    /// <summary>
    /// Used only for debugging purposes in this case. Instantiates/destroys the lights when the showLights boolean is switched in the inspector.
    /// </summary>
    private void Update()
    {
        // Continuously checks whether some car hasn't despawned itself while in the intersection.
        for (int i = occupiedClusters.Keys.Count - 1; i >= 0; i--)
        {
            List<CarNavigation> keys = new List<CarNavigation>(occupiedClusters.Keys);
            if (!keys[i].gameObject.activeSelf)
            {
                FreeCluster(keys[i]);
            }
        }
    }

    /// <summary>
    /// See interface for documentation.
    /// </summary>
    public void AddWaypoint(IntersectionWaypoint iwp)
    {
        directionIndicators.Add(iwp, 0);

        // Add the waypoints that follow this one to directionFlags.
        for (int i = 0; i < 3; i++)
        {
            if (iwp.GetNextWaypoint((Direction)i) != null)
            {
                directionIndicators.Add(iwp.GetNextWaypoint((Direction)i), 0);
            }
        }
    }

    /// <summary>
    /// See interface for documentation.
    /// </summary>
    public bool IsFree(Waypoint wp1, Waypoint wp2, IntersectionExitWaypoint exitWp)
    {
        if (wp1 == null || wp2 == null || exitWp == null)
        {
            return false;
        }

        return directionIndicators[wp1] == 0 && directionIndicators[wp2] == 0 && exitWp.IsFree();
    }

    /// <summary>
    /// See interface for documentation.
    /// </summary>
    public void CarAt(CarNavigation cn, IntersectionWaypoint iwp)
    {
        if (!occupiedClusters.ContainsKey(cn))
        {
            var cluster = iwp.GetComponent<WaypointCluster>().GetClusterForDirection(cn.GetNextDirection());
            occupiedClusters.Add(cn, cluster);
            ChangeClusterIndicators(cluster, false);
        }
    }

    /// <summary>
    /// See interface for documentation.
    /// </summary>
    public void FreeCluster(CarNavigation cn)
    {
        if (occupiedClusters.ContainsKey(cn))
        {
            var cluster = occupiedClusters[cn];
            if (ChangeClusterIndicators(cluster, true))
            {
                occupiedClusters.Remove(cn);

                // Notify the intersection that there has been a change in the directionFlags.
                intersection.Change();
            }
        }
    }

    /// <summary>
    /// Changes the direction indicators of a given waypoint cluster.
    /// </summary>
    private bool ChangeClusterIndicators(List<Waypoint> cluster, bool flag)
    {
        bool result = true;

        foreach (Waypoint wp in cluster)
        {
            bool changedDirection = ChangeDirectionIndicator(wp, flag);
            result |= changedDirection;
        }

        return result;
    }

    /// <summary>
    /// Changes the direction indicator of the waypoint, as well as its occupation status and
    /// the light assigned to the waypoint, if debugging lights are supposed to be shown.
    /// </summary>
    private bool ChangeDirectionIndicator(Waypoint wp, bool flag)
    {
        bool result = (!flag && directionIndicators[wp] == 0) || (flag && directionIndicators[wp] == 1);
        if (flag)
        {
            directionIndicators[wp]--;
        }
        else
        {
            directionIndicators[wp]++;
        }
        wp.ChangeOccupied(!flag);

        return result;
    }
}
