﻿// An interface for scripts that decide which vehicle should go when at an intersection.
public interface IIntersectionDirector
{
    /// <summary>
    /// Determines whether a set of waypoints can be passed through.
    /// </summary>
    /// <param name="wp1"> The waypoint through which the vehicle enters the intersection. </param>
    /// <param name="wp2"> Another waypoint through which the vehicle has to pass on its way. </param>
    /// <param name="exitWp"> The waypoint through which the vehicle will exit the intersection. </param>
    bool IsFree(Waypoint wp1, Waypoint wp2, IntersectionExitWaypoint exitWp);

    /// <summary>
    /// Notifies the director of an intersection waypoiny (a waypoint through which cars can enter an intersection).
    /// </summary>
    void AddWaypoint(IntersectionWaypoint iwp);

    /// <summary>
    /// Used to let the director know that a vehicle is going to pass through an intersection waypoint.
    /// </summary>
    /// <param name="iwp">The waypoint at which the vahicle has arrived.</param>
    /// <param name="going">The direction in which it wants to go.</param>
    void CarAt(CarNavigation cn, IntersectionWaypoint iwp);

    /// <summary>
    /// Frees a cluster of waypoint that is blocked by the given vehicle.
    /// </summary>
    /// <param name="cn"> CarNavigation component of the given vehicle. </param>
    void FreeCluster(CarNavigation cn);
}
