﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public enum Direction { Straight, Left, Right, None};

public abstract class Waypoint : MonoBehaviour
{
    // Indicates whether the waypoint is the beginning of a curve in the straight direction.
    // 0 <=> no, 1 <=> yes and it's a right curve, -1 <=> yes and it's a left curve
    public int curving;

    // The amount of vehicles currently blocking the waypoint.
    public int occupied = 0;

    // A list of vehicles that are occupying the waypoint.
    public List<CarNavigation> occupiers = new List<CarNavigation>();

    // Determines whether this waypoint has been disconnected from a road prefab. (This happens when a new district is connected.)
    public bool discarded = false;

    /// <summary>
    /// Get next waypoint in the desired direction of travel
    /// </summary>
    /// <param name="direction">Desired direction</param>
    /// <returns>Next waypoint</returns>
    public abstract Waypoint GetNextWaypoint(Direction direction = Direction.Straight);

    /// <summary>
    /// Set next waypoint in the desired direction of travel
    /// </summary>
    /// <param name="nextWaypoint">Waypoint to set</param>
    /// <param name="direction">Desired direction</param>
    public abstract void SetNextWaypoint(Waypoint nextWaypoint, Direction direction = Direction.Straight);

    /// <summary>
    /// Draw line from this waypoint to next waypoint(s)
    /// </summary>
    public abstract void DrawNextWaypointGizmo();

    /// <summary>
    /// Link waypoint with given waypoint
    /// </summary>
    /// <param name="nextWaypoint">Waypoint to be linked</param>
    public abstract void LinkWithWaypoint(Waypoint nextWaypoint);

    /// <summary>
    /// Indicates whether the waypoint is at the beginning of a curve in the straight direction.
    /// </summary>
    /// <returns>0 if the waypoint isn't the beginning of a curve, 1 if it is the beginning of a right curve,
    /// -1 if it is the beginning of a left curve</returns>
    public int IsCurving()
    {
        return curving;
    }

    /// <summary>
    /// Increments or decrements the occupied variable.
    /// </summary>
    /// <param name="b"> True <=> increment </param>
    public void ChangeOccupied(bool b)
    {
        if (b)
        {
            occupied++;
        }
        else
        {
            occupied--;
        }
    }

    /// <summary>
    /// Returns the first of the list of vehicles that is blocking the waypoint.
    /// </summary>
    public CarNavigation GetFirstOccupier()
    {
        return occupiers.Count > 0 ? occupiers[0] : null;
    }

    /// <summary>
    /// Returns true if the given vehicle is the only one currently occupying the waypoint.
    /// </summary>
    public bool IsOnlyOccupiedBy(CarNavigation cn)
    {
        return occupiers.Count == 1 && occupiers.Contains(cn);
    }

    /// <summary>
    /// Returns true if one of the cars in the occupiers list is stopping / has stopped.
    /// </summary>
    public bool IsThereAStoppingOccupier()
    {
        foreach (CarNavigation o in occupiers)
        {
            if (o.IsStopping())
            {
                return true;
            }
        }

        return false;
    }

    public bool IsOccupied()
    {
        return occupied >= 1;
    }

    /// <summary>
    /// Finds out whether car is at this waypoint
    /// </summary>
    /// <returns>True = some car is at the waypoint</returns>
    public bool CarOnTheWaypoint()
    {
        var overlapingObjects = Physics.OverlapSphere(transform.position, 1);
        foreach (var item in overlapingObjects)
        {
            if (item.CompareTag("Car"))
            {
                return true;
            }
        }

        return false;
    }
    
    /// <summary>
    /// Finds the waypoint that precedes this one.
    /// </summary>
    /// <returns></returns>
    public IEnumerator FindPreviousWayPoint()
    {
        SaveManager.numberOfBlockers += 1;

        Waypoint previous = null;
        while (previous == null)
        {
            var colliders = Physics.OverlapSphere(transform.position, 0.4f);

            // Check collider in given radius and choose waypoint
            foreach (var collider in colliders)
            {
                if (collider.gameObject != this.gameObject &&
                    collider.gameObject.CompareTag("CarWaypoint") &&
                    !collider.GetComponent<Waypoint>().discarded)
                {
                    previous = collider.gameObject.GetComponent<Waypoint>();
                    previous.SetNextWaypoint(this);

                    break;
                }
            }

            yield return new WaitForEndOfFrame();
        }

        SaveManager.numberOfBlockers -= 1;
    }

    public bool ContainsMovingOccupier()
    {
        bool allStopping = true;

        foreach (CarNavigation occupier in occupiers)
        {
            allStopping &= occupier.IsStopping();
        }

        return occupied > occupiers.Count || !allStopping;
    }
}
