﻿using UnityEngine;

/// <summary>
/// Waypoint with multiple successors, used in intersections
/// </summary>
public class IntersectionWaypoint : Waypoint
{
    public Waypoint[] nextWaypoints = new Waypoint[3];

    // The intersection which this waypoint is a part of.
    private Intersection intersection;

    private void Awake()
    {
        intersection = transform.parent.parent.GetComponent<Intersection>();
    }

    /// <summary>
    /// Get next waypoint into desired direction
    /// </summary>    
    /// <returns>Waypoint in given direction from this waypoint</returns>
    public override Waypoint GetNextWaypoint(Direction direction = Direction.Straight)
    {
        if (direction == Direction.None)
        {
            return null;
        }

        int directionIndex = (int)direction;
        return nextWaypoints[directionIndex];
    }

    /// <summary>
    /// Sets next waypoint in given direction
    /// </summary>
    /// <param name="nextWaypoint">Waypoint to set</param>
    /// <param name="direction">Direction in which to set waypoint</param>
    public override void SetNextWaypoint(Waypoint nextWaypoint, Direction direction = Direction.Straight)
    {
        int directionIndex = (int)direction;
        nextWaypoints[directionIndex] = nextWaypoint;
    }
    
    public override void DrawNextWaypointGizmo()
    {
        if (nextWaypoints != null)
        {            
            foreach (var nextWaypoint in nextWaypoints)
            {
                if (nextWaypoint == null)
                    continue;

                // Draw first half of the line in white color
                Gizmos.color = Color.white;
                Vector3 midpointCenter = (transform.position + nextWaypoint.transform.position) * 0.5f;

                // Draw line from this waypoint to the half way of next waypoint
                Gizmos.DrawLine(transform.position, midpointCenter);

                // Draw second half of the line in green color
                Gizmos.color = Color.green;
                Gizmos.DrawLine(midpointCenter, nextWaypoint.transform.position);
            }
        }
    }

    /// <summary>
    /// Link nextWaypoint to the first free spot
    /// </summary>
    /// <param name="nextWaypoint">Waypoint to be linked</param>
    public override void LinkWithWaypoint(Waypoint nextWaypoint)
    {
        if (nextWaypoints == null)
        {
            nextWaypoints = new Waypoint[3];
        }

        // Find first free spot
        for (int i = 0; i < nextWaypoints.Length; i++)
        {
            if (nextWaypoints[i] == null)
            {
                nextWaypoints[i] = nextWaypoint;
                break;
            }
        }
    }

    /// <summary>
    /// Informs the intersection that there is a car near it every fixed update.
    /// </summary>
    private void OnTriggerStay(Collider other)
    {
        if (!Intersection.isTriggerActive || discarded)
            return;

        if (!other.isTrigger)
        {
            CarNavigation cn = other.GetComponent<CarNavigation>();
            if (cn)
            {
                intersection.StartAddCarCoroutine(this, cn);
            }
        }
    }

    /// <summary>
    /// Removes a car from the interesection when it has left the waypoint's collider.
    /// </summary>
    private void OnTriggerExit(Collider other)
    {
        if (!Intersection.isTriggerActive || discarded)
            return;

        if (!other.isTrigger)
        {
            CarNavigation cn = other.GetComponent<CarNavigation>();
            if (cn)
            {
                intersection.RemoveCar(this, cn);
            }
        }
    }
}
