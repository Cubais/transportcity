﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Waypoint with one successor
/// </summary>
public class StraightWaypoint : Waypoint
{
    public Waypoint nextWaypoint;
        
    protected void Awake()
    {
        StartCoroutine(FindNextWaypointCoroutine());
    }    

    public override Waypoint GetNextWaypoint(Direction direction = Direction.Straight)
    {        
        return nextWaypoint;
    }

    public override void SetNextWaypoint(Waypoint nextWaypoint, Direction direction = Direction.Straight)
    {
        this.nextWaypoint = nextWaypoint;
    }

    public override void DrawNextWaypointGizmo()
    {
        if (nextWaypoint == null)
            return;

        Gizmos.color = Color.white;
        Vector3 midpointCenter = (transform.position + nextWaypoint.transform.position) * 0.5f;

        Gizmos.DrawLine(transform.position, midpointCenter);

        Gizmos.color = Color.green;
        Gizmos.DrawLine(midpointCenter, nextWaypoint.transform.position);
    }

    public override void LinkWithWaypoint(Waypoint nextWaypoint)
    {        
        this.nextWaypoint = nextWaypoint;
    }

    /// <summary>
    /// Finds the waypoint that follows after this one.
    /// </summary>
    /// <returns></returns>
    internal IEnumerator FindNextWaypointCoroutine()
    {
        SaveManager.numberOfBlockers += 1;

        while (nextWaypoint == null)
        {
            FindNextWaypoint();

            yield return new WaitForEndOfFrame();
        }

        SaveManager.numberOfBlockers -= 1;
    }

    internal void FindNextWaypoint()
    {
        if (nextWaypoint != null) { return; }

        var colliders = Physics.OverlapSphere(transform.position, 0.4f);

        // Check collider in given radius and choose waypoint
        foreach (var collider in colliders)
        {
            if (collider.gameObject != this.gameObject &&
                collider.gameObject.CompareTag("CarWaypoint") &&
                !collider.GetComponent<Waypoint>().discarded)
            {
                var waypoint = collider.gameObject.GetComponent<Waypoint>();
                nextWaypoint = waypoint;

                break;
            }
        }
    }
}
