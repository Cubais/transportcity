﻿using UnityEngine;

/// <summary>
/// A script connected to any waypoint through which a vehicle can exit an intersection.
/// It exists to stop vehicles from entering the intersection when another vehicel is blocking the exit.
/// </summary>
public class IntersectionExitWaypoint : MonoBehaviour
{
    // The intersection to which this exit belongs.
    private Intersection intersection;

    // The Waypoint component of this waypoint.
    private Waypoint wpComponent;

    [SerializeField]
    internal int numOfCollidersInTrigger = 0;

    // Indicates whether the waypoint was blocked during the previous update.
    internal bool prevBlocked = false;

    private void Start()
    {
        intersection = transform.parent.parent.GetComponent<Intersection>();
        wpComponent = GetComponent<Waypoint>();
    }

    private void Update()
    {
        // If the waypoint stopped being occupied (and it isn't because it has been discarded), it notifies the intersection.
        if (numOfCollidersInTrigger == 0 && !wpComponent.discarded)
        {
            if (prevBlocked)
            {
                intersection.Change();
                prevBlocked = false;
            }
        }
        else
        {
            prevBlocked = true;
        }
    }

    private void FixedUpdate()
    {
        // Basically unblocks this waypoint every fixed, so that OnTriggerStay can the re-block it,
        // given that there's still a car in the collider. This works because the OnTriggerStay happens
        // after FixedUpdate and before Update.
        if (numOfCollidersInTrigger != 0)
        {
            numOfCollidersInTrigger = 0;
            wpComponent.ChangeOccupied(false);
            wpComponent.occupiers.Clear();
        }
    }

    // Sets the waypoint's occupation status to occupied given that there is something in the collider.
    private void OnTriggerStay(Collider other)
    {
        if (wpComponent.discarded)
            return;

        if (!other.isTrigger)
        {
            CarNavigation cn = other.GetComponent<CarNavigation>();

            // If the object is a vehicle.
            if (cn)
            {
                if (numOfCollidersInTrigger == 0)
                {
                    wpComponent.ChangeOccupied(true);
                    wpComponent.occupiers.Add(cn);
                }

                numOfCollidersInTrigger++;
            }
        }
    }

    public bool IsFree()
    {
        return numOfCollidersInTrigger == 0;
    }
}
