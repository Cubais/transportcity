﻿using System.ComponentModel;

public static class Constants
{
    #region Tags and names

    /// <summary>
    /// Tag of every road prefab (IRoad, TRoad, LRoad...)
    /// </summary>
    public const string roadTag = "Road";

    /// <summary>
    /// Tag of every building block next to road (few blocks make a zone)
    /// </summary>
    public const string blockTag = "BuildingBlock";

    /// <summary>
    /// Tag of every building zone next to rad (zone is composed of blocks)
    /// </summary>
    public const string zoneTag = "BuildingZone";

    /// <summary>
    /// Tag for the terrain components
    /// </summary>
    public const string terrainTag = "Terrain";

    /// <summary>
    /// Tag for the bus stop prefab.
    /// </summary>
    public const string busStopTag = "BusStop";

    /// <summary>
    /// Tag for every waypoint
    /// </summary>
    public const string waypointTag = "Waypoint";

    /// <summary>
    /// Beginning of every straight waypoint name
    /// </summary>
    public const string straightWaypointName = "StraightWaypoint";

    /// <summary>
    /// Name of car waypoints in all road prefabs
    /// </summary>
    public const string carWaypointsName = "CarWaypoints";
    
    /// <summary>
    /// Name of ZoneBlock prefab
    /// </summary>
    public const string blockName = "ZoneBlock";

    /// <summary>
    /// Name of the IRoad prefab
    /// </summary>
    public const string IRoadName = "IRoad";
    
    /// <summary>
    /// Name of the IRoadC prefab
    /// </summary>
    public const string IRoadCName = "IRoadC";

    /// <summary>
    /// Name of the TRoad prefab
    /// </summary>
    public const string TRoadName = "TRoad";

    /// <summary>
    /// Name of the LRoad prefab
    /// </summary>
    public const string LRoadName = "LRoad";

    /// <summary>
    /// Name of the XRoad prefab
    /// </summary>
    public const string XRoadName = "XRoad";

    /// <summary>
    /// Name of the URoad prefab
    /// </summary>
    public const string URoadName = "URoad";

    /// <summary>
    /// The tag shared by all buildings.
    /// </summary>
    public const string buildingTag = "Building";

	/// <summary>
    /// Name of each building zone
    /// </summary>
    public const string zoneName = "Building Zone";

    #endregion

    #region Buildings

    // Terminology: 
    // block:   cube, 1 square of zone
    // zone:    outside of road, empty game object, contains children - blocks
    
    /// <summary>
    /// Path to folder where building prefabs live
    /// </summary>
    public const string folderPathBuildingPrefabs = "Prefabs/Buildings";

    /// <summary>
    /// Radius for Physics.OverlapShere for detecting which blocks are on the same space
    /// </summary>
    public const float buildingBlockColliderRadius = 0.2f;

    /// <summary>
    /// Maximum size for a building in one direction
    /// </summary>
    public const int maxBuildingSize = 3;

    /// <summary>
    /// Height of all buildings in a city
    /// </summary>
    public const int buildingHeight = 3;

    /// <summary>
    /// Capacity of a basic building
    /// </summary>
    public const int basicCapacity = 10;

    /// <summary>
    /// Probability of building a school
    /// </summary>
    public const float buildSchoolProbability = 0.1f;

    /// <summary>
    /// Width and height of one tree
    /// </summary>
    public const int treeSize = 2;

    /// <summary>
    /// Probability of planting a tree on a free space.
    /// </summary>
    public const float treePlantingProbability = 0.05f;

    #endregion

    #region ZonesNextToRoad

    /// <summary>
    /// Percentage of the city's zones that is for residential purposes <0 ; (1 - workZonePart)>
    /// </summary>
    public const float residentialZonePart = 0.5f;

    /// <summary>
    /// Percentage of the city's zones that is for work puposes <0 ; (1 - residentialZonePart)>
    /// </summary>
    public const float workZonePart = 0.3f;

    // Commercial part of the city is calculated as 1 - residentialZonePart - workZonePart

    /// <summary>
    /// City will be divided into cityZonesDivision x cityZonesDivision squares and each square will have type assigned
    /// </summary>
    public const int cityZonesDivision = 3;

    // Note: LRoad has unique zone, it has 1 zone shaped like an L.
    //      so, internally, we have length * length and we cut out part, 
    //      so we're left with parts with width * length on both sides.

    /// <summary>
    /// Size of the road prefab in in-game metres
    /// </summary>
    public const int roadPrefabSize = 6;

    /// <summary>
    /// How many metres around the road is the building zone
    /// </summary>
    public const int zoneWidth = 3;

    /// <summary>
    /// Roads are not generated above this limit
    /// </summary>
    public const float heightLimit = 5f;

    #endregion

    #region People

    /// <summary>
    /// Types of persons
    /// </summary>
    public enum PersonType
    {
        Child,
        Adult,
        Pensioner
    }

    // Ratios, which divides population of the city, !! Sum of all must be 1 !!
    public const float childrenRatio = 0.3f;
    public const float adultRatio = 0.5f;
    public const float pensionerRatio = 0.2f;

    // How many people should be generated at start of the game out of all possible spots
    public const float peopleRatioAtStart = 0.3f;

    // How many people should be generated each day
    public const float peopleRatioDaily = 0.05f;

    #endregion

    #region District

    /// <summary>
    /// Districts distance from main district (city centre)
    /// </summary>
    public const int districtDistance = 1;

    /// <summary>
    /// District's relative size to main district (city centre), how much smaller every new district's side is
    /// </summary>
    public const int districtSizeRatio = 2;

    /// <summary>
    /// Districts keep distance from the other ones 
    /// </summary>
    public const int distanceToOtherDistricts = 3;

    /// <summary>
    /// Minimal rectangle size when generating blocks of roads.
    /// </summary>
    public const int minRectangleSize = 2;

    /// <summary>
    /// Maximal rectangle size when generating blocks of roads.
    /// </summary>
    public const int maxRectangleSize = 4;

    /// <summary>
    /// Ratio of city residents and building capacity needed for building new district.
    /// </summary>
    public const float residentsCapacityDistrictThreshold = 0.4f;

    /// <summary>
    /// How much of city capacity should be needed to generate new district with each district.
    /// </summary>
    public const float newDistrictThreshold = 0.1f;

    /// <summary>
    /// Maximum occupancy rate when generating new people. Subway buildings destroy buildings, we need padding space for those new people.
    /// </summary>
    public const float maxOccupancyRateForNewPeople = 1 - peopleRatioDaily;

    /// <summary>
    /// District may not be built on some places, we need to have info that actual district is not there, 
    /// but this place was taken into consideration.
    /// </summary>
    public enum DistrictState
    {
        NotThere,
        There
    }

    public enum RoadType
    {
        None,

        XRoad,
        TRoad,
        IRoad,
        IRoadC,
        LRoad,
        URoad,
    }

    #endregion

    #region TimeConstants

    /// <summary>
    /// Parts of day as defined in the specification
    /// </summary>
    public enum PartOfDay
    {
        MORNING_RUSH,
        MIDDAY,
        AFTERNOON_RUSH,
        EVENING,
        NIGHT
    }

    /// <summary>
    /// Start hour of morning rush hour, set based on specification
    /// </summary>
    public const int morningRushStartHour = 6;

    /// <summary>
    /// Start hour of the midday part of day, set based on specification
    /// </summary>
    public const int middayStartHour = 10;

    /// <summary>
    /// Start hour of afternoon rush hour, set based on specification
    /// </summary>
    public const int afternoonRushStartHour = 15;

    /// <summary>
    /// Start hour of the evening part of day, set based on specification
    /// </summary>
    public const int eveningStartHour = 19;

    /// <summary>
    /// Start hour of the night part of day, set based on specification
    /// </summary>
    public const int nightStartHour = 23;

    #endregion

    #region Money

    /// <summary>
    /// Amount of money player is starting with
    /// </summary>
    public const int startingMoney = 28540;

    /// <summary>
    /// How many hours does one day have
    /// </summary>
    public const int hoursInDay = 24;

    /// <summary>
    /// Cost of one bus ticket
    /// </summary>
    public const int busTicketCost = 5;

    /// <summary>
    /// Cost of one subway ticket
    /// </summary>
    public const int subwayTicketCost = 10;

    /// <summary>
    /// Not enough money string for pop-up window
    /// </summary>
    public const string notEnoughMoney = "Not enough money to complete the operation.";

    /// <summary>
    /// Cost of one subway train
    /// </summary>
    public const int subwayTrainCost = 25000;

    /// <summary>
    /// Cost of one bus line
    /// </summary>
    public const int busLineCost = 2000;

    /// <summary>
    /// Cost of one subway line
    /// </summary>
    public const int subwayLineCost = 20000;

    /// <summary>
    /// Multiplier which will be applied to initial price to get selling cost
    /// </summary>
    public const float sellFromCostMultiplier = 0.5f;

    /// <summary>
    /// Multiplier which will be applied to initial price to get repair cost
    /// </summary>
    public const float repairFromCostMultiplier = 0.01f;

    /// <summary>
    /// Maintenance cost multiplier (how much does maintenance cost from original price)
    /// </summary>
    public const float subwayLineMaintenanceMultiplier = 0.02f;

    #endregion

    #region City Council

    /// <summary>
    /// Number of minutes after which will be called update function in city council events.
    /// </summary>
    public const int cityCouncilUpdateFreq = 5;

    #endregion

    #region Scheduler

    // The avergae speed of a person.
    public const float avgPersonSpeed = 1.5f;

    // The average speed of a vehicle.
    public const float avgVehicleSpeed = 8f;

    // A coefficient used to approximate the actual distance between two points.
    public const float approximationCoefficient = 1.25f;

    #endregion

    /// <summary>
    /// Types of zones there are
    /// </summary>
    public enum ZoneType
    {
        Residential,
        Commercial,
        Work,
        None                // just because it's not nullable
}

    /// <summary>
    /// States the block can be in
    /// </summary>
    public enum BlockState
    {
        None,       // if there isn't a building zone
        Free,
        Taken,
        Road,
        Tree
    }

    /// <summary>
    /// Cardinal directions
    /// </summary>
    public enum CardinalDirection
    {
        None,
        North,
        South,
        East,
        West
    }

    /// <summary>
    /// Type of vehicle of CarNavigation
    /// </summary>
    public enum VehicleType
    {
        Car,
        Bus,
        SubwayTrain
    }
    public static string GetVehiclePrefabFolderName(VehicleType vehicleType)
    {
        switch (vehicleType)
        {
            case VehicleType.Car:
                return "Cars";
            case VehicleType.Bus:
                return "Buses";
            default:
                throw new InvalidEnumArgumentException($"Vehicle type '{vehicleType}' not supported");
        }
    }

    /// <summary>
    /// The path to the LineRenderer prefab.
    /// </summary>
    public const string lineRendererPath = "Prefabs/Stops/LineRenderer";
}

