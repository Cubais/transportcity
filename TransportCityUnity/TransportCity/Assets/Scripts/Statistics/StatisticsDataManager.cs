﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatisticsDataManager : MonoBehaviour
{
    /// <summary>
    /// Singleton instance of this class
    /// </summary>
    public static StatisticsDataManager instance;

    // Update all statistics with this interval in seconds
    public float statisticsUpdateInterval = 5;

    public int maxDataLength = 4000;

    /// <summary>
    /// The Dictionary of all saved numerical series. The keys are the names of the series
    /// and the values are lists of pairs (time of entry, value)
    /// </summary>
    public Dictionary<string, List<(long, float)>> NumericSeries { get; internal set; }

    // Time manager reference
    private TimeManager timeManager;

    // Time in seconds since statistics were last updated
    internal float lastStatisticsUpdateTime;

    void Awake()
    {
        NumericSeries = new Dictionary<string, List<(long, float)>>();

        // Singleton assignment
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }

    private void Start()
    {
        timeManager = TimeManager.instance;
    }

    private void Update()
    {
        lastStatisticsUpdateTime += Time.deltaTime;

        if (lastStatisticsUpdateTime > statisticsUpdateInterval)
        {
            InsertValueIntoSeries("Money", MoneyManager.instance.Money);
            InsertValueIntoSeries("Money flow", MoneyManager.instance.MoneyFlow);
            InsertValueIntoSeries("Number of buses", BusInstanceManager.GetInstance().GetBusInstances().Count);
            InsertValueIntoSeries("Number of subway trains", SubwayInstanceManager.GetInstance().GetSubwayTrainInstances().Count);
            InsertValueIntoSeries("Population", PeopleManager.instance.people.Count);
            InsertValueIntoSeries("Car Usage", PeopleManager.instance.CalculateCarUsage());
            InsertValueIntoSeries("Happiness", PeopleManager.instance.CalculateHappiness());
            CheckAndPurgeData();
            lastStatisticsUpdateTime = 0;
        }
    }

    /// <summary>
    /// Inserts the value into the specified series with current in-game timestamp
    /// </summary>
    /// <param name="seriesName">The series to insert the value into</param>
    /// <param name="value">The value to insert</param>
    public void InsertValueIntoSeries(string seriesName, float value)
    {
        if (!NumericSeries.ContainsKey(seriesName))
        {
            NumericSeries.Add(seriesName, new List<(long, float)>());
        }

        NumericSeries[seriesName].Add((timeManager.ConvertDateTimeToMilliseconds(timeManager.CurrentDateTime), value));
    }

    // Check each series and if it is too long, throw out every odd element
    private void CheckAndPurgeData()
    {
        // Copy the keys because we cannot enumerate over them otherwise
        string[] keysArray = new string[NumericSeries.Keys.Count];
        NumericSeries.Keys.CopyTo(keysArray, 0);

        for (int i = 0; i < NumericSeries.Keys.Count; i++)
        {
            string key = keysArray[i];
            if (NumericSeries[key].Count >= maxDataLength)
            {
                Debug.Log("Purging " + key);
                List<(long, float)> newList = new List<(long, float)>();
                for (int j = 0; j < NumericSeries[key].Count; j++)
                {
                    if (j % 2 == 0)
                    {
                        newList.Add(NumericSeries[key][j]);
                    }
                }

                NumericSeries[key] = newList;
            }
        }
    }
}
