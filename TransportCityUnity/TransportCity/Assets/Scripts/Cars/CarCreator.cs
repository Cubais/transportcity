﻿using System.Collections.Generic;
using UnityEngine;

// Temporary script that can spawn cars. Used for debugging purposes
public class CarCreator : MonoBehaviour
{
    // Singleton
    public static CarCreator instance;

    // List of all the waypoints on the map.
    internal List<GameObject> waypoints;

    // When this flag is turned to true, cars are created through the update method.
    public bool activator = false;

    // When this flag is turned to true, cars are spawned at random points on the roadmap when created.
    // Otherwise, the starting and ending point have to be specified.
    public bool random = true;

    // The number of cars that the creator should spawn (is only applied if the spawning is random).
    public int carCount = 3;

    // The Waypoint at which a car should be spawned.
    public Waypoint start;

    // The Waypointto which a car should head after being spawned.
    public Waypoint end;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    private void Update()
    {
        if(activator)
        {
            if (random)
            {
                for (int i = 0; i < carCount; i++)
                {
                    SendToRandom();
                }
                waypoints = null;
            }
            else
            {
                SendAlongSetPath();
            }
        }
    }

    /// <summary>
    /// Determines whether a GameObject (which is presumed to be a waypoint) is part of an intersection.
    /// (The first part of the condition ensures that waypoints that were discarded during the creation
    /// of a new district but haven't yet been destroyed aren't taken into account.)
    /// </summary>
    private static bool IsIntersection(GameObject wp)
    {
        return wp.transform.parent.parent != null && wp.transform.parent.parent.GetComponent<Intersection>();
    }

    /// <summary>
    /// Spawns the car at a random point and sends it to a random point.
    /// </summary>
    private void SendToRandom()
    {
        if (waypoints == null)
        {
            waypoints = new List<GameObject>(GameObject.FindGameObjectsWithTag("CarWaypoint"));
            waypoints.RemoveAll(IsIntersection);
        }

        GameObject spawnPoint = waypoints[Random.Range(0, waypoints.Count)];
        
        CarNavigation c = CreateRandomCar(spawnPoint.transform.position, Quaternion.identity);
        c.SetStartingWaypoint(spawnPoint.GetComponent<Waypoint>());
        c.GoTo(waypoints[Random.Range(0, waypoints.Count)].GetComponent<Waypoint>());

        activator = false;
    }

    /// <summary>
    /// Spawns the car at the specified start waypoint and sends it to the specified end waypoint.
    /// </summary>
    private void SendAlongSetPath()
    {
        if (start != null && end != null)
        {
            if (waypoints == null)
            {
                waypoints = new List<GameObject>(GameObject.FindGameObjectsWithTag("CarWaypoint"));
            }

            CarNavigation c = CreateRandomCar(start.transform.position, Quaternion.identity);

            c.SetStartingWaypoint(start.GetComponent<Waypoint>());
            c.GoTo(end);
        }

        activator = false;
    }

    /// <summary>
    /// Creates random car from Prefabs/Cars folder
    /// </summary>
    /// <param name="position">Postion to spawn car on</param>
    /// <param name="rotation">Rotation of spawned car</param>
    /// <returns></returns>
    internal CarNavigation CreateRandomCar(Vector3 position, Quaternion rotation)
    {
        // Choose random car prefab from folder
        var carList = Resources.LoadAll<GameObject>("Prefabs/Cars/");
        var carToSpawn = carList[Random.Range(0, carList.Length)];

        var car = Instantiate(carToSpawn, position, rotation, this.transform);
        var carNav = car.GetComponent<CarNavigation>();
        carNav.vehicleType = Constants.VehicleType.Car;

        CarNavigation.allCars.Add(carNav);

        return carNav;
    }

    /// <summary>
    /// Creates car from Prefabs/Cars folder
    /// </summary>
    /// <param name="position">Postion to spawn car on</param>
    /// <param name="rotation">Rotation of spawned car</param>
    /// <param name="carName">Name of the car prefab</param>
    /// <returns></returns>
    internal CarNavigation CreateCar(Vector3 position, Quaternion rotation, string carName)
    {
        // Choose random car prefab from folder
        var carPrefab = Resources.Load<GameObject>($"Prefabs/Cars/{carName}");

        var car = Instantiate(carPrefab, position, rotation, this.transform);
        var carNav = car.GetComponent<CarNavigation>();
        carNav.vehicleType = Constants.VehicleType.Car;

        CarNavigation.allCars.Add(carNav);

        return carNav;
    }
}
