﻿using System.Collections.Generic;
using UnityEngine;
using CarPathFinding;

public class PathManager : MonoBehaviour
{
    private static PathManager instance;

    // An object that has a method for computing the A* algorithm.
    private AStar aStar;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance == this)
        {
            Destroy(this.gameObject);
        }
        DontDestroyOnLoad(this.gameObject);

        aStar = new AStar();
    }

    public static PathManager GetInstance()
    {
        return instance;
    }

    /// <summary>
    /// Get path length 
    /// </summary>
    /// <param name="start">Start waypoint</param>
    /// <param name="end">End waypoint</param>
    /// <param name="dirs">Directions where to turn on intersections</param>
    /// <returns>Number of waypoints on the path</returns>
    public int GetPathLength(Waypoint start, Waypoint end, Stack<Direction> dirs)
    {
        var currentWaypoint = start;
        int pathLength = 0;
        while (currentWaypoint != end)
        {
            if (currentWaypoint is IntersectionWaypoint)
            {
                currentWaypoint = currentWaypoint.GetNextWaypoint(dirs.Pop());
            }
            else
            {
                currentWaypoint = currentWaypoint.GetNextWaypoint();
            }
            pathLength++;
                   
        }

        return pathLength;
    }

    /// <summary>
    /// Calculates the best path that the car can take from waypoint start to waypoint end
    /// using the A* algorithm.
    /// </summary>
    public Stack<Direction> GetPath(Waypoint start, Waypoint end)
    {
        if (start == null || end == null || start == end)
        {
            return null;
        }

        // Straight waypoints are skipped as the car only needs information about where
        // to move at intersections.
        while (start is StraightWaypoint && start != end)
        {
            start = start.GetNextWaypoint();
            if (start == end)
            {
                return new Stack<Direction>();
            }
        }

        IPathPoint<Direction, Waypoint> res = aStar.GetPath<Direction, Waypoint>(new WaypointWrapper(start), new WaypointWrapper(end));
        
        if (res == null)
        {
            return null;
        }

        Stack<Direction> path = new Stack<Direction>();
        IPathPoint<Direction, Waypoint> currentNode = res;

        // Compute the actual path by backtracking from the last path point.
        while (currentNode.Previous != null)
        {
            path.Push(currentNode.Direction);
            currentNode = currentNode.Previous;
        }

        return path;
    }
}
