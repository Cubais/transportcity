﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using static Constants;

public class CarNavigation : MonoBehaviour
{
    public VehicleType vehicleType;

    // A list of all the currently active cars.
    public static readonly List<CarNavigation> activeCars = new List<CarNavigation>();
    public static readonly List<CarNavigation> allCars = new List<CarNavigation>();

    // The speed of the car upon its instantiation.
    private const float startSpeed = 10f;

    // A correction angle used when the car is turning at a curve.
    // If this angle wasn't subtracted from the angle that the car is supposed to turn,
    // it would turn a little too much which would look strange.
    public float correctionAngle = 0;

    // Indicates whether car is at destination
    public bool carAtDestination { get; internal set; }

    // Determines whether the car is currently stopped at an intersection.
    [SerializeField]
    internal bool stoppedAtIntersection = false;

    // The speed of the car.
    [SerializeField]
    internal float speed = 0;

    // The speed to which the car is supposed to speed up after stopping.
    [SerializeField]
    internal float defaultSpeed = startSpeed;

    // The path that the car is supposed to take.
    [SerializeField]
    internal Stack<Direction> path;

    // The next waypoint at which the car is supposed to arrive. If the car isn't moving,
    // this is the waypoint at which it is standing.
    [SerializeField]
    internal Waypoint nextOrCurrentWaypoint;

    // The destination at which the car is supposed to arrive.
    [SerializeField]
    internal Vector3 destination;

    // The centre of the circle around which the car is supposed to turn if it gets to a curve.
    [SerializeField]
    internal Vector2 centre;

    // The radius of the circle around which the car is supposed to turn if it gets to a curve.
    [SerializeField]
    internal float radius;

    // The next direction in which the car is supposed to turn.
    [SerializeField]
    internal Direction nextDir = Direction.None;

    // The angular speed at which the car is supposed to move around a curve.
    [SerializeField]
    internal float angularSpeed = 0;

    // The remaining angle that the car is supposed to turn around at a curve.
    [SerializeField]
    internal float angle = float.MinValue;

    // The waypoint which caused the car to stop.
    [SerializeField]
    internal Waypoint stoppingWP;

    // A list of GameObjects that are preventing the car from moving forward.
    [SerializeField]
    internal List<CarNavigation> blockers = new List<CarNavigation>();

    // A lookahead that allows the car to check whether a waypoint in front of it is blocked.
    [SerializeField]
    internal Waypoint lookahead;

    // Indicates whether the car is currently stopping or has already stopped.
    [SerializeField]
    internal bool stopping = false;

    // Indicates whether the car is currently accelerating or is at least not stopping.
    [SerializeField]
    internal bool accelerating = false;

    // The length from the centre of the car to its end.
    // (Used when counting how fast the car should decelerate.)
    [SerializeField]
    internal float length;

    internal ChangeCarSpeedCoroutineContext changeSpeedCC = new ChangeCarSpeedCoroutineContext();

    protected void Update()
    {
        // If the car has a destination, move it along the given path. The car doesn't have to be
        // exactly at its given destination though, because otherwise it couldn't stop at curves.
        if (destination != null && path != null && (transform.position - destination).magnitude >= 0.3f)
        {
            carAtDestination = false;

            // If the car is close to its destination, it should start decelerating.
            if ((transform.position - destination).magnitude <= 3 && !stopping && path.Count <= 1)
            {
                float deceleration = (speed * speed) / (2 * (transform.position - destination).magnitude);
                Stop(deceleration * 0.05f);
            }

            // Compute the lookahead.
            if (nextOrCurrentWaypoint is StraightWaypoint)
            {
                lookahead = nextOrCurrentWaypoint.GetNextWaypoint();
            }
            else
            {
                lookahead = nextOrCurrentWaypoint.GetNextWaypoint(nextDir);
            }

            // If the lookahead waypoint is currently occupied by another car which is stopping, stop.
            if (lookahead != null && lookahead.IsOccupied() && !lookahead.IsOnlyOccupiedBy(this) &&
                !stopping && (!lookahead.GetFirstOccupier() || lookahead.IsThereAStoppingOccupier()))
            {
                //Computes the deceleration from the distance to the waypoint and the cars speed.
                float distance = Vector3.Distance(lookahead.transform.position, transform.position) - 1.2f;
                if (distance == 0)
                {
                    distance = -1;
                }

                float deceleration = (speed * speed) / (2 * distance);
                if (deceleration < 0)
                {
                    deceleration = speed / 0.05f;
                }

                stoppingWP = lookahead;
                Stop(deceleration * 0.05f);
            }
            // Otherwise, if the car isn't stopping at an intersection, is moving slower than default
            // and nothing is blocking its way, it should speed up. (Unless it is nearing the end of its path.)
            else if (
                !stoppedAtIntersection && speed < defaultSpeed && !accelerating &&
                CheckPathPassability() && ((transform.position - destination).magnitude > 5 || speed == 0)
                )
            {
                Go(1f);
            }
            // If the car has arrived at the next waypoint, pick the waypoint to which it is supposed to go next.
            else if (transform.position == nextOrCurrentWaypoint.transform.position && speed != 0)
            {
                if (path.Count != 0)
                {
                    nextDir = path.Peek();
                }
                else
                {
                    nextDir = Direction.None;
                }

                if (nextOrCurrentWaypoint is StraightWaypoint)
                {
                    // If the point is the start of a curve, calculate it.
                    if (nextOrCurrentWaypoint.IsCurving() != 0)
                    {
                        GetCircle(
                            GetXandZ(nextOrCurrentWaypoint.transform.position),
                            GetXandZ(nextOrCurrentWaypoint.GetNextWaypoint().transform.position),
                            GetXandZ(nextOrCurrentWaypoint.GetNextWaypoint().GetNextWaypoint().transform.position)
                        );
                        angularSpeed = Mathf.Rad2Deg * speed / radius;

                        if (nextOrCurrentWaypoint.IsCurving() == -1)
                        {
                            nextDir = Direction.Left;
                        }
                        else
                        {
                            nextDir = Direction.Right;
                        }
                        nextOrCurrentWaypoint = nextOrCurrentWaypoint.GetNextWaypoint().GetNextWaypoint();
                    }
                    // Otherwise just go straight to the next waypoint.
                    else
                    {
                        nextOrCurrentWaypoint = nextOrCurrentWaypoint.GetNextWaypoint();
                    }
                }
                else if (nextDir != Direction.None)
                {
                    nextDir = path.Pop();

                    // If the next direction isn't straight, calculate the curve.
                    if (nextDir != Direction.Straight)
                    {
                        GetCircle(
                            GetXandZ(nextOrCurrentWaypoint.transform.position),
                            GetXandZ(nextOrCurrentWaypoint.GetNextWaypoint(nextDir).transform.position),
                            GetXandZ(nextOrCurrentWaypoint.GetNextWaypoint(nextDir).GetNextWaypoint().transform.position)
                        );
                        angularSpeed = Mathf.Rad2Deg * speed / radius;

                        nextOrCurrentWaypoint = nextOrCurrentWaypoint.GetNextWaypoint(nextDir).GetNextWaypoint();
                    }
                    // Otherwise just go straight to the next waypoint.
                    else
                    {
                        nextOrCurrentWaypoint = nextOrCurrentWaypoint.GetNextWaypoint();
                    }
                }
            }

            // If the car is still supposed to turn by an angle.
            if(angle > 0)
            {
                //Rotate the car in the given direction.
                if (nextDir == Direction.Right)
                {
                    transform.RotateAround(
                        new Vector3(centre.x, 0, centre.y),
                        Vector3.up,
                        Mathf.Min(angularSpeed * Time.deltaTime, angle)
                    );
                }
                else
                {
                    transform.RotateAround(
                        new Vector3(centre.x, 0, centre.y),
                        Vector3.down,
                        Mathf.Min(angularSpeed * Time.deltaTime, angle)
                    );
                }
                angle -= Mathf.Min(angularSpeed * Time.deltaTime, angle);
            }
            else
            {
                // The speed is multiplied by 0.5 here so that the car doesn't turn too abruptly.
                transform.rotation = Quaternion.LookRotation(
                    Vector3.RotateTowards(
                        transform.forward,
                        nextOrCurrentWaypoint.transform.position - transform.position,
                        speed * 0.5f * Time.deltaTime,
                        0.0f
                    )
                );
                transform.position = Vector3.MoveTowards(
                    transform.position,
                    nextOrCurrentWaypoint.transform.position,
                    speed * Time.deltaTime
                );
            }
        }
        else if ((transform.position - destination).magnitude < 0.3f)
        {
            carAtDestination = true;
            nextDir = Direction.None;
        }

        // Go through all the objects blocking this car's path and check whether they shouldn't be discarded.
        for (int i = blockers.Count - 1; i >= 0; i--)
        {
            // If the blocker isn't active, has been destroyed or is too far away.            
            if (blockers[i] == null || !blockers[i].gameObject.activeSelf || (transform.position - blockers[i].transform.position).magnitude >= 7)
            {
                blockers.RemoveAt(i);
            }
        }
    }

    private void OnDisable()
    {
        ResetCar();
    }

    /// <summary>
    /// Resets some of the cars values to their default values. Also removes the car from the list of all cars.
    /// </summary>
    private void ResetCar()
    {
        carAtDestination = false;
        stoppedAtIntersection = false;
        correctionAngle = 0;
        stoppedAtIntersection = false;
        speed = 0;
        path = null;
        nextOrCurrentWaypoint = null;
        destination = Vector3.positiveInfinity;
        centre = Vector2.positiveInfinity;
        nextDir = Direction.None;
        angularSpeed = 0;
        angle = float.MinValue;
        stoppingWP = null;
        blockers = new List<CarNavigation>();
        lookahead = null;
        stopping = false;
        accelerating = false;

        activeCars.Remove(this);
    }

    /// <summary>
    /// Used for setting the waypoint at which the car is spawned.
    /// </summary>
    /// <param name="start"></param>
    public void SetStartingWaypoint(Waypoint start)
    {
        nextOrCurrentWaypoint = start;
    }

    /// <summary>
    /// Fetches the path towards the given waypoint.
    /// </summary>
    public void GoTo(Waypoint destination)
    {
        if (destination != nextOrCurrentWaypoint)
        {
            this.destination = destination.transform.position;
            path = PathManager.GetInstance().GetPath(nextOrCurrentWaypoint, destination);          
        }
    }

    /// <summary>
    /// Stops the car using one of the three waypoints as the stopping waypoint.
    /// Used by the intersection script.
    /// </summary>
    /// <param name="firstWp"> The waypoint closer to the car. </param>
    /// <param name="secondWp"> The waypoint further from the car. </param>
    /// <param name="thirdWp"> The intersection exit waypoint. </param>
    /// <param name="lengthFromStop"> The distance which the car should keep from the first waypoint. </param>
    public void Stop(Waypoint firstWp, Waypoint secondWp, Waypoint thirdWp, float lengthFromStop)
    {
        if (firstWp.IsOccupied())
        {
            stoppingWP = firstWp;
        }
        else if (secondWp.IsOccupied())
        {
            stoppingWP = secondWp;
        }
        else
        {
            stoppingWP = thirdWp;
        }

        Stop(firstWp.transform.position, lengthFromStop);
    }

    /// <summary>
    /// Stops the car.
    /// </summary>
    /// <param name="position"> The position before which the car should stop. </param>
    /// <param name="lengthFromStop"> The distance which the car should keep from the position. </param>
    public void Stop(Vector3 position, float lengthFromStop)
    {
        float distance = (transform.position - position).magnitude - length - lengthFromStop;

        // If distance is smaller than or equal to 0, stop immediately.
        if (distance <= 0)
        {
            Stop(speed);
        }
        // Otherwise compute the deceleration.
        else
        { 
            float deceleration = (speed * speed) / (2 * distance);
            Stop(deceleration * 0.05f);
        }
    }

    /// <summary>
    /// Stops the car.
    /// </summary>
    /// <param name="rate"> The rate at which the car should decelerate. </param>
    public void Stop(float rate)
    {
        accelerating = false;
        stopping = true;
        StopAllCoroutines();
        StartCoroutine(ChangeSpeed(0, -rate));
    }

    /// <summary>
    /// Starts up the car.
    /// </summary>
    /// <param name="rate"> The rate at which the car should accelerate. </param>
    public void Go(float rate)
    {
        stopping = false;
        accelerating = true;
        stoppingWP = null;
        StopAllCoroutines();
        StartCoroutine(ChangeSpeed(defaultSpeed, rate));
    }

    /// <summary>
    /// Makes the car slow down or speed up to the desired speed by gradually changing it every 0.05 seconds.
    /// </summary>
    /// <param name="to">The desired speed.</param>
    /// <param name="amountPerIteration">The amount by which the speed should change every iteration.</param>
    internal IEnumerator ChangeSpeed(float to, float amountPerIteration)
    {
        changeSpeedCC.isRunning = true;
        changeSpeedCC.targetSpeed = to;
        changeSpeedCC.speedChangePerSecond = amountPerIteration;

        while (true)
        {
            if (speed == to || Mathf.Abs(speed - to) < Mathf.Abs(speed + amountPerIteration - to))
            {
                speed = to;
                angularSpeed = Mathf.Rad2Deg * speed / radius;

                break;
            }

            speed += amountPerIteration;
            angularSpeed = Mathf.Rad2Deg * speed / radius;

            yield return new WaitForSeconds(0.05f);
        }

        changeSpeedCC.isRunning = false;
    }


    public Direction GetNextDirection()
    {
        return nextDir;
    }

    /// <summary>
    /// Stops the car if it detects another car in front of it.
    /// </summary>
    private void OnTriggerEnter(Collider other)
    {
        if (!other.isTrigger)
        {
            if (other.CompareTag("Car"))
            {
                blockers.Add(other.GetComponent<CarNavigation>());
                Stop(other.transform.position, other.GetComponent<CarNavigation>().GetLength() + 0.5f);
            }
        }
    }

    /// <summary>
    /// Starts up the car if it no longer has another car in front of it.
    /// </summary>
    private void OnTriggerExit(Collider other)
    {
        if (!other.isTrigger)
        {
            if (other.CompareTag("Car"))
            {
                blockers.Remove(other.GetComponent<CarNavigation>());
            }
        }
    }

    /// <summary>
    /// Used for converting a 3D vector into a desirable 2D vector.
    /// </summary>
    public Vector2 GetXandZ(Vector3 input)
    {
        return new Vector2(input.x, input.z);
    }

    /// <summary>
    /// Computes the centre and radius of a circle going through the 3 input points.
    /// Also calculates by what angle the car is supposed to turn.
    /// I used code and logic from the following sites here: http://csharphelper.com/blog/2016/09/draw-a-circle-through-three-points-in-c/
    /// https://stackoverflow.com/questions/1211212/how-to-calculate-an-angle-from-three-points
    /// </summary>
    public void GetCircle(Vector2 a, Vector2 b, Vector2 c)
    {
        // Get the perpendicular bisector of a and b.
        float x1 = (b.x + a.x) / 2;
        float y1 = (b.y + a.y) / 2;
        float dy1 = b.x - a.x;
        float dx1 = -(b.y - a.y);

        // Get the perpendicular bisector of b and c.
        float x2 = (c.x + b.x) / 2;
        float y2 = (c.y + b.y) / 2;
        float dy2 = c.x - b.x;
        float dx2 = -(c.y - b.y);

        // Find out where the lines intersect.
        bool lines_intersect;
        Vector2 intersection;
        FindIntersection(
            new Vector2(x1, y1), new Vector2(x1 + dx1, y1 + dy1),
            new Vector2(x2, y2), new Vector2(x2 + dx2, y2 + dy2),
            out lines_intersect, out intersection
        );
        {
            centre = intersection;
            float dx = centre.x - a.x;
            float dy = centre.y - a.y;
            radius = Mathf.Sqrt(dx * dx + dy * dy);
        }

        // The distance between the two waypoints which are further apart.
        float wayPointDistance = Mathf.Sqrt(Mathf.Pow(a.x - c.x, 2) + Mathf.Pow(a.y - c.y, 2));

        // Calculate the angle by which the car is supposed to turn.
        GetCorrectionAngle();
        float rSquared = radius * radius;
        float wpdSquared = wayPointDistance * wayPointDistance;
        angle = Mathf.Rad2Deg * Mathf.Acos((2 * rSquared - wpdSquared) / (2 * rSquared)) - correctionAngle;
        angle = Mathf.Min(angle, 90f);
    }

    /// <summary>
    /// Finds the intersection point of 2 lines (each defined by 2 points). Used in GetCircle to find the centre of the circle.
    /// I used the logic and code from the following site here: http://csharphelper.com/blog/2014/08/determine-where-two-lines-intersect-in-c/
    /// </summary>
    /// <param name="p1">Beginning of the first line segment.</param>
    /// <param name="p2">End of the first line segment.</param>
    /// <param name="p3">Beginning of the second line segment.</param>
    /// <param name="p4">End of the second line segment.</param>
    /// <param name="lines_intersect">Determines whether the lines intersect.</param>
    /// <param name="intersection">The point where the two lines intersect.</param>
    private void FindIntersection(
        Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4,
        out bool lines_intersect, out Vector2 intersection
    )
    {
        // Get the segments' parameters.
        float dx12 = p2.x - p1.x;
        float dy12 = p2.y - p1.y;
        float dx34 = p4.x - p3.x;
        float dy34 = p4.y - p3.y;

        // Solve for t1 and t2.
        float denominator = (dy12 * dx34 - dx12 * dy34);
        float t1 = ((p1.x - p3.x) * dy34 + (p3.y - p1.y) * dx34) / denominator;
        if (float.IsInfinity(t1))
        {
            // The lines are parallel (or close enough to it).
            lines_intersect = false;
            intersection = new Vector2(float.NaN, float.NaN);
            return;
        }

        lines_intersect = true;
        float t2 = ((p3.x - p1.x) * dy12 + (p1.y - p3.y) * dx12) / -denominator;

        // Find the point of intersection.
        intersection = new Vector2(p1.x + dx12 * t1, p1.y + dy12 * t1);
    }

    /// <summary>
    /// Calculates the correction angle for a given circle.
    /// </summary>
    private void GetCorrectionAngle()
    {
        // Not much rhyme or reason here, I just used experimented until I found a good
        // angle for a circle with a radius of 1.356986 and then tried to generalize it.
        float arcLength = 2 * Mathf.PI * 1.356986f / 12;
        float circumference = 2 * Mathf.PI * radius;
        correctionAngle = arcLength / circumference * 360;
    }

    /// <summary>
    /// Checks whether anything is blocking the car's way.
    /// </summary>
    public bool CheckPathPassability()
    {
        // Check if there is any car in front of it and whether it can pass through the waypoint on which it stopped.
        return blockers.Count == 0 && 
            (stoppingWP == null || !stoppingWP.IsOccupied() || (stoppingWP.occupiers.Contains(this) && !stoppingWP.ContainsMovingOccupier()) || 
            (stoppingWP.GetFirstOccupier() && !stoppingWP.IsThereAStoppingOccupier()));
    }

    public bool IsStopping()
    {
        return stopping;
    }

    public float GetLength()
    {
        return length;
    }

    /// <summary>
    /// Used to discard the stopping waypoint in order to enable the car to go on its way.
    /// This may seem paradoxical, since the car should be able to know when it can pass through
    /// this waypoint, but it is necessary, because in the intersection script, the information
    /// about what car is blocking the waypoint isn't stored (since it can be multiple cars) and
    /// therefore the car doesn't know when it itself is blocking the waypoint.
    /// (I hope you're happy now, Adam.)
    /// </summary>
    public void DiscardStoppingWp()
    {
        stoppingWP = null;
    }

    private void OnEnable()
    {
        activeCars.Add(this);
    }

    public void RecomputePath()
    {
        var cols = Physics.OverlapSphere(destination, 0.1f);

        foreach(Collider c in cols)
        {
            if (c.CompareTag("CarWaypoint"))
            {
                GoTo(c.GetComponent<Waypoint>());
                return;
            }
        }
    }
}
