﻿using System.Collections.Generic;
using UnityEngine;

// Defines clusters of waypoints that are blocked if a car is passing through this waypoint in a given direction.
public class WaypointCluster : MonoBehaviour
{
    public List<Waypoint> leftCluster;
    public List<Waypoint> straightCluster;
    public List<Waypoint> rightCluster;

    public List<Waypoint> GetClusterForDirection(Direction d)
    {
        switch(d)
        {
            case Direction.Left:
                return leftCluster;
            case Direction.Straight:
                return straightCluster;
            case Direction.Right:
                return rightCluster;
        }

        return null;
    }
}
