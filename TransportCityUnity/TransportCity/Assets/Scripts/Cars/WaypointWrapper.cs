﻿namespace CarPathFinding
{
    // A wrapper for the Waypoint class used in the A* algorithm for car navigation.
    public class WaypointWrapper : IPathPoint<Direction, Waypoint>
    {
        // The waypoint that this wrapper is wrapping.
        public Waypoint WrappedObject { get; set; }

        // The wrapper of the waypoint from which the algorithm got to this waypoint.
        public IPathPoint<Direction, Waypoint> Previous { get; set; }

        // The direction which leads from the previous waypoint to this one.
        public Direction Direction { get; set; }

        // The distance of the waypoint from the starting position.
        public float Metric { get; set; }

        // Number of the wrapped waypoint's successors (always set to 3).
        public int NumOfSuccessors { get; set; }

        public WaypointWrapper(Waypoint wp)
        {
            WrappedObject = wp;
            NumOfSuccessors = 3;
        }

        /// <summary>
        /// Returns the next waypoint in direction dir.
        /// </summary>
        public IPathPoint<Direction, Waypoint> GetNextPoint(int i, IPathPoint<Direction, Waypoint> end)
        {
            Waypoint next = WrappedObject.GetNextWaypoint((Direction) i);

            if (next == null)
            {
                return null;
            }

            float newDist = (WrappedObject.transform.position - next.transform.position).magnitude;

            // Skip the straight waypoints.
            while(next is StraightWaypoint && next != end.WrappedObject)
            {
                newDist += (next.transform.position - next.GetNextWaypoint().transform.position).magnitude;
                next = next.GetNextWaypoint();
            }

            WaypointWrapper nextWrapped = new WaypointWrapper(next);

            nextWrapped.Previous = this;
            nextWrapped.Direction = (Direction) i;
            nextWrapped.Metric = Metric + newDist;

            return nextWrapped;
        }

        /// <summary>
        /// Computes the distance to another waypoint.
        /// </summary>
        public float MetricTo(IPathPoint<Direction, Waypoint> other)
        {
            return (WrappedObject.transform.position - other.WrappedObject.transform.position).magnitude;
        }

        public override bool Equals(object obj)
        {
            return this.Equals(obj as WaypointWrapper);
        }

        public bool Equals(IPathPoint<Direction, Waypoint> other)
        {
            if (other == null) return false;
            if (other.WrappedObject == WrappedObject) return true;
            return false;
        }

        public override int GetHashCode()
        {
            return WrappedObject.GetHashCode();
        }

        public static bool operator ==(WaypointWrapper leftSide, WaypointWrapper rightSide)
        {
            // Check for null on left side.
            if (System.Object.ReferenceEquals(leftSide, null))
            {
                if (System.Object.ReferenceEquals(rightSide, null))
                {
                    // null == null = true.
                    return true;
                }

                return false;
            }
            return leftSide.Equals(rightSide);
        }

        public static bool operator !=(WaypointWrapper lhs, WaypointWrapper rhs)
        {
            return !(lhs == rhs);
        }
    }
}
