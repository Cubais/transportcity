﻿using System.Collections.Generic;

namespace CarPathFinding
{
    // A custom comparer for floats used in the A* algorithm for car navigation.
    // It never returns that two floats are equal, therefore multiple entries
    // with the same key can be present in the used SortedList.
    public class FloatComparer : IComparer<float>
    {
        public int Compare(float x, float y)
        {
            int result = x.CompareTo(y);

            // If the two are equal, return 1.
            if (result == 0) return 1;
            return result;
        }
    }
}
